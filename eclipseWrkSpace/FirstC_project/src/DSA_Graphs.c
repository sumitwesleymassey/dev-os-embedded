/*
 * DSA_Graphs.c
 *
 *  Created on: 10-May-2022
 *      Author: sumit
 */
#include <stdio.h>
#include <stdlib.h>
#include "Graph.h"
#include "DSA_Graphs.h"
#include "Util.h"
#include "string.h"


typedef struct Node_v
{
	int val;
	int size;
	struct Node_v **neighbors;
}Node_v_d;


typedef struct List_element
{
	Node_v_d *node;
	struct List_element *next;
}List_element_d;

typedef struct List_of_Node_v
{
	List_element_d *head;
	List_element_d *tail;
	int size;
}List_of_Node_v_d;


void addToList(List_of_Node_v_d *list, Node_v_d *node);
Node_v_d *getNodeFromList(List_of_Node_v_d *list, int v);
void printTheGraph(Node_v_d *graph, int array[4]);


AdjList_d *createExampleGraph2(void);
AdjList_d *createExampleGraph3(void);
Node_v_d *createExampleGraph_node_type(void);
List_of_Node_v_d *createExampleGraph4(void);

void DSA_Graphs_BFS(void);
void DSA_Graphs_DFS(void); // works for closed graph but not for open-graphs
void DSA_Graphs_DFS_correctAlgo(void);
void DSA_Graphs_DFS_flood_fill(void);
void DSA_Graphs_clone_a_graph(void);
void DSA_Graphs_topological_sort(void);
void DSA_Graphs_topological_sort_withAdjacencyList(void);


// ----------------------------------------------------
// Main-driver function
// ----------------------------------------------------
void DSA_Graphs_DriverProgram(void)
{
	// DFS & BFS
	/*printf("\nBFS algo for graph\n");
	DSA_Graphs_BFS();
	printf("\n"); //EOL

	printf("\nDFS algo for graph\n");
	DSA_Graphs_DFS();
	printf("\n"); //EOL

	printf("\nDFS algo(correct-algo) for graph\n");
	DSA_Graphs_DFS_correctAlgo();
	printf("\n"); //EOL

	printf("\nGraph-Theory: Flood-fill\n");
	DSA_Graphs_DFS_flood_fill();
	printf("\n"); //EOL

	printf("\nGraph-Theory: clone-a-graph\n");
	DSA_Graphs_clone_a_graph();
	printf("\n"); //EOL
	*/

	printf("\nGraph-Theory: topological-sort with Adjacency-matrix\n");
	DSA_Graphs_topological_sort();
	printf("\n"); //EOL

	printf("\nGraph-Theory: topological-sort with Adjacency-list\n");
	DSA_Graphs_topological_sort_withAdjacencyList();
	printf("\n"); //EOL
}


// ----------------------------------------------------
// topological sort with adjcency-list
// ----------------------------------------------------
void topo_sort_adjlist(int i, int n, List_of_Node_v_d *list, int *visited, Stack_d *stack);

/**
 * need to give the list of nodes not the grah's 0-node pointer
 * cause 4-node is not pointed by any one.
 * And it is a directed graph.
 *
 *  0 --> 3 <--- 4
 * |      +      | \
 * |      |      |   \
 * |      |      |     +
 * +      |      +----> 6
 * 1 ---> 2      5
 *
 */
void DSA_Graphs_topological_sort_withAdjacencyList(void)
{
	int n=7;
	List_of_Node_v_d *list = createExampleGraph4();

	// create stack
	Stack_d *stack = Util_createStack(n);

	int *visited = memset(malloc(sizeof(int)*n), 0, sizeof(int)*n);

	for (int i=0; i<n; i++)
	{
		topo_sort_adjlist(i, n, list, visited, stack);
	}

	// print the stack
	while (!isStackempty(stack))
	{
		printf("%d ", Util_popelement(stack));
	}
}


void topo_sort_adjlist(int i, int n, List_of_Node_v_d *list, int *visited, Stack_d *stack)
{
	if (visited[i])
		return;

	// mark
	visited[i] = 1;

	// get the node
	Node_v_d *node = getNodeFromList(list, i);

	// iterate over neighbors
	for(int j=0; j<node->size; j++)
	{
		Node_v_d *neighnbor = *(node->neighbors+j);
		topo_sort_adjlist(neighnbor->val, n, list, visited, stack);
	}

	// stack it on post
	Util_pushelement(stack, node->val);
}

// ----------------------------------------------------
// topological sort
// ----------------------------------------------------
void topo_sort(int i, int n, int graph[n][n], int *visited, Stack_d *stack);

void DSA_Graphs_topological_sort(void)
{
	int n=7;
	int graph[7][7] = {{0, 1, 0, 1, 0, 0, 0},
					   {0, 0, 1, 0, 0, 0, 0},
					   {0, 0, 0, 1, 0, 0, 0},
					   {0, 0, 0, 0, 0, 0, 0},
					   {0, 0, 0, 1, 0, 1, 1},
					   {0, 0, 0, 0, 0, 0, 1},
					   {0, 0, 0, 0, 0, 0, 0}};

	// create stack
	Stack_d *stack = Util_createStack(n);

	int *visited = memset(malloc(sizeof(int)*n), 0, sizeof(int)*n);

	for (int i=0; i<n; i++)
	{
		topo_sort(i, n, graph, visited, stack);
	}

	// print the stack
	while (!isStackempty(stack))
	{
		printf("%d ", Util_popelement(stack));
	}
}


void topo_sort(int i, int n, int graph[n][n], int *visited, Stack_d *stack)
{
	//base
	if(visited[i])
		return;

	//mark
	visited[i] = 1;

	// traverse neighbors
	int j=0;
	for (; j<n; j++)
	{
		if (graph[i][j] != 0)
			topo_sort(j, n, graph, visited, stack);
	}

	// stack it on post
	Util_pushelement(stack, i);
}


// ----------------------------------------------------
// clone a graph
// ----------------------------------------------------
void clone(Node_v_d **cur, int neighbor_num, Node_v_d *old, List_of_Node_v_d *list);

void DSA_Graphs_clone_a_graph(void)
{
	Node_v_d *graph = createExampleGraph_node_type();
	int array[4] = {0};
	printf("\ngraph:\n");
	printTheGraph(graph, array);

	// create list
	List_of_Node_v_d *list = malloc(sizeof(List_of_Node_v_d));
	list->head = list->tail = NULL;
	list->size=0;

	memset(array, 0, sizeof(int)*4);
	Node_v_d *cloned_graph = NULL;
	clone(&cloned_graph, 0, graph, list);
	free(list);

	printf("\nclone-a-graph:\n");
	printTheGraph(cloned_graph, array);
}

/**
 * recur(NULL, 1)          |   recur([1], 2)         |   recur([2], 1)           |   recur([3], 2)         |   recur([4], 1)
 *	1 exists               |   2 exists              |   1 exists                |   2 exists              |   1 exists
 * 	  -				       |   	  - 		         |   	  - [2] <- 1         |   	  - [1] <- 2       |   	  - [4] <- 1
 * 	  -				       |   	  - 		         |   	  - [2].size++       |   	  - [1].size++     |   	  - [4].size++
 *  				       |                         |                           |                         |
 *  else	          	   |    else	             |    else	                 |    else	               |--------------------------
 *    - [1]	          	   |      - [2]	             |      - 				     |      - 		           |   recur([4], 3)
 *    - add to tracker	   |      - add to tracker   |      - 				     |      - 				   |   3 exists
 *    - head<--[1]     	   |      - [1] <- [2]       |      - 				     |      - 			       |   	  - [4] <- 3
 *    Loop: neighbor  	   |      Loop: neighbor	 |      Loop:  neighbor      |      Loop:   neighbor   |   	  - [4].size++
 *      recur([1], 2)      |        recur([2], 1)    |        				     |        				   |
 *      recur([1], 4)      |        recur([2], 3)    |        				     |        				   |
 *                         |                         |                           |                         |
 *-------------------------+-------------------------+---------------------------+-------------------------+
 *                         |   						 |   recur([2], 3)           |   recur([3], 4)         |
 *                         |   						 |   3 exists                |   4 exists              |
 *                         |   						 |   	  -                  |   	  -                |
 *                         |   						 |   	  -                  |   	  -                |
 *                         |   						 |                           |                         |
 *                         |   						 |    else	                 |    else	               |
 *                         |   						 |      - [3]	             |      - [4]	           |
 *                         |   						 |      - add to tracker     |      - add to tracker   |
 *                         |   						 |      - [2] <- [3]         |      - [3] <- [4]       |
 *                         |   						 |      Loop: neighbor       |      Loop: neighbor     |
 *                         |   						 |        recur([3], 2)      |        recur([4], 1)    |
 *                         |   						 |        recur([3], 4)      |        recur([4], 3)    |
 *                         |   						 |                           |                         |
 *                         |						 +---------------------------+-------------------------+
 *                         |   recur([1], 4)         |
 *                         |   4 exists              |
 *                         |   	  - [1] <- 4         |
 *                         |   	  - [1].size++       |
 *                         |                         |
 *
 */
void clone(Node_v_d **cur, int neighbor_num, Node_v_d *old, List_of_Node_v_d *list)
{
	if (old == NULL)
		return;

	// if exists or already visited
	Node_v_d *exists = getNodeFromList(list, old->val);
	if (exists)
	{
		// add neighbor to new-node created in prev call (i.e. cur)
		*((*cur)->neighbors + neighbor_num) = exists;
		(*cur)->size++;
	}
	else
	{
		// create new node
		Node_v_d *new = malloc(sizeof(Node_v_d));
		new->val = old->val;
		new->neighbors = malloc(sizeof(Node_v_d)*old->size);
		new->size = 0;

		addToList(list, new);	// add to tracker-list

		if ((*cur)!=NULL)
		{
			// add neighbor to new-node created in prev call (i.e. cur)
			*((*cur)->neighbors + neighbor_num) = new;
			(*cur)->size++;
		}
		else
			*cur = new; // first node, add the head

		// iterate over neighbors
		for(int i=0; i<old->size; i++)
		{
			clone(&new, i, *(old->neighbors+i), list);
		}
	}
}


// ----------------------------------------------------
// flood-fill
// ----------------------------------------------------
static void solve(int r, int c, int cur_color, int n, int image[n][n], int newColor /*, int visited[n][n]*/);

void DSA_Graphs_DFS_flood_fill(void)
{
	int n = 3;
	int image[3][3] =  {{1,1,1},
						{1,1,0},
						{1,0,1}};

	int M = 8;
	int screen[8][8] = {{1, 1, 1, 1, 1, 1, 1, 1},
	               	    {1, 1, 1, 1, 1, 1, 0, 0},
					    {1, 0, 0, 1, 1, 0, 1, 1},
					    {1, 2, 2, 2, 2, 0, 1, 0},
					    {1, 1, 1, 2, 2, 0, 1, 0},
					    {1, 1, 1, 2, 2, 2, 2, 0},
					    {1, 1, 1, 1, 1, 2, 1, 1},
					    {1, 1, 1, 1, 1, 2, 2, 1}};

	/*int *visited = malloc(sizeof(int)*n*n);
	memset(visited, 0, sizeof(int)*n*n);*/


	int newColor = 2;
	int r=1;
	int c=1;

	solve(r, c, image[r][c], n, image, newColor/*, visited*/);

	newColor = 3;
	r = 4;
	c = 4;
	solve(r, c, screen[r][c], M, screen, newColor);

	for (int i=0; i<n; i++)
	{
		printf("{");
		for (int j=0; j<n; j++)
			printf("%d ",image[i][j]);

		printf("}\n");
	}

	printf("pixel-screen-2:\n");
	for (int i=0; i<M; i++)
	{
		printf("{");
		for (int j=0; j<M; j++)
			printf("%d ",screen[i][j]);

		printf("}\n");
	}
}

/*
 * Conditions:
 * 1) If x or y is outside the screen, then return.
 * 2) If color of screen[x][y] is not same as prevC, then return
 *
 * Note: visited-array is not required as if cur-color doesn't match you wont proceed further.
 *
 */
static void solve(int r, int c, int cur_color, int n, int image[n][n], int newColor /*, int visited[n][n]*/)
{
	// base: bound
	if (r<0 || r>n || c<0 || c>n)
		return;

	/*if (visited[r][c] != 0)
		return;*/

	// valid then change color of the pixel
	if (image[r][c] == cur_color)
	{
		image[r][c] = newColor;
	}
	else
		return;

	solve(r, c-1, cur_color, n, image, newColor/*, visited*/);	// left
	solve(r, c+1, cur_color, n, image, newColor/*, visited*/);	// right
	solve(r-1, c, cur_color, n, image, newColor/*, visited*/);	// up
	solve(r+1, c, cur_color, n, image, newColor/*, visited*/);	// down
}


// ----------------------------------------------------
// BFS-Algo
// ----------------------------------------------------
void DSA_Graphs_BFS(void)
{
	// adjacency-list
 	AdjList_d *list = createExampleGraph2();
	displayAjdList(list);

	// visited
	int *visited = malloc(sizeof(int) * 9);
	memset(visited, 0, sizeof(int) * 9);

	// create Q
	Queue_d *Q = Util_createQ(9);
	Vertex_d *src = list->head_vertex; // 0

	Util_enqueue(Q, src); // enqueue 1st
	visited[src->v] = 1;

	printf("path is :");
	while (!Util_isQEmpty(Q))
	{
		Vertex_d *p = (Vertex_d *)Util_dequeue(Q);
		if (p == NULL)
		{
			printf("NULL");
			break;
		}
		printf("%d->", p->v);

		// loop over neighbours and enqueue them
		AdjacencyNode_d *cur = p->head_node;
		while(cur)
		{
			if (visited[cur->v] == 1) // check if already visited
			{
				cur = cur->next_node;
				continue;
			}

			Vertex_d *v = getVertex(list, cur->v);
			Util_enqueue(Q, v);
			visited[cur->v] = 1; // mark them visited
			cur = cur->next_node;
		}
	}

	destroyAjdList(list);
}


// ----------------------------------------------------
// DFS-Algo : can have many unique paths
// ----------------------------------------------------
void DSA_Graphs_DFS_correctAlgo(void)
{
	AdjList_d *list = createExampleGraph3();
	displayAjdList(list);

	// create stack
	Stack_d *stack = (Stack_d *)Util_createStack(8);

	// visited array
	int *visited = malloc(sizeof(int)*8);
	memset(visited, 0, sizeof(int) * 8);

	printf("path is :");

	// first vertex
	Vertex_d *src = list->head_vertex;

	// push, mark & print 1st
	printf("%d->", src->v);
	Util_push(stack, src);
	visited[src->v] = 1;

	while (!isStackempty(stack))
	{
		// TOP, dont pop here
		Vertex_d *p = (Vertex_d *)Util_Top(stack);

		// loop over neighbours and enqueue them
		AdjacencyNode_d *cur = p->head_node;
		while(cur)
		{
			// pick only one non-visited node.
			if (visited[cur->v] == 1)
			{
				cur = cur->next_node;
				continue;
			}
			else
			{
				// push, mark & print
				Vertex_d *v = getVertex(list, cur->v);
				printf("%d->", cur->v);
				Util_push(stack, v);
				visited[cur->v] = 1;
				break;
			}
		}

		// if cur is NULL means no-valid vertex is found, then pop the vertex
		if (cur==NULL)
			Util_pop(stack);
	}
}


void DSA_Graphs_DFS(void)
{
	AdjList_d *list = createExampleGraph2();
	displayAjdList(list);

	// create stack
	Stack_d *stack = (Stack_d *)Util_createStack(9);

	// visited array
	int *visited = malloc(sizeof(int)*9);
	memset(visited, 0, sizeof(int) * 9);

	Vertex_d *src = list->head_vertex;

	// push & mark 1st
	Util_push(stack, src);
	visited[src->v] = 1;

	printf("path is :");
	while (!isStackempty(stack))
	{
		// pop & print
		Vertex_d *p = Util_pop(stack);

		if (p==NULL)
		{
			printf("NULL");
			break;
		}
		printf("%d->", p->v);

		// loop over neighbours and enqueue them
		AdjacencyNode_d *cur = p->head_node;
		while(cur)
		{
			// pick only one non-visited node.
			if (visited[cur->v] == 1)
			{
				cur = cur->next_node;
				continue;
			}
			else
			{
				// push & mark
				Vertex_d *v = getVertex(list, cur->v);
				Util_push(stack, v);
				visited[cur->v] = 1;
				break;
			}
		}
	}

	destroyAjdList(list);
}


AdjList_d *createExampleGraph2(void)
{
	int n=9;
	AdjList_d *list = malloc(sizeof(AdjList_d));
	list->head_vertex = list->tail_vertex = NULL;
	list->size = 0;

	// create all vertexes and add to the list
	for (int i=0; i<n ; i++)
		addVertexToList(list, i);

	// add edges
	addAdjEdge(list, 0, 1, 4);
	addAdjEdge(list, 0, 2, 8);
	addAdjEdge(list, 1, 3, 8);
	addAdjEdge(list, 1, 4, 11);
	addAdjEdge(list, 2, 4, 7);
	addAdjEdge(list, 2, 5, 2);
	addAdjEdge(list, 3, 6, 4);
	addAdjEdge(list, 4, 6, 9);
	addAdjEdge(list, 4, 7, 14);
	addAdjEdge(list, 5, 7, 10);
	addAdjEdge(list, 6, 8, 2);
	addAdjEdge(list, 7, 8, 1);

	return list;
}


AdjList_d *createExampleGraph3(void)
{
	int n=9;
	AdjList_d *list = malloc(sizeof(AdjList_d));
	list->head_vertex = list->tail_vertex = NULL;
	list->size = 0;

	// create all vertexes and add to the list
	for (int i=1; i<n ; i++)
		addVertexToList(list, i);

	// add edges
	addAdjEdge(list, 1, 2, 4);
	addAdjEdge(list, 1, 3, 8);
	addAdjEdge(list, 2, 5, 8);
	addAdjEdge(list, 2, 7, 11);
	addAdjEdge(list, 3, 4, 7);
	addAdjEdge(list, 3, 5, 2);
	addAdjEdge(list, 4, 6, 4);
	addAdjEdge(list, 5, 7, 9);
	addAdjEdge(list, 5, 8, 14);
	addAdjEdge(list, 7, 8, 10);

	return list;
}


// ----------------------------------------------------
// graph with node type
// ----------------------------------------------------
Node_v_d *createExampleGraph_node_type(void)
{
	int n=4;

	// create list
	List_of_Node_v_d *list = malloc(sizeof(List_of_Node_v_d));
	list->head = list->tail = NULL;
	list->size=0;


	// create all vertexes and add to the list
	for (int i=1; i<=n ; i++)
	{
		Node_v_d *node = malloc(sizeof(Node_v_d));
		node->val = i;
		node->size=0;

		addToList(list, node);
	}

	// add edges
	// [1] <- 2,4
	Node_v_d *graph;
	Node_v_d *node;
	graph = node = getNodeFromList(list, 1);
	node->neighbors = malloc(sizeof(Node_v_d *)*2);
	*(node->neighbors+0) = getNodeFromList(list, 2);
	*(node->neighbors+1) = getNodeFromList(list, 4);
	node->size +=2;

	// [2] <- 1,3
	node = getNodeFromList(list, 2);
	node->neighbors = malloc(sizeof(Node_v_d *)*2);
	*(node->neighbors+0) = getNodeFromList(list, 1);
	*(node->neighbors+1) = getNodeFromList(list, 3);
	node->size +=2;

	// [3] <- 2,4
	node = getNodeFromList(list, 3);
	node->neighbors = malloc(sizeof(Node_v_d *)*2);
	*(node->neighbors+0) = getNodeFromList(list, 2);
	*(node->neighbors+1) = getNodeFromList(list, 4);
	node->size +=2;

	// [4] <- 1,3
	node = getNodeFromList(list, 4);
	node->neighbors = malloc(sizeof(Node_v_d *)*2);
	*(node->neighbors+0) = getNodeFromList(list, 1);
	*(node->neighbors+1) = getNodeFromList(list, 3);
	node->size +=2;

	return graph;
}


// ----------------------------------------------------
// graph with node type (for topological sort) directed-graph
// ----------------------------------------------------
/**
 * need to give the list of nodes not the grah's 0-node pointer
 * cause 4-node is not pointed by any one.
 *
 *  0 --> 3 <--- 4
 * |      +      | \
 * |      |      |   \
 * |      |      |     +
 * +      |      +----> 6
 * 1 ---> 2      5
 *
 */
List_of_Node_v_d *createExampleGraph4(void)
{
	int n=7;

	// create list
	List_of_Node_v_d *list = malloc(sizeof(List_of_Node_v_d));
	list->head = list->tail = NULL;
	list->size=0;


	// create all vertexes and add to the list
	for (int i=0; i<n ; i++)
	{
		Node_v_d *node = malloc(sizeof(Node_v_d));
		node->val = i;
		node->size=0;

		addToList(list, node);
	}

	// add edges
	// [0] <- 1,3
	//Node_v_d *graph;
	Node_v_d *node;
	//graph = node = getNodeFromList(list, 0);
	node = getNodeFromList(list, 0);
	node->neighbors = malloc(sizeof(Node_v_d *)*2);
	*(node->neighbors+0) = getNodeFromList(list, 1);
	*(node->neighbors+1) = getNodeFromList(list, 3);
	node->size +=2;

	// [1] <- 2
	node = getNodeFromList(list, 1);
	node->neighbors = malloc(sizeof(Node_v_d *)*1);
	*(node->neighbors+0) = getNodeFromList(list, 2);
	node->size +=1;

	// [2] <- 3
	node = getNodeFromList(list, 2);
	node->neighbors = malloc(sizeof(Node_v_d *)*1);
	*(node->neighbors+0) = getNodeFromList(list, 3);
	node->size +=1;

	// [4] <- 3,5,6
	node = getNodeFromList(list, 4);
	node->neighbors = malloc(sizeof(Node_v_d *)*3);
	*(node->neighbors+0) = getNodeFromList(list, 3);
	*(node->neighbors+1) = getNodeFromList(list, 5);
	*(node->neighbors+2) = getNodeFromList(list, 6);
	node->size +=3;

	// [5] <- 6
	node = getNodeFromList(list, 5);
	node->neighbors = malloc(sizeof(Node_v_d *)*1);
	*(node->neighbors+0) = getNodeFromList(list, 6);
	node->size +=1;

	return list;
}

/**
 * take one-vertex and then go for neighors
 * then go for next vertex
 *
 */
void printTheGraph(Node_v_d *graph, int array[4])
{
	Node_v_d *cur = graph;

	if (array[cur->val-1] == 1)
	{
		return;
	}
	else
	{
		array[cur->val-1] = 1;
		printf("%d: ", cur->val);
	}

	printf("[");
	for (int i=0; i<cur->size; i++)
	{
		printf("%d ", (*(cur->neighbors+i))->val);
	}
	printf("]\n");


	for (int i=0; i<cur->size; i++)
	{
		printTheGraph(*(cur->neighbors+i), array);
	}
}


void addToList(List_of_Node_v_d *list, Node_v_d *node)
{
	List_element_d *element = malloc(sizeof(List_element_d));
	element->node = node;
	element->next = NULL;

	if (list->head == NULL)
	{
		list->head = list->tail = element;
	}
	else
	{
		list->tail->next = element;
		list->tail = element;
	}
	list->size++;
}

Node_v_d *getNodeFromList(List_of_Node_v_d *list, int v)
{
	Node_v_d *found = NULL;

	List_element_d *cur_element = list->head;
	while (cur_element)
	{
		//if(cur_element->node->val == v)
		if(cur_element->node->val == v)
		{
			found = cur_element->node;
			break;
		}
		cur_element = cur_element->next;
	}

	return found;
}

