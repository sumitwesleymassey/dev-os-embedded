/*
 * Subsequence.c
 *
 *  Created on: 27-May-2022
 *      Author: sumit
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Util.h"
#include "Subsequence.h"

void Subsequence_LIS(void);
void Subsequence_LCS_2Strings(void);
void Subsequence_LRS_nosharing(void);
void Subsequence_maxsum_increasing_subseq(void);
void Subsequence_maxsumIS_Rec(void);
void Subsequence_LSubseq_adjacentOne(void);
void Subsequence_countDistinctseq(void);
void Subsequence_LAS(void);

// ----------------------------------------------------
// Main-driver function
// ----------------------------------------------------
void Subsequence_Program(void)
{
	// LIS: Longest-Increasing-Subsequence.
	printf("LIS: \n");
	Subsequence_LIS();

	// LCS: Longest-common-Subsequence between 2 Strings
	printf("\nLCS: ");
	Subsequence_LCS_2Strings();

	// LRS: Longest Repeating Subsequence (no-sharing)
	printf("\nLRS: ");
	Subsequence_LRS_nosharing();

	// maxsum of increasin subseq
	printf("\nmaxsum of inc-subseq: ");
	Subsequence_maxsum_increasing_subseq();

	// maxsum of increasin subseq
	printf("\nmaxsumIS: using Rec: ");
	Subsequence_maxsumIS_Rec();

	// lonngest subsequence with diff of adjcent num is 1.
	printf("\nLSub_adjis_1: ");
	Subsequence_LSubseq_adjacentOne();

	// maxsum of increasin subseq
	printf("Distinct sub-sequences\n");
	Subsequence_countDistinctseq();

	// LAS subsequence
	printf("Longest alternating sub-sequences\n");
	Subsequence_LAS();
}


// ----------------------------------------------------
// DP : LIS- longest-increasing subsequence
// ----------------------------------------------------
void Subsequence_LIS(void)
{
	int arr[] = {10, 22, 9, 33, 21, 50, 41, 60, 80, 1};
	//int arr[] = {0, 4, 12, 2, 10, 6};
	int n= sizeof(arr)/sizeof(arr[0]);

	int *LIS = memset(malloc(sizeof(int)*n), 0, sizeof(int)*n);
	for (int k=0; k<n; k++)
		LIS[k] = 1;

	int *seq = memset(malloc(sizeof(int)*n), 0, sizeof(int)*n);
	for (int k=0; k<n; k++)
		seq[k] = -1;

	int LIS_max = INT_MIN;
	int index = -1;

	for (int i=1; i<n; i++)
	{
		for (int j=0; j<i; j++)
		{
			// ith num is greater than jth num, then its in the increasing sequence.
			// and the new leng is greater than whats already there, then update LIS
			if ((arr[i] > arr[j]) && ((LIS[j] + 1) >= LIS[i]))
			{
				LIS[i] = LIS[j] + 1;
				seq[i] = j;

				if (LIS[i] >= LIS_max)
				{
					LIS_max = LIS[i]; // update LIS
					index = i;
				}
			}
		}
	}

	// ----------------------------------
	// print subsequence in order.
	Stack_d *stack = Util_createStack(n);
	while (index >= 0)
	{
		Util_pushelement(stack, arr[index]);
		index = seq[index];
	}

	while (!isStackempty(stack))
		printf("%d ", Util_popelement(stack));

	Util_destroyStack(stack);
	printf("\n"); // EOL
	// ----------------------------------

	printf("lis = %d\n", LIS_max);
}


// ----------------------------------------------------
// DP : LIS- longest-increasing subsequence
// ----------------------------------------------------
static int findLCS(char *string1, char * string2, int n1, int n2, int dp[n1][n2], int *seq);

void Subsequence_LCS_2Strings(void)
{
	char *string1 = "abcvdefgh";
	char *string2 = "bqdrcvefgh";

	int n1 = strlen(string1) +1;
	int n2 = strlen(string2) +1;

	int dp[n1][n2];
	memset(dp, 0, sizeof(int)*n1*n2);

	int *seq = malloc(sizeof(int) * n2);
	for (int k=0; k<(n2-1); k++)
		seq[k] = -1;

	int lcs = findLCS(string1, string2, n1, n2, dp, seq);
	printf("LCS len = %d\n", lcs);
	printf("\n"); // EOL
}

static int findLCS(char *string1, char * string2, int n1, int n2, int dp[n1][n2], int *seq)
{
	int result = -1;
	int index = -1;

	for (int i=1; i<n1; i++)
	{
		for (int j=1; j<n2; j++)
		{
			if (string1[i-1] == string2[j-1])	// if matches
			{
				dp[i][j] = dp[i-1][j-1] + 1; // diag + 1

				if ((j-1) > index) // new index is greater than prev index then only update index and seq
				{
					seq[j-1] = index;
					index = j-1;
				}
			}
			else
			{
				dp[i][j] = MAX(dp[i-1][j], dp[i][j-1]); // max(Upper, left)
			}
		}
	}

	result = dp[n1-1][n2-1];

	// ----------------------------------
	// print subsequence in order.
	Stack_d *stack = Util_createStack(n2-1);
	while (index >= 0)
	{
		Util_pushelement(stack, string2[index]);
		index = seq[index];
	}

	while (!isStackempty(stack))
		printf("%c ", Util_popelement(stack));

	Util_destroyStack(stack);
	printf("\n"); // EOL
	// ----------------------------------

	return result;
}


// ----------------------------------------------------
// DP: LRS- longest-reapeating subsequence (no-sharing)
// ----------------------------------------------------
static int findLRS(char *string, int n, int dp[n][n], int *seq);

void Subsequence_LRS_nosharing(void)
{
	char *string = "axxxy";
	int n = strlen(string) +1;

	int dp[n][n];
	memset(dp, 0, sizeof(int)*n*n);

	int *seq = malloc(sizeof(int)*(n-1));
	for (int k=0; k<n-1; k++)
		seq[k] = -1;

	printf("LRS_len = %d \n", findLRS(string, n, dp, seq));
}

static int findLRS(char *string, int n, int dp[n][n], int *seq)
{
	int result = -1;
	int index = -1;

	for (int i=1; i<n; i++)
	{
		for (int j=1; j<n; j++)
		{
			if ((string[i-1] == string[j-1]) && i!=j)
			{
				dp[i][j] = dp[i-1][j-1] + 1;	// update dp
				if (j-1 > index)
				{
					seq[j-1] = index;
					index = j-1;
				}
			}
			else
			{
				dp[i][j] = MAX(dp[i-1][j], dp[i][j-1]);
			}

		}
	}

	// ----------------------------------
	// print subsequence in order.
	Stack_d *stack = Util_createStack(n-1);
	while (index >= 0)
	{
		Util_pushelement(stack, string[index]);
		index = seq[index];
	}

	while (!isStackempty(stack))
		printf("%c ", Util_popelement(stack));

	Util_destroyStack(stack);
	printf("\n"); // EOL
	// ----------------------------------

	result = dp[n-1][n-1];
	return result;
}



// ----------------------------------------------------
// DP: Maximum sum increasing subsequence
// ----------------------------------------------------
void Subsequence_maxsum_increasing_subseq(void)
{
	int sum = 0; // result
	int index = -1; // prev-index
	int arr[] = {1, 101, 2, 3, 100};
	//int arr[] = {1, 101, 2, 3, 5};
	int n = sizeof(arr)/sizeof(arr[0]);

	int *maxSum = memset(malloc(sizeof(int)*n), 0, sizeof(int)*n);
	maxSum[0] = arr[0]; // maxsum of first element is itself

	int *seq = memset(malloc(sizeof(int)*n), 0, sizeof(int)*n);
	for(int k=0; k<n; k++)
		seq[k] = -1;

	for (int j=1; j<n; j++)
	{
		for (int i=0; i<j; i++)
		{
			if ((arr[j] > arr[i]) && (maxSum[i]+arr[j] > maxSum[j]))	// if jth numb is greater than ith number
			{
				maxSum[j] = maxSum[i]+arr[j];
				seq[j] = i;

				if (maxSum[j] > sum)
				{
					sum = maxSum[j];
					index = j;
				}
			}
		}
	}

	printf("%d\n", sum);

	// ----------------------------------
	// print subsequence in order.
	Stack_d *stack = Util_createStack(n);
	while (index >= 0)
	{
		Util_pushelement(stack, arr[index]);
		index = seq[index];
	}

	while (!isStackempty(stack))
		printf("%d ", Util_popelement(stack));

	Util_destroyStack(stack);
	printf("\n"); // EOL
	// ----------------------------------
}



// ----------------------------------------------------
// DP: Maximum sum increasing subsequence
// ----------------------------------------------------
static void maxSumIS(int i, int j, int n, int arr[n], int *maxSum, int *seq, int *index, int *sum);

void Subsequence_maxsumIS_Rec(void)
{
	int sum = 0; // result
	int index = -1; // prev-index
	int arr[] = {1, 101, 2, 3, 100};
	//int arr[] = {1, 101, 2, 3, 5};
	int n = sizeof(arr)/sizeof(arr[0]);

	int *maxSum = memset(malloc(sizeof(int)*n), 0, sizeof(int)*n);
	maxSum[0] = arr[0]; // maxsum of first element is itself

	int *seq = memset(malloc(sizeof(int)*n), 0, sizeof(int)*n);
	for(int k=0; k<n; k++)
		seq[k] = -1;

	for (int j=1; j<n; j++)
	{
		maxSumIS(0, j, n, arr, maxSum, seq, &index, &sum);
	}

	printf("%d \n", sum);

	// ----------------------------------
	// print subsequence in order.
	Stack_d *stack = Util_createStack(n);
	while (index >= 0)
	{
		Util_pushelement(stack, arr[index]);
		index = seq[index];
	}

	while (!isStackempty(stack))
		printf("%d ", Util_popelement(stack));

	Util_destroyStack(stack);
	printf("\n"); // EOL
	// ----------------------------------
}


static void maxSumIS(int i, int j, int n, int arr[n], int *maxSum, int *seq, int *index, int *sum)
{
	// bounds
	if (i>=n || j>=n)
		return;

	if ((arr[j] > arr[i]) && (maxSum[i]+arr[j] > maxSum[j]))	// if jth numb is greater than ith number
	{
		maxSum[j] = maxSum[i]+arr[j];
		seq[j] = i;

		if (maxSum[j] > *sum)
		{
			*sum = maxSum[j];
			*index = j;
		}
	}

	maxSumIS(i+1, j, n, arr, maxSum, seq, index, sum);

	return;
}



// -----------------------------------------------------------
// DP: Longest subsequence with diff between adjacent is one
// -----------------------------------------------------------
void Subsequence_LSubseq_adjacentOne(void)
{
	int A[] = {10, 9, 4, 5, 4, 8, 6};
	//int A[] = {1, 2, 3, 4, 5};
	int n = sizeof(A)/sizeof(A[0]);

	int *LS = memset(malloc(sizeof(int)*n), 0, sizeof(int)*n);
	for (int k=0; k<n; k++)
		LS[k] = 1;

	int *seq = malloc(sizeof(int)*n);
	for (int k=0; k<n; k++)
		seq[k] = -1;

	int max = -1;

	for (int j=1; j<n; j++)
	{
		for (int i=0; i<j; i++)
		{
			if ((DIFF(A[i], A[j]) == 1) && (LS[i]+1 > LS[j]))
			{
				LS[j] = LS[i]+1;
				seq[j] = i;

				if (LS[j] > max)
					max = LS[j];
			}
		}
	}

	// longest-subsequence len with adjacent one
	printf("%d \n", max);

	// print all relevant subsequence in order.
	Stack_d *stack = Util_createStack(n);
	for (int k=0; k<n; k++)
	{
		if (LS[k] == max)
		{
			printf("{"); // EOL
			int index = k;
			while (index >= 0)
			{
				Util_pushelement(stack, A[index]);
				index = seq[index];
			}

			while (!isStackempty(stack))
				printf("%d ", Util_popelement(stack));

			printf("}\n"); // EOL
		}
	}

	Util_destroyStack(stack);
	printf("\n"); // EOL
}


// -----------------------------------------------------------
// DP: Count Distinct Subsequences Dynamic Programming
// -----------------------------------------------------------
void Subsequence_countDistinctseq(void)
{
	char *string = "abcbac";
	int n = strlen(string) + 1;

	int *dp = memset(malloc(sizeof(int) *n), 0, sizeof(int) *n);
	dp[0] = 1;	// blank sequence is itself a seq, so the len is 1

	DynamicMemory_Map_d *map = Util_createHashMap(0, 0, 0);

	for (int i=1; i<n; i++)
	{
		dp[i] = 2*dp[i-1];

		// update dp incase of duplicated char and already calculated sub-sequence
		int j = Util_HashMap_getValueIndex(map, string+(i-1), 1);
		if (j >= 0) // it means it exists
		{
			dp[i] = dp[i] - dp[j-1];
		}

		// "a" --> 1
		// "b" --> 2
		// "c" --> 3
		// "b" --> 4
		// "a" --> 5
		// "c" --> 6
		//
		// update index
		// 		a --> 1, 5
		// 		b --> 2, 4
		// 		b --> 3, 6
		Util_HashMap_put_updateValue(map, string+(i-1), 1, i);
	}

	printf("count = %d\n", dp[n-1] -1);	// substracting black seq length
}

// ----------------------------------------------------
// DP : LAS (Longest alternating sequence)
// ----------------------------------------------------
void Subsequence_LAS(void)
{
	//int arr[] = {1, 17, 5, 10, 13, 15, 10, 5, 16, 8};
	//int arr[] = {0, 4, 12, 2, 10, 6};
	//int arr[] = {1,5,4};
	int arr[] = {17, 5, 10, 13, 15, 10, 5, 16, 8};
	int n= sizeof(arr)/sizeof(arr[0]);

	// if n is 1 or 2 just return n
	// because for n=1: there will be only 1 LAS of size 1
	//		   for n=2: there will be only 1 LAS of size 2
	//					0 --> 1 or 1--->0
	//				  smaller --> bigger or bigger --> smaller
	if (n>2)
	{
		int *LAS = memset(malloc(sizeof(int)*n), 0, sizeof(int)*n); // dp
		for (int k=0; k<n; k++)
			LAS[k] = 1;

		// traverse for the sequence
		// start could from smaller --> bigger
		//					bigger --> smaller
		int flag = (arr[0] > arr[1])? 1:0;

		//for (int i=1; i<n; i++)
		{
			/*for (int j=1; j<n; j++)
			{
				if (flag == 0)	// if prev is smaller, then next shall be bigger
				{
					if (arr[j] > arr[j-1])
					{
						LAS[j] = LAS[j-1] + 1;
						flag = 1;
					}
					else
						LAS[j] = LAS[j-1];
				}
				else if (flag == 1) // if prev is bigger, then next shall be smaller
				{
					if (arr[j] < arr[j-1])
					{
						LAS[j] = LAS[j-1] + 1;
						flag = 0;
					}
					else
						LAS[j] = LAS[j-1];
				}
			}*/

			for (int j=1; j<n; j++)
			{
				if ((flag == 0) && (arr[j] > arr[j-1])) // if prev is smaller, then next shall be bigger
				{
					LAS[j] = LAS[j-1] + 1;
					flag = 1;
				}
				else if ((flag == 1) && (arr[j] < arr[j-1])) // if prev is bigger, then next shall be smaller
				{
					LAS[j] = LAS[j-1] + 1;
					flag = 0;
				}
				else
					LAS[j] = LAS[j-1];
			}

		}
		printf("LAS len = %d\n", LAS[n-1]);
	}
	else
		printf("LAS len = %d\n", n);
}

