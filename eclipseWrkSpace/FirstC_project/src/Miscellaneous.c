/*
 * Miscellaneous.c
 *
 *  Created on: 06-Aug-2023
 *      Author: sumit
 */
#include <stdio.h>
#include <stdlib.h>
#include "Util.h"
#include <string.h>
#include <time.h>
#include "Miscellaneous.h"

static void Miscellaneous_CopyArrayRandom_Program(void);

// ----------------------------------------------------
// Main-driver function
// ----------------------------------------------------
void Miscellaneous_DriverProgram(void)
{
    // Miscellaneous : Random array copy
    printf("Miscellaneous program: copy array randomly\n");
    Miscellaneous_CopyArrayRandom_Program();
    printf("\n"); // EOL
}

// ----------------------------------------------------
// Miscellaneous : Random array copy
// ----------------------------------------------------
static void fisher_yates_shuffle(int *arr, int n);

static void Miscellaneous_CopyArrayRandom_Program(void)
{
    int array[] = {0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};
    int size = sizeof(array);
    int length = size/sizeof(array[0]);
    int random_index = 0;
    srand(time(NULL));

    int *dest = memset(malloc(size), 0, size);

    //fisher -yates algorith to shuffle data
//    fisher_yates_shuffle(array, length);

    // in this loop random inex can be same.
//    for (int i=0; i<length; i++)
//    {
//        random_index = rand()%length;
//        dest[random_index] = array[random_index];
//        printf("random_index = %d\n", random_index);
//    }

    char selected[length];
    int selected_index = 0;
    int num_copies = length;

    while (selected_index < num_copies)
    {
        random_index = rand()%length;

        // check if slected already
        char already_selected = 0;
        for (int i=0; i<selected_index; i++)
        {
            if (random_index == selected[i])
            {
                already_selected = 1;
            }
        }

        // if not selected then copy
        if (already_selected == 0)
        {
            dest[random_index] = array[random_index];
            printf("random_index = %d\n", random_index);
            selected[selected_index] = random_index;
            selected_index++;
        }
    }


    printf("print the arrays:\n");
    for(int a=0; a<length; a++)
    {
        printf("array[%d] = 0x%x\n", a, array[a]);
        printf("dest[%d] = 0x%x\n", a, dest[a]);
    }
}

// ----------------------------------------------------
// Miscellaneous : Random array copy
// ----------------------------------------------------








// Function to shuffle an array of integers using Fisher-Yates algorithm
static void fisher_yates_shuffle(int *arr, int n) {
    for (int i = n - 1; i > 0; i--) {
        int j = rand()%(i + 1);
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}


// ----------------------------------------------------
// Miscellaneous : Random code
// ----------------------------------------------------
int power(int a, int b);
void print_Stars(void);
void MergeSortArray();
void mergeSort(int a[], int s, int size);
void merge(int a[], int s, int m, int e);

//static void Miscellaneous(void);
static void Miscellaneous_2(void);


static int x = 10;

static inline int f()
{
   return x;
}

int g()
{
   //volatile int x = 20;
   static int x = 10;
   int result = f();
   return result;
}


static void Miscellaneous_2(void)
{
    printf("%d", g());
    printf("\n");
}


/***
 *
 *
static void Miscellaneous(void)
{
    struct a { int x; int y;};
    struct a p1[] = {1,2,3,4,5,6};
    struct a *p2 = p1;

    char *sw = "\0\0\0\0\0\0\0\0\0\0";
    printf("sizeof(sw): %u\n", sizeof(sw));         // size of the char pointer which is 8 in 64bit
    printf("strlen(sw): %d\n", strlen(sw));         // 0
    printf("strnlen(sw): %d\n", strnlen(sw, 8));    // 0


    // ------------------------------------------------------------------------------
    // this function is equivalent to (strlen (s) < maxlen ? strlen (s) : maxlen),
    // but it is more efficient and works even if s is not null-terminated
    // so long as maxlen does not exceed the size of s�s array.
    char string[32] = "hello, world";
    printf("%d\n",strnlen (string, 32));    // 12
    printf("%d\n", strnlen (string, 5));    // 5

    //register i =20;
    //register *j = &i;
    //printf("---- %d", *j);
    int x =10, y=20;
    float a =0.9;
    if (a == 0.9)
        printf(" %d %d\n",x, y);

    //int x =10, y=20;
    int t; t= x, x=y, y=t;

    printf(" %d %d\n",x, y);
    //char a[10];
    //strcpy(a, "turing");
    //printf(" s%", a);

    //File *fp = stdout;

    struct a ab;
    struct a *abc = &ab;

    //printf('A');

    //char a[10][5] = {"hi", "C", "Developers"};

    int (*(X()))[2];

    union test
    {
        char a;
        char b;
        int c;
    }t1;

    struct dev
    {
        char a;
        char b;
        int c;
    }t2;

    printf("----%d\n", sizeof(t1));
    printf("----%d\n", sizeof(t2));


    int array[2][3] = {{3,2,1}, {1,2,3}};
    printf("--stupid size-- %d\n", sizeof(array) /sizeof(array[1][1]));
}
*
**/


/*
 * Mergesort function
 */
void MergeSortArray()
{
    //int arrA[] = {23,  4, 56, 13, 21, 37, 1};
    int arrb[] = {45, 67,  0,  5, 27, 91, 3};

    int size = 7;

    mergeSort(arrb, 0, size-1);

    // print array
    for (int i=0; i<size; i++)
    {
        printf("%d ", arrb[i]);
    }
}


void mergeSort(int a[], int s, int size)
{
    if(s<size)
    {
        int m = s + (size-s)/2;
        int e = size;

        // left
        mergeSort(a, s, m);

        // right
        mergeSort(a, m+1, e);

        //Merge the 2 halves
        merge(a, s, m, e);
    }
}


void merge(int a[], int s, int m, int e)
{
    int l[m-s+1];
    int r[e-m];

    int n1 = m-s+1;
    int n2 = e-m;

    // copy the array
    for(int i=0; i<n1; i++)
    {
        l[i] = a[s+i];
    }

    for(int i=0; i<n2; i++)
        r[i] = a[m+1+i];


    int k=s;
    int i=0,j=0;
    // compare
    for( ;i<n1 && j<n2;)
    {
        if(l[i]<r[j])
        {
            a[k] = l[i];
            i++;
        }
        else
        {
            a[k] = r[j];
            j++;
        }
        k++;
    }


    // remaining in left
    while(i<n1)
    {
        a[k] = l[i];
        k++;
        i++;
    }

    // remaining in right
    while(j<n2)
    {
        a[k] = r[j];
        k++;
        j++;
    }
}



/*
 *
 */
int power(int a, int b)
{
    if(b==0)
        return 1;

    return power(a,b-1) * a;
}

/*
 *
 */
void print_Stars()
{
    // Example1
    int a=0;
    int L=10;

Step1:
    for (a=0; a<L; a++)
        printf("*");

    printf("\n");

    if(L--)
        goto Step1;
}

