/*
 * KnapSack.c
 *
 *  Created on: 12-Mar-2022
 *      Author: sumit
 */
#include <stdio.h>
#include <stdlib.h>
#include "KnapSack.h"
#include "Util.h"

typedef struct Object
{
	int value;
	int weight;
} Object_d;

static int KnapSack(Object_d *ps, int capacity, int n);

void DP_MaxCostInAGivenBag_Program(void);
void DP_MinCostToFillAGivenBag_Program(void);
void MinRemoval_tomake_Max_Sub_Min_lessThanK_Program(void);
void DP_MinRemoval_tomake_Max_Sub_Min_lessThanK_Program(void);
void DP_CoinChange_Permutation_Program(void);
void DP_Reach_a_given_score_Combination_Program(void);
void KnapSackDP_program(void);


void KnapSackProgram(void)
{
	int n = 3;
	int result = 0;
	int capacity = 50;

	// init array pf objects
	Object_d arrayofObj[n];
	arrayofObj[0].value = 60;
	arrayofObj[0].weight = 10;
	arrayofObj[1].value = 100;
	arrayofObj[1].weight = 20;
	arrayofObj[2].value = 120;
	arrayofObj[2].weight = 30;

	// call the recursive function
	result = KnapSack(arrayofObj, capacity, n);

	// print the result
	printf("max profit = %d\n", result);

	// DP : Max-cost of items in a given bag (unbounded-knapsack).
	printf("Max-cost of items in a given bag: \n");
	DP_MaxCostInAGivenBag_Program();
	printf("\n"); // EOL

	// DP : MIN-cost of items to fill a given bag with C capacity (unbounded-knapsack).
	printf("min-cost of items in a given bag: \n");
	DP_MinCostToFillAGivenBag_Program();
	printf("\n"); // EOL

	// Minimum removals from array to make max � min <= K
	printf("Minimum removals from array to make max � min <= K: \n");
	MinRemoval_tomake_Max_Sub_Min_lessThanK_Program();
	printf("\n"); // EOL

	// DP : Minimum removals from array to make max � min <= K
	printf("Minimum removals from array to make max � min <= K: \n");
	DP_MinRemoval_tomake_Max_Sub_Min_lessThanK_Program();
	printf("\n"); // EOL

	// DP : num of ways coin-change problem
	printf("Find num of ways to coin-change problem \n");
	DP_CoinChange_Permutation_Program();
	printf("\n"); // EOL

	// DP : num of ways coin-change problem
	printf("Find num of ways to score given runs \n");
	DP_Reach_a_given_score_Combination_Program();
	printf("\n"); // EOL

	// DP : unbounded knapsack
	printf("Knapsack with Duplicate Items: (unbounded knapsack) \n");
	KnapSackDP_program();
	printf("\n"); // EOL
}


/**
 * Generic KnapSack with output order.
 */
void KnapSack_Out_Order(void)
{

}


// ----------------------------------------------------
// DP : Knapsack with Duplicate Items
// ----------------------------------------------------
static void KnapSackDP(int n, int w, int *val, int *wt);

void KnapSackDP_program(void)
{
	printf("Example-1: \n");
	int val[] = {1, 1};
	int wt[] = {2, 1};
	KnapSackDP(sizeof(wt)/sizeof(wt[0]), 3, val, wt);
	printf("\n"); // EOL

	printf("Example-2: \n");
	int val1[] = {1, 4, 5, 7};
	int wt1[] = {1, 3, 4, 5};
	KnapSackDP(sizeof(wt1)/sizeof(wt1[0]), 8, val1, wt1);
	printf("\n"); // EOL

	/*printf("Example-3: \n");
	int arr3[] = {10, 15, 45, 30, 14};
	KnapSackDP(arr3, sizeof(arr3)/sizeof(arr3[0]), 7);
	printf("\n"); // EOL
	*/
}

static void KnapSackDP(int n, int w, int *val, int *wt)
{
	int *dp = memset(malloc(sizeof(int)*(w+1)), 0, sizeof(int)*(w+1));

	for (int j=1; j<=w; j++)
	{
		for (int i=0; i<n; i++)
		{
			if (wt[i]<=j) // wt can fit capacity
			{
				int rem = j - wt[i];
				if (dp[j] <= (val[i]+dp[rem]))
				{
					dp[j] = (val[i]+dp[rem]);
				}
			}
		}
	}

	printf("max-profit = %d\n", dp[w]);
}



/**
 * Generic KnapSack fractional code
 */
void KnapSackFractional(void)
{

}


/**
 * Generic KnapSack recursive function
 */
static int KnapSack(Object_d *ps, int capacity, int n)
{
	// exit or base condition
	if (capacity == 0 || n == 0)
		return 0;

	int Wi = ps[n-1].weight;
	int Vi = ps[n-1].value;

	// wi <= capacity, then max of include or exclude
	if (Wi <= capacity)
	{
		return MAX_VALUE((KnapSack(ps, (capacity-Wi), n-1) + Vi),	// Include = current + skip
					KnapSack(ps, capacity, n-1));			// Exclude = skip
	}
	else // wi > capacity, then skip or exclude.
	{
		return KnapSack(ps, capacity, n-1);
	}
}


// ----------------------------------------------------
// DP : max cost of items in given bag of C-capacity
// ----------------------------------------------------
static void DP_MaxCostInAGivenBag(int *cost, int n, int c);

void DP_MaxCostInAGivenBag_Program(void)
{
	printf("Example-1: \n");
	int arr[] = {20, 10, 4, 50, 100};
	DP_MaxCostInAGivenBag(arr, sizeof(arr)/sizeof(arr[0]), 5);
	printf("\n"); // EOL

	printf("Example-2: \n");
	int arr2[] = {-1, -1, 4, 3, -1};
	DP_MaxCostInAGivenBag(arr2, sizeof(arr2)/sizeof(arr2[0]), 5);
	printf("\n"); // EOL

	printf("Example-3: \n");
	int arr3[] = {10, 15, 45, 30, 14};
	DP_MaxCostInAGivenBag(arr3, sizeof(arr3)/sizeof(arr3[0]), 7);
	printf("\n"); // EOL
}

static void DP_MaxCostInAGivenBag(int *cost, int n, int c)
{
	int *dp = memset(malloc(sizeof(int)*(c+1)), 0, sizeof(int)*(c+1));	// max-cost filling the items till capacity.
	dp[0] = 0; // bag capacity is 0 then 0 cost
	dp[1] = cost[0]; // bag capacity is 1 then cost of 1kg packet only

	for (int j=2; j<=c; j++)	// capacity
	{
		int max = 0;
		for (int i=1; (i<=j && i<=n); i++) // packets: 1 till packats
		{
			if (cost[i-1] == -1)	// if '-1' means packet is not available then skip. (anyway dp will 0 for that)
				continue;

			// cost[i] + dp[j-1] >= max i.e. pick a packate + remaing ones for the capacity
			//
			// IMP: corner case: {-1, -1, 4, 3, -1};
			// 					Since cost[1] = -1 then also dp[1] should be -1
			//					And if dp[**] is -1 then skip and in all the iteration if max is 0,
			//					it means theres no way to fill the capacity so ans should be -1
			//
			int remBest = dp[j-i];
			if (remBest == -1)
				continue;

			int value = cost[(i-1)] + dp[j-i];	// 1 + 2-1
			if (value > max)
			{
				max = value;
			}
		}

		dp[j] = (max == 0) ? -1 : max;
	}

	printf("Max-cost: %d", dp[c]);
}


// ----------------------------------------------------
// DP : max cost of items in given bag of C-capacity
// ----------------------------------------------------
static void DP_MinCostToFillAGivenBag(int *cost, int n, int c);

void DP_MinCostToFillAGivenBag_Program(void)
{
	printf("Example-1: \n");
	int arr[] = {20, 10, 4, 50, 100};
	DP_MinCostToFillAGivenBag(arr, sizeof(arr)/sizeof(arr[0]), 5);
	printf("\n"); // EOL

	printf("Example-2: \n");
	int arr2[] = {-1, -1, 4, 3, -1};
	DP_MinCostToFillAGivenBag(arr2, sizeof(arr2)/sizeof(arr2[0]), 5);
	printf("\n"); // EOL

	printf("Example-3: \n");
	int arr3[] = {10, 15, 45, 30, 14};
	DP_MinCostToFillAGivenBag(arr3, sizeof(arr3)/sizeof(arr3[0]), 7);
	printf("\n"); // EOL
}

static void DP_MinCostToFillAGivenBag(int *cost, int n, int c)
{
	int *dp = memset(malloc(sizeof(int)*(c+1)), 0, sizeof(int)*(c+1));	// max-cost filling the items till capacity.
	dp[0] = 0; // bag capacity is 0 then 0 cost
	dp[1] = cost[0]; // bag capacity is 1 then cost of 1kg packet only, Since cost[1] = -1 then also dp[1] should be -1

	for (int j=2; j<=c; j++)	// capacity
	{
		int min = INT_MAX;
		for (int i=1; (i<=j && i<=n); i++) // packets: 1 till packats
		{
			// cost[i] + dp[j-1] >= max i.e. pick a packate + remaing ones for the capacity
			//
			// IMP: corner case: {-1, -1, 4, 3, -1};
			//					And if dp[*] is -1 then skip.
			//					In all the iteration if max is 0, it means theres no way to
			//					fill the capacity so ans should be -1
			int remBest = dp[j-i];
			if ((cost[i-1] == -1) || (remBest == -1))
				continue;

			int value = cost[(i-1)] + dp[j-i];	// 1 + 2-1
			if (value < min)
			{
				min = value;
			}
		}

		dp[j] = (min == INT_MAX) ? -1 : min;
	}

	printf("min-cost: %d", dp[c]);
}


// ----------------------------------------------------
// Rec: Minimum removals from array to make max � min <= K
// ----------------------------------------------------
static void MinRemoval_tomake_Max_Sub_Min_lessThanK(int i, int j, int *arr, int n, int removed, int *ans, int K);

void MinRemoval_tomake_Max_Sub_Min_lessThanK_Program(void)
{
	int ans = INT_MAX;	// min removals
	int n = 0;			// elements in array

	printf("Example-1: \n");
	int arr[] = {1, 3, 4, 9, 10, 11, 12, 17, 20};
	n = sizeof(arr)/sizeof(arr[0]);
	Util_Sorting_MergeSort(arr, n);
	MinRemoval_tomake_Max_Sub_Min_lessThanK(0, n-1, arr, n, 0, &ans, 4);
	printf("min-removal is %d", ans);
	printf("\n"); // EOL

	printf("Example-2: \n");
	int arr2[] = {1, 5, 6, 2, 8};
	n = sizeof(arr2)/sizeof(arr2[0]);
	ans = INT_MAX;
	Util_Sorting_MergeSort(arr2, n);
	MinRemoval_tomake_Max_Sub_Min_lessThanK(0, n-1, arr2, n, 0, &ans, 2);
	printf("min-removal is %d", ans);
	printf("\n"); // EOL

	/*printf("Example-3: \n");
	int arr3[] = {10, 15, 45, 30, 14};
	n = sizeof(arr3)/sizeof(arr3[0]);
	DP_MinRemoval_tomake_Max_Sub_Min_lessThanK(0, n-1, arr2, n, removed, &ans);
	printf("min-removal is %d", ans);
	printf("\n"); // EOL
	*/
}

static void MinRemoval_tomake_Max_Sub_Min_lessThanK(int i, int j, int *arr, int n, int removed, int *ans, int K)
{
	// base condition
	if ((i == j) || (i == n) || (j<0))
		return;

	// good condition : if A_max - A_min <= K then reached an end
	if ((arr[j] - arr[i]) <= K)
	{
		if (removed < *ans) {	*ans = removed; }	// ans found
		return;
	}

	// remove left
	MinRemoval_tomake_Max_Sub_Min_lessThanK(i+1, j, arr, n, removed + 1, ans, K);

	// remove right
	MinRemoval_tomake_Max_Sub_Min_lessThanK(i, j-1, arr, n, removed + 1, ans, K);

	return;
}


// ----------------------------------------------------
// DP : Minimum removals from array to make max � min <= K
// ----------------------------------------------------
static int DP_MinRemoval_tomake_Max_Sub_Min_lessThanK(int i, int j, int *arr, int n, int K, int dp[n][n]);

void DP_MinRemoval_tomake_Max_Sub_Min_lessThanK_Program(void)
{
	int n = 0;			// elements in array

	// --------------------------------------
	printf("Example-1: \n");
	int arr[] = {1, 3, 4, 9, 10, 11, 12, 17, 20};
	n = sizeof(arr)/sizeof(arr[0]);
	int dp[n][n];

	// reset dp
	for (int k=0; k<n; k++)
	{
		for (int l=0; l<n; l++)
			dp[k][l] = -1;
	}

	Util_Sorting_MergeSort(arr, n);
	printf("min-removal is %d", DP_MinRemoval_tomake_Max_Sub_Min_lessThanK(0, n-1, arr, n, 4, dp));
	printf("\n"); // EOL
	// --------------------------------------


	// --------------------------------------
	printf("Example-2: \n");
	int arr2[] = {1, 5, 6, 2, 8};
	n = sizeof(arr2)/sizeof(arr2[0]);

	int dp2[n][n];

	// reset dp
	for (int k=0; k<n; k++)
	{
		for (int l=0; l<n; l++)
			dp2[k][l] = -1;
	}

	Util_Sorting_MergeSort(arr2, n);
	printf("min-removal is %d", DP_MinRemoval_tomake_Max_Sub_Min_lessThanK(0, n-1, arr2, n, 2, dp2));
	printf("\n"); // EOL
	// --------------------------------------


	/*printf("Example-3: \n");
	int arr3[] = {10, 15, 45, 30, 14};
	n = sizeof(arr3)/sizeof(arr3[0]);
	DP_MinRemoval_tomake_Max_Sub_Min_lessThanK(0, n-1, arr2, n, removed, &ans);
	printf("min-removal is %d", ans);
	printf("\n"); // EOL
	*/
}

static int DP_MinRemoval_tomake_Max_Sub_Min_lessThanK(int i, int j, int *arr, int n, int K, int dp[n][n])
{
	// base condition
	if (i >= j)
		return 0;	// only one element left, so no-more removals

	// good condition : if A_max - A_min <= K then reached an end
	if ((arr[j] - arr[i]) <= K)
	{
		return 0; 	// no-more removals required
	}

	if (dp[i][j] != -1) // already calculated
	{
		return dp[i][j]; // no need to calc further just return from stored dp
	}

	// remove left & remove right
	dp[i][j] = 1 + MIN(DP_MinRemoval_tomake_Max_Sub_Min_lessThanK(i+1, j, arr, n, K, dp),
				       DP_MinRemoval_tomake_Max_Sub_Min_lessThanK(i, j-1, arr, n, K, dp));

	return dp[i][j];
}


// ----------------------------------------------------
// DP: Coin-change permutation : num-of-ways
// ----------------------------------------------------
static void DP_CoinChange_Permutation(int *coins, int n, int capacity);

void DP_CoinChange_Permutation_Program(void)
{
	printf("Example-1: \n");
	int coins[] = {2, 3, 5, 6};
	int n = sizeof(coins)/sizeof(coins[0]);
	DP_CoinChange_Permutation(coins, n, 8);
	printf("\n"); // EOL

	printf("Example-2: \n");
	int coins2[] = {3, 5, 10};
	n = sizeof(coins2)/sizeof(coins2[0]);
	DP_CoinChange_Permutation(coins2, n, 20);
	printf("\n"); // EOL

	/*printf("Example-3: \n");
	int arr3[] = {10, 15, 45, 30, 14};
	n = sizeof(arr3)/sizeof(arr3[0]);
	DP_MinRemoval_tomake_Max_Sub_Min_lessThanK(0, n-1, arr2, n, removed, &ans);
	printf("min-removal is %d", ans);
	printf("\n"); // EOL
	*/
}

static void DP_CoinChange_Permutation(int *coins, int n, int c)
{
	int *dp = memset(malloc(sizeof(int)*c+1), 0, sizeof(int)*c+1);
	dp[0] = 1; 	// ways for capacity is 0

	// find for each capacity
	for (int j=1; j<c+1; j++)
	{
		int ways = 0;

		for (int i=0; i<n; i++)	// for each type of coins
		{
			if (coins[i] <= j)
			{
				ways += dp[j - coins[i]]; // remaining coins: ways
			}
		}

		dp[j] = ways;
	}

	// c= 8 --> 3,5 & 5,3 are considered 2 different ways.
	printf("num-of-ways: %d\n", dp[c]);
}


// ----------------------------------------------------
// DP: Reach a given score
// ----------------------------------------------------
static void DP_Reach_a_given_score_Combination(int *score, int n, int runs);

void DP_Reach_a_given_score_Combination_Program(void)
{
	printf("Example-1: \n");
	int score[] = {3, 5, 10};
	int n = sizeof(score)/sizeof(score[0]);
	DP_Reach_a_given_score_Combination(score, n, 3);
	printf("\n"); // EOL

	printf("Example-2: \n");
	DP_Reach_a_given_score_Combination(score, n, 8);
	printf("\n"); // EOL

	printf("Example-3: \n");
	DP_Reach_a_given_score_Combination(score, n, 20);
	printf("\n"); // EOL

	printf("Example-4: \n");
	DP_Reach_a_given_score_Combination(score, n, 13);
	printf("\n"); // EOL
}

static void DP_Reach_a_given_score_Combination(int *score, int n, int runs)
{
	int *dp = memset(malloc(sizeof(int)*(runs+1)), 0, sizeof(int)*(runs+1));
	dp[0] = 1; 	// ways for capacity is 0

	for (int i=0; i<n; i++) // score
	{
		for (int j=1; j<runs+1; j++)
		{
			if (score[i] <= j)
			{
				dp[j] += dp[j-score[i]];
			}
		}

	}

	// c= 8 --> 2,5 is considered only.
	printf("num-of-ways: %d\n", dp[runs]);
}

