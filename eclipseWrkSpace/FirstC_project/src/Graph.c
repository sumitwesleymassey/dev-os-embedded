/*
 * Graph.c
 *
 *  Created on: 24-Apr-2022
 *      Author: sumit
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "Graph.h"


void Graph_AdjacencyMatrix(void);
void Graph_AdjacencyList(void);

// ----------------------------------------------------
// Main-driver function
// ----------------------------------------------------

void Graph_Program(void)
{
	// Matrix
	printf("Adjacency Matrix: \n");
	Graph_AdjacencyMatrix();

	// List
	printf("Adjacency List: \n");
	Graph_AdjacencyList();
}


// ----------------------------------------------------
// Adjacency-List
// ----------------------------------------------------
void Graph_AdjacencyList(void)
{
	int n=5;

	AdjList_d *list = malloc(sizeof(AdjList_d));
	list->head_vertex = list->tail_vertex = NULL;
	list->size = 0;

	// create all vertexes and add to the list
	for (int i=0; i<n ; i++)
		addVertexToList(list, i);

	// add edges
	addAdjEdge(list, 0, 1, 5);
	addAdjEdge(list, 0, 2, 4);

	displayAjdList(list);
	destroyAjdList(list);
}

void addAdjEdge(AdjList_d *list, int u, int v, int w)
{
	// create u
	AdjacencyNode_d *node_u = malloc(sizeof(AdjacencyNode_d));
	node_u->v = u;
	node_u->w = w;
	node_u->next_node = NULL;

	// create v
	AdjacencyNode_d *node_v = malloc(sizeof(AdjacencyNode_d));
	node_v->v = v;
	node_v->w = w;
	node_v->next_node = NULL;

	// create edges or relations
	Vertex_d *vertex_u = getVertex(list, u);
	if (vertex_u->head_node == NULL)
	{
		vertex_u->head_node = vertex_u->tail_node = node_v;
	}
	else
	{
		vertex_u->tail_node->next_node = node_v;
		vertex_u->tail_node = node_v;
	}
	vertex_u->num_edges++;

	// create edges or relations
	Vertex_d *vertex_v = getVertex(list, v);
	if (vertex_v->head_node == NULL)
	{
		vertex_v->head_node = vertex_v->tail_node = node_u;
	}
	else
	{
		vertex_v->tail_node->next_node = node_u;
		vertex_v->tail_node = node_u;
	}
	vertex_v->num_edges++;
}

void addVertexToList(AdjList_d *list, int u)
{
	Vertex_d *vertex = malloc(sizeof(Vertex_d));
	vertex->v = u;
	vertex->head_node = vertex->tail_node = NULL;
	vertex->next_vertex = NULL;
	vertex->num_edges = 0;

	// addToList
	if (list->head_vertex == NULL) // first node
	{
		list->head_vertex = list->tail_vertex = vertex;
	}
	else
	{
		list->tail_vertex->next_vertex = vertex;
		list->tail_vertex = vertex;
	}
	list->size++;
}


Vertex_d *getVertex(AdjList_d *list, int uv)
{
	Vertex_d *cur = list->head_vertex;
	while (cur)
	{
		if (cur->v == uv)
		{
			break;
		}
		cur = cur->next_vertex;
	}

	return cur;
}

void displayAjdList(AdjList_d *list)
{
	printf("display Adjacency-list: \n");
	Vertex_d *cur = list->head_vertex;
	while (cur)
	{
		printf("[%d]: ", cur->v);

		AdjacencyNode_d *cur_node = cur->head_node;
		while (cur_node)
		{
			printf("%d->", cur_node->v);
			cur_node = cur_node->next_node;
		}
		printf("NULL\n");
		cur = cur->next_vertex;
	}
}


void destroyAjdList(AdjList_d *list)
{
	Vertex_d *cur = list->head_vertex;
	while (cur)
	{
		AdjacencyNode_d *next_vertex = cur->next_vertex;

		AdjacencyNode_d *cur_node = cur->head_node;
		while (cur_node)
		{
			AdjacencyNode_d *next_node = cur_node->next_node;
			free(cur_node);
			cur_node = next_node;
		}

		free(cur);
		cur = next_vertex;
	}

	free(list);
}


// ----------------------------------------------------
// Adjacency-Matrix
// ----------------------------------------------------
void Graph_AdjacencyMatrix(void)
{
	// using adjacency matrix
	int n = 5;
	int **graph = malloc((sizeof(int))*n*n);
	memset(graph, 0, (sizeof(int))*n*n);

	createGraph(n, graph);
	displayGraph(n, graph);

	// destroy the graph.
}


void createGraph(int n, int graph[n][n])
{
	addEdge(n, graph, 0, 1);	// edge 0--1
	addEdge(n, graph, 0, 2);	// edge 0--2
	addEdge(n, graph, 0, 3);	// edge 0--3

	addEdge(n, graph, 1, 0); 	// edge 1--0
	addEdge(n, graph, 1, 2); 	// edge 1--2

	addEdge(n, graph, 2, 1); 	// edge 2--1
	addEdge(n, graph, 2, 0); 	// edge 2--0
	addEdge(n, graph, 2, 3); 	// edge 2--3
	addEdge(n, graph, 2, 4); 	// edge 2--4

	addEdge(n, graph, 3, 0); 	// edge 3--0
	addEdge(n, graph, 3, 2); 	// edge 3--2
	addEdge(n, graph, 3, 4); 	// edge 3--4

	addEdge(n, graph, 4, 3); 	// edge 4--3
	addEdge(n, graph, 4, 2); 	// edge 4--2
}

void addEdge(int n, int graph[n][n], int u, int v)
{
	if (u>=n || v>=n)
		return; // error

	graph[u][v] = 1;
	graph[v][u] = 1;
}

void displayGraph(int n, int graph[n][n])
{
	for (int i=0; i<n; i++)
	{
		for(int j=0; j<n; j++)
		{
			printf("%d ", graph[i][j]);
		}
		printf("\n"); //EOL
	}
}
