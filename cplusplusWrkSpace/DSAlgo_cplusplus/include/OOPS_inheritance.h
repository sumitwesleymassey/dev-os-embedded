/*
 * OOPS_inheritance.h
 *
 *  Created on: 04-Apr-2022
 *      Author: sumit
 */

#ifndef INCLUDE_OOPS_INHERITANCE_H_
#define INCLUDE_OOPS_INHERITANCE_H_

#include <iostream>
#include <bits/stdc++.h>

using namespace std;

class Vehicle {
public:
	int a;
	int b;

	int getPetrol(void);
	int getMeterReading(void);
	int getModelNumber(void);

protected:
	int x;
	int y;

private:
	int z;
};


class Car : public Vehicle
{
public:
	int type;
	int model_num;

	Car()
	{
		cout << "Car Object\n" << endl;
	}

	Car(int ptype, int pmodel_num)
	{
		cout << "Car Object parameterized\n" << endl;
		type = ptype;
		model_num = pmodel_num;
	}


	void whatTypeOfCar();
};



class Truck : public Vehicle
{
public:
	Truck()
	{
		cout << "Truck Object\n";
	}

	void whatDoesItDo();
};


class Bus : public Vehicle
{
public:
	Bus()
	{
		cout << "Bus Object\n";
	}
};


#endif /* INCLUDE_OOPS_INHERITANCE_H_ */
