/*
 * SubString.c
 *
 *  Created on: 08-Jun-2022
 *      Author: sumit
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "SubString.h"
#include "Util.h"


void SubString_LCSubstring(void);
void SubString_printAllSubstring(void);
void SubString_countAllPalindromicSubstring(void);
void SubString_printAllPalindromicSubstring(void);
void SubString_longestPalindromicSubstring(void);
void SubString_longestSubstring_nonrepeating(void);



// ----------------------------------------------------
// Main-driver function
// ----------------------------------------------------
void SubString_Program(void)
{
	// DP : Longest-common-substring
	printf("Longest-common-substring: \n");
	SubString_LCSubstring();
	printf("\n"); // EOL

	// print all substrings
	printf("all substring: \n");
	SubString_printAllSubstring();
	printf("\n"); // EOL

	// DP : count all palindromic-substrings
	printf("all substring: \n");
	SubString_countAllPalindromicSubstring();
	printf("\n"); // EOL

	// DP : print all palindromic-substrings
	printf("print all palindromic substring: \n");
	SubString_printAllPalindromicSubstring();
	printf("\n"); // EOL

	// DP : longest palindromic-substrings
	printf("longest palindromic substring: \n");
	SubString_longestPalindromicSubstring();
	printf("\n"); // EOL

	// DP : longest substrings with non-repeating chars
	printf("longest substrings without repeating characters: \n");
	SubString_longestSubstring_nonrepeating();
	printf("\n"); // EOL
}


// ----------------------------------------------------
// Longest-common-substring
// ----------------------------------------------------
void SubString_LCSubstring(void)
{
	char *str1 = "pqabcpxy";
	char *str2 = "xyzabcp";

	int n1 = strlen(str1)+1;
	int n2 = strlen(str2)+1;

	int dp[n1][n2];
	memset(dp, 0, sizeof(int)*n1*n2);

	int max = 0;

	// find longest suffix of between all the prefixes of s1 and s2
	//
	//			 x	y	z	a	b	c	p
	//		  0	 0	0	0	0	0	0 	0
	//		p 0							1
	//		q 0
	//		a 0				1
	//		b 0					1
	//		c 0						1
	//		p 0							1
	//		x 0 1
	//		y 0    1
	//
	for (int i=1; i<n1; i++)
	{
		for (int j=1; j<n1; j++)
		{
			if(str1[i-1] == str2[j-1])
			{
				dp[i][j] = dp[i-1][j-1] + 1;
			}
			else
			{
				dp[i][j] = 0;
			}

			if(dp[i][j] > max)
				max = dp[i][j];
		}
	}

	printf("LCS-len = %d\n", max);
}


// ----------------------------------------------------
// print all substrings
// ----------------------------------------------------
void SubString_printAllSubstring(void)
{
	char *str = "abccbc";
	int n = strlen(str);

	for (int i=0; i<n; i++)
	{
		for (int j=i; j<n; j++)
		{
			printf("%s\n", Util_substring(str, i, j));
		}
		printf("\n");
	}
}


// ----------------------------------------------------
// count all palindromic substrings
// ----------------------------------------------------
void SubString_countAllPalindromicSubstring(void)
{
	char *str = "abccbc";
	int n = strlen(str);

	int dp[n][n];
	memset(dp, 0, sizeof(int)*n*n);

	int count = 0;

	for (int g=0; g<n; g++)
	{
		for (int i=0, j=g; j<n; j++, i++)
		{
			if (g==0)
			{
				dp[i][j] = 1; // its a palindrome of size 1
			}
			else if (g==1)
			{
				if (str[i] == str[j])
					dp[i][j] = 1; // its a palindrome of size 2
			}
			else
			{
				if ((str[i] == str[j]) && dp[i+1][j-1])	// c1 == c2 then check middle part
				{
					dp[i][j] = 1;	// its a palindrome of size > 2
				}
			}

			if (dp[i][j] == 1)
				count++;
		}
	}

	printf("count of PS =%d\n", count);
}


// ----------------------------------------------------
// print all palindromic substrings
// ----------------------------------------------------
void SubString_printAllPalindromicSubstring(void)
{
	char *str = "abccbc";
	int n = strlen(str);

	int dp[n][n];
	memset(dp, 0, sizeof(int)*n*n);

	// gap between i and j,
	//	g = 0 means i=0 and j=0
	//	g = 1 means i=0 and j=1
	//	g = 2 means i=0 and j=2
	//	g = 3 means i=0 and j=3
	//	....	....	....
	for (int gap=0; gap<n; gap++)
	{
		for (int i=0, j=gap; j<n; i++, j++)
		{
			if (gap == 0)
			{
				dp[i][j] = 1;
			}
			else if (gap == 1)
			{
				if (str[i] == str[j])
					dp[i][j] = 1;
			}
			else
			{
				if ((str[i] == str[j]) && dp[i+1][j-1])
				{
					dp[i][j] = 1;
				}
			}

			if (dp[i][j] == 1)
				printf("%s\n", Util_substring(str, i, j));
		}
	}

}


// ----------------------------------------------------
// longest palindromic substrings
// ----------------------------------------------------
//		   a  b	 c	c	b	c
//		a  1  0	 0	0	0	0
//		b     1         1
//		c        1  1
//		c  			1       1
//		b  			    1
//		c  				    1
//
void SubString_longestPalindromicSubstring(void)
{
	char *str = "abccbc";
	int n = strlen(str);

	int dp[n][n];
	memset(dp, 0, sizeof(int)*n*n);

	int max = 0;
	int si = -1, ei = -1;

	for (int gap=0; gap<n; gap++)
	{
		for (int i=0, j=gap; j<n; j++, i++)
		{
			if (gap==0)
			{
				dp[i][j] = 1;
			}
			else if (gap == 1)
			{
				if (str[i] == str[j])
					dp[i][j] = 1;
			}
			else
			{
				if ((str[i] == str[j]) && (dp[i+1][j-1])) // c1 == c2 and check middle as well i.e. b-cc-b
					dp[i][j] = 1;
			}

			if (dp[i][j] == 1)
			{
				max = gap + 1;
				si = i;
				ei = j;
			}
		}
	}

	printf("longest-PS is: %s of len %d\n", Util_substring(str, si, ei), max);
}

// ----------------------------------------------------
// longest substrings wihtout repeating characters
// ----------------------------------------------------
static void SubString_longestSubstring_nonrepeating_Program(char *str, int n);

void SubString_longestSubstring_nonrepeating(void)
{
	char *str = "abbacbcdbadbdbbdcb";
	printf("Example-1: \n");
	SubString_longestSubstring_nonrepeating_Program(str, strlen(str));

	char *str2 = "abcdefgh";
	printf("Example-2: \n");
	SubString_longestSubstring_nonrepeating_Program(str2, strlen(str2));
}


static void SubString_longestSubstring_nonrepeating_Program(char *str, int n)
{
	int i = -1;
	int j = -1;

	DynamicMemory_Map_d *map = Util_createHashMap(0,0,0);
	int invalid_found = 0;

	int max = 0;
	//int len=0;
	char *ans;

	// loop till the end
	while (i<n-1)
	{
		// acquire
		while(i<n-1)
		{
			i++;
			Util_HashMap_put(map, &str[i], 1, Util_HashMap_getValue(map, &str[i], 1) + 1);

			// after putting, substring becomes invalid (or it contain duplicate chars now).
			if (Util_HashMap_getValue(map, &str[i], 1) == 2)
			{
				invalid_found = 1; // substring becomes invalid
				break;
			}
			//else
			//	len++;
			else
			{
				int len = i-j;
				if (len > max) // a, ab, bac, acb, bcd, cdb, cdba.....
				{
					ans = Util_substring(str, j+1, i); // why "j+1", check the code for the position of j.
					max = len;
				}
			}

			//if (len > max) // a, ab, bac, acb, bcd, cdb, cdba.....
			//	max = len;
		}

		// release
		while(j<n-1 && invalid_found)
		{
			j++;
			Util_HashMap_put_updateValue(map, &str[j], 1, Util_HashMap_getValue(map, &str[j], 1) - 1);

			// check if the offended char becomes valid now, if not relealse another one from "j"
			if (Util_HashMap_getValue(map, &str[i], 1) == 1)
			{
				invalid_found = 0; // made char-subtring valid
				break;
			}
			//else
			//	len--;
		}
	}

	printf("substring= %s of len %d\n", ans, max);
}

