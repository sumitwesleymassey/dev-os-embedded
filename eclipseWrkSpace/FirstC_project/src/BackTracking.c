/*
 * BackTracking.c
 *
 *  Created on: 20-Apr-2022
 *      Author: sumit
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "BackTracking.h"
#include "Util.h"
#include "Partition_Subset.h"
#include "Graph.h"

void BackTracking_Rat_In_A_Maze(void);
void BackTracking_wordbreakpart2(void);
void BackTraing_solverSudoku(void);
void BackTraing_M_Coloring(void);
void BackTraing_Knight_Tour_Problem(void);
void BackTracking_Combination_Sum(void);
void BackTracking_LargerNum_In_K_Swap(void);
void BackTracking_Permutation_Problem(void);
void BackTracking_LongestPath_in_matrix_with_hurldles(void);
void BackTracking_ShortestRoute_in_matrix_with_landmines(void);
void BackTracking_Kth_permuation(void);
void BackTracking_all_paths_in_grid(void);
void BackTracking_find_path_of_morethan_k_len_from_src(void);


// ----------------------------------------------------
// Main-driver function
// ----------------------------------------------------
void BackTracking_Program(void)
{
	/*printf("Rat in a Maze\n");
	BackTracking_Rat_In_A_Maze();
	//main2();
	printf("word break part2\n");
	BackTracking_wordbreakpart2();

	printf("Sodoku solver\n");
	BackTraing_solverSudoku();

	printf("M-colorin problem\n");
	BackTraing_M_Coloring();

	printf("Knight-Tour problem\n");
	BackTraing_Knight_Tour_Problem();

	printf("Combination-Sum problem\n");
	BackTracking_Combination_Sum();

	printf("Larger num in K-swaps problem\n");
	BackTracking_LargerNum_In_K_Swap();

	printf("permutations: \n");
	BackTracking_Permutation_Problem();

	printf("Longest path in matrix with hurdles: \n");
	BackTracking_LongestPath_in_matrix_with_hurldles();

	printf("shortest safe route in matrix with landmines: \n");
	BackTracking_ShortestRoute_in_matrix_with_landmines();

	printf("Kth permutation: \n");
	BackTracking_Kth_permuation();*/

	printf("all paths in a grid: \n");
	BackTracking_all_paths_in_grid();
	printf("\n"); //EOL

	printf("paths of morethan k-len from a src: \n");
	BackTracking_find_path_of_morethan_k_len_from_src();
	printf("\n"); //EOL
}


// ----------------------------------------------------
// path of more than k length from a source
// ----------------------------------------------------
typedef struct Node_x
{
	int v;
	int w;
	struct Node_x *next;
}Node_x_d;

typedef struct PathSet
{
	Node_x_d *head;
	Node_x_d *tail;
	int size;
}PathSet_d;

typedef struct List
{
	PathSet_d *head;
	PathSet_d *tail;
	int size;
}List_d;


AdjList_d *createExampleGraph(void);
bool_t solvePaths(AdjList_d *list, int src, int k, int visited[], PathSet_d *set, List_d *ans);

void addToSet(PathSet_d *set, int v, int w);
void remFromSet(PathSet_d *set, int v);
void printSet(PathSet_d *set);

void BackTracking_find_path_of_morethan_k_len_from_src(void)
{
	AdjList_d *list = createExampleGraph();
	PathSet_d *set = malloc(sizeof(PathSet_d));
	set->head = set->tail = NULL;
	set->size = 0;

	List_d *ans = malloc(sizeof(List_d));
	ans->head = ans->tail = NULL;
	ans->size = 0;

	// visited
	int *visited = malloc(sizeof(int) * 9);
	memset(visited, 0, sizeof(int) * 9);

	int src = 0; 			// source
	visited[src] = 1; 		// mark the src before going

	// add '0' to set but without weight as it will already be covered in 0-1, 0-2, 0-3
	//
	//	       1				0-1 : 4w
	// 		0 --- 3				0-2 : 2w
	// 		| \   |				0-3 : 1w
	// 	   4|  2\ | 2			 so, 0 shall not include 1 again, wont be able to find
	// 		1 --- 2
	//  		3
	//
	//
	addToSet(set, src, 0);

	if (solvePaths(list, src, 58, visited, set, ans))
		printf("YES for more than 58\n");
	else
		printf("NO for more than 58\n");

	if (solvePaths(list, src, 62, visited, set, ans))
		printf("YES for more than 62\n");
	else
		printf("NO for more than 62\n");

	displayAjdList(list);
	destroyAjdList(list);
}

bool_t solvePaths(AdjList_d *list, int src, int k, int visited[], PathSet_d *set, List_d *ans)
{
	bool_t result = false;

	// base-codition:
	// If k is 0 or negative, return true;
	if (k<=0)
	{
		printSet(set);
		return true;
	}

	// find all the adjacent neighbors of the src
	// Get all adjacent vertices of source vertex src and
	// recursively explore all paths from src.
	Vertex_d *vertex = getVertex(list, src);

	AdjacencyNode_d *cur = vertex->head_node;
	while (cur)
	{
		int v = cur->v;
	    int w = cur->w;

		// bound : If weight of is more than k, return true
	    // 		   this is an optimisation when you want to return true when
	    //		   any single path of more-than-k len is found.
		/*if (w >= k)
			return true;
		*/

		// validity
		if (visited[v] == 1)
		{
			cur = cur->next_node; // update for next iteration
			continue; //return false; // dont return false here, continue with other paths.
		}

		addToSet(set, v, w);	//
		visited[v] = 1;			// set
		result |= solvePaths(list, v, k-w, visited, set, ans); // solve for each-adjacent neighbor
		visited[v] = 0; 		// back-track
		remFromSet(set, v);		//

		// next-neighbor
		cur = cur->next_node;
	}

	// If no adjacent could produce longer path, return
	// false
	return result;
}


void addToSet(PathSet_d *set, int v, int w)
{
	Node_x_d *node = malloc(sizeof(Node_x_d));
	node->v = v;
	node->w = w;
	node->next = NULL;

	if (set->head == NULL)
	{
		set->head = set->tail = node;
	}
	else
	{
		set->tail->next = node;
		set->tail = node;
	}
	set->size++;
}

void remFromSet(PathSet_d *set, int v)
{
	Node_x_d *cur = set->head;
	Node_x_d *prev = NULL;

	while (cur)
	{
		if (cur->v == v)
		{
			if (set->head == cur)
			{
				set->head = cur->next;
			}
			else if (set->tail == cur)
			{
				prev->next = NULL;
				set->tail = prev;
			}
			else
			{
				prev->next = cur->next;
			}

			break;
		}
		prev = cur;
		cur = cur->next;
	}

	free(cur);
	set->size--;
}


AdjList_d *createExampleGraph(void)
{
	int n=9;
	AdjList_d *list = malloc(sizeof(AdjList_d));
	list->head_vertex = list->tail_vertex = NULL;
	list->size = 0;

	// create all vertexes and add to the list
	for (int i=0; i<n ; i++)
		addVertexToList(list, i);

	// add edges
	addAdjEdge(list, 0, 1, 4);
	addAdjEdge(list, 0, 7, 8);
	addAdjEdge(list, 1, 2, 8);
	addAdjEdge(list, 1, 7, 11);
	addAdjEdge(list, 2, 3, 7);
	addAdjEdge(list, 2, 8, 2);
	addAdjEdge(list, 2, 5, 4);
	addAdjEdge(list, 3, 4, 9);
	addAdjEdge(list, 3, 5, 14);
	addAdjEdge(list, 4, 5, 10);
	addAdjEdge(list, 5, 6, 2);
	addAdjEdge(list, 6, 7, 1);
	addAdjEdge(list, 6, 8, 6);
	addAdjEdge(list, 7, 8, 7);

	return list;
}

void printSet(PathSet_d *set)
{
	int w = 0;
	printf("path: ");
	Node_x_d *cur = set->head;
	while (cur)
	{
		w += cur->w;
		printf("%d->", cur->v);
		cur = cur->next;
	}
	printf("\t w:%d\n", w); //EOL
}

// ----------------------------------------------------
// All possible paths in a grid
// ----------------------------------------------------
typedef struct Node_2
{
	int val;
	struct Node_2 *next;
}Node_2_d;

typedef struct Path
{
	Node_2_d *head;
	Node_2_d *tail;
	struct Path *next;
	int size;
}Path_d;


typedef struct ListOfPath
{
	Path_d *head;
	Path_d *tail;
	int size;
}ListOfPath_d;

void addNodeToPath(Path_d *path, int value);
void remNodeFromPath(Path_d *path, int value);
void addPathToList(ListOfPath_d *list, Path_d *path);
Path_d *createPathCopy(Path_d *path);
static void displayPath(Path_d *path);

static bool_t solveGridPaths(int i, int j, int m, int n, int grid[m][n], int visited[m][n], ListOfPath_d *list, Path_d *path);

void BackTracking_all_paths_in_grid(void)
{
	/*int grid[5][5] = {{1, 0, 1, 0, 1},
					   {1, 1, 1, 1, 1},
					   {0, 1, 0, 1, 0},
					   {1, 0, 0, 1, 1},
					   {1, 1, 1, 0, 1}};*/


	int grid[2][3] = {{1, 2, 3},
			          {4, 5, 6}};

	int visited[2][3] = {{0, 0, 0},
			             {0, 0, 0}};


	ListOfPath_d *list = malloc(sizeof(ListOfPath_d));
	list->head = list->tail = NULL;
	list->size = 0;

	Path_d *path = malloc(sizeof(Path_d));
	path->head = path->tail = NULL;
	path->next = NULL;
	path->size = 0;

	bool_t result = solveGridPaths(0, 0, 2, 3, grid, visited, list, path);
	/*if (!result)
	{
		printf("No paths found\n");
	}
	else
	{
		Path_d *cur = list->head;
		while (cur)
		{
			Node_2_d *cur_node = cur->head;
			while(cur_node)
			{
				printf("%d->" ,cur_node->val);
				cur_node = cur_node->next;
			}
			printf("NULL\n");
			cur = cur->next;
		}
	}*/
}

static void displayPath(Path_d *path)
{
	Node_2_d *cur_node = path->head;
	while(cur_node)
	{
		printf("%d->" ,cur_node->val);
		cur_node = cur_node->next;
	}
	printf("NULL\n");
}

static bool_t solveGridPaths(int i, int j, int m, int n, int grid[m][n], int visited[m][n], ListOfPath_d *list, Path_d *path)
{
	bool_t result = false;

	//base
	if (i==m-1 && j==n-1)
	{
		// update the last element
		addNodeToPath(path, grid[i][j]);

		// add to the list
		//addPathToList(list, path);

		displayPath(path);

		remNodeFromPath(path, grid[i][j]);
		return true;
	}

	// bounds
	if (i>=m || j>=n)
		return false;

	if (visited[i][j] != 0) // issafe or isvalid
		return false;

	addNodeToPath(path, grid[i][j]);
	visited[i][j] = 1; // mark
	result |= solveGridPaths(i+1, j, 2, 3, grid, visited, list, path); // down
	result |= solveGridPaths(i, j+1, 2, 3, grid, visited, list, path); // right
	visited[i][j] = 0; // back-track
	remNodeFromPath(path, grid[i][j]);

	return result;
}


void addPathToList(ListOfPath_d *list, Path_d *path)
{
	// copy the path
	Path_d *new_path = createPathCopy(path);

	if (list->head == NULL)
	{
		list->head = list->tail = new_path;
	}
	else
	{
		list->tail->next = new_path;
		list->tail = new_path;
	}

	list->size++;
}

void addNodeToPath(Path_d *path, int value)
{
	Node_2_d *new_node = malloc(sizeof(Node_2_d));
	new_node->next = NULL;
	new_node->val = value;

	if (path->head == NULL)
	{
		path->head = path->tail = new_node;
	}
	else
	{
		path->tail->next = new_node;
		path->tail = new_node;
	}

	path->size++;
}


void remNodeFromPath(Path_d *path, int value)
{
	Node_2_d *cur = path->head;
	Node_2_d *prev = NULL;
	while (cur)
	{
		if (cur->val == value)
		{
			if (cur == path->head) /// if its head
			{
				path->head = cur->next; // new head
			}
			else if (cur == path->tail)
			{
				prev->next = cur->next;
				path->tail = prev;
			}
			else
			{
				prev->next = cur->next;
			}
			free(cur);
			path->size--;
			break;
		}
		prev = cur;
		cur = cur->next;
	}
}


Path_d *createPathCopy(Path_d *path)
{
	Path_d *new_path = malloc(sizeof(new_path));
	new_path->head = new_path->tail = NULL;
	new_path->next = NULL;
	new_path->size = 0;

	Node_2_d *cur = path->head;
	while (cur)
	{
		// alloc new node
		Node_2_d *new_node = malloc(sizeof(Node_2_d));
		new_node->val = cur->val;
		new_node->next = cur->next;
		if (new_path->head == NULL)
		{
			new_path->head = new_path->tail = new_node;
		}
		else
		{
			new_path->tail->next = new_node;
			new_path->tail = new_node;
		}
		cur = cur->next;
	}

	new_path->size = path->size ;
	return new_path;
}


// ----------------------------------------------------
// Kth permutation (with optimized algo)
// ----------------------------------------------------
static void findKthPermutation(int n, int k, int fact, int i, char *string, int size, int *digits);

void BackTracking_Kth_permuation(void)
{
	int n = 5;
	int k = 86;

	char *string = malloc(n+1);
	memset(string, 0 , n+1);
	int *digits = malloc(n);
	int fact = 1;

	// compute factorial for (n-1)
	// 1, 2, 6
	//
	// and compute digits
	// [1, 2, 3 , 4]
	int i=1;
	for (; i<n; i++)
	{
		fact = fact*i;
		digits[i-1] = i;
	}
	digits[i-1]= i;

	findKthPermutation(n,k-1, fact, 0, string, n+1, digits);

	// print the string
	printf(" == %s\n", string);
}


/**
 *	For, n=4, k = 17
 *
 *		n=4, k= 16                  |  n=3, k= 4                    |  n=2, k= 0                  |  n=1, k= 0
 *      fact = 6                    |  fact = 2                     |  fact = 1                   |  fact = 1
 *      k = k%(n-1)! == k%fact = 4  |  k = k%(n-1)! == k%fact = 0   |  k = k%(n-1)! == k%fact = 0 |  k = k%(n-1)! == k%fact = 0
 *      i = k/(n-1)! = 2            |  i = k/(n-1)! = 2             |  i = k/(n-1)! = 0           |  i = k/(n-1)! = 0
 *      fact = fact/(n-1) =2        |  fact = fact/(n-1) = 1        |  fact = fact/(n-1) = 1      |  fact = fact/(n-1) = 1
 *                                  |                               |                             |
 *                                  |                               |                             |
 *
 *
 *  Note: 1) atoi = char_value - '0' ==> (ascii value of interger - ascii value of 0)
 *        2) itoa = int_value + '0' ==> (integer value as ascii + ascii value of 0)
 *
 *
 * No need of dynamic allocation:- deepcopy to new and delete old
 *
 *	1) digits = [1, 2, 3, 4]    |  2) digits = [1, 2, 4]     |  3) digits = [1, 2]     |  4) digits = [2]
 *	   new = [1, 2, 4]          |     new = [1, 2]           |     new = [2]           |     new = []
 *
 **/
static void findKthPermutation(int n, int k, int fact, int i, char *string, int size, int *digits)
{
	int index = 0;

	// base condition
	if (n==1)
	{
		string[i] = digits[0] + '0'; // it will the last digit
		return;
	}

	// find index
	index = k/fact; // {16/6 == 2} -> {4/2 == 2} -> {0/1 == 0} -> (0}

	//update the digits
	string[i] = (char)digits[index] + '0';

	// update, k, fact
	k = k%(fact); // 16%6 -> 4%2 -> 0
	fact = fact/(n-1); // 24 -> 6 -> 2 -> 1

	// erase digits: deep copy with a kip
	int l=0;
	int m=0;
	int *new = malloc(n-1);

	while (l<n && m<n)
	{
		if (m==index)
		{
			m++;
			continue;
		}
		*(new+l) = *(digits+m);
		l++;
		m++;
	}

	free(digits);

	findKthPermutation(n-1, k, fact, i+1, string, size, new);
}

// ----------------------------------------------------
// shortest safe route in matrix with landmines
// ----------------------------------------------------
typedef struct answer
{
	int *path;
	int max_min_path; // minfor landmines
}answer_d;


#define SHORTEST DISABLED // if disabled then print the longest path frm col-0 to col-n

int R[4] = { 0,  0, -1, 1};
int C[4] = {-1,  1,  0, 0};

static bool_t shortestRoute(int m, int n, int matrix[m][n], int row, int col, int max_path, int visited[m][n], answer_d *ans, int value);
static void mark_unsafe_cells(int m, int n, int matrix[m][n]);
static bool_t isValid(int x, int y, int m, int n);

void BackTracking_ShortestRoute_in_matrix_with_landmines(void)
{
	int m = 12;
	int n = 10;
	int matrix[12][10] = {{ 1,  1,  1,  1,  1,  1,  1,  1,  1,  1 },
			 	 	 	  { 1,  0,  1,  1,  1,  1,  1,  1,  1,  1 },
						  { 1,  1,  1,  0,  1,  1,  1,  1,  1,  1 },
						  { 1,  1,  1,  1,  0,  1,  1,  1,  1,  1 },
						  { 1,  1,  1,  1,  1,  1,  1,  1,  1,  1 },
						  { 1,  1,  1,  1,  1,  0,  1,  1,  1,  1 },
						  { 1,  0,  1,  1,  1,  1,  1,  1,  0,  1 },
						  { 1,  1,  1,  1,  1,  1,  1,  1,  1,  1 },
						  { 1,  1,  1,  1,  1,  1,  1,  1,  1,  1 },
						  { 0,  1,  1,  1,  1,  0,  1,  1,  1,  1 },
						  { 1,  1,  1,  1,  1,  1,  1,  1,  1,  1 },
						  { 1,  1,  1,  0,  1,  1,  1,  1,  1,  1 }};

	int *visited = malloc(sizeof(int) *m*n);
	memset(visited, 0, sizeof(int) *m*n);

	// answer-list
	answer_d *ans = malloc(sizeof(answer_d));
	ans->path = malloc(sizeof(int) *m*n);

#if (SHORTEST == ENABLED)
	ans->max_min_path = 0x0000FFFF;
#else
	ans->max_min_path = 0;
#endif

	// mark-unsafe-cells
	// 1) find land-mine '0'
	// 2) mark neighbours '-1' first
	// 3) mark all '-1' as '0'.
	//
	// note: marking first '-1' and then '0' makes sure all the elements in
	// 		 matrix is not converted to '0'
	mark_unsafe_cells(m, n, matrix);


	// ------------------------
	bool_t found = false;
	for (int row=0; row<m; row++)
	{
		memset(visited, 0, sizeof(int) *m*n);
		found |= shortestRoute(m, n, matrix, row, 0, matrix[row][0], visited, ans, 's');
	}

	if (found)
	{
		// print the paths
		for (int i=0; i<m; i++)
		{
			printf("{");
			for(int j=0; j<n; j++)
			{
				printf("%c ", *(ans->path + i*n +j));
			}
			printf("}\n");

		}

		printf("max_min_path: %d\n", ans->max_min_path);
	}
	else
		printf("Destination not reachable from given source\n");
}


static bool_t shortestRoute(int m, int n, int matrix[m][n], int row, int col, int min_path, int visited[m][n], answer_d *ans, int value)
{
	bool_t found = false;

	// base condition
	if (col == n-1)
	{
		// add to the list
#if (SHORTEST == ENABLED)
		if (min_path < ans->max_min_path)
#else
		if (min_path > ans->max_min_path)
#endif
		{
			visited[row][col] = value;	// set, as it was not before call
			ans->max_min_path = min_path;
			memset(ans->path, 0, sizeof(int)*m*n);	// clear prev path
			memcpy(ans->path, visited, sizeof(int)*m*n);
			visited[row][col] = 0;	// back-track
			return true;
		}

		return false;
	}

#if (SHORTEST == ENABLED)
	// if the cur min_path is greater than min-so-far then skip
	if (min_path > ans->max_min_path)	// little- optimisation
		return false;
#endif

	// bound, already visited, safe or not
	if (row<0 || col<0 || row>=m || col>=n || (visited[row][col]!=0) || (matrix[row][col] == 0))
	{
		return false;
	}


	visited[row][col] = value;	// set
	found |= shortestRoute(m, n, matrix, row, col-1, min_path+matrix[row][col-1], visited, ans, 'L');	// left
	found |= shortestRoute(m, n, matrix, row, col+1, min_path+matrix[row][col+1], visited, ans, 'R');	// right
	found |= shortestRoute(m, n, matrix, row-1, col, min_path+matrix[row-1][col], visited, ans, 'U');	// up
	found |= shortestRoute(m, n, matrix, row+1, col, min_path+matrix[row+1][col], visited, ans, 'D');	// down
	visited[row][col] = 0;	// back-track

	return found;
}


/**
 *
 *
 *               {{ 1, -1,  1,  1,  1,  1,  1,  1,  1,  1 },              {{ 1,  M,  1,  1,  1,  1,  1,  1,  1,  1 },
 *                {-1,  0, -1, -1,  1,  1,  1,  1,  1,  1 },               { M,  0,  M,  M,  1,  1,  1,  1,  1,  1 },
 *                { 1, -1, -1,  0, -1,  1,  1,  1,  1,  1 },               { 1,  M,  M,  0,  M,  1,  1,  1,  1,  1 },
 *                { 1,  1,  1, -1,  0, -1,  1,  1,  1,  1 },               { 1,  1,  1,  M,  0,  M,  1,  1,  1,  1 },
 *                { 1,  1,  1,  1, -1, -1,  1,  1,  1,  1 },               { 1,  1,  1,  1,  M,  M,  1,  1,  1,  1 },
 *                { 1, -1,  1,  1, -1,  0, -1,  1, -1,  1 },               { 1,  M,  1,  1,  M,  0,  M,  1,  M,  1 },
 *                {-1,  0, -1,  1,  1, -1,  1, -1,  0, -1 },               { M,  0,  M,  1,  1,  M,  1,  M,  0,  M },
 *                { 1, -1,  1,  1,  1,  1,  1,  1, -1,  1 },               { 1,  M,  1,  1,  1,  1,  1,  1,  M,  1 },
 *                {-1,  1,  1,  1,  1, -1,  1,  1,  1,  1 },               { M,  1,  1,  1,  1,  M,  1,  1,  1,  1 },
 *                { 0, -1,  1,  1, -1,  0, -1,  1,  1,  1 },               { 0,  M,  1,  1,  M,  0,  M,  1,  1,  1 },
 *                {-1,  1,  1, -1,  1, -1,  1,  1,  1,  1 },               { M,  1,  1,  M,  1,  M,  1,  1,  1,  1 },
 *                { 1,  1, -1,  0, -1,  1,  1,  1,  1,  1 }};              { 1,  1,  M,  0,  M,  1,  1,  1,  1,  1 }};
 *
 *
 *
 **/
static void mark_unsafe_cells(int m, int n, int matrix[m][n])
{
	for (int i=0; i<m; i++)
	{
		for (int j=0; j<n; j++)
		{
			if (matrix[i][j] == 0) // land mine found
			{
				for (int k=0; k<4; k++)
				{
					if (isValid(i+R[k], j+C[k], m, n))
						matrix[i+R[k]][j+C[k]] = -1;
				}
			}
		}
	}

	// now mark all adjacent cells
	for (int i=0; i<m; i++)
	{
		for (int j=0; j<n; j++)
		{
			if (matrix[i][j] == -1)
				matrix[i][j] = 0;
		}
	}

}


static bool_t isValid(int x, int y, int m, int n)
{
	if (x<0 || y<0 || x>=m || y>=n)
	{
		return false;
	}

	return true;
}

// ----------------------------------------------------
// longest path in the matrix with hurdles
// ----------------------------------------------------
typedef struct coordinates
{
	int x;
	int y;
}coordinates_d;

static bool_t findlongestPath(int m, int n, int matrix[m][n], int row, int col, int max_path, coordinates_d *cord, int visited[m][n], answer_d *ans, int value);


/**
 *
 *                   {{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },          {s R _ U R R R R R R }
 *                    { 1, 1, 0, 1, 1, 0, 1, 1, 0, 1 },          {L D _ L U _ U R _ D }
 *                    { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }};         {D R R R R _ L L L D }
 *
 *
 *                     -> ->    ^  -> -> -> -> -> ->
 *                     <- +|    <-  ^     ^ ->     |
 *                     |+ -> -> -> ->    <- <- <- +|
 *
 *
 *
 */

void BackTracking_LongestPath_in_matrix_with_hurldles(void)
{
	int m = 3;
	int n = 10;
	int matrix[3][10] = {{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	                	 { 1, 1, 0, 1, 1, 0, 1, 1, 0, 1 },
						 { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }};
	coordinates_d cord;
	cord.x = 1;
	cord.y = 7;


	/*int m=4;
	int n=4;
	int matrix[4][4] = {{ 1, 0, 0, 1 },
						{ 1, 0, 1, 1 },
						{ 1, 0, 1, 1 },
						{ 1, 1, 1, 0 }};

	coordinates_d cord;
	cord.x = 2;
	cord.y = 3;*/

	int *visited = malloc(sizeof(int) *m*n);
	memset(visited, 0, sizeof(int) *m*n);

	// answer-list
	answer_d *ans = malloc(sizeof(answer_d));
	ans->path = malloc(sizeof(int) *m*n);
	ans->max_min_path = 0;


	if (findlongestPath(m, n, matrix, 0, 0, 1, &cord, visited, ans, 's'))
	{
		// print the paths
		for (int i=0; i<m; i++)
		{
			printf("{");
			for(int j=0; j<n; j++)
			{
				//printf("%d ", *((ans->path+i)+j)); // failure: undefined behaviour.
				//printf("%d ", ans->path[i][j]);	 // compilation error
				printf("%c ", *(ans->path + i*n +j));
			}
			printf("}\n");

		}

		printf("max_path: %d\n", ans->max_min_path);
	}
	else
		printf("Destination not reachable from given source\n");
}


static bool_t findlongestPath(int m, int n, int matrix[m][n], int row, int col, int max_path, coordinates_d *cord, int visited[m][n], answer_d *ans, int value)
{
	bool_t found = false;

	// base condition
	if (row == cord->x && col == cord->y)
	{
		// add to the list
		if (max_path > ans->max_min_path)
		{
			visited[row][col] = value;	// set, as it was not before call

			ans->max_min_path = max_path;
			memset(ans->path, 0, sizeof(int)*m*n);	// clear prev path
			memcpy(ans->path, visited, sizeof(int)*m*n);

			visited[row][col] = 0;	// back-track

			return true;
		}
		return false;
	}

	// bound, already visited, valid or not
	if (row<0 || col<0 || row>=m || col>=n || (visited[row][col]!=0) || (matrix[row][col] == 0))
	{
		return false;
	}

	visited[row][col] = value;	// set
	found |= findlongestPath(m, n, matrix, row, col-1, max_path+matrix[row][col-1], cord, visited, ans, 'L');	// left
	found |= findlongestPath(m, n, matrix, row, col+1, max_path+matrix[row][col+1], cord, visited, ans, 'R');	// right
	found |= findlongestPath(m, n, matrix, row-1, col, max_path+matrix[row-1][col], cord, visited, ans, 'U');	// up
	found |= findlongestPath(m, n, matrix, row+1, col, max_path+matrix[row+1][col], cord, visited, ans, 'D');	// down
	visited[row][col] = 0;	// back-track

	return found;
}


// ----------------------------------------------------
// all permutations of the string
// ----------------------------------------------------
static void findPermutations(int n, char *string, int i, char *permutation);

void BackTracking_Permutation_Problem(void)
{
	char string[] = "ABSG";
	int n = strlen(string);
	char *permutation = malloc(n+1);
	memset(permutation, 0, n+1);

	findPermutations(n, string, 0, permutation);
}

static void findPermutations(int n, char *string, int i, char *permutation)
{
	// base consition
	if (i==n)
	{
		printf("%s\n", permutation);
		return;
	}

	for (int j=0; j<n; j++)
	{
		if (*(permutation+j) == '\0')
		{
			*(permutation+j) = *(string+i);
			findPermutations(n, string, i+1, permutation);
			*(permutation+j) = '\0'; //back-track
		}
	}
}

// ----------------------------------------------------
// Larger num in K-swaps problem
// ----------------------------------------------------
static int solve_large_num_in_k_swap(char *string, int n, int k);
static int swap(int n, char *string, int i, int j);
static int powerof(int base, int exp);

void BackTracking_LargerNum_In_K_Swap()
{
	char string[] = "1234567";
	char string2[] = "3435335";

	int n = strlen(string2); // without null-char
	int k  = 4;

	printf(":%d \n", solve_large_num_in_k_swap(string2, n, k));
	printf("string: %s\n", string2);
}


static int solve_large_num_in_k_swap(char *string, int n, int k)
{
	int large_num = 0;
	char *large_string = malloc(n+1);
	memset(large_string, 0, n+1);

	for (int i=0; i<n && k!=0; i++)
	{
		for (int j=i; j<n && k!=0; j++)
		{
			if (string[j] > string[i])
			{
				int num = swap(n, string, i, j);

				// now check for the largest num formed
				if (num > large_num)
				{
					large_num = num;
					memcpy(large_string, string, n);
				}

				swap(n, string, j, i); // back track
			}
		}

		memcpy(string, large_string, n);

		k--;
	}
	return large_num;
}

static int swap(int n, char *string, int i, int j)
{
	int num = 0;
	char temp = *(string+i);
	*(string+i) = *(string+j);
	*(string+j) = temp;

	// ascii to integer
	for (int a=0; a<n; a++)
	{
		int atoi = (*(string+a) - '0');
		num += (powerof(10, n-a-1))*(atoi);
	}

	return num;
}

static int powerof(int base, int exp)
{
	int result = 1;

	while (exp--)
	{
		result = result*base;
	}

	return result;
}


// ----------------------------------------------------
// Combination-Sum
// ----------------------------------------------------
static void BackTracking_solve_combination(int i, int N, int array[N], int target, Set_d *set, Dynamic_List_d *list);

static void BackTracking_solve_combination(int i, int N, int array[N], int target, Set_d *set, Dynamic_List_d *list)
{
	// base condition
	if (target == 0)
	{
		addSetToList(list, set);
		printSets(set);
		return;
	}

	// bounds:
	// Dont do bound check here or else you would end-up
	// doing more iteration which are waste
	/*if (target < 0)
		return;
	*/

	// all options
	for (int j=i; j<N; j++)
	{
		// bounds: do the bound check first and then call with the next element.
		if ((target-array[j]) < 0)
			break;

		addElementToSet(set, 0, array[j]);
		BackTracking_solve_combination(j, N, array, (target-array[j]), set, list);
		removeElementFromSet(set, 0, array[j]);
	}
}


void BackTracking_Combination_Sum(void)
{
	int array[] = {2,5,6,7};
	int N = sizeof(array)/sizeof(array[0]);
	int target = 16;

	Dynamic_List_d *list = createList();
	Set_d *set = createSet(1);

	BackTracking_solve_combination(0, N, array, target, set, list);

	//print the sets
	printf("\n"); // EOL
	printf("print from the list: \n");
	printSets(list->head);
}



// ----------------------------------------------------
// Knight-tour problem
// ----------------------------------------------------
#define OPTIMIZE_WITH_LOOP	ENABLED


#if OPTIMIZE_WITH_LOOP == ENABLED
bool_t BackTracking_solve_tour(int move, int N, int board[N][N], int i, int j, int xMove[N], int yMove[N]);
#else
bool_t BackTracking_solve_tour(int node, int N, int board[N][N], int i, int j);
#endif

void BackTraing_Knight_Tour_Problem(void)
{
	int board[8][8];
	memset(board, -1, sizeof(board));

#if OPTIMIZE_WITH_LOOP == ENABLED
	int xMove[8] = { 2, 1, -1, -2, -2, -1,  1,  2 };
	int yMove[8] = { 1, 2,  2,  1, -1, -2, -2, -1 };

	// this order will take forever
	//int xMove[8] = { -2, -2, -1,  1,  2, 2, 1, -1,};
	//int yMove[8] = {  1, -1, -2, -2, -1, 1, 2,  2,};
#endif

#if OPTIMIZE_WITH_LOOP == ENABLED
	if(!BackTracking_solve_tour(0, 8, board, 0, 0, xMove, yMove))
		printf("No, not possible to partition\n");
	printf("\n"); // EOL
#else
	if(!BackTracking_solve_tour(0, 8, board, 0, 0))
		printf("No, not possible to partition\n");
	printf("\n"); // EOL
#endif

}



#if OPTIMIZE_WITH_LOOP == ENABLED
bool_t BackTracking_solve_tour(int move, int N, int board[N][N], int i, int j, int xMove[N], int yMove[N])
#else
bool_t BackTracking_solve_tour(int move, int N, int board[N][N], int i, int j)
#endif
{
	// hceck bounds first
	if (i>=N || j>=N || i<0 || j<0 || board[i][j] != -1 )
	{
		return false;
	}

	// base case
	if (move == (N*N)-1) // reached end
	{
		//fill the move before printing as this current move is not been updated
		board[i][j] = move; // set

		// print board and return
		for (int a=0; a<N; a++)
		{
			printf("[");
			for (int b=0; b<N; b++)
			{
				printf("%2d ", board[a][b]);
			}
			printf("]\n");
		}

		board[i][j] = -1; // set
		return true;
	}

	// all possible options
#if OPTIMIZE_WITH_LOOP == ENABLED
	for (int k=0; k<N; k++)
	{
		board[i][j] = move; // set
		if (BackTracking_solve_tour(move+1, 8, board, i + xMove[k], j + yMove[k], xMove, yMove))
			return true;
		board[i][j] = -1; // back-track
	}
#else
	{
		board[i][j] = move; // set
		if (BackTracking_solve_tour(move+1, 8, board, i+2, j+1))
			return true; // skip cause, if dont cause it calls further.

		if (BackTracking_solve_tour(move+1, 8, board, i+1, j+2))
			return true;

		if(BackTracking_solve_tour(move+1, 8, board, i-1, j+2))
			return true;

		if(BackTracking_solve_tour(move+1, 8, board, i-2, j+1))
			return true;

		if (BackTracking_solve_tour(move+1, 8, board, i-2, j-1))
			return true;

		if (BackTracking_solve_tour(move+1, 8, board, i-1, j-2))
			return true;

		if (BackTracking_solve_tour(move+1, 8, board, i+1, j-2))
			return true;

		if (BackTracking_solve_tour(move+1, 8, board, i+2, j-1))
			return true;
		board[i][j] = -1; // back-track
	}
#endif

	return false;
}


// ----------------------------------------------------
// m-coloring : can color or not
// ----------------------------------------------------
bool_t BackTracking_solveGraph_coloring(int node, int N, int graph[N][N], int M, int color_array[]);
static bool_t isSafe(int node, int N, int graph[N][N], int M, int color_array[], int color);

void BackTraing_M_Coloring(void)
{
	bool_t retval = false;
	int graph_adjacency_matrix[4][4] = {{0, 1, 0, 1},
										{1, 0, 1, 0},
										{0, 1, 0, 1},
										{1, 0, 1, 0}};

	//int *color_array = malloc(4*sizeof(4));
	int color_array[4] = {0};

	retval = BackTracking_solveGraph_coloring(0, 4, graph_adjacency_matrix, 3, color_array);
	printf("Can color this graph: %d\n", retval);
}

bool_t BackTracking_solveGraph_coloring(int node, int N, int graph[N][N], int M, int color_array[])
{
	if (node == N) // reached end and filled all colors
	{
		// add one possible outcome in the ans-list
		return true;
	}
	// all possible options
	for (int option=1; option<= M; option++)
	{
		// try all safe options and if any in the chain-tree is not safe, before we reached M.
		// then revert the changes in color[] i.e back track and then check for next possible option.
		//
		// when all the options are done and N is not reacheed means can not color the this
		// graph with 'M' colors
		if (isSafe(node, N, graph, M, color_array, option))
		{
			color_array[node] = option;
			if (BackTracking_solveGraph_coloring(node+1, N, graph, M, color_array))
				return true; // return and no need to track further as the combination is found.

			color_array[node] = 0;
		}
	}

	return false;
}

static bool_t isSafe(int node, int N, int graph[N][N], int M, int color_array[], int color)
{
	//
	// find adjacent vertexes for the current node and check the color
	// set for that adjacent vertex is the same color passed to set for cur-node
	// 	----> if yes then False
	//		  else True;
	for (int i=0; i<N; i++)
	{
		if ((graph[node][i] == 1) && (color_array[i] == color))
			return false;
	}

	return true;
}



// ----------------------------------------------------
// Sudoku
// ----------------------------------------------------
static void solve_sudoku(int boardsize, int board[boardsize][boardsize], int i, int j);
bool_t isValidOption(int boardsize, int board[boardsize][boardsize], int i, int j, int options);

void BackTraing_solverSudoku(void)
{
	int board[9][9] = {{3, 0, 6, 5, 0, 8, 4, 0, 0},
						{5, 2, 0, 0, 0, 0, 0, 0, 0},
						{0, 8, 7, 0, 0, 0, 0, 3, 1},
						{0, 0, 3, 0, 1, 0, 0, 8, 0},
						{9, 0, 0, 8, 6, 3, 0, 0, 5},
						{0, 5, 0, 0, 9, 0, 6, 0, 0},
						{1, 3, 0, 0, 0, 0, 2, 5, 0},
						{0, 0, 0, 0, 0, 0, 0, 7, 4},
						{0, 0, 5, 2, 0, 6, 3, 0, 0}};


	solve_sudoku(9, board, 0, 0);

	// display board
	/*for (int i=0; i<9; i++)
	{
		printf("[");
		for (int j=0; j<9; j++)
		{
			printf("%d ", board[i][j]);
		}
		printf("],");
		printf("\n");
	}*/
}

static void solve_sudoku(int boardsize, int board[boardsize][boardsize], int i, int j)
{
	// base condition to return
	if (i == boardsize)
	{
		// display board
		for (int i=0; i<9; i++)
		{
			printf("[");
			for (int j=0; j<9; j++)
			{
				printf("%d ", board[i][j]);
			}
			printf("],");
			printf("\n");
		}
		return;
	}

	// update the rows_i and col_i
	int rows_i = i;
	int cols_i = j;

	// end of matrix
	if (j == boardsize)
	{
		rows_i = i+1;
		cols_i = 0;
	}

	// if non-zero, then skip
	//if ( *((board+rows_i) + cols_i) != 0)
	if (board[rows_i][cols_i] != 0)
	{
		solve_sudoku(boardsize, board, rows_i, cols_i+1);
	}
	else // else, find and try valid options
	{
		for (int options=1; options<=9; options++)
		{
			if (isValidOption(boardsize, board, rows_i, cols_i, options))
			{
				// put value
				//*((board+rows_i) + cols_i) = options;
				board[rows_i][cols_i] = options;
				solve_sudoku(boardsize, board, rows_i, cols_i+1); // call for next
				//*((board+rows_i) + cols_i) = 0; // back track
				board[rows_i][cols_i] = 0; // back track
			}
		}
	}
}


bool_t isValidOption(int boardsize, int board[boardsize][boardsize], int i, int j, int options)
{
	// look for rows
	for (int y=0; y<boardsize; y++)
	{
		//if (*((board+i) + y) == options)
		if (board[i][y] == options)
			return false;
	}

	// look for cols
	for (int x=0; x<boardsize; x++)
	{
		//if (*((board+x) + j) == options)
		if (board[x][j] == options)
			return false;
	}

	// look in a sub-matrix
	//
	// (3,5) = (3,3) is the start of sub-matrix
	//
	int sub_row = 3*(i/3);
	int sub_col = 3*(j/3);

	for (int a= sub_row; a<sub_row+3; a++)
		for (int b=sub_col; b<sub_col+3; b++)
		{
			//if (*((board+sub_row) + sub_col) == options)
			if (board[a][b] == options)
				return false;
		}

	return true;
}


// ----------------------------------------------------
// RAT in a Maze
// ----------------------------------------------------
//bool_t solveMaze(int **maze, int m, int row, int col, int **result, char *list);
bool_t solveMaze(int maze[5][5], int m, int row, int col, int result[5][5], char *list);
#define isSafe(maze, row, col)	maze[row][col]? true: false

void BackTracking_Rat_In_A_Maze(void)
{
	int maze[4][4] = {{1, 0 , 0, 0},
					  {1, 1 , 0, 1},
					  {1, 1 , 0, 0},
					  {0, 1 , 1, 1}};

	int result[4][4] = {{0, 0 , 0, 0},
					    {0, 0 , 0, 0},
					    {0, 0 , 0, 0},
					    {0, 0 , 0, 0}};

	int maze2[5][5] = {{1, 0, 1, 0, 1},
					   {1, 1, 1, 1, 1},
					   {0, 1, 0, 1, 0},
					   {1, 0, 0, 1, 1},
					   {1, 1, 1, 0, 1}};

	int result2[5][5] = {{0, 0 , 0, 0, 0},
						 {0, 0 , 0, 0, 0},
						 {0, 0 , 0, 0, 0},
						 {0, 0 , 0, 0, 0},
						 {0, 0 , 0, 0, 0}};

	int m = 5;
	char *list = malloc(m*m);

	solveMaze((int **)maze2, m, 0, 0, (int **)result2, list);

	// print output
	for (int i=0; i<m; i++)
	{
		printf("{");
		for (int j=0; j<m; j++)
			printf("%d ", result2[i][j]);
		printf("}\n");
	}
}


bool_t solveMaze(int maze[5][5], int m, int row, int col, int result[5][5], char *list)
{
	// base conditions
	if (row == m-1 && col == m-1)
	{
		// set the last result
		result[row][col] = 1;

		// add path to the list
		return true;
	}

	// boundary conditions
	if (row >= m || col >= m)
		return false;


	if (!isSafe(maze, row, col))
		return false;

	// set the result
	result[row][col] = 1;

	// down traverse
	bool_t d = solveMaze(maze, m, row+1, col, result, list);

	// down traverse
	bool_t r = solveMaze(maze, m, row, col+1, result, list);

	if (!d && !r)
	{
		// set the result
		result[row][col] = 0;
		return false;
	}

	return true;
}



// ----------------------------------------------------
// RAT in a Maze
// ----------------------------------------------------



// ----------------------------------------------------
// word break problem 2
// ----------------------------------------------------
// Dynamic string node
typedef struct string_node {
	char *string;
	int string_size;
	struct string_node *next;
} string_node_d;

// dynamic mem header
typedef struct dynamic_list {
	string_node_d *head;
	string_node_d *tail;
	int size;
} dynamic_list_d;


void dynamic_list_add(dynamic_list_d *list, char *string, int size)
{
	// create node
	string_node_d *node = malloc(sizeof(string_node_d));
	node->next = NULL;
	node->string = malloc(size);
	node->string_size = size;
	strncpy(node->string, string, size);

	// add to list
	if (list->head == NULL && list->size == 0) // first entry
	{
		list->head = list->tail = node;
	}
	else
	{
		list->tail->next = node;
		list->tail = node;
	}
	list->size++;
}

void dynamic_list_remove(dynamic_list_d *list, char *string, int size)
{
	string_node_d *current = list->head;
	string_node_d *prev = NULL;
	while (current)
	{
		if ((size == current->string_size) && (!memcmp(current->string, string, size)))
		{
			// remove node
			if (current == list->head && current == list->tail) //single node
			{
				list->head = list->tail = NULL;
			}
			else if (current == list->head)	// head to delete
			{
				list->head = current->next;
			}
			else if (current == list->tail)	// tail to delete
			{
				prev->next = current->next;
				list->tail = prev;
			}
			else	// anywhere in the middle
				prev->next = current->next;

			free(current);
			list->size--;
			break;
		}

		prev = current;
		current = current->next;
	}
}



static bool_t solve2(char **dict, int dSize, char *toSearch, int patt_size, dynamic_list_d *list, dynamic_list_d *statement);

void  BackTracking_wordbreakpart2(void)
{
	char *toSearch = "catsanddog";
	char *dictionary[5] = {"cats", "cat", "and", "sand", "dog"};

	// create dynamic list
	dynamic_list_d *list = malloc(sizeof(dynamic_list_d));
	list->head = list->tail = NULL;
	list->size = 0;

	// create dynamic list for final statement
	dynamic_list_d *statement = malloc(sizeof(dynamic_list_d));
	statement->head = statement->tail = NULL;
	statement->size = 0;

	solve2(dictionary, 5, toSearch, strlen(toSearch), list, statement);

	// free list
	free(list);

	// print the list
	string_node_d *current = statement->head;

	if (current == NULL)
	{
		printf("not found\n");
	}
	else
	{
		while (current)
		{
			if (current->string_size == 2 && !strcmp(current->string, " "))
			{
				printf("\n");
			}
			else
				printf("%s ", current->string);

			current = current->next;
		}
	}
}

static bool_t solve2(char **dict, int dSize, char *toSearch, int patt_size, dynamic_list_d *list, dynamic_list_d *statement)
{
	// base case
	if (patt_size == 0)
	{
		// all broken words founds, add to final statementlist
		string_node_d *current = list->head;
		while (current)
		{
			dynamic_list_add(statement, current->string, current->string_size);
			current = current->next;
		}

		// add delimeter to statement list
		dynamic_list_add(statement, " ", 2);

		return false;
	}

	for (int i=0; i<patt_size; i++)
	{
		bool_t found = false;
		char *pattern = malloc(i+1+1);
		memcpy(pattern, toSearch, i+1); // + null char

		// search in dictionary
		for (int j=0; j<dSize; j++)
		{
			int dict_entry_len = strlen(dict[j]);
			//if ((i+1 == dict_entry_len) && (!memcmp(dict[j], toSearch, i+1)))
			if ((i+1 == dict_entry_len) && (!memcmp(dict[j], pattern, i+1)))
			{
				found = true;
				// add to the list
				dynamic_list_add(list, pattern, i+1+1);
				break;
			}
		}

		// if found then find the remainings
		if (found && (!solve2(dict, dSize, (toSearch+i+1), patt_size-(i+1), list, statement)))
		{
			//back track
			dynamic_list_remove(list, pattern, i+1+1);
		}
	}

	return false;
}


// ----------------------------------------------------
// C-Problems
// ----------------------------------------------------
int main2()
{
    char arr[5][7][6];
    char (*p)[5][7][6] = &arr;

    /* Hint: &arr - is of type const pointer to an array of
       5 two dimensional arrays of size [7][6] */


	int maze[4][4] = {{1, 0 , 0, 0},
					  {1, 1 , 0, 1},
					  {1, 1 , 0, 0},
					  {0, 1 , 1, 1}};

	int (*ptr)[4][4] = &maze;



    printf("%p\n", &maze);
    printf("%p\n", (&maze + 1));
    printf("%p\n", (int *)(&maze + 1));
    printf("%p\n", (int *)&maze);

    printf("%p\n", (unsigned)(maze + 1));
    printf("%p\n", (unsigned)maze);
    printf("%p\n", (unsigned)(ptr + 1));
    printf("%p\n", (unsigned)ptr);


    printf("%d\n", (&maze + 1) - &maze);
    printf("%d\n", (int *)(&maze + 1) - (int *)&maze);
    printf("%d\n", (unsigned)(maze + 1) - (unsigned)maze);
    printf("%d\n", (unsigned)(ptr + 1) - (unsigned)ptr);

    return 0;
}
