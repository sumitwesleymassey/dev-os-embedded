/*
 * Arrays.h
 *
 *  Created on: 04-May-2022
 *      Author: sumit
 */

#ifndef INCLUDE_ARRAYS_H_
#define INCLUDE_ARRAYS_H_

void dynamic_array_single_ptr(void);
void dynamic_array_double_ptr(void);



#endif /* INCLUDE_ARRAYS_H_ */
