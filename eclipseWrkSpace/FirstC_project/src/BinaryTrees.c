/*
 * BinaryTrees.c
 *
 *  Created on: 24-Mar-2022
 *      Author: sumit
 */

//-------------------------
// Header file incusions.
#include <stdio.h>
#include <stdlib.h>
#include "BinaryTrees.h"
#include "BinaryTrees_session_2.h"
#include "Util.h"


//-------------------------
// prototype declaration
TreeNode_d *BinaryTrees_CreateTree(int nodes[], int size);
TreeNode_d *insertNode(TreeNode_d *root, TreeNode_d *new);
void BinaryTrees_TraverseAndPrint(TreeNode_d *root, e_mode_d mode);
ebool_d isMirror(TreeNode_d *r1, TreeNode_d* r2);
TreeNode_d *convertToMirror(TreeNode_d *root);
void Program_toConstructTreeFromOrdersGiven();
TreeNode_d *constructTree(int *pInorder, int start, int end, int *pPostorder, int size);

void Program_toConstructTreeFromPostAndPre(void);
TreeNode_d *constructTreeFromPostAndPreorder(int *pPreorder, int *pPostorder, int size);
TreeNode_d * BinaryTrees_TraverseAndSearchNode(TreeNode_d *root, e_mode_d mode, int node);


TreeNode_d *BinaryTrees_Predecessor(TreeNode_d *root, int pOf);
TreeNode_d *BinaryTrees_Successor(TreeNode_d *root, int sOf);

static TreeNode_d *BinaryTrees_TraverseTillExtremeLeaf(TreeNode_d *node, bool_t toRight);

#if (FTR_NORMAL_SEARCH == ENABLED)
	TreeNode_d *BinaryTree_SearchNode(TreeNode_d *root, int node);
#else
	TreeNode_d *BinaryTree_SearchNode(TreeNode_d *root, int node, TreeNode_d **lastR, TreeNode_d **lastL);
#endif // FTR_NORMAL_SEARCH


void BinaryTrees_BFS(TreeNode_d *root, int size);
int BinaryTrees_findHeight(TreeNode_d *root, int depth);

void displayQContent(Queue_d *Q, int qSize);

//-------------------------
// Global Variabes




//-------------------------
// Function defitions

/**
 *                 	     5
 *                 	   /   \
 *                    /     \
 *                   /    	 \
 *                  / 		  \
 *    l-most--->   1		   9
 *                  \ 	  	   /
 *                   2  	  7
 *                    \	     / \
 *                     3	6	8 <----r-most
 *                 	    \
 *                       4
 *
 *
 */
void BinaryTrees_Program(void)
{
	// un-sorted arrays
	// node inserted    L                       R
	//					------------------------->
	int nodesArray[] = {5, 1, 9, 2, 7, 3, 6, 4, 8};

	// sorted arrays
	// node inserted    L                                   R
	//					------------------------------------->
	int nodesArray1[] = {1, 3, 5, 10, 11, 13, 37, 54, 99, 101};

	TreeNode_d *root = BinaryTrees_CreateTree(nodesArray, (sizeof(nodesArray)/sizeof(4)) );
	BinaryTrees_TraverseAndPrint(root, MODE_IN_ORDER);
	printf("\n");

	// find predecessor and successor of a node.
	int node = nodesArray[2];
	TreeNode_d *p = BinaryTrees_Predecessor(root, node);
	TreeNode_d *s = BinaryTrees_Successor(root, node);
	printf("pOf(%d) = %d\n", node, (p!=NULL)? p->value :p);
	printf("sOf(%d) = %d\n", node, (s!=NULL)? s->value :s);

	printf("height = %d \n", BinaryTrees_findHeight(root, 0));

	BinaryTrees_BFS(root, sizeof(nodesArray));	// size = (no_of_elements * pointer_size)

	TreeNode_d *r1 = BinaryTrees_CreateTree(nodesArray, (sizeof(nodesArray)/sizeof(4)) );
	TreeNode_d *r2 = BinaryTrees_CreateTree(nodesArray, (sizeof(nodesArray)/sizeof(4)) );
	printf("is mirror : %s\n", isMirror(r1, r2)? "true" : "false");

	printf("converted to mirror\n");
	//BinaryTrees_TraverseAndPrint(convertToMirror(root), MODE_IN_ORDER);

	convertToMirror2(root);
	BinaryTrees_TraverseAndPrint(root, MODE_IN_ORDER);
	printf("\n"); // EOL

	printf("Construct Tree\n");
	// construct trees
	Program_toConstructTreeFromOrdersGiven();


	// construct trees
	printf("Construct Tree from post and pre\n");
	Program_toConstructTreeFromPostAndPre();
}


/**
 *
 */
void Program_toConstructTreeFromPostAndPre(void)
{
	int preorder[] = {8, 5, 9, 7, 1, 12, 2, 4, 11, 3}; //8, 5, 9, 7, 1, 12, 2, 4, 11, 3
	int postorder[] = {9, 1, 2, 12, 7, 5, 3, 11, 4, 8};

	TreeNode_d *root = constructTreeFromPostAndPreorder(preorder, postorder, (sizeof(postorder)/sizeof(postorder[0])));
	printf("in-order   :");
	BinaryTrees_TraverseAndPrint(root, MODE_IN_ORDER);
	printf("\n"); // EOL

	printf("pre-order  :");
	BinaryTrees_TraverseAndPrint(root, MODE_PRE_ORDER);
	printf("\n"); // EOL

	printf("post-order :");
	BinaryTrees_TraverseAndPrint(root, MODE_POST_ORDER);
	printf("\n"); // EOL
}


TreeNode_d *constructTreeFromPostAndPreorder(int *pPreorder, int *pPostorder, int size)
{
	int start = 0;
	int i = 0;
	int j = 0;
	TreeNode_d *root;

	for ( ;i<size; i++)	// pre loop
	{
		// allocate the node
		TreeNode_d *temp = malloc(sizeof(TreeNode_d));
		temp->value = pPreorder[i];
		temp->left = NULL;
		temp->right = NULL;


		// to dinf i and j
		for ( ; j<size; j++) // post loop
		{
			if (pPreorder[i] == pPostorder[j])
				break;
		}


		// if found in post order
		if (j+1<size)
		{
			// j part of y means j is part of pPostorder[j+1]
			// so get y or pPostorder[j+1] node from the root.
			//
			// y->left != NULL
			//	 y->right = j
			// else
			//	 x->left = y;
			for (int x = j+1; x<size; x++)
			{
				// find the 'y' or "pPostorder[j+1]" node in the tree
				TreeNode_d *found_node = BinaryTrees_TraverseAndSearchNode(root, MODE_PRE_ORDER, pPostorder[x]);
				if (found_node == NULL)
					continue;

				if (found_node->left == NULL)
				{
					found_node->left = temp;
					break;
				}
				else if (found_node->right == NULL)
				{
					found_node->right = temp;
					break;
				}
			}
		}
		else // root condition
		{
			root = temp;
		}

		// reset j
		j = start;
	}

	return root;
}



/**
 *
 */
void Program_toConstructTreeFromOrdersGiven()
{
	int inorder[] = {9, 5, 1, 7, 2, 12, 8, 4, 3, 11};
	int postorder[] = {9, 1, 2, 12, 7, 5, 3, 11, 4, 8};

	TreeNode_d *root = constructTree(inorder, 0, (sizeof(inorder)/sizeof(inorder[0]))-1, postorder, (sizeof(postorder)/sizeof(postorder[0])));
	BinaryTrees_TraverseAndPrint(root, MODE_IN_ORDER);
	printf("\n"); // EOL
	BinaryTrees_TraverseAndPrint(root, MODE_POST_ORDER);
	printf("\n"); // EOL
	BinaryTrees_TraverseAndPrint(root, MODE_PRE_ORDER);
	printf("\n"); // EOL
}


/**
 *
 *                 	      8
 *                 	   /    \
 *                   / 	      \
 *   			   5		   4
 *               /  \ 	  	    \
 *              9    7  	    11
 *                  / \	        /
 *                 1   12	   3
 *                 	   /
 *                    2
 *
 *	inorder	9, 5, 1, 7, 2, 12, 8, 4, 3, 11
 *  post 	9, 1, 2, 12, 7, 5, 3, 11, 4, 8
 *  pre		8, 5, 9, 7, 1, 12, 2, 4, 11, 3
 *
 */
TreeNode_d *constructTree(int *pInorder, int start, int end, int *pPostorder, int size)
{
	TreeNode_d *root;
	ebool_d iffound = false;

	if (start > end) 	// if out of bounds
	{
		return NULL;
	}

	// find root node
	int i = size-1;
	int j = start;

	// Important:
	//	start = 0 and  end = 6 means these are indexes from 0-6 i.e. of size 7
	//	  i>=0	and j<end+1
	//
	// 		include boundaries as start and end are indexes
	//
	for (;i>=0 && start!=end ; i--) //post loop
	{
		for ( ;j<end+1; j++) // pre loop
		{
			if (*(pPostorder+i) == *(pInorder+j))
			{
				// found root
				iffound = true;
				break;
			}
		}

		if (true == iffound)
			break;
		j = start;
	}

	// allocate root
	root = malloc(sizeof(TreeNode_d));
	root->value = pInorder[j];

	// traverse for subtrees
	root->left = constructTree(pInorder, start, j-1, pPostorder, size);
	root->right = constructTree(pInorder, j+1, end, pPostorder, size);

	return root;
}


/**
 *  used the traversal flow of post-order type
 *
 *   	1) go Left
 *   	2) go Right
 *   	3) and then swap the childs for a root node
 *
 *
 *
 */
void convertToMirror2(TreeNode_d *root)
{
	if (root == NULL)
		return;

	convertToMirror2(root->left);
	convertToMirror2(root->right);

	// swap the nodes
	TreeNode_d *tempnode = root->left;
	root->left = root->right;
	root->right = tempnode;
	return;
}


/**
 *
 */
TreeNode_d *convertToMirror(TreeNode_d *root)
{
	TreeNode_d *localright;
	TreeNode_d *localleft;

	// r1 && r2 is NULL
	if (root->left == NULL && root->right == NULL)
		return root;

	if(root->left != NULL)
		localright = convertToMirror(root->left);
	else
		localright = NULL;

	if (root->right != NULL)
		localleft = convertToMirror(root->right);
	else
		localleft = NULL;

	// update the childs as mirrors
	root->left = localleft;
	root->right = localright;

	return root;
}


/*
 * 	Binary Mirrors:
 *
 *                   	 a             |/           	a
 *                 	   /   \           |/         	  /   \
 *                    /     \          |/            /     \
 *                   /    	 \         |/           /       \
 *                  / 		  \        |/          / 		 \
 *      		   b		   c       |/   	  c		      b
 *               /  \ 	  	 /  \      |/       /  \ 	  	 /  \
 *              d    e  	f    g     |/      g    f  	    e    d
 *                                     |/
 *
 *   left child is the right child and
 *   right child becomes the left child
 *
 */
ebool_d isMirror(TreeNode_d *r1, TreeNode_d* r2)
{
	// r1 && r2 is NULL
	if (r1 == NULL && r2 == NULL)
		return true;

	if ((r1 == NULL && r2 != NULL) ||
	    (r1 != NULL && r2 == NULL))
		return false;

	if (r1->value == r2->value)
	{
		return (isMirror(r1->left, r2->right) && isMirror(r1->right, r2->left));
	}
	else
		return false;
}




/**
 * DFS of BT:
 *
 *          1
 *        /  \
 *       2    3
 *     /   \
 *    4    5
 *
 *
 * the Depth First Traversals of this Tree will be:
 * (a) Inorder (Left, Root, Right) : 4 2 5 1 3
 * (b) Preorder (Root, Left, Right) : 1 2 4 5 3
 * (c) Postorder (Left, Right, Root) : 4 5 2 3 1
 */
void BinaryTrees_DFS(TreeNode_d *root, e_mode_d mode)
{
	BinaryTrees_TraverseAndPrint(root, mode);
}


/**
 * BFS of BT:
 *
 *          1
 *        /  \
 *       2    3
 *     /   \
 *    4    5
 *
 *
 * the BFS requires a Queue:
 *
 *		+-------------------------------+
 *		| 1 | 2 | 3 | 4 | 5| .....    <---- filling
 *		+--------------------------------+
 *    FIFO
 *
 *    a) if first node, en-queue(root)
 *    b) while (queue is not null)
 *          p = dequeue;
 *          printf(p)
 *
 *          if p->left is not nulll
 *          	enqueue(left)
 *          if p->right is not null
 *          	en-queue(right)
 *    c) exit
 *
 *
 */
void BinaryTrees_BFS(TreeNode_d *root, int size)
{
	TreeNode_d *p = NULL;

	// create Q
	Queue_d *Q = Util_createQ(size);

	if (root!=NULL)
		Util_enqueue(Q, root);

	// while q is not empty
	while ((p = (TreeNode_d *)Util_dequeue(Q)) && (p!=NULL))
	{
		printf("%d ", p->value);

		if(p->left != NULL)
		{
			Util_enqueue(Q, p->left);
		}
		if(p->right != NULL)
		{
			Util_enqueue(Q, p->right);
		}
	}

	// delete Q
	Util_destroyQ(Q);
}



// ----------------------------------------------------
// Queues
// ----------------------------------------------------
void displayQContent(Queue_d *Q, int qSize)
{
	int i = Q->front;

	printf("Q contains: ");
	while(i!=Q->end)
	{
		TreeNode_d *p = (TreeNode_d *)(*(((int *)Q->ptr)+i));
		printf("%d ", p->value);
		i = ((i+1)%qSize); // i++
	}
	printf("\n");
}


// ----------------------------------------------------
// Search a node and thier last right or left
// ----------------------------------------------------
TreeNode_d *BinaryTree_SearchNode(TreeNode_d *root, int node, TreeNode_d **lastR, TreeNode_d **lastL)
{
	if(root->value == node)
		return root;

	if(node > root->value)
	{
		if (lastR != NULL) // when i need last right
			*lastR = root;

		return BinaryTree_SearchNode(root->right, node, lastR, lastL);
	}
	else
	{
		if (lastL != NULL)	// when i need last left
			*lastL = root;
		return BinaryTree_SearchNode(root->left, node, lastR, lastL);
	}
}


// ----------------------------------------------------
// Predecessor or successor
// ----------------------------------------------------
/**
 *				    n
 *				 /      \
 *              /        \
 *			   /		  \
 *			l-subtree     r-subtree
 *			    .             .
 *			     .           .
 *			      .         .
 *			      n-1     n+1
 *
 *
 *
 *	so, Pof(n) is right-most of l-subtree i.e. n-1
 *	    Sof(n) is left-most of r-subtree i.e. n+1
 *
 *
 */
TreeNode_d *BinaryTrees_Predecessor(TreeNode_d *root, int pOf)
{
	TreeNode_d *lastRight = NULL;
	TreeNode_d *node = BinaryTree_SearchNode(root, pOf, &lastRight, NULL);
	if (node->left != NULL)
		return BinaryTrees_TraverseTillExtremeLeaf(node->left, 1); // go left, then extreme right
	else
		return lastRight;
}


TreeNode_d *BinaryTrees_Successor(TreeNode_d *root, int sOf)
{
	TreeNode_d *lastLeft = NULL;
	TreeNode_d *node = BinaryTree_SearchNode(root, sOf, NULL, &lastLeft);
	if (node->right != NULL)
		return BinaryTrees_TraverseTillExtremeLeaf(node->right, 0);	// go right, then extreme left
	else
		return lastLeft;
}


/**
 *
 */
TreeNode_d *BinaryTrees_TraverseTillExtremeLeaf(TreeNode_d *node, bool_t toRight)
{
	if(toRight)
	{
		if(node->right == NULL)
			return node;
		else
			return BinaryTrees_TraverseTillExtremeLeaf(node->right, toRight);
	} else
	{
		if(node->left == NULL)
			return node;
		else
			return BinaryTrees_TraverseTillExtremeLeaf(node->left, toRight);
	}
}


/**
 * Height of a tree
 */
int BinaryTrees_findHeight(TreeNode_d *root, int depth)
{
	if (root == NULL)
		return depth;

	// inc depth
	depth++;

	int depth_l = BinaryTrees_findHeight(root->left, depth);
	int depth_r = BinaryTrees_findHeight(root->right, depth);

	if(depth_l > depth_r)
	{
		return depth_l;
	}
	else
	{
		return depth_r;
	}

	return depth_r;
}



// ----------------------------------------------------
//Create Trees and Print
// ----------------------------------------------------
/**
 *
 *
 */
TreeNode_d *BinaryTrees_CreateTree(int nodes[], int size)
{
	TreeNode_d *root = NULL;
	for (int i=0; i<size; i++)
	{
		TreeNode_d *new = (TreeNode_d *)malloc(sizeof(TreeNode_d));
		new->value = nodes[i];
		new->left = NULL;
		new->right = NULL;

		root = insertNode(root, new);
	}

	return root;
}


TreeNode_d *insertNode(TreeNode_d *root, TreeNode_d *new)
{
	if (root == NULL)
	{
		return new;
	}

	if(new->value > root->value)	// right side
	{
		root->right = insertNode(root->right, new);
	}
	else	// left side
	{
		root->left = insertNode(root->left, new);
	}

	return root;
}

/**
 *
 */
void BinaryTrees_TraverseAndPrint(TreeNode_d *root, e_mode_d mode)
{
	if(root== NULL)
	{
		return;
	}

	// pre-order  	: Rlr
	// post-order 	: lrR
	// inorder		: lRr 
	//
	if(mode == MODE_PRE_ORDER)
		printf("%d ", root->value);

	BinaryTrees_TraverseAndPrint(root->left, mode);

	if(mode == MODE_IN_ORDER)
		printf("%d ", root->value);

	BinaryTrees_TraverseAndPrint(root->right, mode);

	if(mode == MODE_POST_ORDER)
		printf("%d ", root->value);
}


/**
 * 1) this is Depth first search (L-root-R) inorder
 *
 * which is different from the BST
 *
 * 2) BST is having the logic of left < root while right > root
 * 	  where BT doesnt have this rule.
 *
 */
TreeNode_d *BinaryTrees_TraverseAndSearchNode(TreeNode_d *root, e_mode_d mode, int node)
{
	TreeNode_d *L_result = NULL;
	TreeNode_d *R_result = NULL;

	if(root== NULL)
	{
		return NULL;
	}

	// pre-order  	: Rlr
	// post-order 	: lrR
	// inorder		: lRr
	//
	if(mode == MODE_PRE_ORDER)
	{
		if (root->value == node)
			return root;
	}

	// if left tree gives NULL then search right tree.
	L_result = BinaryTrees_TraverseAndSearchNode(root->left, mode, node);
	if (L_result != NULL)
		return L_result;

	if(mode == MODE_IN_ORDER)
	{
		if (root->value == node)
			return root;
	}

	// whatever right subtree returns, we return it to the function.
	// let it be NULL or non-NULL value just return
	R_result = BinaryTrees_TraverseAndSearchNode(root->right, mode, node);

	if(mode == MODE_POST_ORDER)
	{
		if (root->value == node)
			return root;
	}

	return R_result;
}
