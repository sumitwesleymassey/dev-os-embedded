/*
 * Graphs_ShortestPath.c
 *
 *  Created on: 15-May-2022
 *      Author: sumit
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "Graphs_ShortestPath.h"
#include "Util.h"


void Graphs_Dijkstra_1(void);
void Graphs_ShortestPath_bellman_ford(void);
void  Graphs_ShortestPath_floyd_warshal(void);


typedef struct Pair
{
	int val;
	int weight;
	char *path;
	int path_size;
	struct Pair *next;
}Pair_d;

typedef struct List
{
	Pair_d *head;
	Pair_d *tail;
	int size;
}List_d;


void Graphs_Dijkstra_PriorityList(int n, int graph[n][n], int visited[n]);
Pair_d *createNewPair(Pair_d *vertex, int neighbor, int n, int graph[n][n]);
void addPairToList(List_d *list, Pair_d *pair);
Pair_d *remFromList(List_d *list);

static void displayGraph(int n, int graph[n][n]);


// ----------------------------------------------------
// Main-driver function
// ----------------------------------------------------
void Graphs_ShortestPath_Program(void)
{
	printf("dijkstra-alog:\n");
	Graphs_Dijkstra_1();

	printf("bellman-ford-alog:\n");
	Graphs_ShortestPath_bellman_ford();

	printf("floyd-warshal:\n");
	Graphs_ShortestPath_floyd_warshal();
}


// ----------------------------------------------------
// Floy-Warshal algo (All-pair shortest path)
// ----------------------------------------------------
#define	INFINITY	0x7FFFFFFF

static void solveFloydWarshall(int src, int n, int in[n][n]);
static void solveFloydWarshall_optimised(int src, int n, int in[n][n]);

void  Graphs_ShortestPath_floyd_warshal(void)
{
#define N 4

	int input[N][N] = {{0, 9, -4, INFINITY},
					   {6, 0, INFINITY, 2},
					   {INFINITY, 5, 0, INFINITY},
					   {INFINITY, INFINITY, 1, 0}};

	//int *out = memset(malloc(sizeof(int)*N), 0, sizeof(int)*N);
	//int out[4][4];
	//memset(out, 0, sizeof(int)*4*4);

	//solveFloydWarshall(0, N, input);

	solveFloydWarshall_optimised(0, N, input);
	displayGraph(N, input);

	int input2[5][5] = {{0, 5, INFINITY, 9, 1},
					   {5, 0, 2, INFINITY, INFINITY},
					   {INFINITY, 2, 0, 7, INFINITY},
					   {9, INFINITY, 7, 0, 2},
					   {1, INFINITY, INFINITY, 2, 0}};
	solveFloydWarshall_optimised(0, 5, input2);
	printf("floydwarshall graph-2\n");
	displayGraph(5, input2);
}


/**
 * 1) dist[i][j] > (dist[i][src] + dist[src][j])
 * 			dist[i][j] = dist[i][src] + dist[src]+[j]
 *
 * 2) update the same input and pass it to recursion
 * 3) skip src row and col elements.
 * 4) check same vertex for -ve length.
 *
 */
static void solveFloydWarshall_optimised(int src, int n, int in[n][n])
{
	// base
	if (src==n)
		return;

	for (int i=0; i<n; i++)
	{
		for (int j=0; j<n; j++)
		{
			// 1) skip same vertex
			// 2) skip src row or col
			//		or
			// 1) check for the same vertex and it shouldn't be -ve
			//		i.e. -ve length check
			if (i!=j && (i!=src || j!=src))
			{
				int min = 0;

				if (in[i][src] == INFINITY || in[src][j] == INFINITY)
					//min = INFINITY;
					continue;
				else
					min = in[i][src] + in[src][j];

				// update input
				if (min < in[i][j])
					in[i][j] = min;
			}
		}
	}

	solveFloydWarshall_optimised(src+1, n, in);
}


static void solveFloydWarshall(int src, int n, int in[n][n])
{
	//base
	if (src == n)
		return;

	// intermediate out
	/*int intermediate_out[4][4];
	memset(intermediate_out, 0, sizeof(int)*4*4);

	// copy-as-it-is for "src" as col and row
	for (int j=0; j<n; j++)
	{
		//*((intermediate_out+src)+j) = in[src][j];	// same row
		//*((intermediate_out+j)+src) = in[j][src]; // same col

		intermediate_out[j][src] = in[j][src]; // same col
		intermediate_out[src][j] = in[src][j]; // same row
	}*/

	// update the distances
	for (int i=0; i<n; i++)
	{
		for (int j=0; j<n; j++)
		{
			if (i!=j && i!=src && j!=src) // skip same vertex
			{
				int min = 0;

				if (in[i][src] == INFINITY || INFINITY == in[src][j])
					min = INFINITY;
				else
					min = in[i][src]+in[src][j];

				// update out
				if (min < in[i][j])
				{
					//intermediate_out[i][j] = min;
					//*((intermediate_out+i)+j) = min;
					in[i][j]=min;
				}
				else
				{
					//intermediate_out[i][j] = in[i][j];
					//*((intermediate_out+i)+j) = in[i][j];
				}
			}
		}
	}

	/*for(int i=0; i<n; i++)
	{
		memcpy(&in[0][i], (intermediate_out+i), n);
		free(intermediate_out+i);
	}

	free(intermediate_out);*/
	//memcpy(in, intermediate_out, sizeof(int)*n*n);

	solveFloydWarshall(src+1, n, in);
}

// ----------------------------------------------------
// bellman-ford algo
// ----------------------------------------------------
struct Edge
{
	int src;
	int dst;
	int weight;
};

struct Graph
{
	int V;
	int E;
	struct Edge *edges;
};

struct Graph *createGraphWithEdgeArry(int V, int E);

void Graphs_ShortestPath_bellman_ford(void)
{
	//create graph
	int V=5, E=8;
	struct Graph *graph = createGraphWithEdgeArry(V, E);

	// distance array
	int *distance = memset(malloc(sizeof(int)*V), 0, sizeof(int)*V);
	int *parent = memset(malloc(sizeof(int)*V), 0, sizeof(int)*V);

	for (int i=0; i<V; i++)
	{
		distance[i] = 0x7FFFFFFF;
	}

	distance[0] = 0;
	parent[0] = -1;

	bool_t found = false;

	for (int i=1; i<V-1; i++)
	{
		found = false;

		for (int j=0; j<E; j++)
		{
			int u =  graph->edges[j].src;
			int v =  graph->edges[j].dst;
			int Wuv = graph->edges[j].weight;

			if(distance[u] + Wuv < distance[v])
			{
				distance[v] = distance[u] + Wuv; // the better path and distance
				parent[v] = u;
				found = true; // changes in finding the short-path
			}
		}

		if (found == false)
			break; // done
	}

	// one more loop for -ve cycle
	for (int j=0; j<E; j++)
	{
		int u =  graph->edges[j].src;
		int v =  graph->edges[j].dst;
		int Wuv = graph->edges[j].weight;

		if(distance[u] + Wuv < distance[v])
		{
			printf("-ve cycle is found\n");
			return;
		}
	}

	// print the spg
	for (int i=0; i<V; i++)
	{
		printf("%d->%d @cost of %d\n", parent[i], i, distance[i]);
	}

	free(graph->edges);
	free(graph);
	free(distance);
	free(parent);
}

struct Graph *createGraphWithEdgeArry(int V, int E)
{
	struct Graph *graph = memset(malloc(sizeof(struct Graph)), 0, sizeof(struct Graph));
	graph->V = V;
	graph->E = E;
	graph->edges = memset(malloc(sizeof(struct Edge)* graph->E), 0, sizeof(struct Edge *)* graph->E);

	// 0->1
	graph->edges[0].src = 0;
	graph->edges[0].dst = 1;
	graph->edges[0].weight = -1;

    // add edge 0-2 (or A-C in above figure)
    graph->edges[1].src = 0;
    graph->edges[1].dst = 2;
    graph->edges[1].weight = 4;

    // add edge 1-2 (or B-C in above figure)
    graph->edges[2].src = 1;
    graph->edges[2].dst = 2;
    graph->edges[2].weight = 3;

    // add edge 1-3 (or B-D in above figure)
    graph->edges[3].src = 1;
    graph->edges[3].dst = 3;
    graph->edges[3].weight = 2;

    // add edge 1-4 (or B-E in above figure)
    graph->edges[4].src = 1;
    graph->edges[4].dst = 4;
    graph->edges[4].weight = 2;

    // add edge 3-2 (or D-C in above figure)
    graph->edges[5].src = 3;
    graph->edges[5].dst = 2;
    graph->edges[5].weight = 5;

    // add edge 3-1 (or D-B in above figure)
    graph->edges[6].src = 3;
    graph->edges[6].dst = 1;
    graph->edges[6].weight = 1;

    // add edge 4-3 (or E-D in above figure)
    graph->edges[7].src = 4;
    graph->edges[7].dst = 3;
    graph->edges[7].weight = -3;

	return graph;
}


// ----------------------------------------------------
// Dijkstra algo
// ----------------------------------------------------
void Graphs_Dijkstra_1(void)
{
	// create-undirected-graph
	int n=7;
    int graph[7][7] = { { 0, 10,  0, 40, 0, 0, 0},
    					{10,  0, 10,  0, 0, 0, 0},
						{ 0, 10,  0, 10, 0, 0, 0},
						{40,  0, 10,  0, 2, 0, 0},
						{ 0,  0,  0,  2, 0, 3, 8},
						{ 0,  0,  0,  0, 3, 0, 3},
						{ 0,  0,  0,  0, 8, 3, 0}};

    // visisted array
    int visited[7] = {0};

    Graphs_Dijkstra_PriorityList(n, graph, visited);

    /* Let us create the example graph discussed above */
    /*
     * file:///D:/SmiffyRepository/dev-os-embedded/eclipseWrkSpace/FirstC_project/img/Fig-11.jpg
     */
    int graph2[9][9] = { { 0, 4, 0, 0, 0, 0, 0, 8, 0 },
                        { 4, 0, 8, 0, 0, 0, 0, 11, 0 },
                        { 0, 8, 0, 7, 0, 4, 0, 0, 2 },
                        { 0, 0, 7, 0, 9, 14, 0, 0, 0 },
                        { 0, 0, 0, 9, 0, 10, 0, 0, 0 },
                        { 0, 0, 4, 14, 10, 0, 2, 0, 0 },
                        { 0, 0, 0, 0, 0, 2, 0, 1, 6 },
                        { 8, 11, 0, 0, 0, 0, 1, 0, 7 },
                        { 0, 0, 2, 0, 0, 0, 6, 7, 0 } };

    // visisted array
    int visited2[9] = {0};

    printf("\nnewinput:\n");
    Graphs_Dijkstra_PriorityList(9, graph2, visited2);


}

void Graphs_Dijkstra_PriorityList(int n, int graph[n][n], int visited[n])
{
	// create priority list
    List_d *list = memset(malloc(sizeof(List_d)), 0, sizeof(List_d));
    /*list->head = list->tail = NULL;
    list->size = 0;*/

    // create pair of 0-node
    Pair_d *pair = malloc(sizeof(Pair_d));
    memset(pair, 0, sizeof(Pair_d));
    pair->val = 0;
    pair->weight = 0;
    pair->path = malloc(2);
    memset(pair->path, 0, 2);
    pair->path = "0";
    pair->path_size = 1;

    addPairToList(list, pair);

    // r-c&m-w-a
    while(list->size!=0) // list is not empty
    {
    	Pair_d *vertex = remFromList(list);	// r
    	if ((visited[vertex->val]!=0)) 	// c
    		continue;

    	visited[vertex->val] = 1;	// m

    	// add to list of answers????
    	printf("pathto:%d via %s @costof %d\n", vertex->val, vertex->path, vertex->weight);	// w

    	for(int i=0; i<n; i++)
    	{
     		if ((graph[vertex->val][i] != 0) && (visited[i]!=1))
    		{
    			addPairToList(list, createNewPair(vertex, i, n, graph));
    		}
    	}
    }
}


// ----------------------------------------------------
// priority-list
// ----------------------------------------------------
Pair_d *createNewPair(Pair_d *vertex, int neighbor, int n, int graph[n][n])
{
	// create new-pair for the visiting-neighbor
    Pair_d *pair = malloc(sizeof(Pair_d));
    memset(pair, 0, sizeof(Pair_d));
    pair->val = neighbor;
    pair->weight = vertex->weight + graph[vertex->val][neighbor];

    pair->path = malloc(vertex->path_size +1+1);
    memset(pair->path, 0, vertex->path_size +1+1);
    strcat(pair->path, vertex->path);
    neighbor += '0'; // itoa --> integer + asciiof(0);
    strcat(pair->path, (char *)&neighbor);
    pair->path_size = vertex->path_size++;

    return pair;
}


void addPairToList(List_d *list, Pair_d *pair)
{
	if (list->head == NULL)
	{
		list->head = list->tail = pair;
	}
	else
	{
		list->tail->next = pair;
		list->tail = pair;
	}

	list->size++;
}


/**
 *
 *  priority list only removes the node based on priority
 *   for eg: here the priority is the lowest weight.
 *
 *
 */
Pair_d *remFromList(List_d *list)
{
	Pair_d *result = NULL;
	unsigned int min = 0xFFFFFFFF;

	Pair_d *cur = list->head;
	Pair_d *prev = NULL;
	Pair_d *last_prev = NULL;
	while(cur)
	{
		if (cur->weight < min) // check total wieght
		{
			min = cur->weight;
			result = cur;
			last_prev = prev;
		}
		prev = cur;
		cur = cur->next;
	}

	// remove the prioity-node and return
	if (result == list->head)
	{
		list->head = result->next;
		if(list->tail == result)
			list->tail = NULL;
	}
	else if (result == list->tail)
	{
		list->tail = last_prev;
		list->tail->next = NULL;
	}
	else
	{
		last_prev->next = result->next;
	}

	list->size--;
	return result;
}


/**
 * Display-graph
 *
 */
static void displayGraph(int n, int graph[n][n])
{
	for (int i=0; i<n; i++)
	{
		for(int j=0; j<n; j++)
		{
			printf("%d ", graph[i][j]);
		}
		printf("\n"); //EOL
	}
}
