/*
 * This module defines the system that is operating and monitoring multiple relays,
 * and the prototypes for which are defined in this header files.
 *
 * This will cause the other system to control and check the operation on relays.
 */

#ifndef MDL_RELAY_H
#define MDL_RELAY_H

#include <stdint.h>
#include "mdl_di.h"
#include "mdl_do.h"


//! Digital Output (DO) states
typedef enum RELAY_state_ENUM {
    RELAY_state_NO_ERROR = 0u,          //!< Success
    RELAY_state_WELDED = 1u,            //!< Error, if trying to open it but feedback says it is still closed.
    RELAY_state_CONSTANTLY_OPEN = 2u    //!< Error, if trying to close it but feedback says it is still open.
} RELAY_state_E;


#define CONTROL_RELAY_OPEN              0x01    // control-request to open the specific relay.
#define CONTROL_RELAY_CLOSE             0x00    // control-request to close the specific relay.


/*****************************************************************************************************//**
* @details Function intializes the relay module (could be one-time init during the startup or
*          could be used for configuring new settings or connections).
*          REQUIREMENT-7: "a task/thread/process that needs separated functionality for setup/initialization".
*          REQUIREMENT-10: Will be part of the library and then used in different systems with a
*                         different number of relays (but up to some value), different digital inputs and
*                         different digital outputs indexes.
*********************************************************************************************************
* @param [in] CONNECTION_CONFIG[] - Input table to configure the Relay connections (using the pair of Do and Di).
* @param [in] u32TotalNumOfRelays - Total number of Relays (connected in the system) to operate on.
* @return Nothing.
********************************************************************************************************/
void RELAY_moduleInitAndRelaySetup(uint32_t CONNECTION_CONFIG[], uint32_t u32TotalNumOfRelays);


/*****************************************************************************************************//**
* @details Function cleans-up the relay module (post-execution part).
*          Should be called before setting new configuration of the Relays.
*          REQUIREMENT-7: Exists in a simple task scheduler environment as a task/thread/process that needs
*                         separated functionality for setup/initialization, run-time task execution,
*                         and post-execution/destruction.
*********************************************************************************************************
* @param [in] Nothing.
* @return Nothing.
********************************************************************************************************/
void RELAY_relayCleanup(void);


/*****************************************************************************************************//**
* @details Function send the control request to the relay module.
*          REQUIREMENT-1: Controls (opens or closes) the relay over the digital output of the system.
*          REQUIREMENT-2: Receives controls of the relay on demands from the rest of the system
*                         (other SW units/modules) over it’s exposed API.
*********************************************************************************************************
* @param [in] u32Relay_Num - Relay no. (connected in the system) to operate on.
* @param [in] u32Control - opens or closes the relay based on the configuration in the system.
* @param [in] u32Delay - provides delay (in ms) before the open or close of the relay.
* @return Status (state) of relays:
*           RELAY_state_NO_ERROR
*           RELAY_state_WELDED
*           RELAY_state_CONSTANTLY_OPEN
********************************************************************************************************/
RELAY_state_E RELAY_controlRequest(uint32_t u32Relay_Num, uint32_t u32Control, uint32_t u32Delay);


/*****************************************************************************************************//**
* @details Function to check the status of the Relay.
*          REQUIREMENT-5: Provide closed status (state) of relays to the rest of the system (open/closed).
*********************************************************************************************************
* @param [in] u32Relay_Num - Relay no. (connected in the system) to check status of.
* @return Status of relay either open or close:
*           CONTROL_RELAY_OPEN
*           CONTROL_RELAY_CLOSE
********************************************************************************************************/
uint32_t RELAY_getCurrentStatus(uint32_t u32Relay_Num);


/*****************************************************************************************************//**
* @details Function to detect error in Relay by reading the table (used by the other system to diagnose error).
*          REQUIREMENT-6: Provide error status (state) of relays to the rest of the system
*                         (no error/welded/constantly open).
*********************************************************************************************************
* @param [in] u32Relay_Num - Relay no. (connected in the system) to check status of.
* @return Status of relay:
*           RELAY_state_NO_ERROR
*           RELAY_state_WELDED
*           RELAY_state_CONSTANTLY_OPEN
********************************************************************************************************/
uint32_t RELAY_detectError(uint32_t u32Relay_Num);

#endif /* MDL_RELAY_H */
