/*
 * BinaryTrees_session_2.h
 *
 *  Created on: 10-Apr-2022
 *      Author: sumit
 */

#ifndef INCLUDE_BINARYTREES_SESSION_2_H_
#define INCLUDE_BINARYTREES_SESSION_2_H_

#include "BinaryTrees.h"
#include "Types.h"

void BinaryTrees_ProgramSession2(void);
void BinaryTrees_TopView(TreeNode_d *root, int size);
void BinaryTrees_BotView(TreeNode_d *root, int size);
void BinaryTrees_VerticalOrder_Traversal(TreeNode_d *root, int size);
void BinaryTrees_rightview(TreeNode_d *root, int size);
void BinaryTrees_Sideview_Traversal(TreeNode_d *root, int size);
void BinaryTrees_LevelOrder_Traversal(TreeNode_d *root, int size);
void BinrayTrees_Spiral(TreeNode_d *root, int size);
TreeNode_d *constructCustomBinaryTree(void);
TreeNode_d *constructCustomBinaryTree2(void);
TreeNode_d *constructCustomSubTree(void);
TreeNode_d *constructCustomTreeWithDuplicates(void);

void BinaryTree_DiagonalTraversal(TreeNode_d *root);
void BinaryTrees_TraverseDiagonals(TreeNode_d * root, int size);
int BinaryTrees_Nodes_with_K_Leaves(TreeNode_d *root, int K);
TreeNode_d *BinaryTrees_LowestCommonAncestor(TreeNode_d *root, int A, int B);
//TreeNode_d *BinaryTrees_LowestCommonAncestor(TreeNode_d *root, TreeNode_d *A, TreeNode_d *B)
int BinaryTrees_findDiameter(TreeNode_d *root);
bool_t BinaryTrees_PrintAllAncestors(TreeNode_d *root, int A);
int BinaryTrees_distance_between(TreeNode_d *root, int A, int B);


bool_t BinaryTrees_isIdentical(TreeNode_d *root1, TreeNode_d *root2);
bool_t BinaryTrees_isSubTree(TreeNode_d *root1, TreeNode_d *root2);
bool_t BinaryTrees_isDupalicateSubTreePresent(TreeNode_d *root);
bool_t BinaryTrees_isIsoMorphic(TreeNode_d *root);
bool_t BinaryTrees_isGraphATree(TreeNode_d *root);

#endif /* INCLUDE_BINARYTREES_SESSION_2_H_ */
