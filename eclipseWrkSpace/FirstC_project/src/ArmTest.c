/*
 * ArmTest.c
 *
 *  Created on: 01-May-2023
 *      Author: sumit
 */
#include <stdio.h>
#include <stdlib.h>

static void Miscellaneous_CommunicationProtocolImpl_Arm(void);
static void Miscellaneos_StorageProgram(void);

// ----------------------------------------------------
// Main-driver function
// ----------------------------------------------------
void ArmTest_Program(void)
{
    // Comm-Protocol implementation
    Miscellaneous_CommunicationProtocolImpl_Arm();

    // storage program
    //Miscellaneos_StorageProgram();
}

// ----------------------------------------------------
// Arm Test: Communication Protocol
// ----------------------------------------------------
/*
 * Part 2 - C Problems
 * ===================
 *
 * 2.1 A Communications Protocol
 * -----------------------------
 *
 * This problem concerns a (hypothetical) communications protocol linking two computing devices.
 * The protocol uses a serial interface and transmits bytes in packets. To provide error correction
 * and synchronisation, the packets are delimited by start and stop characters and contain a
 * rudimentary checksum.
 * The structure of the packet is as shown in the following diagram:
 * [SOP][ �data� ][CSUM][EOP] // SOP ff ff CKSUM EOP
 *
 * SOP and EOP are bytes with defined values.
 * The �data� block contains an arbitrary number of bytes. It is valid for the data block to be empty.
 * The CSUM is a single byte and is defined as the sum (mod 256) of all the bytes contained in the
 *  data block � this does not include the SOP at the start.
 *
 * You are asked to define (in any language, structured English, flowchart etc.) a routine which is to be
 * called every time a new byte is received on the communications link. The routine is required to process
 * the incoming byte and return the value 1 when it has detected a complete and valid packet (i.e. SOP
 * following by some number of data bytes, a valid checksum, terminated by EOP);
 * at all other times (i.e. in the middle of a packet and on reception of an invalid packet) the routine
 * is to return the value 0. In �C�, the framework of such a function would look something like this:
 * a) Fill in the body of the function and list any assumptions that you have had to make. [8 marks]
 * b) Can you suggest an alternative way of defining the checksum which would simplify the problem? [2 marks]
 *
 */

// SOP and EOP are not part of the data
#define SOP                 0x00
#define EOP                 0xFF
#define INVALID             0xFFFFFFFF
#define FRAME_HEADER_SIZE   0x03

unsigned char transmission = 0;
unsigned int sum = 0; // lastcksm + newbyte
unsigned int prev = 0;
unsigned int byteReceived = 0;

static void TEST(unsigned char *data, unsigned int length);
static unsigned char calcChecksum(unsigned char *data, unsigned char len);
static int ReceiveNewByte(unsigned char NewByte);
static unsigned char *createFrame(unsigned char *data, unsigned int length);

static void Miscellaneous_CommunicationProtocolImpl_Arm(void)
{
    // TEST-1   : SOP data FF EOP
    unsigned char data_1[7] = {0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0x02, 0x01};
    unsigned int length = sizeof(data_1)/sizeof(unsigned char);
    unsigned char *frame1 = createFrame(data_1, length);
    TEST(frame1, length + FRAME_HEADER_SIZE);
    //free((void *)frame1);

    // TEST-2   :   {SOP EOP}               // invalid
    unsigned char frame2[2] = {SOP, EOP};
    TEST(frame2, 0 + 2);

    // TEST-3   :   {SOP EOP EOP}           // invalid
    unsigned char frame3[3] = {SOP, EOP, EOP};
    TEST(frame3, 0 + FRAME_HEADER_SIZE);

    // TEST-4   :   {SOP CHKSUM(00) EOP}    // should be valid, but becomes invalid as SOP == 0
    unsigned char frame4[3] = {SOP, 0x00, EOP};
    TEST(frame4, 0 + FRAME_HEADER_SIZE);

    // TEST-5   : SOP data FF EOP
    unsigned char data_2[8] = {1, 17, 18, 3, 7, 10, 2, 6};
    unsigned int length2 = sizeof(data_2)/sizeof(unsigned char);
    unsigned char *frame5 = createFrame(data_2, length2);
    TEST(frame5, length2 + FRAME_HEADER_SIZE);
    //free((void *)frame5);

    // TEST-6   :
    printf("TEST-6\n");
    unsigned char frame6[29];
    memcpy(frame6, frame2, 2);
    memcpy(frame6+2, frame1, length + FRAME_HEADER_SIZE);
    memcpy(frame6+12, frame3, 3);
    memcpy(frame6+15, frame5, 11);
    memcpy(frame6+26, frame4, 3);
    TEST(frame6, 29);

    // TEST-7   :   {SOP EOP EOP EOP}           // valid
    printf("TEST-7\n");
    unsigned char frame7[4] = {SOP, EOP, EOP, EOP};
    TEST(frame7, 1 + FRAME_HEADER_SIZE);
}

// SOP EOP EOP          // invalid
// SOP EOP              // invalid
// SOP CHKSUM(00) EOP   // should be valid, but becomes invalid as SOP == 0
// SOP data ff EOP      // valid
// SOP SOP EOP          // should be valid, but becomes invalid as SOP == 0

// SOP EOP EOP EOP              // valid
// SOP SOP EOP EOP EOP          // valid
// SOP EOP SOP EOP EOP EOP      // invalid but has valid internal frame.

static int ReceiveNewByte(unsigned char NewByte)
{
    /* process new byte */
    // check for SOP, and read data byte also sum it up, till I get EOP
    if (NewByte == (unsigned char)SOP)
    {
        // update transmssion state
        transmission = 1; //started

        // reset byterecevied, prev
        byteReceived = 0;
        prev = 0;
        sum = 0;

        return 0;
    }
    else if (NewByte == EOP)
    {
        //if ((transmission != 1) && (byteReceived > 0))
        //if ((transmission != 1) || (byteReceived < 1))
        if (transmission != 1)
        {
            transmission = 0; // reset transmission-state
            return 0;         // ignore EOP
        }

        if ((byteReceived == 1) && (sum == 0u))
        {
            transmission = 2; //completed

            // reset metadata
            sum = 0;  // -1
            prev = 0; // -1
            byteReceived = 0;
            return 1; //completed
        }
        // verify checksum
        // (0xFF + 0xFF + 0xFE -0xFE)%0x100 == 0xFE == 0xFE
        //if (((sum-prev)%0x100) == prev)
        //if ((sum-prev) == prev)
        else if ((byteReceived > 0) && ((sum-prev) == prev)) // if some data then verify checksum
        {
            transmission = 2; //completed

            // reset metadata
            sum = 0;  // -1
            prev = 0; // -1
            byteReceived = 0;
            return 1; //completed
        }
        else // if checksum doesnt match means it is data, and hopes for next byte be EOP
        {
            byteReceived++;
            sum = (sum%0x100) + NewByte; //-- (lastcksum + NewByte)
            prev = NewByte;
            return 0;
        }
    }
    else
    {
        if (transmission != 1)
            return 0; // ignore data

        byteReceived++;
        //sum += NewByte;
        //cksm = ((cksm + NewByte) % 0x100);
        sum = (sum%0x100) + NewByte; //-- (lastcksum + NewByte)
        prev = NewByte;
        return 0;
    }

    return 0;
}


static unsigned char calcChecksum(unsigned char *data, unsigned char len)
{
    unsigned char cksm = 0;
    unsigned char index = len;
    while(index--)
    {
        cksm = ((cksm + data[index]) % 0x100);
    }

    return cksm;
}


static unsigned char *createFrame(unsigned char *data, unsigned int length)
{
    unsigned char *Data_Frame = memset((void *)malloc((size_t)(sizeof(unsigned char) * (length+FRAME_HEADER_SIZE))), 0, length+FRAME_HEADER_SIZE);
    unsigned int frameSize = length + FRAME_HEADER_SIZE;

    Data_Frame[0x00] = SOP;                                         // SOP
    memcpy((void *)(&Data_Frame[1]), (const void *)data, length);   // data
    Data_Frame[frameSize - 2] = calcChecksum(data, length);         // chksm
    Data_Frame[frameSize - 1] = EOP;                                // EOP

    return Data_Frame;
}

static void TEST(unsigned char *frame, unsigned int frameSize)
{
    int i = 0;
    int result = 0;

    // receive frame and verify
    while (i<frameSize)
    {
        result = ReceiveNewByte(frame[i]);
        if (result == 1)
        {
            // Complete Frame received
            printf("complete frame recevied\n");
        }

        i++;
    }

    // frame is invalid.
    if (result == 0)
    {
        printf("Frame is Invalid\n");
    }
}


// ----------------------------------------------------
// Arm Test: A Storage Problem
// ----------------------------------------------------
/*
 * 2.2 A Storage Problem
 * ---------------------
 *
 * This is the sort of problem with which you might be faced when implementing software which has to run in an environment which is extremely short of storage space.
 * You have an array of 64 variables to store, x[i], where 0 <= i <= 63. These variables can take values ranging from 0 to 15 inclusive. The easiest way to store such an array would be to declare an array of byte-sized variables:
 * byte a[64];
 *
  *The problem with this is that it is very wasteful of memory. The restricted range of values of each array element means that it actually takes only 4 bits to store. The array as declared above, therefore, wastes half of the memory which it occupies.
 * A more efficient method stores two values in each 8-bit byte as follows (just showing the first few bytes):
 * [Byte 0   ] [Byte 1   ] [Byte 2   ] [Byte 3   ]
 * [x[1] x[0]] [x[3] x[2]] [x[5] x[4]] [x[7] x[6]]
 *
 * In memory: x[0] x[1] x[2] x[3] x[[4] x[5] x[6] x[7]
 *
 * (You will realise why the values appear reversed within each byte when you try to implement the solution!)
 * Design two functions, with declarations given below, which store and retrieve the values from this data structure. [10 marks]
 *
 */

unsigned char X[0x0a];

void Store(unsigned char Value, int Index);
unsigned char Recall(int Index);
static void printArray(unsigned char array[], int size);


static void Miscellaneos_StorageProgram(void)
{
    // [Byte 0   ] [Byte 1   ] [Byte 2   ] [Byte 3   ]
    // [x[1] x[0]] [x[3] x[2]] [x[5] x[4]] [x[7] x[6]]

    X[0x00] = 0x9a; // [9, a] ==> x[1], x[0]
    X[0x01] = 0x13; // [1, 3] ==> x[3], x[2]
    X[0x02] = 0xfc; // [f, c] ==> x[5], x[4]
    X[0x03] = 0x4d; // [4, d] ==> x[7], x[6]

    Store(0x07, 7);
    Store(0x04, 2);
    printf("value-updated: %d\n", Recall(7));
    printf("\n"); // EOF
    printArray(X, sizeof(X)/sizeof(unsigned char));
}


void Store(unsigned char Value, int Index)
{
    /* store Value as the Index�th element */
    unsigned char byteIndex = Index/2;

    if ((Index & 01) == 0)
    {
        //clear and update
        X[byteIndex] = (X[byteIndex]&(0xF0)) | (0x0f & Value);
    }
    else
        X[byteIndex] = (X[byteIndex]&(0x0F)) | (0xF0 & (Value<<4));

}

unsigned char Recall(int Index)
{
    unsigned char byteIndex = Index/2;

    /* return the value of the Index�th element */
    if ((Index & 0x01) == 0) // even
    {
        return X[byteIndex] & 0x0F;
    }
    else
    {
        return (X[byteIndex]>>4)&0x0F;
    }

}

static void printArray(unsigned char array[], int size)
{
    for(int i=0; i<size; i++)
    {
        printf("X[0x%x] = 0x%x\n", i, array[i]);
    }
}
