/*
 * LinkedList.c
 *
 *  Created on: 07-Mar-2022
 *      Author: sumit
 */
#include <stdio.h>
#include <stdlib.h>
#include "Types.h"

struct Node
{
	int data;
	struct Node *next;
}*head;

//#define NULL (void*)0


struct Node *LinkedList_Reverse(struct Node *start);
bool_t LinkedList_DetectLoop(struct Node *start);
void LinkedList_AddLoop(struct Node **start);
struct Node *LinkedList_Intersection(struct Node *headA, struct Node *headB);

void LinkedList_Create(int n);
void LinkedList_InsertNode(int n, struct Node **node);
void LinkedList_Print(struct Node *head);

void LinkedListProgram(void)
{
	LinkedList_Create(5);
	printf("List-A\n");
	LinkedList_Print(head);

	// reverse a list
	//struct Node *reverse = LinkedList_Reverse(head);
	//LinkedList_Print(reverse);

	/*
	// create loop,
	// loop can come from last element only.
	LinkedList_AddLoop(&head);

	if (LinkedList_DetectLoop(head)) {
		printf("LOOP-Detected\n");
	}
	else
		printf("LOOP-not-Detected\n");
	*/

	// ---------------------------------------------------------------------
	struct Node *tempNode = (struct Node *)malloc(sizeof(struct Node));
	tempNode->data = 0;
	tempNode->next = NULL;

	LinkedList_InsertNode(1, &tempNode);
	LinkedList_InsertNode(2, &tempNode);
	LinkedList_InsertNode(3, &tempNode);
	LinkedList_InsertNode(7, &tempNode);
	LinkedList_InsertNode(8, &tempNode);
	printf("List-B\n");
	LinkedList_Print(tempNode);

	struct Node *intersect = LinkedList_Intersection(head, tempNode);

	printf("Intersected List\n");
	LinkedList_Print(intersect);
}


struct Node *LinkedList_Intersection(struct Node *headA, struct Node *headB)
{
	struct Node *newNode;
	struct Node *intersect;
	struct Node *temp = (struct Node *) malloc(sizeof(struct Node ));
	temp->data = 0;
	temp->next = NULL;
	intersect = temp;

	while ((headA->next != NULL) && (headB->next != NULL))
	{
		if(headA->data == headB->data)
		{
			newNode = (struct Node *) malloc(sizeof(struct Node ));
			newNode->data = headA->data;
			newNode->next = NULL;
			temp->next = newNode;

			// increment
			temp = temp->next;
			headA = headA->next;
			headB = headB->next;
		}
		else if (headA->data > headB->data) // correct check incase of decreasing order
		{
			headA = headA->next;
		}
		else
			headB = headB->next;
	}

	return intersect->next;
}

bool_t LinkedList_DetectLoop(struct Node *start)
{
	bool_t detected = 0;
	struct Node *fast = start;
	struct Node *slow = start;

//	while ((fast->next!=NULL) && (slow->next!=NULL))
//	{
//		fast = fast->next;
//		slow = slow->next;
//
//		if (fast->next != NULL)
//		{
//			fast = fast->next;
//		}
//
//		if(fast == slow)
//		{
//			detected = 1;
//			break;
//		}
//	}

    while((fast != NULL) && (slow != NULL))
    {
        fast = fast->next;
        slow = slow->next;

        if (fast != NULL)
        {
            fast = fast->next;
        }

        if (fast == slow)
        {
            detected = 1;
            break;
        }
    }

	return detected;
}


void LinkedList_AddLoop(struct Node **start)
{
	struct Node *current = *start;
	// traverse till NULL
	while(current->next != NULL)
	{
		current = current->next;
	}

	current->next = *start;
}

struct Node *LinkedList_Reverse(struct Node *start)
{
	struct Node* current = start;
	struct Node* prev = NULL;
	struct Node* next = NULL;

//	while(current->next != NULL)
//	{
//		next = current->next;
//		current->next = prev;
//		prev = current;
//		current = next;
//	}
//  //last node
//  current->next = prev;
//  return current;

    while(current != NULL)
    {
        next = current->next;
        current->next = prev;
        prev = current;
        current = next;
    }
    return prev;
}

//----------------------------------
// create list
//----------------------------------
void LinkedList_Create(int n)
{
	struct Node *tempNode = (struct Node *)malloc(sizeof(struct Node));
	tempNode->data = 0;
	tempNode->next = NULL;
	head = tempNode;

	int i=1;

	while(i<=n)
	{
		LinkedList_InsertNode(i, &tempNode);
		i++;
	}

	head = tempNode;
}


/**
 * Insert Node in the List
 */
void LinkedList_InsertNode(int n, struct Node **node)
{
	struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
	newNode->data = n;
	newNode->next = *node;
	*node = newNode;
}


/**
 * Print the List
 */
void LinkedList_Print(struct Node *head)
{
    struct Node *current = head;
    while (current != NULL)
    {
        printf("%d ->", current->data);
        current = current->next;
    }
}
