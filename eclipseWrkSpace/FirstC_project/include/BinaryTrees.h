/*
 * BinaryTrees.h
 *
 *  Created on: 24-Mar-2022
 *      Author: sumit
 */

#ifndef _BINARYTREES_H_
#define _BINARYTREES_H_

#define FTR_NORMAL_SEARCH 		DISABLED


//-------------------------
// structs and typedefs
typedef struct TreeNode
{
	int value;
	struct TreeNode *left;
	struct TreeNode *right;
}TreeNode_d;

typedef enum modes
{
	MODE_PRE_ORDER = 0,
	MODE_POST_ORDER,
	MODE_IN_ORDER
}e_mode_d;



void BinaryTrees_Program(void);


#endif /* _BINARYTREES_H_ */
