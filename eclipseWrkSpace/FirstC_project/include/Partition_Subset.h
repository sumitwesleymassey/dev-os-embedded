/*
 * Partition_Subset.h
 *
 *  Created on: 30-Apr-2022
 *      Author: sumit
 */

#ifndef INCLUDE_PARTITION_SUBSET_H_
#define INCLUDE_PARTITION_SUBSET_H_

void Partions_Subset_Program(void);


#define K_SUM_PARTITION	ENABLED
#define OPTIMIZED_OPTIONS ENABLED

typedef struct Node
{
	int element;
	struct Node *next;
}Node_d;


typedef struct Partition
{
	Node_d *head;
	Node_d *tail;
	int size;
#if K_SUM_PARTITION == ENABLED
	int sum;
#endif
}Partition_d;


typedef struct Set
{
	Partition_d *partions;
	struct Set *next;
	int partition_size;
}Set_d;

typedef struct Dynamic_List
{
	Set_d *head;
	Set_d *tail;
	int num_sets;
}Dynamic_List_d;


Set_d *createSet(int k);
void addElementToSet(Set_d *set, int i, int element);
void removeElementFromSet(Set_d *set, int i, int element);
Dynamic_List_d *createList(void);
void copyPartition(Partition_d *old, Partition_d *new_partition);
void addSetToList(Dynamic_List_d *list, Set_d *set);
void removeLastSetFromList(Dynamic_List_d *list, Set_d *set);
void destroyList(Dynamic_List_d *list);
void printSets(Set_d *set);

#endif /* INCLUDE_PARTITION_SUBSET_H_ */
