/*
 * Partition_Subset.c
 *
 *  Created on: 30-Apr-2022
 *      Author: sumit
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Partition_Subset.h"
#include "Util.h"


bool_t Partition_Subsets_into_K_Partitions(int n, int array[n], int k);
bool_t Partition_Subsets_Tug_of_war(int n, int array[n]);



// ----------------------------------------------------
// Main-driver function
// ----------------------------------------------------
void Partions_Subset_Program(void)
{
	// ----------------------------------------------------
	// K partition subsets
	// ----------------------------------------------------
	printf("K-partition subsets: {1, 2, 3, 4} : n=4, k=2\n");
	int array1[] = {1, 2, 3, 4};
	if(!Partition_Subsets_into_K_Partitions(sizeof(array1)/ sizeof(array1[0]), array1, 2))
		printf("No, not possible to partition\n");
	printf("\n"); // EOL

	printf("K-partition subsets: {1, 5, 11, 5} : n=4, k=2\n");
	int array2[] = {1, 5, 11, 5};
	if(!Partition_Subsets_into_K_Partitions(sizeof(array2)/ sizeof(array2[0]), array2, 2))
		printf("No, not possible to partition\n");
	printf("\n"); // EOL

	printf("K-partition subsets: {2, 1, 4, 5, 6} : n=5, k=3\n");
	int array3[] = {2, 1, 4, 5, 6};
	if(!Partition_Subsets_into_K_Partitions(sizeof(array3)/ sizeof(array3[0]), array3, 3))
		printf("No, not possible to partition\n");
	printf("\n"); // EOL


	// ----------------------------------------------------
	// Tug of War
	// ----------------------------------------------------
	printf("Tug of War: {23, 45, -34, 12, 0, 98, -99, 4, 189, -1, 4} : n=11, k=2\n");
	int array4[] = {23, 45, -34, 12, 0, 98, -99, 4, 189, -1, 4};
	if(!Partition_Subsets_Tug_of_war(sizeof(array4)/ sizeof(array4[0]), array4))
		printf("No, not possible to partition\n");
	printf("\n"); // EOL

	printf("Tug of War: {3, 4, 5, -3, 100, 1, 89, 54, 23, 20} : n=10, k=2\n");
	int array5[] = {3, 4, 5, -3, 100, 1, 89, 54, 23, 20};
	if(!Partition_Subsets_Tug_of_war(sizeof(array5)/ sizeof(array5[0]), array5))
		printf("No, not possible to partition\n");
	printf("\n"); // EOL
}


// ----------------------------------------------------
// Tug of War
// ----------------------------------------------------
static bool_t solveTOW(int i, int n, int array[n], int sets_filled, int sum, Set_d *set, Dynamic_List_d *list);

int minDiff = 0xFFFFFFF;

static bool_t solveTOW(int i, int n, int array[n], int sets_filled, int sum, Set_d *set, Dynamic_List_d *list)
{
	bool_t result = false;

	// base condition
	if (i == n)
	{
#if OPTIMIZED_OPTIONS == ENABLED
#else
		// this ensures that:
		//	1) both sets are filled in, and not a single one is empty set (thus fulling the constraint)
		//  2) each partition is either half-half (incase of even n) or half+1 and half (incase of odd)
		//		#even				#odd
		//		n/2 | n/2			(n-1)/2 | (n+1)/2 	or 	(n+1)/2 | (n-1)/2
		//
		//		  5 | 5				 	  5 | 6			or        6 | 5
		//
		if (sets_filled == 2 && ((set->partions[0].size == (n+1)/2) ||
								 (set->partions[1].size == (n+1)/2))
		   )
#endif
		{
			// absolute value
			int diff = set->partions[0].sum - set->partions[1].sum;
			if (diff<0)
				diff = diff*(-1);

			// constraint
			if (diff < minDiff)
			{
				removeLastSetFromList(list, set); // remove last set that was added earlier
				minDiff = diff;				// update minDiff
				addSetToList(list, set);	// add to list
			}
			return true;
		}
#if OPTIMIZED_OPTIONS == ENABLED
#else
		else
			return false;
#endif
	}




#if OPTIMIZED_OPTIONS == ENABLED
	if (set->partions[0].size < (n+1)/2) // do not add more than half in one set
	{
		// to add it to (non-empty set)
		addElementToSet(set, 0, array[i]);
		result |= solveTOW(i+1, n, array, sets_filled+1, sum, set, list); // sets filled is of no use here, since we are filling half half
		removeElementFromSet(set, 0, array[i]);
	}

	if (set->partions[1].size < (n+1)/2) // do not add more than half in one set
	{
		// to add it to (empty set)
		addElementToSet(set, 1, array[i]);
		result |= solveTOW(i+1, n, array, sets_filled+1, sum, set, list); // sets filled is of no use here, since we are filling half half
		removeElementFromSet(set, 1, array[i]);
	}

#else
	for (int j=0; j<2; j++)
	{
		if (set->partions[j].size != 0) // adding to same set
		{
			// to add it to (non-empty set)
			addElementToSet(set, j, array[i]);
			result |= solveTOW(i+1, n, array, sets_filled, sum, set, list);
			removeElementFromSet(set, j, array[i]);
		}
		else
		{
			// to add it to (empty set)
			addElementToSet(set, j, array[i]);
			result |= solveTOW(i+1, n, array, sets_filled+1, sum, set, list);
			removeElementFromSet(set, j, array[i]);
		}
	}
#endif

	return result;
}

bool_t Partition_Subsets_Tug_of_war(int n, int array[n])
{
	bool_t result = false;
	// create list and set
	Dynamic_List_d *list = createList();
	Set_d *set = createSet(2);

	// calculate sum
	int sum = 0;
	for (int i=0; i<n ; i++)
	{
		sum += array[i];
	}

	// call for solution
	result = solveTOW(0, n, array, 0, sum, set, list);

	// print min-sum diff
	printf("min-sum-diff: %d\n", minDiff);

	// print the set
	printSets(list->head);

	return result;
}




// ----------------------------------------------------
// set and list
// ----------------------------------------------------

/**
  *
  *						      Set
  *			   +----------->  +-------------------------------+------+------+
  *            |              | partitions         			  |      |      |
  *            |              +-------------------------------+ size | next |
  *      list  |              | h, t, s | h, t, s  | h, t, s  |		 |		|
  *		+------+              +-------------------------------+------+------+
  *     | head |			   |  |      |  |         |  |
  *     | tail |			   |  |      |  |         |  |
  *     | size |			   |  |      |  |         |  |
  *     +------+               |  |      |  |         |  |
  *          |                 |  |      |  |         |  +-------------------------------------------------------+
  *          |                 |  |      |  |         |                                                          |
  *          |                 |  |      |  |         |                                                          |
  *          |                 |  |      |  |         |	 Node              Node              Node              Node
  *          |                 |  |      |  |         +--> +------+------+   +------+------+   +------+------+   +------+------+
  *          |                 |  |      |  |              | elmnt| next |-->| elmnt| next |-->| elmnt| next |-->| elmnt| next |-->NULL
  *          |                 |  |      |  |              +------+------+   +------+------+   +------+------+   +------+------+
  *          |                 |  |      |  |
  *          |                 |  |      |  |
  *          |                 |  |      |  |
  *          |                 |  |      |  +-------------------------------------------------------+
  *          |                 |  |      |                                                          |
  *          |                 |  |      |                                                          |
  *          |                 |  |      |	 Node              Node              Node              Node
  *          |                 |  |      +--> +------+------+   +------+------+   +------+------+   +------+------+
  *          |                 |  |           | elmnt| next |-->| elmnt| next |-->| elmnt| next |-->| elmnt| next |-->NULL
  *          |                 |  |           +------+------+   +------+------+   +------+------+   +------+------+
  *          |                 |  |
  *          |                 |  |
  *          |                 |  |
  *          |                 |  +-------------------------------------------------------+
  *          |                 |                                                          |
  *          |                 |                                                          |
  *          |                 |	 Node              Node              Node              Node
  *          |                 +--> +------+------+   +------+------+   +------+------+   +------+------+
  *          |                      | elmnt| next |-->| elmnt| next |-->| elmnt| next |-->| elmnt| next |-->NULL
  *          |                      +------+------+   +------+------+   +------+------+   +------+------+
  *          |
  *          |
  *          |
  *          |
  *          |
  *          |                 Set
  *          +------------->  +-------------------------------+------+------+
  *                           | partitions         			  |      |      |
  *                           +-------------------------------+ size | next |
  *                           | h, t, s | h, t, s  | h, t, s  |		 |		|
  *                           +-------------------------------+------+------+
  *                            |  |      |  |         |  |
  *                            |  |      |  |         |  |
  *                            |  |      |  |         |  |
  *                            |  |      |  |         |  |
  *                            |  |      |  |         |  +-------------------------------------------------------+
  *                            |  |      |  |         |                                                          |
  *                            |  |      |  |         |                                                          |
  *                            |  |      |  |         |	 Node              Node              Node              Node
  *                            |  |      |  |         +--> +------+------+   +------+------+   +------+------+   +------+------+
  *                            |  |      |  |              | elmnt| next |-->| elmnt| next |-->| elmnt| next |-->| elmnt| next |-->NULL
  *                            |  |      |  |              +------+------+   +------+------+   +------+------+   +------+------+
  *                            |  |      |  |
  *                            |  |      |  |
  *                            |  |      |  |
  *                            |  |      |  +-------------------------------------------------------+
  *                            |  |      |                                                          |
  *                            |  |      |                                                          |
  *                            |  |      |	 Node              Node              Node              Node
  *                            |  |      +--> +------+------+   +------+------+   +------+------+   +------+------+
  *                            |  |           | elmnt| next |-->| elmnt| next |-->| elmnt| next |-->| elmnt| next |-->NULL
  *                            |  |           +------+------+   +------+------+   +------+------+   +------+------+
  *                            |  |
  *                            |  |
  *                            |  |
  *                            |  +-------------------------------------------------------+
  *                            |                                                          |
  *                            |                                                          |
  *                            |	 Node              Node              Node              Node
  *                            +--> +------+------+   +------+------+   +------+------+   +------+------+
  *                                 | elmnt| next |-->| elmnt| next |-->| elmnt| next |-->| elmnt| next |-->NULL
  *                                 +------+------+   +------+------+   +------+------+   +------+------+
  *
  *
  *
  *
  */



Set_d *createSet(int k)
{
	Set_d *set = malloc(sizeof(Set_d));
	set->next = NULL;
	set->partition_size = k;
	set->partions = malloc(sizeof(Partition_d)*k);
	set->partions->head = set->partions->tail = NULL;

	// fill the partitions info
	/*for (int j=0; j<k; j++)
	{
		set->partions[j].head = set->partions[j].tail = NULL;
		set->partions[j].size = 0;
	}*/

	// or
	memset(set->partions, 0, sizeof(Partition_d)*k);
	return set;
}

void addElementToSet(Set_d *set, int i, int element)
{
	// create node
	Node_d *newNode = malloc(sizeof(Node_d));
	newNode->element = element;
	newNode->next = NULL;

	// first element
	if (set->partions[i].head == NULL)
	{
		set->partions[i].head = set->partions[i].tail = newNode;
	}
	else
	{
		set->partions[i].tail->next = newNode;
		set->partions[i].tail = newNode; // update the tail aswell.
	}

	set->partions[i].size++;
#if K_SUM_PARTITION == ENABLED
	set->partions[i].sum += element;
#endif
}

void removeElementFromSet(Set_d *set, int i, int element)
{
	Node_d *cur = set->partions[i].head;
	Node_d *prev = NULL;
	while(cur!=NULL)
	{
		if (cur->element == element)
		{
			if (cur == set->partions[i].head) // delete head
			{
				set->partions[i].head = cur->next; // update head

				if(cur == set->partions[i].tail)	// means only element
					set->partions[i].tail = cur->next; // update tail
			}
			else if (cur == set->partions[i].tail) // delete tail
			{
				prev->next = cur->next;
				set->partions[i].tail = prev; // update tail
			}
			else // in the middle.
			{
				prev->next = cur->next;
			}
			free(cur);
			set->partions[i].size--;
#if K_SUM_PARTITION == ENABLED
			set->partions[i].sum -= element;
#endif
			break;
		}

		prev = cur;
		cur = cur->next;
	}
}


Dynamic_List_d *createList(void)
{
	Dynamic_List_d *list = malloc(sizeof(Dynamic_List_d));
	list->head = NULL;
	list->tail = NULL;
	list->num_sets = 0;

	return list;
}

/*
 * param->set : 1) is the original set
 * 				2) do not assign the same nodes to the list
 * 				3) create a copy of it and then assign it.
 *
 */
void copyPartition(Partition_d *old, Partition_d *new_partition)
{
	Node_d *cur = old->head;
	Node_d *prev = NULL;
	while(cur)
	{
		Node_d *new = malloc(sizeof(Node_d));
		new->element = cur->element;
		new->next = NULL;

		if (prev != NULL)
			prev->next = new;
		else
			new_partition->head = new;

		prev = new;
		cur = cur->next;
	}

	new_partition->tail = prev;
	new_partition->size = old->size;
#if K_SUM_PARTITION == ENABLED
	new_partition->sum = old->sum;
#endif
}


void addSetToList(Dynamic_List_d *list, Set_d *set)
{
	int k = set->partition_size;

	// create a copy of the set
	Set_d *set_copy = createSet(k);

	// fill the partitions info
	for (int j=0; j<k; j++)
		copyPartition(&set->partions[j], &set_copy->partions[j]);

	// first element in the list
	if (list->num_sets == 0)
		list->head = list->tail = set_copy;
	else
	{
		list->tail->next = set_copy;
		list->tail = set_copy;
	}

	list->num_sets++;
}


void removeLastSetFromList(Dynamic_List_d *list, Set_d *set)
{
	int k = set->partition_size;
	Set_d *cur = list->head;
	Set_d *prev = NULL;
	while(cur!=NULL)
	{
		//if (cur == set)
		{
			if (cur == list->head) // delete head
			{
				list->head = cur->next; // update head

				if(cur == list->tail)	// means only element
					list->tail = cur->next; // update tail
			}
			else if (cur == list->tail) // delete tail
			{
				prev->next = cur->next;
				list->tail = prev; // update tail
			}
			else // in the middle.
			{
				prev->next = cur->next;
			}

			// free cur-set
			for (int l=0; l<k; l++)
			{
				Node_d *cur_node = cur->partions[l].head;
				Node_d *next = NULL;
				while (cur_node)
				{
					next = cur_node->next;
					free(cur_node);
					cur_node = next;
				}
			}
			free(cur->partions);
			free(cur);
			list->num_sets--;
			break;
		}

		prev = cur;
		cur = cur->next;
	}
}


void destroyList(Dynamic_List_d *list)
{
	// free all the inner items
}



// ----------------------------------------------------
// K-Partions subset
// ----------------------------------------------------
#if K_SUM_PARTITION == ENABLED
static bool_t solve(int i, int n, int array[n], int k, int sets_filled, int sum, Set_d *set, Dynamic_List_d *list);
#else
static bool_t solve(int i, int n, int array[n], int k, int sets_filled, Set_d *set, Dynamic_List_d *list);
#endif

/*
 *
 * 				</		  x		  x      <= only single call with new element on empty partition.
 *               |		  |		  |			or else would end up in duplicate partitions
 * 				1/-/-   -/1/-	-/-/1		means {1/2/34}, {2/1/34}, {34/1/2}, {34/2/1}, {1/34/2}, {2/34/1}
 *											all are same.
 * 						-/-/-				thats why "break" is important
 *
 *
 *
 *
 *
 *
 * condition to pick equal sum partition:
 * 		1) all set partitions filled
 * 		2) no duplicate
 * 		3) ///sum of partion-0 * k == total-sum
 * 		   ///	1, 5, 11, 5
 * 		   ///	n=4, k=2
 *
 * 		   ///	total/k = 22/2 = 11
 *
 * 		   ///  part-0 + part-1 = total_sum
 * 		   /// 	1,5,5  + 11		= 22
 *
 *  	///so, it the sum in 1st part * 2 == toatl sum
 *  	///then other parts shall be equal sums as well.
 *
 *  	(3) is invalid for {2,1,4,5,6} can lead to (2,4} {6,1} {5} which is wrong.
 *  		so have to check all three.
 *
 */
#if K_SUM_PARTITION == ENABLED
static bool_t solve(int i, int n, int array[n], int k, int sets_filled, int sum, Set_d *set, Dynamic_List_d *list)
#else
static bool_t solve(int i, int n, int array[n], int k, int sets_filled, Set_d *set, Dynamic_List_d *list)
#endif
{
	bool_t result1 = false;
	bool_t result2 = false;

	// base condition
	if (i == n)
	{
		// if all partitons are filled in a set then, add the set to the list.
		// or else discard the set and return false
		if (sets_filled == k)
		{
#if K_SUM_PARTITION == ENABLED
			for (int j=0; j<k ; j++)
			{
				if ((set->partions[j].sum) != sum/k)
				{
					return false; // if any of the partition does match then ret false.
				}
			}

			// if reached here means all partitions are valid matches.
			addSetToList(list, set);
			printSets(set);
			return true;
#else
			// instead of adding ot the list, just print the set here itself
			// this will save the creation of copies of the sets to be added to the list
			// which is very cumbersome
			addSetToList(list, set);
			/// or
			//print the sets here itself : answer-so-far
			printSets(set);
			return true; // valid set
#endif
		}
		return false; // invalid set
	}

	// all the options for each partition
	for (int j=0; j<k; j++)
	{
		if (set->partions[j].size != 0) // adding to same set
		{
			addElementToSet(set, j, array[i]);
			result1 |= solve(i+1, n, array, k, sets_filled, sum, set, list);	// bit-wise OR the results, as loop will re-update the result
			removeElementFromSet(set, j, array[i]);
		}
		else // adding to empty set
		{
			addElementToSet(set, j, array[i]);
			result2 |= solve(i+1, n, array, k, sets_filled+1, sum, set, list); 	// bit-wise OR the results, as loop will re-update the result
			removeElementFromSet(set, j, array[i]);
			break; //very important step to avoid duplicate partitions
		}
	}
	return result1 || result2;
}

bool_t Partition_Subsets_into_K_Partitions(int n, int array[n], int k)
{
	bool_t result = false;
	Dynamic_List_d *list = createList();
	Set_d *set = createSet(k);

	// error conditions
	if ((k>n) || (k==0 || n==0))
		return false; // 0
	if (k == 1)
		return true; // 1


#if K_SUM_PARTITION == ENABLED
	int sum = 0;
	for (int a=0; a<n; a++)
	{
		sum += array[a];
	}

	if (sum%k != 0) // if not equally divisible with K then cant be partitioned.
		return false;

	result = solve(0, n, array, k, 0, sum, set, list);
#else
	result = solve(0, n, array, k, 0, set, list));
#endif

	//print the sets
	printf("\n"); // EOL
	printf("print from the list: \n");
	printSets(list->head);

	return result;
}


void printSets(Set_d *set)
{
	Set_d *cur_set = set;

	// null condition, if no set is found
	if (set == NULL)
		return;

	int k = set->partition_size;

	while (cur_set)
	{
		printf("{");
		for (int j=0; j<k; j++)
		{
			Node_d *cur_node = cur_set->partions[j].head;
			printf("{");
			while (cur_node)
			{
				printf("%d", cur_node->element);
				cur_node = cur_node->next;
				if (cur_node)
					printf(", ");
			}
			printf("}");
			if (j<k-1)
				printf(", ");

		}
		printf("},\n");
		cur_set = cur_set->next;
	}
}


// ----------------------------------------------------
// K-Partions subset of equal sums
// ----------------------------------------------------

