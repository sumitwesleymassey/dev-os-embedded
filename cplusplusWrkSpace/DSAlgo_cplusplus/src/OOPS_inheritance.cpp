/*
 * OOPS_inheritance.cpp
 *
 *  Created on: 04-Apr-2022
 *      Author: sumit
 */
#include <iostream>
#include "OOPS_inheritance.h"

using namespace std;


int Vehicle::getMeterReading()
{
	return -1;
}


void Truck::whatDoesItDo()
{
	cout<< "Truck Honks" << endl;
}

void Car::whatTypeOfCar()
{
	cout << "Sedan Car" << endl;
}
