/*
 * Util.h
 *
 *  Created on: 14-Apr-2022
 *      Author: sumit
 */

#ifndef INCLUDE_UTIL_H_
#define INCLUDE_UTIL_H_

#include "Types.h"


#define MAX(x,y)	(((x)>(y))? x: y)
#define MIN(x,y)	(((x)>(y))? y: x)
#define DIFF(x,y)	(((x)-(y)) > 0 ? ((x)-(y)) : ((y)-(x)))


#define SIZEOFARRAY(array) 	(sizeof(array)/sizeof(array[0]))

#define ENABLED 1
#define DISABLED 0

typedef struct LL_TreeNode
{
	void *node;
	struct LL_TreeNode *next;
}LL_TreeNode_d;

typedef struct node
{
	int value;
	struct node *next;
}LL_d;

typedef struct dynamic_memory
{
	LL_d *head;
	LL_d *tail;
	int size;
}DynamicMemory_d;

typedef struct Queue
{
	void *ptr;
	int front;
	int end;
	int qSize;
}Queue_d;

typedef struct STACK
{
	int sp;
	void *pStack;
	int stackSize;
	void (*push)(struct STACK *, void *);
	void *(*pop)(struct STACK *);
}Stack_d;


typedef struct LL_Entry
{
	char *key;
	int keysize;
	int value;
	struct LL_Entry *next;
}LL_Entry_d;


typedef struct dynamic_memory_map
{
	LL_Entry_d *head;
	LL_Entry_d *tail;
	int size;
}DynamicMemory_Map_d;


typedef struct dynamic_memory_list
{
	LL_TreeNode_d *head;
	LL_TreeNode_d *tail;
	int size;
}DynamicMemory_List_d;


void *Util_createQ(int size);
void Util_enqueue(void *Q, void *elements);
void *Util_dequeue(void *Q);
bool_t Util_isQEmpty();
void Util_destroyQ(Queue_d *Q);


void *Util_createStack(int size);
void *Util_pop(Stack_d *stack);
void Util_push(Stack_d *stack, void *value);
void *Util_Top(Stack_d *stack);
void Util_destroyStack(void *stack);

DynamicMemory_d *Util_createDynamicMem(void);
void Util_destroyDynamicMem(DynamicMemory_d *out);
void Util_DynamicMemory_add(DynamicMemory_d *d_mem, int element);

DynamicMemory_d *Util_createHashTable(int nodeSize, int min, int max);
void Util_HashTable_add(DynamicMemory_d *HT, int hd, int element);


// ----------------------------------------------------
// Hash-Map
// ----------------------------------------------------
DynamicMemory_Map_d *Util_createHashMap(int nodeSize, int min, int max);
void Util_HashMap_put(DynamicMemory_Map_d *map, char *key, int keysize, int value);
char *Util_HashMap_getKey(DynamicMemory_Map_d *map, int value);
int Util_HashMap_getValue(DynamicMemory_Map_d *map, char *key, int keysize);

void Util_HashMap_put_updateValue(DynamicMemory_Map_d *map, char *key, int keysize, int value);
int Util_HashMap_getValueIndex(DynamicMemory_Map_d *map, char *key, int keysize);

// ----------------------------------------------------
// List of nodes
// ----------------------------------------------------
DynamicMemory_List_d *Util_createList(void);
void Util_List_add(DynamicMemory_List_d *list, void *element);


// ----------------------------------------------------
// stack
// ----------------------------------------------------
void Util_pushelement(Stack_d *stack, int value);
int Util_popelement(Stack_d *stack);
bool_t isStackempty(Stack_d *stack);
int Util_peekTop(Stack_d *stack);


// ----------------------------------------------------
// sub-string
// ----------------------------------------------------
char *Util_substring(char *str, int s, int e);


// ----------------------------------------------------
// Merge-sort
// ----------------------------------------------------
void Util_Sorting_MergeSort(int *a, int size);

#endif /* INCLUDE_UTIL_H_ */
