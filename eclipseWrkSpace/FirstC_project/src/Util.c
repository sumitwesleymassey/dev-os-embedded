/**
 *  Utils Module with utility APIOs defined here.
 *
 */

#include<stdio.h>
#include<stdlib.h>
#include "Util.h"
#include "string.h"



// ----------------------------------------------------
// Create List
// ----------------------------------------------------
DynamicMemory_List_d *Util_createList(void)
{
	DynamicMemory_List_d *list = malloc(sizeof(DynamicMemory_Map_d));
	list->head = list->tail = NULL;
	list->size = 1;

	return list;
}

void Util_List_add(DynamicMemory_List_d *list, void *element)
{
	// create node
	LL_TreeNode_d *node = malloc(sizeof(LL_TreeNode_d));
	node->node = element;
	node->next = NULL;

	// add node to the tail
	if (list->head == NULL)
	{
		list->head = list->tail  = node;
	}
	else
	{
		list->tail->next = node;
		list->tail = node;
	}

	// increment size & reallocate with a new size
	list->size++;
}

// ----------------------------------------------------
// Create HASH MAP
// ----------------------------------------------------
static LL_Entry_d *Util_HashMap_getNode(DynamicMemory_Map_d *map, char *key, int keysize);

DynamicMemory_Map_d *Util_createHashMap(int nodeSize, int min, int max)
{
	int size = (max - (min)) + 1;
	DynamicMemory_Map_d *map = malloc(sizeof(DynamicMemory_Map_d)*size);
	map->head = map->tail = NULL;
	map->size = 0;

	return map;
}

void Util_HashMap_put(DynamicMemory_Map_d *map, char *key, int keysize, int value)
{
	LL_Entry_d *node = Util_HashMap_getNode(map, key, keysize);
	if (node)
	{
		// update the count or value
		node->value++;						//>>> update frequency or occurence
	}
	else
	{
		// create a new node
		LL_Entry_d *node = malloc(sizeof(LL_Entry_d));
		node->value = value;
		node->key = malloc(sizeof(keysize)); // includeing null character
		node->keysize = keysize;
		memcpy(node->key, key, keysize);
		node->next = NULL;

		// add it to the map
		if (map->size == 0) // for first entry
		{
			map->head = node;
			map->tail = node;
		}
		else
		{
			map->tail->next = node;
			map->tail = node;
		}

		map->size++; // increase the number of elements in the map
	}
}

void Util_HashMap_put_updateValue(DynamicMemory_Map_d *map, char *key, int keysize, int value)
{
	LL_Entry_d *node = Util_HashMap_getNode(map, key, keysize);
	if (node)
	{
		// update the count or value
		node->value = value;				//>>> update new value
	}
	else
	{
		// create a new node
		LL_Entry_d *node = malloc(sizeof(LL_Entry_d));
		node->value = value;
		node->key = malloc(sizeof(keysize)); // includeing null character
		node->keysize = keysize;
		memcpy(node->key, key, keysize);
		node->next = NULL;

		// add it to the map
		if (map->size == 0) // for first entry
		{
			map->head = node;
			map->tail = node;
		}
		else
		{
			map->tail->next = node;
			map->tail = node;
		}

		map->size++; // increase the number of elements in the map
	}
}

int Util_HashMap_getValueIndex(DynamicMemory_Map_d *map, char *key, int keysize)
{
	LL_Entry_d *cur = map->head;
	int value = -1;	// if not found

	while(cur)
	{
		if ((keysize == cur->keysize) && (!memcmp(cur->key, key, keysize)))
		{
			value = cur->value;
			break;
		}
		cur = cur->next;
	}

	return value;
}

static LL_Entry_d *Util_HashMap_getNode(DynamicMemory_Map_d *map, char *key, int keysize)
{
	LL_Entry_d *cur = map->head;

	while(cur)
	{
		if ((keysize == cur->keysize) && (!memcmp(cur->key, key, keysize)))
		{
			break;
		}
		cur = cur->next;
	}

	return cur;
}


char *Util_HashMap_getKey(DynamicMemory_Map_d *map, int value)
{
	LL_Entry_d *cur = map->head;
	char *str = NULL;

	while(cur)
	{
		if (cur->value == value)
		{
			str = cur->key; // dont return the same key pointer, create new key and then return.
			break;
		}
		cur = cur->next;
	}

	return str;
}

int Util_HashMap_getValue(DynamicMemory_Map_d *map, char *key, int keysize)
{
	LL_Entry_d *cur = map->head;
	int value = 0;

	while(cur)
	{
		if ((keysize == cur->keysize) && (!memcmp(cur->key, key, keysize)))
		{
			value = cur->value;
			break;
		}
		cur = cur->next;
	}

	return value;
}


// ----------------------------------------------------
// Create Table or HASH TABLE
// ----------------------------------------------------
DynamicMemory_d *Util_createHashTable(int nodeSize, int min, int max)
{
	int size = (max - (min)) + 1;

	DynamicMemory_d *HT = malloc(size*sizeof(DynamicMemory_d));

	// init each element with initial values
	for (int i = 0; i< size; i++)
	{
		(HT+i)->head = NULL;
		(HT+i)->tail = NULL;
		(HT+i)->size = 0;
	}

	// when, m=-3 and max=3, size is 7
	//
	// -3 -2 -1  0  1  2  3
	//  0  1  2  3  4  5  6
	//			 ^
	//			 |

	return (HT - (min));
}


void Util_HashTable_add(DynamicMemory_d *HT, int hd, int element)
{
	// create node
	LL_d *node = malloc(sizeof(LL_d));
	node->value = element;
	node->next = NULL;

	// add node to the tail
	if (HT[hd].head == NULL)
	{
		HT[hd].tail = HT[hd].head = node;
	}
	else
	{
		HT[hd].tail->next = node;
		HT[hd].tail = node;
	}

	// increment size & reallocate with a new size
	HT[hd].size++;
}


// ----------------------------------------------------
// Dynamic Memory
// ----------------------------------------------------

DynamicMemory_d *Util_createDynamicMem(void)
{
	DynamicMemory_d *d_mem = malloc(sizeof(DynamicMemory_d));
	d_mem->size = 0; // at init as no assignment is done yet.
	d_mem->head = NULL;
	d_mem->tail = NULL;
	return d_mem;
}

void Util_destroyDynamicMem(DynamicMemory_d *out)
{
	LL_d *cur = out->head;

	while(cur)
	{
		LL_d *next = cur->next;
		free(cur);
		cur = next;
	}

	free(out);
}

void Util_DynamicMemory_add(DynamicMemory_d *d_mem, int element)
{
	// create node
	LL_d *node = malloc(sizeof(LL_d));
	node->value = element;
	node->next = NULL;

	// add node to the tail
	if (d_mem->head == NULL)
	{
		d_mem->tail = d_mem->head = node;
	}
	else
	{
		d_mem->tail->next = node;
		d_mem->tail = node;
	}

	// increment size & reallocate with a new size
	d_mem->size++;
}

// ----------------------------------------------------
// Queues
// ----------------------------------------------------

void *Util_createQ(int size)
{
	Queue_d *Q = malloc(sizeof(Queue_d));
	Q->ptr = malloc(size *sizeof(int *));
	Q->front = -1;
	Q->end = -1;
	Q->qSize = size;

	return Q;
}


void Util_destroyQ(Queue_d *Q)
{
	free(Q->ptr);
	free(Q);
}


bool_t Util_isQEmpty(Queue_d *Q)
{
	bool_t retval = false;

	if((Q->front==-1) && (Q->end==-1))
		retval = true;

	return retval;
}

/**
 *
 *
 **/
void Util_enqueue(void *Q, void *elements)
{
	Queue_d *q = Q;

	// prepareQ for 1st element
	if (q->front == -1 && q->end == -1)
	{
		q->front = ((q->front+1)%q->qSize);	// fornt++
		q->end = ((q->end+1)%q->qSize);	// end++
		*(((int *)q->ptr)+q->end) = (int)elements;
	}
	else if (((q->end+1)%q->qSize) == q->front)	// increment end, and then check if equal to front that means overflow
	{
		printf("Q is full\n");
	}
	else
	{
		//end++
		q->end = ((q->end+1)%q->qSize);
		*(((int *)q->ptr)+q->end) = (int)elements;
	}
}

void *Util_dequeue(void *Q)
{
	Queue_d *q = Q;

	void *front_elmnt = NULL;

	if (q->front == -1 && q->end == -1)
	{
		printf("\nQ is empty\n");
		return front_elmnt;
	}

	// last element check
	if (q->front == q->end)
	{
		front_elmnt = *(((int *)q->ptr)+q->front);
		q->front = q->end = -1;					// when both points to same, then its the last element
	}
	else
	{
		front_elmnt = *(((int *)q->ptr)+q->front);

		//front++
		q->front = ((q->front+1)%q->qSize);
	}
	return front_elmnt;
}


// ----------------------------------------------------
// Stack
// ----------------------------------------------------
void *Util_createStack(int size)
{
	Stack_d *pvStack;
	pvStack = malloc(sizeof(Stack_d));
	pvStack->pStack = malloc(size*sizeof(void *));
	pvStack->sp = -1;
	pvStack->stackSize = size;
	pvStack->push = &Util_push;
	pvStack->pop = &Util_pop;

	return pvStack;
}


void *Util_pop(Stack_d *stack)
{
	void *retval = NULL;

	if(stack->sp > -1)
	{
		retval = (void *)(*((int *)stack->pStack + stack->sp));
		stack->sp--;
	}
	else
	{
		printf("stack is empty\n");
	}

	return retval;
}


void Util_push(Stack_d *stack, void *value)
{
	if (value == NULL)
	{
		//printf("nothing to push\n");
		return;
	}

	if(stack->sp > stack->stackSize)
	{
		printf("stack is full\n");
	}

	stack->sp++;
	*((int *)stack->pStack + stack->sp) = (int)value;
}


void *Util_Top(Stack_d *stack)
{
	void *retval = NULL;

	if(stack->sp > -1)
	{
		retval = (void *)(*((int *)stack->pStack + stack->sp));
	}
	else
	{
		printf("stack is empty\n");
	}

	return retval;
}


void Util_destroyStack(void *stack)
{
	free(((Stack_d *)stack)->pStack);
	free(stack);
}


void Util_pushelement(Stack_d *stack, int value)
{
	/*if (value == NULL)
	{
		//printf("nothing to push\n");
		return;
	}*/

	if(stack->sp > stack->stackSize)
	{
		printf("stack is full\n");
	}

	stack->sp++;
	*((int *)stack->pStack + stack->sp) = value;
}

int Util_popelement(Stack_d *stack)
{
	int retval = -1;

	if(stack->sp > -1)
	{
		retval = (*((int *)stack->pStack + stack->sp));
		stack->sp--;
	}
	else
	{
		printf("stack is empty\n");
	}

	return retval;
}

int Util_peekTop(Stack_d *stack)
{
	int retval = -1;

	if(stack->sp > -1)
	{
		retval = (*((int *)stack->pStack + stack->sp));
	}
	else
	{
		printf("stack is empty\n");
	}

	return retval;
}



bool_t isStackempty(Stack_d *stack)
{
	bool_t retval = false;

	if(stack->sp == -1)
		retval = true;

	return retval;
}


// ----------------------------------------------------
// base^exp
// ----------------------------------------------------
int powerOf(int base, int exp)
{
	int result = 1;

	while (exp!=0)
	{
		result = result * base;
		exp--;
	}

	return result;
}


// ----------------------------------------------------
// sub-strings
// ----------------------------------------------------
char *Util_substring(char *str, int s, int e)
{
	char *temp = memset(malloc(sizeof(char)*(e-s+2)), 0, sizeof(char)*(e-s+2)); // include itself and null-char
	memcpy(temp, str+s, e-s+1); // copy the actual string without null-char
	return temp;
}


// ----------------------------------------------------
// Merge Sorting
// ----------------------------------------------------
static void Util_Sorting_Merge(int *left, int *right, int *a, int size);

void Util_Sorting_MergeSort(int *a, int size)
{
	// divide array
	int middle = size/2;
	int leftSize = middle;
	int rightSize = size - middle;

	// exit condition
	if(size<=1)
		return;

	// create and copy to new sub-arrays
	int *leftArray = (int *)malloc(leftSize);
	int *rightArray = (int *)malloc(rightSize);

	int i=0, l=0, r=0;
	while (i<size)
	{
		if(i<leftSize)
		{
			leftArray[l] = a[i];
			i++;
			l++;
		}
		else
		{
			rightArray[r] = a[i];
			i++;
			r++;
		}
	}

	// call sort for left and right
	Util_Sorting_MergeSort(leftArray, leftSize);
	Util_Sorting_MergeSort(rightArray, rightSize);

	// Merge sub-arrays into primary array
	Util_Sorting_Merge(leftArray, rightArray, a, size);
}


static void Util_Sorting_Merge(int *left, int *right, int *a, int size)
{
	// left and right are already sorted in them.
	int i=0,l=0,r=0;

	// compare left < right then left ---> a
	while(l<(size/2) && r<(size-(size/2)))
	{
		if(left[l]<right[r])
		{
			a[i] = left[l];
			i++;
			l++;
		}
		else
		{
			a[i] = right[r];
			i++;
			r++;
		}
	}

	// remaining left
	while(l<(size/2))
	{
		a[i] = left[l];
		i++;
		l++;
	}

	// remaining right
	while(r<(size-(size/2)))
	{
		a[i] = right[r];
		i++;
		r++;
	}
}

/* END_OF_FILE */
