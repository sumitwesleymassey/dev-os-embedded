/*
 * Stack&Queue.c
 *
 *  Created on: 27-May-2022
 *      Author: sumit
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Types.h"

void StackAndQueue_CreateQ_using2Stacks(void);

// ----------------------------------------------------
// Main-driver function
// ----------------------------------------------------
void StackAndStack_Program(void)
{
	// LIS: Longest-Increasing-Subsequence.
	printf("Q using 2-stacks: \n");
	StackAndQueue_CreateQ_using2Stacks();
}


// ----------------------------------------------------
// Queue using 2 stacks
// ----------------------------------------------------
typedef struct Stack
{
	int *val;
	int sp;
	int size;
}Stack_1_d;

typedef struct Queue
{
	Stack_1_d *stack1;
	Stack_1_d *stack2;
}Queue_1_d;

Queue_1_d *createQ(int size);
static int dequeue(struct Queue *Q);
static void enqueue(struct Queue *Q, int val);

void StackAndQueue_CreateQ_using2Stacks(void)
{
	Queue_1_d *Q = createQ(6);
	enqueue(Q, 1);
	enqueue(Q, 2);
	enqueue(Q, 3);
	printf("%d ", dequeue(Q));
	enqueue(Q, 4);
	enqueue(Q, 5);
	printf("%d ", dequeue(Q));
	printf("%d ", dequeue(Q));
	printf("%d ", dequeue(Q));
	enqueue(Q, 6);
	printf("%d ", dequeue(Q));
	printf("%d ", dequeue(Q));
}

static Stack_1_d *createStack(int size);
static int push(Stack_1_d *stack, int val);
static int pop(Stack_1_d *stack);
static bool_t isEmpty(Stack_1_d *stack);


Queue_1_d *createQ(int size)
{
	Queue_1_d *Q = memset(malloc(sizeof(int)*size), 0, sizeof(int)*size);
	Q->stack1 = createStack(size);
	Q->stack2 = createStack(size);

	return Q;
}

static Stack_1_d *createStack(int size)
{
	Stack_1_d *stack = memset(malloc(sizeof(int)*size), 0, sizeof(int)*size);
	stack->val = memset(malloc(sizeof(int)*size), 0, sizeof(int)*size);
	stack->sp = -1;
	stack->size = size;
	return stack;
}

static bool_t isEmpty(Stack_1_d *stack)
{
	if (stack->sp == -1)
		return true;

	return false;
}

static int push(Stack_1_d *stack, int val)
{
	if (stack->sp+1 > stack->size)
	{
		printf("overflow\n");
		return -1;
	}

	stack->sp++;
	stack->val[stack->sp] = val;

	return 0; // success
}

static int pop(Stack_1_d *stack)
{
	int ret = -1;
	if (stack->sp == -1)
		printf("empty\n");

	ret = stack->val[stack->sp];
	stack->sp--;

	return ret;
}

static void enqueue(struct Queue *Q, int val)
{
	if (push(Q->stack1, val) == -1)
	{
		printf("Q is full\n");
	}
}

static int dequeue(struct Queue *Q)
{
	int retVal = 0;
//	if (isEmpty(Q->stack2))
//	{
//		while (!isEmpty(Q->stack1))
//		{
//			push(Q->stack2, pop(Q->stack1));
//		}
//
//		retVal = pop(Q->stack2);
//	}
//	else if (isEmpty(Q->stack2))
//	{
//		printf("Q is empty\n");
//	}
//	else
//	{
//		retVal = pop(Q->stack2);
//	}

	// fix: issue with popping.
    if (isEmpty(Q->stack2) && !isEmpty(Q->stack1))
    {
        while (!isEmpty(Q->stack1))
        {
            push(Q->stack2, pop(Q->stack1));
        }

        retVal = pop(Q->stack2);
    }
    else if (!isEmpty(Q->stack2))
    {
        retVal = pop(Q->stack2);
    }
    else
    {
        printf("Q is empty\n");
    }

	return retVal;
}


