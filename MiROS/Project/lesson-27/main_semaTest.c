#include "qpc.h"
#include "bsp.h"

Q_DEFINE_THIS_FILE

QXSemaphore SW1_sema_2;


void Send_Uart (uint8_t *str)
{
	QXSemaphore_wait(&SW1_sema_2, QXTHREAD_NO_TIMEOUT);
	QXThread_delay(BSP_TICKS_PER_SEC * 3U / 5U);
  // HAL_UART_Transmit(&huart2, str, strlen (str), HAL_MAX_DELAY);
	QXSemaphore_signal(&SW1_sema_2);
}


uint32_t stack_HPT[40];
QXThread HPT_Task_handle;
void HPT_Task (QXThread * const me)
{
	while (1)
	{
		char *strtosend = "Entered HPT and About to take Semaphore\n";
		//HAL_UART_Transmit(&huart2, str, strlen (str), HAL_MAX_DELAY);

		Send_Uart((uint8_t *)strtosend);

		//char *str2 = "Leaving HPT\n\n";
		//HAL_UART_Transmit(&huart2, str2, strlen (str2), HAL_MAX_DELAY);

		QXThread_delay(BSP_TICKS_PER_SEC * 3U / 5U);
	}
}

uint32_t stack_MPT[40];
QXThread MPT_Task_handle;
void MPT_Task (QXThread * const me)
{
	//uint8_t *strtosend = "IN MPT...........................\n";
	while (1)
	{
		char *strtosend = "Entered MPT and About to take Semaphore\n";
		//HAL_UART_Transmit(&huart2, str, strlen (str), HAL_MAX_DELAY);

		//Send_Uart((uint8_t *)strtosend);

		//char *str2 = "Leaving MPT\n\n";
		//HAL_UART_Transmit(&huart2, str2, strlen (str2), HAL_MAX_DELAY);

		QXThread_delay(BSP_TICKS_PER_SEC * 3U / 5U);
	}
}

uint32_t stack_LPT[40];
QXThread LPT_Task_handle;
void LPT_Task (QXThread * const me)
{
	//uint8_t *strtosend = "IN MPT...........................\n";
	while (1)
	{
		char *strtosend = "Entered LPT and About to take Semaphore\n";
		//HAL_UART_Transmit(&huart2, str, strlen (str), HAL_MAX_DELAY);

		Send_Uart((uint8_t *)strtosend);

		//char *str2 = "Leaving MPT\n\n";
		//HAL_UART_Transmit(&huart2, str2, strlen (str2), HAL_MAX_DELAY);

		QXThread_delay(BSP_TICKS_PER_SEC * 6U / 5U);
	}
}


int main_semaTest() {
    BSP_init();
    QF_init();

    /* initialize the SW1_sema semaphore as binary, signaling semaphore */
    QXSemaphore_init(&SW1_sema_2, /* pointer to semaphore to initialize */
                     1U,  /* initial semaphore count (singaling semaphore) */
                     1U); /* maximum semaphore count (binary semaphore) */

    /* initialize and start blinky1 thread */
    QXThread_ctor(&HPT_Task_handle, &HPT_Task, 0);
    QXTHREAD_START(&HPT_Task_handle,
                   5U, /* priority */
                   (void *)0, 0, /* message queue (not used) */
                   stack_HPT, sizeof(stack_HPT), /* stack */
                   (void *)0); /* extra parameter (not used) */

    /* initialize and start blinky2 thread */
    QXThread_ctor(&MPT_Task_handle, &MPT_Task, 0);
    QXTHREAD_START(&MPT_Task_handle,
                   2U, /* priority */
                   (void *)0, 0, /* message queue (not used) */
                   stack_MPT, sizeof(stack_MPT), /* stack */
                   (void *)0); /* extra parameter (not used) */

    /* initialize and start blinky3 thread */
    QXThread_ctor(&LPT_Task_handle, &LPT_Task, 0);
    QXTHREAD_START(&LPT_Task_handle,
                   1U, /* priority */
                   (void *)0, 0, /* message queue (not used) */
                   stack_LPT, sizeof(stack_LPT), /* stack */
                   (void *)0); /* extra parameter (not used) */

    /* transfer control to the RTOS to run the threads */
    return QF_run();
}
