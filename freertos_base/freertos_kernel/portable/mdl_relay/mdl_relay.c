/*
 * This module defines the system that is operating and monitoring multiple relays,
 * according to the prototypes which are defined in the header files.
 *
 */

#include <stdlib.h>
#include "mdl_relay.h"
#include "mdl_clock.h"


typedef struct RELaY_PINS
{
	uint32_t u32DOPin;
	uint32_t u32DIPin;
} tRelayPins;


/*
 * A Relay control block (RCB) is associated for each relay,
 * and stores Relay state information, including a mapping of the relay number with the pins.
 */
typedef struct RELAY_relayControlBlock
{
    uint32_t u32DOPin;                  /* Digital output pin */
    uint32_t u32DIPin;                  /* Digital input pin */
	  //tRelayPins tPins;;
    //uint8_t u8Config;                   /* NO or NC config */
    uint32_t u32LastState;              /* Last state requested */
    uint32_t u32MonitorResult;          /* Periodic Monitor result */
} tRCB;



//! ASSUMPTION: The relays input and output connections are done according to the below parameters.
//! ASSUMPTION: Consider the HW pin config to the Relay is hidden from the other systems and,
//!               they could only access using the API with relay-number.
/*
 * Configuration table to setup connections of the relays.
 * In any system, with specific table any relay can be connected to any Digital in or out Pins.
 */
#define TOTAL_NUM_OF_RELAYS_CONNECTED   0x05    // Assume 5 different relays are connected to system.
uint32_t CONNECTION_CONFIG[TOTAL_NUM_OF_RELAYS_CONNECTED*2] = {DO_index_00, DI_index_01,  // Relay-0
                                                               DO_index_01, DI_index_03,  // Relay-1
                                                               DO_index_02, DI_index_05,  // Relay-2
                                                               DO_index_03, DI_index_07,  // Relay-3
                                                               DO_index_04, DI_index_09,  // Relay-4
                                                               };


/*-----------------------------------------------------------
* static vaiable and function declarations
*----------------------------------------------------------*/
static void RELAY_MonitorRelays_HighPriority(void);

/*
 * A global variable to store the total number of relays connected in the system.
 */
static uint32_t g_u32TotalNumOfRelays;

/*
 * A global table to map each Relay with specific Relay control block.
 * It stores Relay state information, including a mapping of the relay number with the DI and DO pins.
 */
static tRCB *gpt_RelayControlBlk;

/*
 * Type by which tasks are referenced in FreeRtos
 */
//static TaskHandle_t xMonitorRelaysHandle;


/*-----------------------------------------------------------
* PUBLIC LIST API documented in mdl_relay.h
*----------------------------------------------------------*/
void RELAY_moduleInitAndRelaySetup(uint32_t CONNECTION_CONFIG[], uint32_t u32TotalNumOfRelays)
{
    // Step-0: Check for invalid params.
    if (CONNECTION_CONFIG == NULL)
    {
        return; // Invalid-Param.
    }

    // Step-1: allocate memory for a table to map each Relay with specific Relay control block.
    gpt_RelayControlBlk = malloc(sizeof(tRCB) * u32TotalNumOfRelays);
    g_u32TotalNumOfRelays = u32TotalNumOfRelays;

    // Step-2: init the configuration for all the relays based on either NO or NC for all.
    //! ASSUMPTION: Consider NO-config, so Vcc of the primary-circuit is connected to +12V and INT is connected to DO pins.
    for (uint32_t index = 0; index < u32TotalNumOfRelays; index++)
    {
        // DO-0, DO-1, DO-2, DO-3, DO-4
        // DI-1, DI-3, DI-5, DI-7, DI-9
        gpt_RelayControlBlk[index].u32DOPin = CONNECTION_CONFIG[index*2];
        gpt_RelayControlBlk[index].u32DIPin = CONNECTION_CONFIG[index*2+1];

        // Incase of NO-config, relays are open.
        // Here, semaphore is not required as the "Monitor task" has not been registered yet and
        // hence race-condition wont occur right now.
        DO_setOutputState((DO_index_E)gpt_RelayControlBlk[index].u32DOPin, DO_state_ON);

        gpt_RelayControlBlk[index].u32LastState = (uint32_t) DO_state_ON;
        gpt_RelayControlBlk[index].u32MonitorResult = (uint32_t) RELAY_state_NO_ERROR;
    }

    // Step-3: Registration of the HPT-handler or could consider it is done one-time by the startup.
    //xTaskCreate(RELAY_MonitorRelays_HighPriority, "Monitor-Relays", 512, NULL, MONITOR_RELAYS_HIGH_PRIORITY, &xMonitorRelaysHandle);
}


void RELAY_relayCleanup(void)
{
    // Step-1: De-allocate or free the memory allocated during setup.
    free(gpt_RelayControlBlk);

    // Step-2: rest the number of relays.
    g_u32TotalNumOfRelays = 0;

    // Step-3: Delete the monitor Task created during setup.
    //vTaskDelete(xMonitorRelaysHandle);
}


RELAY_state_E RELAY_controlRequest(uint32_t u32Relay_Num, uint32_t u32Control, uint32_t u32Delay)
{
    // Step-0: Check for invalid params.
    if (u32Relay_Num >= g_u32TotalNumOfRelays)    // out-of-bound
    {
        return RELAY_state_WELDED; // Invalid-Param, could return any-error status.
    }

    CLOCK_ticks_T tStartTime = CLOCK_getTicks();
    RELAY_state_E eRelayState = RELAY_state_NO_ERROR;

    // For NO config: set the status accordingly.
    //                 +----------+                                   +----------+
    // (12V) Vcc |-----|          |                   (12V) Vcc |-----|          |
    //                 |          |---- open                          |          |---- close
    // (12V) INT |-----|          |                   (0V)  INT |-----|          |
    //                 +----------+                                   +----------+
    //
    //          Relay is open                                   Relay is close
    //
    DO_state_E digitalOut = (u32Control == CONTROL_RELAY_CLOSE) ? DO_state_OFF : DO_state_ON; // If the u32Control is wrong, relay shall stay open.

    //! ASSUMPTION: Consider time overhead of the function calling and setting DO's as negligible while calculating delay.
    // Step-1: wait for specified delay.
    while (tStartTime + u32Delay <= CLOCK_getTicks());

    // Step-2: set the control of the specific relay.
    // It will be simultaneously read by the HPT to monitor the Relays status.
    // In this case, to avoid race-condition Semaphore shall be used.

    //! ASSUMPTION: Or could consider "DO_setOutputState()" and "DI_getInputState()" are already protected against race-condition.
    //---------------------------------------------------
    // Semaphore_wait(&sema) <--- Semaphore acquired!!!
    DO_setOutputState((DO_index_E)(gpt_RelayControlBlk[u32Relay_Num].u32DOPin), digitalOut);

    //! ASSUMPTION: we can only set the DO's and we can only read the DI's.
    //!             That's why store the last-control set here, in the table for later comparison by Monitor task.
    // set the current set state as the LastState int RCB
    gpt_RelayControlBlk[u32Relay_Num].u32LastState = (uint32_t)digitalOut;

    // Step-3: Read the feedback from digital input.
    //! ASSUMPTION: DI will be set automatically if the corresponding DO is set (useful for verification).
    DI_state_E inState = DI_getInputState((DI_index_E)(gpt_RelayControlBlk[u32Relay_Num].u32DIPin));

    // Semaphore_signal(&sema) <--- Semaphore released!!!
    //---------------------------------------------------

    // Step-4: Verify the control that is just set with the feedback.
    if (((uint16_t)digitalOut != (uint16_t)inState) && (inState != DO_state_OFF)) // relay should have been close.
    {
        eRelayState = RELAY_state_CONSTANTLY_OPEN;
    }
    else if (((uint16_t)digitalOut != (uint16_t)inState) && (inState != DO_state_ON)) // relay should have been open.
    {
        eRelayState = RELAY_state_WELDED;
    }

    return eRelayState;
}


uint32_t RELAY_getCurrentStatus(uint32_t u32Relay_Num)
{
    // read the digital input pin of the specific relay.
    return (DI_getInputState((DI_index_E)(gpt_RelayControlBlk[u32Relay_Num].u32DIPin)) == DI_state_ON) ? CONTROL_RELAY_CLOSE : CONTROL_RELAY_OPEN;	// todo:ms: fix return status, values should be swapped.
}

uint32_t RELAY_detectError(uint32_t u32Relay_Num)
{
    // read the relay-state(from the table) updated by monitor task.
    return gpt_RelayControlBlk[u32Relay_Num].u32MonitorResult;
}


/*****************************************************************************************************//**
* @details A Task Handler function to continuously monitor the status of the Relays to detect errors.
*          Compares what was set during control-request with reading current status via Digital input.
*
*          REQUIREMENT-3: Continuously monitors relay by reading its feedback status (if it has one)
*                         over system digital inputs.
*          REQUIREMENT-4: Detects errors by recognizing a mismatch between relays control and feedback line.
*          REQUIREMENT-7: Exists in a simple task scheduler environment as a task/thread/process that needs
*                         separated functionality for setup/initialization, run-time task execution,
*                         and post-execution/destruction.
*          REQUIREMENT-8: Will assume periodic calls from the scheduler of its run-time task
*                         execution part (function).
*********************************************************************************************************
* @param [in] Nothing.
* @return Nothing.
********************************************************************************************************/
static void RELAY_MonitorRelays_HighPriority(void)
{
    //! ASSUMPTION: this function is called periodically from the scheduler.
    //! ASSUMPTION: 1) Could update the error-status to IOT module or the LED display screen,
    //!             2) trigger emergency stop or circuit breaker to avoid damage to the device,
    //!             3) or update status back to the table to be diagnosed by the other systems via API.

    // Step-1: Loop over all the relay control block.
    for (int index = 0; index < g_u32TotalNumOfRelays; index++)
    {
        //---------------------------------------------------
        // Semaphore_wait(&sema) <--- Semaphore acquired!!!

        // Step-2: Read the feedback from digital input.
        DI_state_E curState = DI_getInputState((DI_index_E)(gpt_RelayControlBlk[index].u32DIPin));

        gpt_RelayControlBlk[index].u32MonitorResult = RELAY_state_NO_ERROR;

        // Step-3: Verify the last control set with the feedback,
        //         and store the Relay state back to the table for later detection by other sub-systems.
        if (((uint32_t)gpt_RelayControlBlk[index].u32LastState != (uint32_t)curState) && (curState != DO_state_OFF))
        {
            gpt_RelayControlBlk[index].u32MonitorResult = RELAY_state_CONSTANTLY_OPEN; // relay should have been close.
        }
        else if (((uint32_t)gpt_RelayControlBlk[index].u32LastState != (uint32_t)curState) && (curState != DO_state_ON))
        {
            gpt_RelayControlBlk[index].u32MonitorResult = RELAY_state_WELDED; // relay should have been open.
        }
        // Semaphore_signal(&sema) <--- Semaphore released!!!
        //---------------------------------------------------
    }
}
