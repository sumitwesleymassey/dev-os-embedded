/*
 * Grid.c
 *
 *  Created on: 18-Apr-2022
 *      Author: sumit
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


void Grid_FindNumofIslands(void);


// ----------------------------------------------------
// Main-driver function
// ----------------------------------------------------
void Grid_Program(void)
{
	// Matrix
	printf("find Number of Islands: \n");
	Grid_FindNumofIslands();
}


// ----------------------------------------------------
// find number of islands
// ----------------------------------------------------
static void solve(int i, int j, int m, int n, int grid[m][n], int visited[m][n]);
int ROW[8] = {-1,  1,  0,  0, -1, -1,  1, 1};
int COL[8] = { 0,  0, -1,  1, -1,  1, -1, 1};

void Grid_FindNumofIslands(void)
{
	int m=2;
	int n=7;
	int grid[2][7] = {{0, 1, 1, 1, 0, 0, 0},
					  {0, 0, 1, 1, 0, 1, 0}};

	int visited[2][7];
	memset(visited, 0, sizeof(int)*2*7);
	int count=0;

	for (int i=0; i<m; i++)
	{
		for (int j=0; j<n; j++)
		{
			if (grid[i][j]!=0 && visited[i][j]==0)	// if its a land only and not-visited
			{
				count++;
				solve(i, j, m, n, grid, visited);
			}
		}

	}

	printf("num of islands = %d\n", count);
}


static void solve(int i, int j, int m, int n, int grid[m][n], int visited[m][n])
{
	// bound
	if ( i<0 || i>=m || j<0 || j>=n)
		return;

	// base
	if (grid[i][j] == 0 || visited[i][j] == 1)	// already visited and is not a land.
		return;

	// mark
	visited[i][j] = 1;

	for (int k=0; k<8; k++)
	{
		solve(i+ROW[k], j+COL[k], m, n, grid, visited);
	}
}

