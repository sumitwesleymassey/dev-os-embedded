/*
 * Queue.h
 *
 *  Created on: 03-Apr-2022
 *      Author: sumit
 */

#ifndef INCLUDE_QUEUE_H_
#define INCLUDE_QUEUE_H_


void *creatQ(int size);
void deleteQ(void *Q);
void enqueue(void *Q, void *element, int qSize);
void *dequeue(void *Q, int qSize);
bool isQueueEmpty(void *Q);


#endif /* INCLUDE_QUEUE_H_ */
