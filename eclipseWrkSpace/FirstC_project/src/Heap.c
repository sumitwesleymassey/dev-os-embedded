/*
 * Heap.c
 *
 *  Created on: 05-Mar-2022
 *      Author: sumit
 */


#include <stdio.h>
#include <stdlib.h>
#include "Sorting.h"
#include "Heap.h"

/**
 *   Corresponding Complete Binary Tree is:            |
 *                    1                                |                    17
 *                 /     \                             |                 /      \
 *               3         5                           |               15         13
 *            /    \     /  \                          |             /    \      /  \
 *           4      6   13  10                         |            9      6    5   10
 *          / \    / \                                 |           / \    /  \
 *         9   8  15 17                                |          4   8  3    1
 *                                                     |
 *                                                     |
 *
 *
 *
 *
 * Note:
 * Root is at index 0 in array.
 * Left child of i-th node is at (2*i + 1)th index.
 * Right child of i-th node is at (2*i + 2)th index.
 * Parent of i-th node is at (i-1)/2 index.
 *
 * Last non-leaf node = parent of last-node.
 * or, Last non-leaf node = parent of node at (n-1)th index.
 * or, Last non-leaf node = Node at index ((n-1) - 1)/2.
 *                        = (n/2) - 1.
 *
 *
 *
 */

void Heap_DeleteRootNode(int *pa, int size);
void Heap_InsertNode(int *pa, int size, int newNode);
void Heap_Sorting_HeapSortArray(int *pa, int size);

static void Heap_CreateHeap(int a[], int size);
static void Heap_PrintHeap(int a[], int size);
static void Heap_Heapify(int a[], int size, int index);
static void swap(int *pa, int *b);

static void Heap_HeapifyRecursive(int *pa, int size, int index);

#define GREATER(x, y) ( ((x) > (y))? &x:&y )
#define SMALLER(x, y) ( ((x) < (y))? &x:&y )
#define OPTIMISATION
#define MAX_HEAP


// ----------------------------------------------------
// Merge Sorting
// ----------------------------------------------------
void Heap_BinaryTree_Heap(void)
{
    //int a[] = {4, 10, 3, 5, 1};
    //int a[] = {1, 3, 5, 4, 6};
    int a[] = {1, 3, 5, 4, 6, 13, 10, 9, 8, 15, 17};
    int size = (sizeof(a)/4);

    // create heap
    Heap_CreateHeap(a, size);

    // print heap
    Heap_PrintHeap(a, size);

    // delete heap node
    Heap_DeleteRootNode(a, size);

    // heap size is reduced by 1.
    // so last element is not consider.
    size--;
    // print heap
    Heap_PrintHeap(a, size);

    //insert heap node
    size++;
    Heap_InsertNode(a, size, 11);

    // print heap
    Heap_PrintHeap(a, size);

    // sorting via heap-sort
    printf("/********Heap-Sort************/\n");
    Heap_Sorting_HeapSortArray(a, size);
    Heap_PrintHeap(a, size);
}


/**
 * HeapSort, will lead to an array which wont be Heap anymore
 *
 */
void Heap_Sorting_HeapSortArray(int *pa, int size)
{
    // exit statement: when last node is left then return.
    if (size==1)
        return;

    // delete heap node
    Heap_DeleteRootNode(pa, size);

    size--;

    // call recursively with reduced length
    // and it will delete node and add it to the last
    Heap_Sorting_HeapSortArray(pa, size);
}


/**
 * Create Heap,
 */
static void Heap_CreateHeap(int *pa, int size)
{
    // find last non-leaf node.
    //      Last node i = n-1
    //      parent of last node = (i-1)/2 == ((n-1)-1)/2
    int lastNonLeafNode = ((size-1) - 1)/2;

    int i;
    for (i=lastNonLeafNode; i>=0; i--)
    {
        // heapify the passed array.
        //Heap_Heapify(pa, size, i);
        Heap_HeapifyRecursive(pa, size, i);
    }

    //Heap_HeapifyRecursive(pa, size, 0);
}

/**
 * Heapify starts @index, below its child all are already in heap format.
 * so no need to heapify the child's child.
 */
static void Heap_HeapifyRecursive(int *pa, int size, int index)
{
    // exit condition
    if (index<0) {
        return;
    }

    {// check and swap if necessary
        int *child;

#ifdef MAX_HEAP
        if ((index*2)+2 < size )
            child = (int *)GREATER(pa[(index*2)+1], pa[(index*2)+2]);
        else if ((index*2)+1 < size )
            child = &pa[(index*2)+1];
        else
            return;

        // if parent is lesser than the child, then swap
        // with the greater child
        if (pa[(index)] < *child)
#else // MIN_HEAP

        if ((index*2)+2 < size )
            child = (int *)SMALLER(pa[(index*2)+1], pa[(index*2)+2]);
        else if ((index*2)+1 < size )
            child = &pa[(index*2)+1];
        else
            return;

        if (pa[(index)] > *child)
#endif
        {
            swap(&pa[index], child);
#ifdef OPTIMISATION
            index = child-pa;

            {// just heapify at the largest one, other child and its child's child will be already heapified.
             // one recursive call should be enough
                Heap_HeapifyRecursive(pa, size, index);
            }
#endif
        }

#ifndef OPTIMISATION
        // left
        index++;
        Heap_HeapifyRecursive(pa, size, index);

        // right
        index++;
        Heap_HeapifyRecursive(pa, size, index);
#endif
    }
}


/**
 * Heapify Method-2
 */
static void Heap_Heapify(int *pa, int size, int index)
{
    int i;

    // -ve index check
    if (index <0)
        return;

    // check if the nodes
    for (i=index; i<size; i++)
    {
        // exit condition
        if (((i+1)*2) +1 >size)
            break;

        int *child = (int *)GREATER(pa[((i+1)*2)-1], pa[((i+1)*2)]);

        // if parent is lesser than the child, then swap
        // with the greater child
        if(pa[(i)] < *child)
        {
            swap(&pa[i], child);
        }
    }
}


/**
 * Swap the values
 */
static void swap(int *pa, int *b)
{
	int temp = *pa;
	*pa = *b;
	*b=temp;
}


/**
 * Print Heap
 */
static void Heap_PrintHeap(int a[], int size)
{
	int i;
	for (i=0; i<size; i++)
	{
		printf("%d ", a[i]);
	}
	
	printf("\n\n");
}


/**
 * insertion in Heap, only added at last leaf and,
 * then just heapify a Heap.
 */
void Heap_InsertNode(int *pa, int size, int newNode)
{
	pa[size-1] = newNode;
	// its not heap now, so create heap or heapify.
	Heap_CreateHeap(pa, size);
}


/**
 * Deletion in Heap, only root can be deleted and last leaf comes at the top
 * then heapify a Heap.
 */
void Heap_DeleteRootNode(int *pa, int size)
{
	// swap root and last node.
	int root = pa[0];
	pa[0] = pa[size-1];
	pa[size-1] = root;
	
	// reduce heap size by 1.
	size--;
	
	// its not heap now, so create heap or heapify.
	Heap_CreateHeap(pa, size);
}
