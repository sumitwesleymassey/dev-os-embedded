/*
 * Sorting_revision.c
 *
 *  Created on: 09-Mar-2022
 *      Author: sumit
 */

#include<stdio.h>
//#include<string.h>
#include<stdlib.h>
#include "Util.h"

static void Sorting_Rev_Print(int a[], int size);
void Sorting_Rev_QuickSort(int *pa, int start, int end);
static void swap(int *piOne, int *piTwo);


static void Sorting_Rev_MergeSort(int *a, int size);
static void Sorting_Rev_Merge(int *left, int *right, int *a, int size);

// ----------------------------------------------------
// Merge Sorting
// ----------------------------------------------------
void Sorting_Rev_MergeSortProgram()
{
	int a[] = {23, 4, 56, 13, 21, 37, 1, 3};
	int size = SIZEOFARRAY(a);

	// call recursive sort
	Sorting_Rev_MergeSort(a, size);

	// print
	Sorting_Rev_Print(a, size);
}

static void Sorting_Rev_MergeSort(int *a, int size)
{
	// divide array
	int middle = size/2;
	int leftSize = middle;
	int rightSize = size - middle;

	// exit condition
	if(size<=1)
		return;

	// create and copy to new sub-arrays
	int *leftArray = (int *)malloc(leftSize);
	int *rightArray = (int *)malloc(rightSize);

	int i=0, l=0, r=0;
	while (i<size)
	{
		if(i<leftSize)
		{
			leftArray[l] = a[i];
			i++;
			l++;
		}
		else
		{
			rightArray[r] = a[i];
			i++;
			r++;
		}
	}

	// call sort for left and right
	Sorting_Rev_MergeSort(leftArray, leftSize);
	Sorting_Rev_MergeSort(rightArray, rightSize);

	// Merge sub-arrays into primary array
	Sorting_Rev_Merge(leftArray, rightArray, a, size);
}


static void Sorting_Rev_Merge(int *left, int *right, int *a, int size)
{
	// left and right are already sorted in them.
	int i=0,l=0,r=0;

	// compare left < right then left ---> a
	while(l<(size/2) && r<(size-(size/2)))
	{
		if(left[l]<right[r])
		{
			a[i] = left[l];
			i++;
			l++;
		}
		else
		{
			a[i] = right[r];
			i++;
			r++;
		}
	}

	// remaining left
	while(l<(size/2))
	{
		a[i] = left[l];
		i++;
		l++;
	}

	// remaining right
	while(r<(size-(size/2)))
	{
		a[i] = right[r];
		i++;
		r++;
	}
}




// ----------------------------------------------------
// Quick Sorting
// ----------------------------------------------------
void Sorting_Rev_QuickSortProgram()
{
	int a[] = {23,  4, 56, 13, 21, 37, 1};
	int size = sizeof(a)/sizeof(a[0]);

	// call recursive sort
	Sorting_Rev_QuickSort(a, 0, size-1);

	// print
	Sorting_Rev_Print(a, size);
}


//
void Sorting_Rev_QuickSort(int *pa, int start, int end)
{
	int j = start;
	int i = start-1;
	int pivot = end; //end is pivot

	// exit condition
	if(start>end)
		return;

	for(;j<pivot; j++)
	{
		if(pa[j]<pa[pivot])
		{
			i++;
			swap(&pa[i], &pa[j]);
		}
	}

	// pivot will be getting its place @(i+1).
	i++;
	swap(&pa[i], &pa[pivot]);
	pivot = i;

	// call left and right
	Sorting_Rev_QuickSort(pa, start, pivot-1);
	Sorting_Rev_QuickSort(pa, pivot+1, end);
}

static void swap(int *piOne, int *piTwo)
{
	if(piOne != piTwo) // if same ptr, then skip swap.
	{
		int temp = *piOne;
		*piOne = *piTwo;
		*piTwo = temp;
	}
}

static void Sorting_Rev_Print(int a[], int size)
{
	int i;
	for(i=0; i<size; i++)
		printf("%d ", a[i]);

	printf("\n");
}
