/*
 * solve.c
 *
 *  Created on: 21-Nov-2021
 *      Author: sumit
 */

#define MAX(a,b)    (((unsigned int)a > (unsigned int)b)? a:b)
#define MIN(a,b)    (((unsigned int)a < (unsigned int)b)? a:b)

int main_egg(void)
{
	return superEggDrop(2,6);
}

int superEggDrop(int k, int n){

    int min_attempt = 0xFFFFFFFF;

    //base condition and exit
    if(n==0 || n==1 || k==1)
    {
        return n;
    }

    //recursion: start dropping from x-floor
    int x=1;



    while(x<=n)
    {
        // add '1' for every attempt,
        //  if breaks reduce eggs and search on below floors
        //  else doesn't break reuse egg and search remaining above floors i.e. (n-x).
        int temp = 1 + MAX(superEggDrop(k-1, x-1), superEggDrop(k, n-x));
        min_attempt = MIN(min_attempt, temp);
        x++;
    }

    return min_attempt;
}
