/*
 * BinaryTrees.cpp
 *
 *  Created on: 03-Apr-2022
 *      Author: sumit
 */

//-------------------------
// Header file incusions.
#include <iostream>
#include <stdlib.h>
#include "Queue.h"

using namespace std;


//-------------------------
// typedefs
typedef struct TreeNode
{
	int data;
	TreeNode *left;
	TreeNode *right;
}TreeNode_d;


typedef enum mode
{
	MODE_PRE_ORDER = 0,
	MODE_IN_ORDER,
	MODE_POST_ORDER
}e_mode;


//-------------------------
// prototype declaration
TreeNode *BinaryTrees_create(int arrayNode[], int size);
void BinaryTrees_printTree(TreeNode_d *root, e_mode mode);
TreeNode_d *BinaryTrees_insertNode(TreeNode_d *root, TreeNode_d *newNode);


void BinaryTrees_FindAndPrintDiagonals(TreeNode_d *root, int size);

//-------------------------
// Function definitions

/**
 * Entry to the binary trees program.
 */
void BinaryTrees_Program(void)
{
	int arrayNode[] = {5, 1, 9, 2, 7, 3, 6, 4, 8};
	int size = sizeof(arrayNode)/sizeof(arrayNode[0]);

	TreeNode_d *root = NULL;
	root = BinaryTrees_create(arrayNode, size);

	// print binary tree;
	BinaryTrees_printTree(root, MODE_IN_ORDER);

	cout << "\n";

	// diagonals distance for each node
	BinaryTrees_FindAndPrintDiagonals(root, size);
}


TreeNode_d *BinaryTrees_create(int arrayNode[], int size)
{
	TreeNode *root = NULL;

	for (int i=0; i<size ; i++)
	{
		TreeNode_d *newNode = (TreeNode_d *)malloc(sizeof(TreeNode_d));
		newNode->data = arrayNode[i];
		newNode->left = NULL;
		newNode->right = NULL;

		root = BinaryTrees_insertNode(root, newNode);
	}

	return root;
}


TreeNode_d *BinaryTrees_insertNode(TreeNode_d *root, TreeNode_d *newNode)
{
	//base condition
	if(root == NULL)
		return newNode;

	if(newNode->data > root->data){
		root->right = BinaryTrees_insertNode(root->right, newNode);
	}
	else
	{
		root->left = BinaryTrees_insertNode(root->left, newNode);
	}

	return root;
}


void BinaryTrees_printTree(TreeNode_d *root, e_mode mode)
{
	if (root == NULL)
		return;

	if (mode == MODE_PRE_ORDER)
		cout << root->data << " ";	// print current node

	// traverse left
	BinaryTrees_printTree(root->left, mode);

	if (mode == MODE_IN_ORDER)
		cout << root->data << " ";	// print current node

	// traverse right
	BinaryTrees_printTree(root->right, mode);

	if (mode == MODE_POST_ORDER)
		cout << root->data << " ";	// print current node
}

/**
 *
 * diagonals of BT:
 *						Q: 1 | NULL
 *          1			Q:   | NULL | 2
 *        /  \			Q:          | 2 |NULL 				<== d0 done
 *       2    3			Q:              |NULL | 4 | x |
 *     /   \   \		Q:					  | 4 | X |	NULL |	<== d1 done
 *    4     5   y		Q:						  | x | NULL | 6 |
 *   / \   / \   \		Q:							  | NULL | 6 |
 *  6   7 x   8   9     Q: 								     | 6 | NULL | <== d2 done
 *						Q:									     | NULL |
 *						Q:  									        | NULL | <== d3 done
 *
 *
 *		+-------------------------------------------------------+
 *		| 1 | null  | 2 | NULL | 4 | x | NULL | 6 | NULL | NULL | .....    <---- filling
 *		+-------------------------------------------------------+
 *
 *	Here, d0 level '1' is the only left child
 *		  d1 level '2' is the only left child
 *		  d2 level '4' & 'x' are the left child
 *		  d3 level '6' is the only left child
 *
 *
 *
 *                 	     5
 *                 	   /   \
 *                    /     \
 *                   /    	 \
 *                  / 		  \
 *    l-most--->  1 		   9
 *                  \ 	  	   /
 *                   2  	  7
 *                    \	     / \
 *                     3	6	8 <----r-most
 *                 	    \
 *                       4
 *
 *
  *		+-------------------------------------------------------+
 *		| 5 | null  | 1 | 7 | NULL | 6 | NULL | NULL | .....    <---- filling
 *		+-------------------------------------------------------+
 *
 */
void BinaryTrees_FindAndPrintDiagonals(TreeNode_d *root, int size)
{
	/* preconditions:
	//				1) if 2 consecutive NULLs in the queue that means one diagonal is done.
	//
	// enqueue(root)
	// enqueue(NULL)	// delimeter
	// while(q is not empty)
	//   p = dequeue
	//	 if p == NULL
	//		enqueue(NULL)
	// 		p = dequeue
	//		if (p == NULL)
	//			break;
	//
	//	 // print whole right series as it has the same diagonal distance
	//   // and add every left child to the queue.
	//   while(p != NULL)
	//		print(p)
	//		if (p->left!=NULL)
	//			enqueue(p->left);
	//		p = p->right;
	*/

	//
	int qSize = size*sizeof(TreeNode_d *);	// no of elements * size of pointer to node
	TreeNode_d *curNode = NULL;

	// create Q
	void *Q = creatQ(qSize);

	enqueue(Q, root, size);
	enqueue(Q, NULL, size);

	while (!isQueueEmpty(Q))	// if Q is not empty
	{
		curNode = (TreeNode_d *)dequeue(Q, size);

		if (curNode == NULL) // all diagonals of prev level is done
		{
			cout << "\n"; // one level done
			enqueue(Q, NULL, size);
			curNode = (TreeNode_d *)dequeue(Q, size);
			if (curNode == NULL)	// end has reached.
				break;
		}

		// traverse all the right nodes and enqueue all the left nodes
		while (curNode != NULL)
		{
			cout << curNode->data << " ";
			if (curNode->left != NULL)
				enqueue(Q, curNode->left, size);

			curNode = curNode->right;
		} // exit of the loop means all the nodes of same diagonal distance are complete, so deQ next and continue traversal
	}


	// delete Q
	deleteQ(Q);
}
