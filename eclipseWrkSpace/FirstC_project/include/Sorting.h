#ifndef _SORTING_H__
#define _SORTING_H__

// to find size
//#define SIZEOFARRAY(var, type) (sizeof(var)/sizeof(type))


void Sorting_MergeSortProgram(void);
void Sorting_QuickSortProgram(void);

#endif /* _SORTING_H__ */
