/*
 * DynamicProgramming.c
 *
 *  Created on: 02-Jun-2022
 *      Author: sumit
 */
#include <stdio.h>
#include <stdlib.h>
#include "Util.h"
#include "DynamicProgramming.h"

void DP_MaxProfit_RODCutting(void);
void DP_MaxProfit_RODCutting_optimised(void);
void DP_LPS(void);
void DP_LPS_recursion(void);
void DP_count_PS(void);
void DP_Weighted_Job_Scheduling(void);
void DP_LongestSquare_SubMatrix_of_1s(void);
void DP_LargestSumIn_SubArray_KADANES_ALGO(void);
void DP_MinCostPath_Program(void);
void DP_MaxDiff_of_zeros_and_ones_Program(void);
void DP_MinJumps_Program(void);

// ----------------------------------------------------
// Main-driver function
// ----------------------------------------------------
void DynamicProgramming_Program(void)
{
	// DP : Rod-cutting.
	printf("Rod-cutting: \n");
	DP_MaxProfit_RODCutting();
	printf("\n"); // EOL

	// DP : Rod-cutting.
	printf("Rod-cutting optimised: \n");
	DP_MaxProfit_RODCutting_optimised();
	printf("\n"); // EOL

	// DP : longest-palindromic-subsequence.
	printf("longest-palindromic-subsequence: \n");
	DP_LPS();
	printf("\n"); // EOL

	// DP : longest-palindromic-subsequence.
	printf("longest-palindromic-subsequence (Rec): \n");
	DP_LPS_recursion();
	printf("\n"); // EOL

	// DP : count all palindromic-subsequence.
	printf("count all palindromic-subsequence (repetitive at diff index a0, a1, a3 are conisdered diferent char): \n");
	DP_count_PS();
	printf("\n"); // EOL

	// DP : weighted-job-scheduling
	printf("weighted-job-scheduling: \n");
	DP_Weighted_Job_Scheduling();
	printf("\n"); // EOL

	// DP : sub-matrix of all 1's
	printf("sub-matrix of all 1's: \n");
	DP_LongestSquare_SubMatrix_of_1s();
	printf("\n"); // EOL

	// DP : KADANE's ALGO
	printf("Largest Sum in a subarray: \n");
	DP_LargestSumIn_SubArray_KADANES_ALGO();
	printf("\n"); // EOL

	// DP : min-cost path
	printf("min-cost path: \n");
	DP_MinCostPath_Program();
	printf("\n"); // EOL

	// DP : max diff btw 1s and 0s using KADANE
	printf("max diff btw 1s and 0s: \n");
	DP_MaxDiff_of_zeros_and_ones_Program();
	printf("\n"); // EOL

	// DP : min jumps
	printf("min jumps: \n");
	DP_MinJumps_Program();
	printf("\n"); // EOL
}


// ----------------------------------------------------
// DP : Rod-cutting
// ----------------------------------------------------
void DP_MaxProfit_RODCutting(void)
{
	int arr[] = {1, 5, 8, 9, 10, 17, 17, 20};
	int n = sizeof(arr)/sizeof(arr[0]);

	// dp
	int *dp = memset(malloc((sizeof(int)*n) +1), 0, (sizeof(int)*n)+1);
	memcpy(dp+1, arr, (sizeof(int)*n));

	//int max = INT_MIN;

	for (int i=2; i<n+1; i++)
	{
		for (int j=1; j<=(i/2); j++)
		{
			/**
			 * METHOD-1:
			 * max profit for ith sized rod = maxof(whole-j + remaining-optimised-(j-i)*, whole-i)
			 * 		max-profit for 3len rod = (whole-1 len rod + optimised 2len rod) vs whole-3 len rod 		<-- iteration 1
			 * 		 						= (whole-2 len rod + optimised 1len rod) vs whole-3 len rod			<-- iteration 2
			 *
			 *
			 * Example-2: 	loop of j runs only half of the targeted rod len i.e. j <= i/2 == 7/2 == 3 times only
			 * 		max-profit for 7len rod = (whole-1 len rod + optimised 6len rod) vs whole-7 len rod 		<-- iteration 1
			 * 		 						= (whole-2 len rod + optimised 5len rod) vs previous-profit			<-- iteration 2
			 * 		 						= (whole-3 len rod + optimised 4len rod) vs previous-profit			<-- iteration 3
			 * 		 						= (whole-4 len rod + optimised 3len rod) vs previous-profit			<-- iteration 4 ---------
			 * 		 						= (whole-5 len rod + optimised 2len rod) vs previous-profit			<-- iteration 5		| after this point onwards
			 * 		 						= (whole-6 len rod + optimised 1len rod) vs previous-profit			<-- iteration 6		| the combination is
			 * 		 						= (whole-7 len rod + optimised 0len rod) vs previous-profit			<-- iteration 7		| repetitive.
			 *
			 */
			//dp[i] = MAX((arr[j-1] + dp[i-j]), dp[i]);


			/**
			 * METHOD-2:
			 * max profit for ith sized rod = maxof(optimised-j + remaining-optimised-(j-i)*, whole-i)
			 * 		max-profit for 3len rod = (optimised-1 len rod + optimised 2len rod) vs whole-3 len rod 		<-- iteration 1
			 * 		 						= (optimised-2 len rod + optimised 1len rod) vs whole-3 len rod			<-- iteration 2
			 *
			 *
			 * Example-2: 	loop of j runs only half of the targeted rod len i.e. j <= i/2 == 7/2 == 3 times only
			 * 		max-profit for 7len rod = (optimised-1 len rod + optimised 6len rod) vs whole-7 len rod 		<-- iteration 1
			 * 		 						= (optimised-2 len rod + optimised 5len rod) vs previous-profit			<-- iteration 2
			 * 		 						= (optimised-3 len rod + optimised 4len rod) vs previous-profit			<-- iteration 3
			 * 		 						= (optimised-4 len rod + optimised 3len rod) vs previous-profit			<-- iteration 4 ---------
			 * 		 						= (optimised-5 len rod + optimised 2len rod) vs previous-profit			<-- iteration 5		| after this point onwards
			 * 		 						= (optimised-6 len rod + optimised 1len rod) vs previous-profit			<-- iteration 6		| the combination is
			 * 		 						= (optimised-7 len rod + optimised 0len rod) vs previous-profit			<-- iteration 7		| repetitive.
			 *
			 */

			// IMP: confusion is for above example both gave the correct answer.
			//  	may look for clarifficatio during revision.
			dp[i] = MAX((dp[j] + dp[i-j]), dp[i]);

			/*if (max < dp[i])
				max = dp[i];*/
		}
	}

	//printf("Max_profit = %d\n", max);
	printf("Max_profit = %d\n", dp[n]);
}


// ----------------------------------------------------
// DP : Rod-cutting with left-right strategy
// ----------------------------------------------------
void DP_MaxProfit_RODCutting_optimised(void)
{
	int arr[] = {1, 5, 8, 9, 10, 17, 17, 20};
	int n = sizeof(arr)/sizeof(arr[0]);

	// dp
	int *dp = memset(malloc((sizeof(int)*n) +1), 0, (sizeof(int)*n)+1);
	memcpy(dp+1, arr, (sizeof(int)*n));

	for (int i=2; i<n+1; i++)
	{
		int li= 1;
		int ri = i-1;

		while(li <= ri)
		{
			if ((dp[li] + dp[ri]) > dp[i])
			{
				dp[i] = dp[li] + dp[ri];
			}

			li++;
			ri--;
		}
	}

	printf("Max_profit = %d\n", dp[n]);
}



// ----------------------------------------------------
// DP : Longest-Palindromic subsequence
// ----------------------------------------------------
void DP_LPS(void)
{
	// a0 b1 k2 c3 c4 b5 c6
	//char *str = "abkccbc";
	char *str = "abaa";
	int n = strlen(str);

	int dp[n][n];
	memset(dp, 0, sizeof(int)*n*n);

	// init diagonals with '1'
	for(int k=0; k<n; k++)
		dp[k][k] = 1;

	for (int k=1; k<n; k++)
	{
		for (int i=0, j=k; j<n; j++, i++)
		{
			// if c1 == c2, then dp = 2 + set(m)
			if(str[i] == str[j])
			{
				dp[i][j] = 2 + dp[i+1][j-1]; // diagonal south-west
			}
			else // if c1 != c2 then dp = max( c1-set(m), set(m)-c2)
			{
				dp[i][j] = MAX(dp[i][j-1], dp[i+1][j]);
			}
		}
	}
	printf("LPS = %d\n", dp[0][n-1]);
}


// ----------------------------------------------------
// DP : Longest-Palindromic subsequence
// ----------------------------------------------------
static void solve(int i, int j, int n, int dp[n][n], char *str);

void DP_LPS_recursion(void)
{
	// a0 b1 k2 c3 c4 b5 c6
	//char *str = "abkccbc";
	//char *str = "abaa";
	char *str = "ababa";
	int n = strlen(str);

	int dp[n][n];
	memset(dp, 0, sizeof(int)*n*n);

	// init diagonals with '1'
	for(int k=0; k<n; k++)
		dp[k][k] = 1;

	for (int k=1; k<n; k++)
	{
		solve(0, k, n, dp, str);
	}

	printf("LPS_recursive = %d\n", dp[0][n-1]);
}

static void solve(int i, int j, int n, int dp[n][n], char *str)
{
	// base or bound
	if (j==n || i==n)
		return;

	// action
	if (str[i] == str[j])
	{
		dp[i][j] = 2 + dp[i+1][j-1];
	}
	else
		dp[i][j] = MAX(dp[i][j-1], dp[i+1][j]);

	// recursion
	solve(i+1, j+1, n, dp, str);
}


// ----------------------------------------------------
// DP : Count all Palindromic-subsequences
// ----------------------------------------------------
void DP_count_PS(void)
{
	// a0 b1 a2 a3
	char *str = "abaa";

	// a0 b1 c2 c3 b4 c5
	//char *str = "abccbc";

	int n = strlen(str);

	int dp[n][n];
	memset(dp, 0, sizeof(int)*n*n);

	// IMP: omptimisation for the diagonal initialisation with '1s'
	// init diagonals with '1'
	/*for(int k=0; k<n; k++)
		dp[k][k] = 1;
	 */

	for (int k=0; k<n; k++)
	{
		for(int j=k, i=0; j<n; j++, i++)
		{
			if (k==0)
			{
				dp[i][j] = 1; // // init diagonals with '1'
			}
			else if (str[i]==str[j]) // c1 = c2 then c1-m + m-c2 + 1
			{
				dp[i][j] = dp[i][j-1] + dp[i+1][j] + 1;
			}
			else // c1 != c2 then c1-m + m-c2 - m
			{
				dp[i][j] = dp[i][j-1] + dp[i+1][j] - dp[i+1][j-1];
			}
		}
	}

	printf("count all PS = %d\n", dp[0][n-1]);
}


// ----------------------------------------------------
// DP : Job scheduling algo
// ----------------------------------------------------
typedef struct JOB
{
	char job;
	int s;
	int e;
	int profit;
}JOB_d;

static bool_t isValid(JOB_d j, JOB_d i);
static void DP_Weighted_Job_Scheduling_Program(JOB_d arr[], int n);

static void MergeSort_tosortJobs(JOB_d arr[], int n);
static void mrg(JOB_d left[], int n1, JOB_d right[], int n2, JOB_d arr[], int n);


void DP_Weighted_Job_Scheduling(void)
{
	printf("Example-1: \n");
	// already sorted acc.to increaasing "end" time.
	// 				  a				b			 c				e			d			f
	JOB_d arr[] = {{'a',1,4,3}, {'b',2,6,5}, {'c',4,7,2}, {'e',6,8,6}, {'d',5,9,4}, {'f',7,10,8}};
	DP_Weighted_Job_Scheduling_Program(arr, sizeof(arr)/sizeof(arr[0]));
	printf("\n"); // EOL

	printf("Example-2: \n");
	JOB_d arr2[] = {{'a',1,4,3}, {'b',2,6,5}, {'c',4,7,2}, {'d',5,9,4}, {'e',6,8,6}, {'f',7,10,5}};
	DP_Weighted_Job_Scheduling_Program(arr2, sizeof(arr2)/sizeof(arr2[0]));
	printf("\n"); // EOL

	printf("Example-3: \n");
	JOB_d arr3[] = {{'a', 3, 10, 20}, {'b', 1, 2, 50}, {'c', 6, 19, 100}, {'d', 2, 100, 200}};	// unsorted
	//JOB_d arr3[] = {{'b', 1, 2, 50}, {'a', 3, 10, 20}, {'c', 6, 19, 100}, {'d', 2, 100, 200}};  	// sorted
	DP_Weighted_Job_Scheduling_Program(arr3, sizeof(arr3)/sizeof(arr3[0]));
	printf("\n"); // EOL
}


void DP_Weighted_Job_Scheduling_Program(JOB_d arr[], int n)
{
	// sort accto end-time
	MergeSort_tosortJobs(arr, n);

	int *dp = memset(malloc(sizeof(int) *n), 0, sizeof(int) *n);
	int *seq = memset(malloc(sizeof(int) *n), 0, sizeof(int) *n);
	Stack_d *stack = Util_createStack(n);

	// set initial values.
	for (int k=0; k<n; k++)
	{
		seq[k] = -1;
		dp[k] = arr[k].profit;
	}

	int max = 0;
	int index = -1;

	// traverse to find the best profit while scheduling a job
	for (int i=1; i<n; i++)
	{
		for (int j=0; j<i; j++)
		{
			if (isValid(arr[j], arr[i]) && (dp[j] + arr[i].profit >= dp[i])) // if best jobs of 'j' can run with individual job of 'i'
			{
				dp[i] = dp[j] + arr[i].profit;
				seq[i] = j;
			}
		}

		if (dp[i]>=max)
		{
			max = dp[i];
			index = i;
		}
	}

	printf("max-profit is %d\n", max);

	// prepare the ans
	while (index != -1)
	{
		Util_pushelement(stack, arr[index].job);
		index = seq[index];
	}

	//print the job in sequence
	while(!isStackempty(stack))
	{
		printf("%c ", Util_popelement(stack));
	}
}


/*
 *
 * startof 'i' should be always on the right side of the endof 'j'
 *
 */
static bool_t isValid(JOB_d j, JOB_d i)
{
	if (i.s >= j.e)
		return true;

	return false;
}


static void MergeSort_tosortJobs(JOB_d arr[], int n)
{
	if (n <= 1)
		return;

	int mid = n/2;
	int l_size = mid;
	int r_size = n-mid;

	// create new and copy
	JOB_d *left = memset(malloc(sizeof(JOB_d) *l_size), 0 , sizeof(JOB_d) *l_size);
	JOB_d *right = memset(malloc(sizeof(JOB_d) *r_size), 0 , sizeof(JOB_d) *r_size);

	int i=0, l=0, r=0;
	while (i<n)
	{
		if (i<l_size)
		{
			left[l] = arr[i];
			i++;
			l++;
		}
		else
		{
			right[r] = arr[i];
			i++;
			r++;
		}
	}

	MergeSort_tosortJobs(left, l_size);
	MergeSort_tosortJobs(right, r_size);

	mrg(left, l_size, right, r_size, arr, n);

	// free the memory
	free(left);
	free(right);
}


static void mrg(JOB_d left[], int n1, JOB_d right[], int n2, JOB_d arr[], int n)
{
	int i=0, l=0, r=0;

	// merge depending upon end-time
	while(i<n && l<n1 && r<n2)
	{
		if (left[l].e <= right[r].e)
		{
			arr[i] = left[l];
			i++;
			l++;
		}
		else
		{
			arr[i] = right[r];
			i++;
			r++;
		}
	}

	//remaining is left-arr
	while (l<n1)
	{
		arr[i] = left[l];
		i++;
		l++;
	}

	//remaining is right-arr
	while (r<n2)
	{
		arr[i] = right[r];
		i++;
		r++;
	}
}


// ----------------------------------------------------
// DP : LongestSquare SubMatrix of 1s
// ----------------------------------------------------
static void DP_LongestSquare_SubMatrix_of_1s_Program(int m, int n, int arr[m][n]);

void DP_LongestSquare_SubMatrix_of_1s(void)
{
	printf("Example-1: \n");
	int arr[5][6] = {{0, 1, 0, 1, 0, 1},
					 {1, 0, 1, 0, 1, 0},
					 {0, 1, 1, 1, 1, 0},
					 {0, 0, 1, 1, 1, 0},
					 {1, 1, 1, 1, 1, 1}};

	DP_LongestSquare_SubMatrix_of_1s_Program(5, 6, arr);
	printf("\n"); // EOL

	printf("Example-2: \n");
	int arr2[5][6] = {{0, 1, 0, 1, 0, 1},
					  {1, 0, 1, 0, 1, 0},
					  {0, 1, 1, 1, 1, 0},
					  {0, 0, 1, 1, 1, 0},
					  {1, 1, 1, 1, 1, 1}};

	DP_LongestSquare_SubMatrix_of_1s_Program(5, 6, arr2);
	printf("\n"); // EOL

	printf("Example-3: \n");
	int arr3[5][6] = {{0, 1, 0, 1, 0, 1},
					  {1, 0, 1, 0, 1, 0},
					  {0, 1, 1, 1, 1, 0},
					  {0, 0, 1, 1, 1, 0},
					  {1, 1, 1, 1, 1, 1}};

	DP_LongestSquare_SubMatrix_of_1s_Program(5, 6, arr3);;
	printf("\n"); // EOL
}


static void DP_LongestSquare_SubMatrix_of_1s_Program(int m, int n, int arr[m][n])
{
	//int **dp = malloc(sizeof(int)*m);
	//for (int k=0; k<m; k++)
	//	*dp = memset(malloc(sizeof(int)*n), 0, sizeof(int)*n);

	int dp[m][n];
	memset(dp, 0, sizeof(int)*m*n);

	int ans = 0;

	for (int i=m-1; i>=0; i--)
	{
		for (int j=n-1; j>=0; j--)
		{
			if (i == m-1 || j == n-1) // last row and col
			{
				dp[i][j] = arr[i][j];
				//*(*(dp+i)+j) = arr[i][j];
			}
			else // other-cells
			{
				if(arr[i][j] == 0)
				{
					dp[i][j] = 0;
					//*(*(dp+i)+j) = 0;
				}
				else
				{
					int min = MIN( MIN(dp[i][j+1], dp[i+1][j]), dp[i+1][j+1]);
					dp[i][j] = min + 1;

					//int min = MIN( MIN(*(*(dp+i)+j+1), *(*(dp+i+1)+j)), *(*(dp+i+1)+j+1));
					//*(*(dp+i)+j) = min + 1;

					if (dp[i][j] > ans)
					{
						ans = dp[i][j];
						//ans = *(*(dp+i)+j);
					}
				}
			}
		}
	}

	printf("SubMatrix size is %d\n", ans);
}


// ----------------------------------------------------
// DP : largest sum in a Subarray
// ----------------------------------------------------
static void DP_LargsetSumIn_SubArray_Program(int *arr, int n);
//static char *string_catenation(char *s1, char s2);

typedef struct set_dp
{
	char *str;
	int sum;
}set_dp_d;


void DP_LargestSumIn_SubArray_KADANES_ALGO(void)
{
	printf("Example-1: \n");
	int arr[] = {4, 3, -2, 6, -14, 7, -1, 4, 5, 7, -10, 2, 9, -10, -5, -9, 6, 1};
	DP_LargsetSumIn_SubArray_Program(arr, sizeof(arr)/sizeof(arr[0]));
	printf("\n"); // EOL

	printf("Example-2: \n");
	int arr2[] = {4, 3, -2, 6, 7, -10, -10, 4, 5, 9, -3, 4, 7, -18, 2, 9, 3, -2, 11};
	DP_LargsetSumIn_SubArray_Program(arr2, sizeof(arr2)/sizeof(arr2[0]));
	printf("\n"); // EOL


	printf("Example-3: \n");
	int arr3[] = {4, 3, -2, 6, 7, -10, -10, 4, 5, 9, -3, 4, 7, -28, 2, 9, 3, -2, 11};
	DP_LargsetSumIn_SubArray_Program(arr3, sizeof(arr3)/sizeof(arr3[0]));
	printf("\n"); // EOL
}


static void DP_LargsetSumIn_SubArray_Program(int *arr, int n)
{
	int *dp = memset(malloc(sizeof(int)* n), 0, sizeof(int)* n);
	int max_sum = 0;
	dp[0] = arr[0];

	//set_dp_d *set = malloc(sizeof(set_dp_d));
	//set->str = "\0";
	//set->sum = 0;
	//char *ans = NULL;

	for (int i=1; i<n; i++)
	{
		if (dp[i-1] + arr[i] >= arr[i])
		{
			// update set
			//set->str = string_catenation(set->str, arr[i]);

			dp[i] = dp[i-1] + arr[i]; // continue in the same set
			//set->sum = dp[i];
		}
		else
		{
			//clear set
			//free(set->str);

			dp[i] = arr[i]; // new set

			// start new
			//set->str = memset(malloc(sizeof(char)+1), 0, sizeof(char)+1);
			//memcpy(set->str, &arr[i], 1);
		}

		// check for max
		if(dp[i] >= max_sum)
		{
			max_sum = dp[i];
			//ans = set->str;
		}
	}

	//printf("Maxsum od subarray is %d and %s\n", max_sum, ans);
	printf("Maxsum od subarray is %d\n", max_sum);

}

/*static char *string_catenation(char *s1, char s2)
{
	char *cat = NULL;

	if ((s1!=NULL))
	{
		int n1 = strnlen(s1);
		int n2=1;

		cat = memset(malloc(sizeof(n1 + n2 +1)), 0, sizeof(n1 + n2 + 1));

		if (n1!=0)
		{
			memcpy(cat, s1, n1);
			free(s1);
		}

		if (n2!=0) memcpy(cat+n1, &s2, n2);
	}
	return cat;

}*/


// ----------------------------------------------------
// DP : min cost path sum
// ----------------------------------------------------
static void DP_MinCostPath(int arr[7][7], int m, int n);

void DP_MinCostPath_Program(void)
{
	printf("Example-1: \n");
	int arr[7][7] = {{2, 8, 4, 1, 6, 4, 2},
			  	 	 {6, 0, 9, 5, 3, 8, 5},
					 {1, 4, 3, 4, 0, 6, 5},
					 {6, 4, 7, 2, 4, 6, 1},
					 {1, 0, 3, 7, 1, 2, 7},
					 {1, 5, 3, 2, 3, 0, 9},
					 {2, 2, 5, 1, 9, 8, 2}};

	DP_MinCostPath(arr, 7, 7);
	printf("\n"); // EOL
}


static void DP_MinCostPath(int arr[7][7], int m, int n)
{
	int dp[m][n];
	memset(dp, 0, sizeof(int)*m*n);

	for (int i=m-1; i>=0; i--)
	{
		for(int j=n-1; j>=0; j--)
		{
			if ((i == m-1) && (j == n-1)) // corner
			{
				dp[i][j] = arr[i][j];
			}
			else if (i == m-1) // last row
			{
				dp[i][j] = dp[i][j+1] + arr[i][j];
			}
			else if (j == n-1) // last col
			{
				dp[i][j] = dp[i+1][j] + arr[i][j];
			}
			else // other cells
			{
				// either can go down or right, choose min-cost
				dp[i][j] = MIN(dp[i][j+1], dp[i+1][j]) + arr[i][j];
			}
		}
	}

	printf("min-cost to reach end is %d\n", dp[0][0]);
}


// ----------------------------------------------------
// DP : max diff of zeros and ones
// ----------------------------------------------------
static void DP_MaxDiff_of_zeros_and_ones(char *str, int n);

void DP_MaxDiff_of_zeros_and_ones_Program(void)
{
	printf("Example-1: \n");
	char *str = "11000010001";
	DP_MaxDiff_of_zeros_and_ones(str, strlen(str));
	printf("\n"); // EOL

	printf("Example-2: \n");
	char *str2 = "11111";
	DP_MaxDiff_of_zeros_and_ones(str2, strlen(str2));
	printf("\n"); // EOL

	printf("Example-3: \n");
	char *str3 = "11110";
	DP_MaxDiff_of_zeros_and_ones(str3, strlen(str3));
	printf("\n"); // EOL
}


static void DP_MaxDiff_of_zeros_and_ones(char *str, int n)
{
	int max_sum = 0;
	int c_sum = 0;

	for (int j=0; j<n; j++)
	{
		int val = 0;
		if (str[j] == '1')
		{
			val = -1;
		}
		else if (str[j] == '0')
		{
			val = +1;
		}

		if (c_sum < 0)
		{
			c_sum = val;
		}
		else
		{
			c_sum += val;
		}

		if (c_sum > max_sum)
			max_sum = c_sum;
	}

	printf("max diff btw 1s and 0s is %d\n", max_sum == 0? -1: max_sum); // max_sum is 0 when all are 1s.
}


// ----------------------------------------------------
// DP : Minimum number of jumps
// ----------------------------------------------------
static void DP_MinJumps(int *arr, int n);

void DP_MinJumps_Program(void)
{
	printf("Example-1: \n");
	int arr[] = {1, 3, 5, 8, 9, 2, 6, 7, 6, 8, 9};
	DP_MinJumps(arr, sizeof(arr)/sizeof(arr[0]));
	printf("\n"); // EOL

	printf("Example-2: \n");
	int arr2[] = {1, 4, 3, 2, 6, 7};
	DP_MinJumps(arr2, sizeof(arr2)/sizeof(arr2[0]));
	printf("\n"); // EOL

	printf("Example-3: \n");
	int arr3[] = {3, 3, 0, 2, 1, 2, 4, 2, 0, 0};
	DP_MinJumps(arr3, sizeof(arr3)/sizeof(arr3[0]));
	printf("\n"); // EOL
}

static void DP_MinJumps(int *arr, int n)
{
	int *dp = memset(malloc(sizeof(int)*n), 0, sizeof(int)*n);
	int *seq = memset(malloc(sizeof(int)*n), 0, sizeof(int)*n);
	dp[n-1] = 0; 	// n-1 s itself a dest.
	seq[n-1] = -1; 	// n-1 s itself a dest.

	for (int i=n-2; i>=0; i--)
	{
		int steps = arr[i];
		int min = INT_MAX;
		int index = -1;

		// search for the min jumps and the path
		for (int j=i+1; j<= (steps+i); j++)
		{
			if (j > n-1) // boundary
				break;

			if ((dp[j] != -1) && dp[j] < min) // if no path means null value then skip
			{
				min = dp[j];
				index = j;
			}
		}


		// update current dp and index.
		if (min != INT_MAX)
		{
			dp[i] = min + 1;
			seq[i] = index;
		}
		else
		{
			dp[i] = -1;
			seq[i] = -1;
		}

	}

	printf("Min jumps is: %d\n", dp[0]);

	// print the paths in order starting from dp[0]
	int index = 0;
	while (index != -1)
	{
		printf("%d -> ", index);
		index = seq[index];
	}
}

