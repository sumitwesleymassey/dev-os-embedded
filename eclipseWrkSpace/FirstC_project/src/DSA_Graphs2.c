/*
 * DSA_Graphs2.c
 *
 *  Created on: 19-May-2022
 *      Author: sumit
 */
#include <stdio.h>
#include <stdlib.h>
#include "Graph.h"
#include "DSA_Graphs.h"
#include "Util.h"
#include "string.h"


void DSA_Graphs2_CanFinishPreRequisite(void);

// ----------------------------------------------------
// Main-driver function
// ----------------------------------------------------
void DSA_Graphs2_DriverProgram(void)
{
	printf("\nGraph-Theory: can finish job\n");
	DSA_Graphs2_CanFinishPreRequisite();
	printf("\n"); //EOL

}


// ----------------------------------------------------
// can finish job (prerequisites)
// ----------------------------------------------------
static bool_t solve(int i, int n, int graph[n][n], int *visited, int *processed);
static void DSA_Graphs2_PreRequisiteUtils(int n, int graph[n][n]);

void DSA_Graphs2_CanFinishPreRequisite(void)
{
	int n=4;
	int graph[4][4] = {{0, 0, 0, 0},
					   {1, 0, 0, 0},
					   {0, 1, 0, 0},
					   {0, 0, 1, 0}	};

	DSA_Graphs2_PreRequisiteUtils(n, graph);


	int a=4;
	int graph2[4][4] = {{0, 0, 0, 0},
					    {1, 0, 1, 0}, // dependency on 2
					    {0, 1, 0, 0}, // dependency on 1, cyclic
					    {0, 0, 1, 0}	};

	DSA_Graphs2_PreRequisiteUtils(a, graph2);
}

static void DSA_Graphs2_PreRequisiteUtils(int n, int graph[n][n])
{
	int *processed = memset(malloc(sizeof(int)*n), 0, (sizeof(int)*n));
	int *visited = memset(malloc(sizeof(int)*n), 0, (sizeof(int)*n));

	for (int i=0; i<n; i++)
	{
		if (solve(i, n, graph, visited, processed)== false)
		{
			printf("cannot finish jobs\n");
			break;
		}

	}
}

/**
 * Main logic for 2 arrays (1 visited, 1 processed):-
 *
 * 		only visited[] array:
 * 		1) if visited --> skip
 * 		2) if not-visited --> visit it
 *
 * 		only processed[] array:
 * 		1) if processed already --> return True
 * 		2) if not processed --> process it now
 * 		Note: but how/when to return False in-case of cyclic dependency???
 *
 *
 * 		both visited[] & processed[]:
 *		1) if visited and processed ---> skip and return True
 *		2) if visited but not processed --> return false // cause cyclic-dependency
 *		3) if not visited (automatically not-processed aswell)	--> then mark visited and process it.
 *			Note: check for neighbors and any already visited node which is not processed
 *					will be detected at step2.
 *
 *
 */
static bool_t solve(int i, int n, int graph[n][n], int *visited, int *processed)
{
	// base: already visited but not processed
	if (visited[i]==1 && processed[i] == 0)
		return false;

	// visited and processed
	if (visited[i]==1 && processed[i] == 1)
		return true;

	// mark
	visited[i] = 1;

	// not-visited then check neighbors
	for(int j=0; j<n; j++)
	{
		if (graph[i][j] == 1)
		{
			if (solve(j, n, graph, visited, processed) == false)
				return false;
		}
	}

	// mark cur processed
	processed[i] = 1;

	return true;
}


// ----------------------------------------------------
// next
// ----------------------------------------------------


