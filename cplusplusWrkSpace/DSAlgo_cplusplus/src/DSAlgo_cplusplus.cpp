//============================================================================
// Name        : DSAlgo_cplusplus.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Main.h"
#include "BinaryTrees.h"
#include "OOPS_inheritance.h"
#include <bits/stdc++.h>


using namespace std;

int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!

	// BST print all the diagonals
	//BinaryTrees_Program();
	Car obj;
	Car b = Car(4, 5);
	Car a[10];

	obj.getMeterReading();
	obj.whatTypeOfCar();

	Truck obj2;
	obj2.whatDoesItDo();
	obj.getMeterReading();

	return 0;
}


class String
{
public:
	int size;
	char *s;

	String(char *);
	~String();
};

String::String(char *c)
{
	int size = strlen(c);
	s = new char[size +1];
	strcpy(s, c);
}

String::~String()
{
	delete[] s;
}
