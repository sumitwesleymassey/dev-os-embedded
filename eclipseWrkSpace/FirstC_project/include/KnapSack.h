/*
 * KnapSack.h
 *
 *  Created on: 12-Mar-2022
 *      Author: sumit
 */

#ifndef KNAPSACK_H_
#define KNAPSACK_H_

#define MAX_VALUE(x, y) ( ((x) > (y))? x:y )
void KnapSackProgram(void);

#endif /* KNAPSACK_H_ */
