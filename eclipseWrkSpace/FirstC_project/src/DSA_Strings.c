/*
 * Strings.c
 *
 *  Created on: 21-Apr-2022
 *      Author: sumit
 */

#include <stdio.h>
#include <strings.h>
#include <stdlib.h>
#include "DSA_Strings.h"
#include "Util.h"

bool_t DSA_Strings_wordbreak(void);
void Program_findLPS(void);
bool_t program_balanceParanthesis(void);
void program_removeInvalidParanthesis(char *pattern);

void DSA_Strings_Program(void)
{
	// word break problem
	printf("word break part1\n");
	printf("found = %d\n", DSA_Strings_wordbreak());

	// find LPS
	printf("print LPS: \n");
	Program_findLPS();
	printf("\n"); // EOL

	// balance paranthesis
	printf("is Balanced parenthesis: %d\n", program_balanceParanthesis());
	printf("\n"); // EOL

	// remove invlaid paranthesis
	printf("remove invalid parenthesis: \n");
	program_removeInvalidParanthesis("()())()");
	program_removeInvalidParanthesis("(a)())()");
	program_removeInvalidParanthesis("(a)())()))");

}


// ----------------------------------------------------
// remove invalid parenthesis:
// ----------------------------------------------------
#define OTH_CHAR_TYPES_SUPPORT	ENABLED
#define MUlTI_REMOVALS_SUPP		ENABLED


void solve_remove_invalid_paranthesis(char *pattern, int size, DynamicMemory_Map_d *map
#if (MUlTI_REMOVALS_SUPP == ENABLED)
	, int min_removals
#endif
	);

static bool_t isValid(char *pattern, int size);
static int getminParanthesisToRemove(char *pattern, int size);

void program_removeInvalidParanthesis(char *pattern)
{
	DynamicMemory_Map_d *map = Util_createHashMap(0, 0, 0);

	int min_removals = getminParanthesisToRemove(pattern, strlen(pattern)+1);
	solve_remove_invalid_paranthesis(pattern, strlen(pattern)+1, map
#if (MUlTI_REMOVALS_SUPP == ENABLED)
	, min_removals
#endif
	); // size = stringlen + null char // cause it will help in %s printing

	// print the map with the valid paranthesis parttern
	LL_Entry_d *cur = map->head;
	while (cur)
	{
		printf("%s \n", cur->key);
		cur = cur->next;
	}

	printf("\n"); // EOL
}


void solve_remove_invalid_paranthesis(char *pattern, int size, DynamicMemory_Map_d *map
#if (MUlTI_REMOVALS_SUPP == ENABLED)
	, int min_removals
#endif
	)
{

#if (MUlTI_REMOVALS_SUPP == ENABLED)
	if (min_removals == 0)
	{
		return;
	}
#endif

	for (int i=0; i<size-1; i++)
	{
		char *newPatt = malloc(size-1);

		if (i!=0)
			memcpy(newPatt, pattern+0, i); // strcat(newPatt, pattern+0, i);

		int remaining_size = (size-1) - (i+1); // 10 - null - (index+1)
		memcpy(newPatt+i,  pattern+(i+1), remaining_size);	// strcat(newPatt, pattern+(i+1), (size-1) - (i+1));

		// recursion call
		solve_remove_invalid_paranthesis(newPatt, size-1, map
#if (MUlTI_REMOVALS_SUPP == ENABLED)
		, min_removals-1
#endif
		);

		// can check validity in the base case and could add it into the map
		if (isValid(newPatt, size-1)) // size-1 with null char
			Util_HashMap_put(map, newPatt, size-1, Util_HashMap_getValue(map, newPatt, size-1) + 1);

		free(newPatt);
	}

}

static bool_t isValid(char *pattern, int size)
{
	bool_t retval = true;
	// create stacks : open and star
	int sizeofStack = (size-1)* sizeof(int);
	Stack_d *open_stack = Util_createStack(sizeofStack);

	// traverse the pattern
	for (int i=0; i<size-1; i++)
	{
		char ch = *(pattern+i);
		if (ch == '(')
		{
			Util_pushelement(open_stack, i);
		}
		else if ( ch == ')' )
		{
			if (!isStackempty(open_stack))
				Util_popelement(open_stack);
			else
			{
				retval = false;
				break;
			}
		}
#if (OTH_CHAR_TYPES_SUPPORT == DISABLED) 			// ignore other char types
		else
		{
			// false and break when other char types not supported
			retval = false;
			break;
		}
#endif
	}

	// if above is true then all ')' are balanced now
	// then check all '(' are balanced by checkng open_Stack is empty or not
	//return (retval && isStackempty(open_stack));
	retval = (retval && isStackempty(open_stack));

	// free the allocations
	Util_destroyStack(open_stack);

	return retval;
}


/*
 * returns : no. of elemen in the stack, if i!=0 means not-empty
 *
 */
static int getminParanthesisToRemove(char *pattern, int size)
{
	int min_removals = 0;
	int sizeofStack = (size-1)* sizeof(int);
	Stack_d *open_stack = Util_createStack(sizeofStack);

	for(int i=0; i<size-1 ; i++)
	{
		char ch = *(pattern+i);
		if (ch == '(')
			Util_pushelement(open_stack, ch);
		else if (ch == ')')
		{
			if (!isStackempty(open_stack) && '(' == Util_peekTop(open_stack))
				Util_popelement(open_stack);
			else
				Util_pushelement(open_stack, ch);
		}
	}

	min_removals = open_stack->sp+1;

	// free the allocations
	Util_destroyStack(open_stack);

	// return the no. of elements left in the stack
	// which signifies that the min no. of pranthesis to be removed.
	return min_removals;

}



// ----------------------------------------------------
// Balanced parenthesis:
// ----------------------------------------------------
static bool_t solveparanthesis(char *pattern, int size);

bool_t program_balanceParanthesis(void)
{
	char *pattern = "(*(**)";
	return solveparanthesis(pattern, strlen(pattern)+1);
}

bool_t solveparanthesis(char *pattern, int size)
{
	bool_t retval = true;
	// create stacks : open and star
	int sizeofStack = (size-1)* sizeof(int);
	Stack_d *star_stack = Util_createStack(sizeofStack);
	Stack_d *open_stack = Util_createStack(sizeofStack);


	// traverse the pattern
	for (int i=0; i<size-1; i++)
	{
		char ch = *(pattern+i);
		if (ch == '(')
		{
			Util_pushelement(open_stack, i);
		}
		else if (ch == '*')
		{
			Util_pushelement(star_stack, i);
		}
		else if ( ch == ')' )
		{
			if (!isStackempty(open_stack))
				Util_popelement(open_stack);
			else if (!isStackempty(star_stack))
				Util_popelement(star_stack);
			else
			{
				retval = false;
				break;
			}
		}
#if (OTH_CHAR_TYPES_SUPPORT == DISABLED) 			// ignore other char types
		else
		{
			// false and break when other char types not supported
			retval = false;
			break;
		}
#endif
	}

	if (retval)
	{
		//
		// sinc ewe have reached here means all ')' brackets are balanced.
		//
		// now take care of the remaining ( and *
		// and if open is empty and * is filled --> all balanced, ignore *'s
		while (!isStackempty(open_stack))
		{
			int index = Util_popelement(open_stack);
			if ((isStackempty(star_stack)) || index > Util_popelement(star_stack))	// * should on right hand side to balance '(' bracket
			{
				retval = false;
				break;
			}
		}
	}

	// destroy the stacks
	Util_destroyStack(star_stack);
	Util_destroyStack(open_stack);

	return retval;
}


// ----------------------------------------------------
// KMP Alorithm
// ----------------------------------------------------
static int *calcLPS(char *pattern, int size);
static int *calcLPS_optimized(char *pattern, int size);

void Program_findLPS(void)
{
	char *pattern = "AAABAAA";
	int *LPS = calcLPS(pattern, strlen(pattern));
	int *lps = calcLPS_optimized(pattern, strlen(pattern));

	// prin the LPS
	for (int i=0; i< strlen(pattern); i++)
	{
		printf("%d ", lps[i]);
	}
}

static int *calcLPS(char *pattern, int size)
{
	int *LPS = malloc(size*sizeof(int));

	int j=0;
	LPS[0] = 0;

	for (int i=1; i<size ;i++)
	{
		if (pattern[i] == pattern[j])
		{
			LPS[i] = j+1;
			j++;
		}
		else
		{
			if (j==0)
			{
				LPS[i] = 0;
			}
			else
			{
				for (;j>0; j--)
				{
					if (pattern[i] == pattern[j-1])
						LPS[i] = j+1;
					else
					{
						if(j-1 == 0)
							LPS[i] = 0;
					}
				}
			}
		}
	}

	return LPS;

}

static int *calcLPS_optimized(char *pattern, int size)
{
	int *LPS = malloc(size*sizeof(int));
	int i=0;
	int j=1;

	// first value will be 0
	LPS[0] = 0;

	// little optimised
	while (j<size)
	{
		if (pattern[i] == pattern[j])
		{
			LPS[j] = ++i;
			j++;
		}
		else
		{
			if (i!=0) {
				i=0;// i = LPS[i-1];
			} else
			{
				LPS[j] = i;
				j++;
			}
		}
	}

	return LPS;
}





// ----------------------------------------------------
// word break problem
// ----------------------------------------------------
static bool_t solve(char **dictionary, int dSize, char *toSearch, int patternSize);

// 64-bit system:
//		a) int = 8bytes
// 		b) int * = 8bytes
//  	c) char = 1bytes
//		d) char * = 8bytes
//		e) char *ptr = "abcde"
//				sizeof(ptr) = size of char * i.e. 8bytes
//				strlen(ptr) = 5 with out null character
//
bool_t DSA_Strings_wordbreak(void)
{
	char *toSearch = "abcde";
	char *dictionary[7] = {"a", "ab", "bcd", "cd", "d", "de", "e"};
	//char *dictionary2[6] = {"a", "ab", "bcd", "cd", "d", "de"};

    // to print few sizes
	int dmemSize = sizeof(dictionary);	// array of 6 pointers = 6*8 = 48
	int charptrsize = sizeof(char *);
	int dSize = dmemSize/ sizeof(dictionary[0]); // 48/(size of char * i.e. 8bytes) = 6
	int pattern_S = sizeof(toSearch);
	int patternSize = strlen(toSearch); //

	return solve(dictionary, dSize, toSearch, patternSize);
}

static bool_t solve(char **dictionary, int dSize, char *toSearch, int patternSize)
{
	if (patternSize == 0 && (!memcmp(toSearch, "\0", patternSize+1)))
	{
		return true;
	}

	// search the string
	for (int i=0; i<patternSize; i++)
	{
		// flag to indicate that patt is found
		bool_t found = false;

		// subtringing tosearch to pattern
		char *pattern = malloc(i+1);
		memcpy(pattern, toSearch, i+1);

		// search A in dict
		for (int j=0; j<dSize; j++)
		{
			if (!memcmp(dictionary[j], pattern, MAX(strlen(dictionary[j]), strlen(pattern))))
			{
				found = true;
				break;
			}
		}

		// if found then split and pass it to recursion
		//
		//							|	else go get next  |  else go get next
		//							|                     | 
		//		"a" + "remaining"	|  "ab" + "remaining" |   "ab.." + ""
		//      /		 \			|	/		  \       | 	/		  \
		//    found		is also		| found		is also   |  found		is also
		//				 found		|			 found    | 			 found
		//							|                     | 
		//		return true			| return true         |  return true
		//
		if (found && solve(dictionary, dSize, toSearch+(i+1), patternSize-(i+1)))
			return true;

		// once done, then free the temp sub-string memory
		free(pattern);
	}

	return false;
}
