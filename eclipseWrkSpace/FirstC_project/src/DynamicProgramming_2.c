/*
 * DynamicProgramming_2.c
 *
 *  Created on: 18-Jul-2022
 *      Author: sumit
 */
#include <stdio.h>
#include <stdlib.h>
#include "Util.h"
#include "DynamicProgramming.h"


void DP_SmallestSumIn_SubArray_KADANES_ALGO(void);
void DP2_LargestSumIn_SubArray_KADANES_ALGO(void);
void DP2_wordbreak_recursion_Program(void);
void DP_LISS_Program(void);
void DP_TargetSumSubsetProgram(void);
void DP_BBTcounterProgram(void);
void DP_MaxLenChainProgram(void);
void DP_EggDropProgram(void);
void DP_maxSumWO3ConsecProgram(void);
void DP_disjointlessthanK_Program(void);
void DP_paintFences_Program(void);
void DP_paintFences_rec_Program(void);
void DP_assemblyLineSched_Program(void);
void DP_assemblyLineSched_Optimised_Program(void);
void DP_goldMine_Program(void);
void DP_EqualPartitionSubset_Program(void);
void DP_EditDistanace_Program(void);
void DP_EditDistanace_Program_dp(void);
void DP_EditDistanace_Tabulation_Program(void);

// ----------------------------------------------------
// Main-driver function
// ----------------------------------------------------
void DynamicProgramming2_DriverProgram(void)
{
	// DP : KADANE's ALGO
	printf("smallest Sum in a subarray: \n");
	DP_SmallestSumIn_SubArray_KADANES_ALGO();
	printf("\n"); // EOL

	// DP : KADANE's ALGO
	printf("KADANES ALGO: \n");
	DP2_LargestSumIn_SubArray_KADANES_ALGO();
	printf("\n"); // EOL

	// DP : word-break
	printf("word-break (recursion): \n");
	DP2_wordbreak_recursion_Program();
	printf("\n"); // EOL

	// DP : Longest Independent Subset size.
	printf("LISS (binary-tree): \n");
	DP_LISS_Program();
	printf("\n"); // EOL

	// DP : Target sum subset
	printf("Target sum subset: \n");
	DP_TargetSumSubsetProgram();
	printf("\n"); // EOL

	// DP : balanced-binary-trees possible mod with 1e*10^7
	printf("balanced-binary-trees: \n");
	DP_BBTcounterProgram();
	printf("\n"); // EOL

	// DP : max len chain
	printf("max len chain: \n");
	DP_MaxLenChainProgram();
	printf("\n"); // EOL

	// DP : egg-drop
	printf("egg-drop: \n");
	DP_EggDropProgram();
	printf("\n"); // EOL

	// DP : max-sum without-3-consecutive
	printf("max-sum without-3-consecutive: \n");
	DP_maxSumWO3ConsecProgram();
	printf("\n"); // EOL

	// DP : disjoints pair with less than K
	printf("max-sum disjoints pair with lessthan K: \n");
	DP_disjointlessthanK_Program();
	printf("\n"); // EOL

	// DP : numof ways to paint fences
	printf("numof ways to paint fences: \n");
	DP_paintFences_Program();
	printf("\n"); // EOL

	// DP : numof ways to paint fences
	printf("Recursion: numof ways to paint fences: \n");
	DP_paintFences_rec_Program();
	printf("\n"); // EOL

	// DP : Assembly line shceduling
	printf("Assembly line shceduling: \n");
	DP_assemblyLineSched_Program();
	printf("\n"); // EOL

	// DP : Assembly line shceduling optimised
	printf("Assembly line shceduling optimised: \n");
	DP_assemblyLineSched_Optimised_Program();
	printf("\n"); // EOL

	// DP : Gold mine problem
	printf("gold mine problem: \n");
	DP_goldMine_Program();
	printf("\n"); // EOL

	// DP : equal-sum partion subset
	printf("equal-sum partion subset problem (with out DP): \n");
	DP_EqualPartitionSubset_Program();
	printf("\n"); // EOL

	// DP : edit distance problem
	printf("edit distance problem (without DP): \n");
	DP_EditDistanace_Program();
	printf("\n"); // EOL

	// DP : edit distance problem
	printf("edit distance problem (DP): \n");
	DP_EditDistanace_Program_dp();
	printf("\n"); // EOL

	// DP : edit distance problem
	printf("edit distance problem (Tabulation dp): \n");
	DP_EditDistanace_Tabulation_Program();
	printf("\n"); // EOL
}


// ----------------------------------------------------
// DP : smallest sum in a subarray
// ----------------------------------------------------
static void DP_SmallestSum_In_Subarray(int *arr, int n);

void DP_SmallestSumIn_SubArray_KADANES_ALGO(void)
{
	printf("Example-1: \n");
	int arr[] = {3, -4, 2, -3, -1, 7, -5};
	DP_SmallestSum_In_Subarray(arr, sizeof(arr)/sizeof(arr[0]));
	printf("\n"); // EOL

	printf("Example-2: \n");
	int arr2[] = {2, 6, 8, 1, 4};
	DP_SmallestSum_In_Subarray(arr2, sizeof(arr2)/sizeof(arr2[0]));
	printf("\n"); // EOL

	printf("Example-3: \n");
	int arr3[] = {3, 3, 0, 2, 1, 2, 4, 2, 0, 0};
	DP_SmallestSum_In_Subarray(arr3, sizeof(arr3)/sizeof(arr3[0]));
	printf("\n"); // EOL
}

static void DP_SmallestSum_In_Subarray(int *arr, int n)
{
	int *dp = malloc(sizeof(int)*(n+1));
	dp[0] = 0;
	for (int k=1; k<(n+1); k++)
		dp[k] = arr[k-1];

	int *seq  = malloc(sizeof(int)*(n+1));
	for (int k=0; k<(n+1); k++)
		seq[k] = -1;

	int min = INT_MAX;
	int index = -1;

	for (int i=1; i<(n+1); i++)
	{
		if ((dp[i-1] + arr[i-1]) <= arr[i-1])
		{
			dp[i] = (dp[i-1] + arr[i-1]);
			seq[i] = i-1;
		}

		if (dp[i] <=  min && i!=1)
		{
			min = dp[i];
			index = i;
		}
	}

	// print the subarray elements
	Stack_d *stack = Util_createStack(n);
	while (index != -1)
	{
		Util_pushelement(stack, arr[index-1]);
		index = seq[index];
	}

	while(!isStackempty(stack))
	{
		printf("%d, ", Util_popelement(stack));
	}

	printf("= %d", min);
}


// ----------------------------------------------------
// DP : largest sum in a Subarray
// ----------------------------------------------------
static void DP2_LargsetSumIn_SubArray_Program(int *arr, int n);


void DP2_LargestSumIn_SubArray_KADANES_ALGO(void)
{
	printf("Example-1: \n");
	int arr[] = {4, 3, -2, 6, -14, 7, -1, 4, 5, 7, -10, 2, 9, -10, -5, -9, 6, 1};
	DP2_LargsetSumIn_SubArray_Program(arr, sizeof(arr)/sizeof(arr[0]));
	printf("\n"); // EOL

	printf("Example-2: \n");
	int arr2[] = {4, 3, -2, 6, 7, -10, -10, 4, 5, 9, -3, 4, 7, -18, 2, 9, 3, -2, 11};
	DP2_LargsetSumIn_SubArray_Program(arr2, sizeof(arr2)/sizeof(arr2[0]));
	printf("\n"); // EOL


	printf("Example-3: \n");
	int arr3[] = {4, 3, -2, 6, 7, -10, -10, 4, 5, 9, -3, 4, 7, -28, 2, 9, 3, -2, 11};
	DP2_LargsetSumIn_SubArray_Program(arr3, sizeof(arr3)/sizeof(arr3[0]));
	printf("\n"); // EOL
}


static void DP2_LargsetSumIn_SubArray_Program(int *arr, int n)
{
	int *dp = memset(malloc(sizeof(int)*(n+1)), 0, sizeof(int)*(n+1));
	for (int k=1; k<(n+1); k++)
		dp[k] = arr[k-1];

	int *seq = malloc(sizeof(int)*(n+1));
	for (int k=0; k<(n+1); k++)
		seq[k] = -1;

	int max = INT_MIN;
	int index = -1;

	for (int i=1; i<(n+1); i++)
	{
		if (dp[i-1] + arr[i-1] >= dp[i])
		{
			dp[i] = dp[i-1] + arr[i-1];
			seq[i] = i-1;
		}

		if (dp[i] >= max)
		{
			max = dp[i];
			index = i;
		}
	}

	// print the subarray
	Stack_d *stack = Util_createStack(n);
	while(index != -1)
	{
		Util_pushelement(stack, arr[index-1]);
		index = seq[index];
	}

	while(!isStackempty(stack))
	{
		printf("%d, ", Util_popelement(stack));
	}

	printf("= %d", max);
}


// ----------------------------------------------------
// DP : word-break
// ----------------------------------------------------
typedef struct sentence_node
{
	char *sentence;
	struct sentence_node *next;

}sentence_node_d;

typedef struct DynList
{
	struct sentence_node *head;
	struct sentence_node *tail;
	int size;
}DynList_d;

static void DP_wordbreak_recursion(char *str, char *dict[], int n);
static void solve(char *str, char *dict[], int n, char *asf, DynList_d *list);
static void addToList(DynList_d *list, char *ans);
static bool_t isInDict(char *pat, int size, char *dict[], int n);
static void printList(DynList_d *list);

void DP2_wordbreak_recursion_Program(void)
{
	printf("Example-1: \n");
	char *str = "abcde";
	char *dict[] = {"a", "ab", "bcd", "cd", "d", "de", "e"};
	DP_wordbreak_recursion(str, dict, sizeof(dict)/sizeof(dict[0]));
	printf("\n"); // EOL

	printf("Example-2: \n");
	char *str2 = "ilike";
	char *dict2[] = { "i", "like", "sam",
			"sung", "samsung", "mobile",
			"ice","cream", "icecream",
			"man", "go", "mango" };
	DP_wordbreak_recursion(str2, dict2, sizeof(dict2)/sizeof(dict2[0]));
	printf("\n"); // EOL

	printf("Example-3: \n");
	char *str3 = "ilikesamsung";
	DP_wordbreak_recursion(str3, dict2, sizeof(dict2)/sizeof(dict2[0]));
	printf("\n"); // EOL

	printf("Example-4: \n");
	char *str4 = "catsanddog";
	char *dict3[] = {"cats", "cat", "and", "sand", "dog"};
	DP_wordbreak_recursion(str4, dict3, sizeof(dict3)/sizeof(dict3[0]));
	printf("\n"); // EOL

	printf("Example-5: \n");
	char *str5 = "catsandog";
	DP_wordbreak_recursion(str5, dict3, sizeof(dict3)/sizeof(dict3[0]));
	printf("\n"); // EOL
}

static void DP_wordbreak_recursion(char *str, char *dict[], int n)
{
	DynList_d *list = memset(malloc(sizeof(DynList_d)), 0, sizeof(DynList_d));
	char *asf = "\0";
	solve(str, dict, n, asf, list);
	printList(list);
}

static void solve(char *str, char *dict[], int n, char *asf, DynList_d *list)
{
	// exit condition
	if (*str == '\0') // if reached null means asf is one of the answer add to the list
	{
		addToList(list, asf);
		return;
	}

	// find the pattern
	for (int i=0; i<strlen(str); i++)
	{
		bool_t found = isInDict(str, i+1, dict, n);
		if (found)
		{
			// concat and send
			char *temp = memset(malloc(strlen(asf) + (i+1) + 1 + 1), '\0', strlen(asf) + (i+1) + 1 + 1); // plus 1 for '\0' plus 1 for space
			memcpy(temp, asf, strlen(asf));   		// len is zero then nothing is copied.
			memcpy(temp+strlen(asf), str, i+1);
			temp[strlen(asf) + i+1] = ' ';

			solve(str+i+1, dict, n, temp, list);
			free(temp); // it would have been set in the list
		}
	}

	return;
}


static void addToList(DynList_d *list, char *ans)
{
	// allocate node
	sentence_node_d *node = memset(malloc(sizeof(sentence_node_d)), 0, sizeof(sentence_node_d));
	node->sentence = memset(malloc(strlen(ans)+1), 0, strlen(ans)+1);
	memcpy(node->sentence, ans, strlen(ans)+1);

	if (list->head == NULL) // first item
	{
		list->head = list->tail = node;
	}
	else
	{
		list->tail->next = node;
		list->tail = node;
	}

	list->size++;
}

static bool_t isInDict(char *pat, int size, char *dict[], int n)
{
	char *pattern = memset(malloc(size+1), '\0', size+1);
	memcpy(pattern, pat, size);

	for (int k=0; k<n; k++)
	{
		if (memcmp(pattern, dict[k], MAX(strlen(pattern), strlen(dict[k]))) == 0) // found
		{
			return true;
		}
	}

	free(pattern);

	return false;
}


static void printList(DynList_d *list)
{
	sentence_node_d *cur = list->head;
	if (!cur)
	{
		printf("sentence cant be formed\n");
		return;
	}

	while(cur)
	{
		printf("%s\n", cur->sentence);
		cur = cur->next;
	}
}


// ----------------------------------------------------
// DP : Longest independent Subset size (in binary tree)
// ----------------------------------------------------
#define DP

typedef struct Tree_node
{
    int data;
    struct Tree_node *left, *right;
#ifdef DP
    int dp;
#endif
}Tree_node_d;

static int DP_LISS(Tree_node_d *root);
static Tree_node_d* newNode(int data);

void DP_LISS_Program(void)
{
	printf("Example-1: \n");

    // Let us construct the tree
    // given in the above diagram
	Tree_node_d *root = newNode(10);
    root->left = newNode(20);
    root->left->left = newNode(40);
    root->left->right = newNode(50);
    root->left->right->left = newNode(70);
    root->left->right->right = newNode(80);
    root->right = newNode(30);
    root->right->right = newNode(60);

    printf("LISS is = %d\n", DP_LISS(root));
}


static int DP_LISS(Tree_node_d *root)
{
	// base case
	if (root == NULL)
		return 0;

#ifdef DP
	if (root->dp != 0)	// if already calculated dp will be non-zero, just return from here.
		return root->dp;

    if (root->left == NULL && root->right == NULL)
        return (root->dp = 1);
#endif

	// exclude itself + all the childrens
	int exclude_size = DP_LISS(root->left) + DP_LISS(root->right);

	// include itself and all the grand-childrens
	int include_size = 1;
	if (root->left != NULL)
		include_size += DP_LISS(root->left->left) + DP_LISS(root->left->right);
	if (root->right != NULL)
		include_size += DP_LISS(root->right->left) + DP_LISS(root->right->right);

#ifdef DP
	root->dp = MAX(exclude_size, include_size); // update dp and then return
	return root->dp;
#else
	return MAX(exclude_size, include_size);
#endif
}


// A utility function to create a node
static Tree_node_d* newNode(int data)
{
	Tree_node_d* temp = memset(malloc(sizeof(Tree_node_d)), 0, sizeof(Tree_node_d));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}


// ----------------------------------------------------
// DP : Target-Sum-Subset
// ----------------------------------------------------
static void DP_TargetSumSubset(int *arr, int n, int target);

void DP_TargetSumSubsetProgram(void)
{
	printf("Example-1: \n");
	int arr[] = {4, 2, 7, 1, 3};
	DP_TargetSumSubset(arr, sizeof(arr)/sizeof(arr[0]), 10);
	printf("\n"); // EOL
}


static void DP_TargetSumSubset(int *arr, int n, int target)
{
	bool_t dp[n+1][target+1];
	memset(dp, 0, sizeof(bool_t)*(n+1)*(target+1));

	for (int i=0; i<(n+1); i++)
	{
		for (int j=0; j<(target+1); j++)
		{
			if (j==0)	// 1st col
			{
				dp[i][j] = true;
			}
			else if (i==0) // 1st row
			{
				dp[i][j] = false;
			}
			else // everything else
			{
				// if '(i' is selected then remaining 'j-arr[i-1]', can be scored by others) ||
				//     ('i' is not selected then all 'j', should be scored by others.
				//
				// if (rem < 0) // -ve
				// {
				// 	result = false;
				// }
				// else
				// {
				// 	result = (dp[i-1][rem]);
				// }

				int rem = j-arr[i-1];
				bool_t result = false;
				if (rem >= 0)
				{
					result = (dp[i-1][rem]);
				}

				dp[i][j] = result || (dp[i-1][j]);
			}
		}
	}

	printf("TSS = %s", (dp[n][target] == true)? "yes": "no");
}


// ----------------------------------------------------
// DP : BBT- counter (Balanced Binary Trees counter)
// ----------------------------------------------------
static void DP_BBTcounter(int h);

void DP_BBTcounterProgram(void)
{
	printf("Example-1: \n");
	printf("binary-trees of height 2\n");
	DP_BBTcounter(2);
	printf("\n"); // EOL

	printf("Example-2: \n");
	printf("binary-trees of height 3\n");
	DP_BBTcounter(3);
	printf("\n"); // EOL

	printf("Example-3: \n");
	printf("binary-trees of height 4\n");
	DP_BBTcounter(4);
	printf("\n"); // EOL
}


static void DP_BBTcounter(int h)
{
	int *dp = memset(malloc(sizeof(int) * (h+1)), 0, sizeof(int) * (h+1));

	// init 0th and 1th height as '1s'
	dp[0] = 1;	// 1-BBT
	dp[1] = 1; 	// 1-BBT

	for (int i=2; i<h+1; i++)
	{
		// (h-2)(h-1) + (h-1)(h-2) + (h-1)(h-1)
		dp[i] = dp[i-2]*dp[i-1] + dp[i-1]*dp[i-2] + dp[i-1]*dp[i-1];
	}

	// print the no. of BTs
	printf("count=%d\n", dp[h]);
}


// ----------------------------------------------------
// DP : the longest chain which can be formed from the given set of pairs.
// ----------------------------------------------------
static void DP_MaxLenChain(int *arr, int n);

void DP_MaxLenChainProgram(void)
{
	printf("Example-1: \n");
	int arr[] = {5, 24, 39, 60, 15, 28, 27, 40, 50, 90};
		       //a,  b,  c,  d,  e,  f,  g,  h,  i,  j

	// c>b or e>d or g>f or i>h, then all are in the chain
	//
	// dp[0] = {}						// 0
	// dp[1] = {5,24}					// 1
	// dp[2] = {5,24}-{39,60}			// 2
	// dp[3] = {15,2}					// 1
	// dp[4] = {5,24}-{27,40}			// 2
	// dp[4] = {15,28}-{27,40}			// 2
	// dp[5] = {5,24}-{50,90}			// 2
	// dp[5] = {15,28}-{50,90}			// 2
	// dp[5] = {5,24}-{27,40}-{50,90}	// 3

	DP_MaxLenChain(arr, 5);
	printf("\n"); // EOL

	printf("Example-2: \n");
	int arr2[] = {5, 10, 1, 11};
	DP_MaxLenChain(arr2, 2);
	printf("\n"); // EOL

	/*printf("Example-3: \n");
	int arr3[] = {5, 24, 39, 60, 15, 28, 27, 40, 50, 90};
	DP_MaxLenChain(arr, 5);
	printf("\n"); // EOL
	*/
}


static void DP_MaxLenChain(int *arr, int n)
{
	int *dp = malloc(sizeof(int)*(n+1));
	for (int k=0; k<(n+1); k++)
		dp[k] = 1;

	for (int j=2; j<(2*n); j+=2)
	{
		for (int i=1; i<j; i+=2)
		{
			if (arr[j]> arr[i])
			{
				if (dp[(j/2)+1] < dp[(i/2)+1] + 1)
				{
					dp[(j/2)+1] = dp[(i/2)+1] + 1;
				}
			}
		}
	}

	printf("maxlen=%d\n", dp[n]);
}


// ----------------------------------------------------
// DP : egg-drop
// ----------------------------------------------------
static void DP_EggDrop(int e, int f);

void DP_EggDropProgram(void)
{
	printf("Example-1: (2,5)\n");
	DP_EggDrop(2, 5);
	printf("\n"); // EOL

	printf("Example-2: (3,7)\n");
	DP_EggDrop(3, 7);
	printf("\n"); // EOL

	printf("Example-3: (1,7)\n");
	DP_EggDrop(1, 7);
	printf("\n"); // EOL

	printf("Example-4: (0,3)\n");
	DP_EggDrop(0, 3);
	printf("\n"); // EOL

	printf("Example-5: (2,0)\n");
	DP_EggDrop(2, 0);
	printf("\n"); // EOL

	printf("Example-6: (3,1)\n");
	DP_EggDrop(3, 1);
	printf("\n"); // EOL

	printf("Example-2: (2,7)\n");
	DP_EggDrop(2, 7);
	printf("\n"); // EOL
}

static void DP_EggDrop(int e, int f)
{
	int dp[e+1][f+1];
	memset(dp, 0, sizeof(int)*(e+1)*(f+1));

	for (int i=1; i<(e+1); i++)
	{
		for (int j=1; j<(f+1); j++)
		{
			if (j==1)
			{
				dp[i][j] = 1;
			}
			else if (i==1)
			{
				dp[i][j] = j;
			}
			else
			{
				int min = INT_MAX;
				for (int k=1; k<=j; k++) // try drpping from each floor
				{
					int below = dp[i-1][k-1];	// breaks: (e-1)(k-1)
					int above = dp[i][j-k]; 	// survives: (e)(f-k)
					int worst = MAX(below, above); // worst attempts to suffice all floors.

					if (min > worst)
					{
						min = worst;
					}
				}

				dp[i][j] = 1 + min;
			}
		}
	}

	printf("min-attempts=%d\n", dp[e][f]);
}


// ----------------------------------------------------
// DP : max sum without-3consecutive
// ----------------------------------------------------
static void DP_maxSumWO3Consec(int *arr, int n);
static void DP_maxSumWO3Consec_simple(int *arr, int n);

void DP_maxSumWO3ConsecProgram(void)
{
	printf("Example-1: \n");
	int arr[] = {1, 2, 3};
	//DP_maxSumWO3Consec(arr, sizeof(arr)/sizeof(arr[0]));
	DP_maxSumWO3Consec_simple(arr, sizeof(arr)/sizeof(arr[0]));
	printf("\n"); // EOL

	printf("Example-2: \n");
	int arr2[] = {3000, 2000, 1000, 3, 10};
	//DP_maxSumWO3Consec(arr2, sizeof(arr2)/sizeof(arr2[0]));
	DP_maxSumWO3Consec_simple(arr2, sizeof(arr2)/sizeof(arr2[0]));
	printf("\n"); // EOL

	printf("Example-3: \n");
	int arr3[] = {100, 1000, 100, 1000, 1};
	//DP_maxSumWO3Consec(arr3, sizeof(arr3)/sizeof(arr3[0]));
	DP_maxSumWO3Consec_simple(arr3, sizeof(arr3)/sizeof(arr3[0]));
	printf("\n"); // EOL

	printf("Example-4: \n");
	int arr4[] = {1, 1, 1, 1, 1};
	//DP_maxSumWO3Consec(arr4, sizeof(arr4)/sizeof(arr4[0]));
	DP_maxSumWO3Consec_simple(arr4, sizeof(arr4)/sizeof(arr4[0]));
	printf("\n"); // EOL

	printf("Example-5: \n"); // issue with last example its sometimes shows 27, 25, 31
	int arr5[] = {1, 2, 3, 4, 5, 6, 7, 8};
	//DP_maxSumWO3Consec(arr5, sizeof(arr5)/sizeof(arr5[0]));
	DP_maxSumWO3Consec_simple(arr5, sizeof(arr5)/sizeof(arr5[0]));
	printf("\n"); // EOL
}


static void DP_maxSumWO3Consec(int *arr, int n)
{
	// sort the array
	Util_Sorting_MergeSort(arr, n);

	int dp[n+1];
	memset(dp, 0, sizeof(int)*(n+1));

	int selection[n+1];
	memset(selection, 0, sizeof(int)*(n+1));

	dp[0] = 0;
	dp[1] = arr[0];
	dp[2] = dp[1] + arr[1];
	selection[2] = -1;

	for (int i=3; i<n+1; i++)
	{
		int max = 0;
		if ((arr[i-1] + dp[i-1] > dp[i]) && selection[i-1] != -1)
		{
			max = arr[i-1] + dp[i-1];
		}

		max = MAX(max, (arr[i-1] + dp[i-2]));

		if ((arr[i-1] + arr[i-2] + dp[i-3]) > max)
		{
			max = (arr[i-1] + arr[i-2] + dp[i-3]);
			selection[i] = -1;
		}

		dp[i] = max;
	}

	printf("maxSum=%d\n", dp[n]);
}


static void DP_maxSumWO3Consec_simple(int *arr, int n)
{
	int dp[n+1];
	memset(dp, 0, sizeof(int)*(n+1));

	if (n >= 1)
		dp[0] = 0;

	if (n >= 2)
		dp[1] = arr[0];

	if (n > 2)
		dp[2] = dp[1] + arr[1];

	for (int i=3; i<n+1; i++)
	{
		dp[i] = MAX(MAX(dp[i-1], (arr[i-1]+dp[i-2])), (arr[i-1] + arr[i-2] + dp[i-3]));
	}

	printf("maxSum=%d\n", dp[n]);
}


// ----------------------------------------------------
// DP : disjoint pairs with less than k
// ----------------------------------------------------
static void DP_disjointlessthanK(int *arr, int n, int k);

void DP_disjointlessthanK_Program(void)
{
	printf("Example-1: \n");
	int arr[] = {3, 5, 10, 15, 17, 12, 9};
	DP_disjointlessthanK(arr, sizeof(arr)/sizeof(arr[0]), 4);
	printf("\n"); // EOL

	printf("Example-2: \n");
	int arr2[] = {5, 15, 10, 300};
	DP_disjointlessthanK(arr2, sizeof(arr2)/sizeof(arr2[0]), 12);
	printf("\n"); // EOL

	printf("Example-3: \n");
	int arr3[] = {100, 1000, 100, 1000, 1};
	DP_disjointlessthanK(arr3, sizeof(arr3)/sizeof(arr3[0]), 901);
	printf("\n"); // EOL

}


static void DP_disjointlessthanK(int *arr, int n, int k)
{
	int dp[n+1];
	memset(dp, 0, sizeof(int) * (n+1));

	Util_Sorting_MergeSort(arr, n);

	for (int j=1; j<n; j++)
	{
		int max = INT_MIN;

		for (int i=0; i<j; i++)
		{
			if (DIFF(arr[i], arr[j]) < k)
			{
				int val = dp[i] + arr[i] + arr[j];
				if (val > max)
					max = val;
			}
		}

		if (max != INT_MIN)
		{
			dp[j+1] = max;
		}
		else
			dp[j+1] = dp[j];
	}

	printf("max-sum pair = %d\n", dp[n]);
}


// ----------------------------------------------------
// DP : num of ways to paint-fences
// ----------------------------------------------------
static void DP_paintFences(int n, int k);

void DP_paintFences_Program(void)
{
	printf("Example-1: \n");
	DP_paintFences(3, 2);
	printf("\n"); // EOL

	printf("Example-2: \n");
	DP_paintFences(5, 3);
	printf("\n"); // EOL

	printf("Example-3: \n");
	DP_paintFences(2, 4);
	printf("\n"); // EOL

	printf("Example-4: \n");
	DP_paintFences(8, 3);
	printf("\n"); // EOL
}


static void DP_paintFences(int n, int k) // atmost 2 consecutive same
{
	int same = k;			// last 2 same : RR, GG, BB
	int diff = k*(k-1);		// last 2 diff : porbability of pick1-pick-rem == (k)*(k-1)

	int total = same + diff;

	for (int i=2; (i<n && k>1); i++)
	{
		same = diff;			// take prev diff and use as it is (cause all the diff can be made to same, only by adding the same color at the end.)
		diff = total*(k-1);		// take all the prev total and to make it diff and all the remaining colors at the end.
		total = same + diff; 	// new total
	}

	printf("numofways to paint = %d\n", total);
}


// ----------------------------------------------------
// DP : num of ways to paint-fences with recursion
// ----------------------------------------------------
static int DP_paintFences_rec(int n, int k, int *dp);

void DP_paintFences_rec_Program(void)
{
	printf("Example-1: \n");
	int *dp = memset(malloc(sizeof(int)*(3+1)), 0, sizeof(int)*(3+1));
	DP_paintFences_rec(3, 2, dp);
	printf("num-of-ways dp= %d\n", dp[3]);
	printf("\n"); // EOL

	printf("Example-2: \n");
	int *dp2 = memset(malloc(sizeof(int)*(5+1)), 0, sizeof(int)*(5+1));
	DP_paintFences_rec(5, 3, dp2);
	printf("num-of-ways dp= %d\n", dp2[5]);
	printf("\n"); // EOL

	printf("Example-3: \n");
	int *dp3 = memset(malloc(sizeof(int)*(2+1)), 0, sizeof(int)*(2+1));
	DP_paintFences_rec(2, 4, dp3);
	printf("num-of-ways dp= %d\n", dp3[2]);
	printf("\n"); // EOL

	printf("Example-4: \n");
	int *dp4 = memset(malloc(sizeof(int)*(8+1)), 0, sizeof(int)*(8+1));
	DP_paintFences_rec(8, 3, dp4);
	printf("num-of-ways dp= %d\n", dp4[8]);
	printf("\n"); // EOL
}


static int DP_paintFences_rec(int n, int k, int *dp)
{
	// already calculated, return dp value
	if (dp[n] != 0)
	{
		return dp[n]; // return total
	}

	// base condition
	if (n == 2)
	{
		// int same = k
		// int diff = k*(k-1)
		// int total = same + diff
		dp[n] = (k + k*(k-1));	// same + diff for n==2 (save total)
		return k*(k-1);			// return diff
	}

	// calculate
	// int same = solve(n-1,k) // same is prev-diff
	// int diff = solve(n-1,k)*(k-1) // diff is prev-total * remaing colors
	int same = DP_paintFences_rec(n-1, k, dp);
	int diff = DP_paintFences_rec(n-1, k, dp)*(k-1);
	dp[n] = same + diff; // save total

	return diff;
}


// ----------------------------------------------------
// DP : Assembly line scheduling
// ----------------------------------------------------
static void DP_assemblyLineSched(int l, int n, int a[l][n], int t[l][n], int e[l], int x[l]);

void DP_assemblyLineSched_Program(void)
{
	printf("Example-1: \n");
    int a[][4] = {{4, 5, 3, 2},
                  {2, 10, 1, 4}};
    int t[][4] = {{0, 7, 4, 5},
                  {0, 9, 2, 8}};
    int e[] = {10, 12}, x[] = {18, 7};
    DP_assemblyLineSched(2, 4, a, t, e, x);
	printf("\n"); // EOL
}


static void DP_assemblyLineSched(int l, int n, int a[l][n], int t[l][n], int e[l], int x[l])
{
	int dp[l][n];
	memset(dp, 0, sizeof(int)*(l*n));

	// leaving 1st station would take exit-time + its own station time
	dp[0][0] = e[0] + a[0][0];
	dp[1][0] = e[1] + a[1][0];

	// evaluate min-time for each station from line-1 or line-2
	for (int j=1; j<n; j++)
	{
		// MIN(from_same_line, from_diff_line)
		dp[0][j] = MIN((dp[0][j-1] + a[0][j]),				// from same = prev-calculated + curr-time.
					   (dp[1][j-1] + t[1][j] + a[0][j]));	// from diff = prev-calculated + change-time + curr-time.

		// MIN(from_same_line, from_diff_line)
		dp[1][j] = MIN((dp[1][j-1] + a[1][j]),				// from same = prev-calculated + curr-time.
					   (dp[0][j-1] + t[0][j] + a[1][j]));	// from diff = prev-calculated + change-time + curr-time.

	}

	int min_time = MIN(dp[0][n-1] + x[0], dp[1][n-1] + x[1]);
	printf("min-time for assembly= %d\n", min_time);
}


// ----------------------------------------------------
// DP : Assembly line scheduling
// ----------------------------------------------------
static void DP_assemblyLineSched_Optimised(int l, int n, int a[l][n], int t[l][n], int e[l], int x[l]);

void DP_assemblyLineSched_Optimised_Program(void)
{
	printf("Example-1: \n");
    int a[][4] = {{4, 5, 3, 2},
                  {2, 10, 1, 4}};
    int t[][4] = {{0, 7, 4, 5},
                  {0, 9, 2, 8}};
    int e[] = {10, 12}, x[] = {18, 7};
    DP_assemblyLineSched_Optimised(2, 4, a, t, e, x);
	printf("\n"); // EOL
}


static void DP_assemblyLineSched_Optimised(int l, int n, int a[l][n], int t[l][n], int e[l], int x[l])
{
	int min_line1 = a[0][0] + e[0];
	int min_line2 = a[1][0] + e[1];

	for (int j=1; j<n; j++)
	{
		int val1 = MIN((min_line1 + a[0][j]),
					   (min_line2 + t[1][j] + a[0][j]));

		int val2 = MIN((min_line2 + a[1][j]),
					   (min_line1 + t[0][j] + a[1][j]));

		min_line1 = val1;
		min_line2 = val2;
	}

	printf("min-time for assembly= %d\n", MIN((min_line1 + x[0]), (min_line2 + x[1])));
}


// ----------------------------------------------------
// DP : Gold Mine Problem
// ----------------------------------------------------
static void DP_goldMine(int m, int n, int mine[m][n]);
static int solve_mine(int m, int n, int mine[m][n], int i, int j, int dp[m][n]);

void DP_goldMine_Program(void)
{
	printf("Example-1: \n");
	int mine[3][3] = {{1, 3, 3},
            		 {2, 1, 4},
					 {0, 6, 4}};

	DP_goldMine(3, 3, mine);
	printf("\n"); // EOL

	printf("Example-2: \n");
	int mine2[4][4] = {{1, 3, 1, 5},
            		   {2, 2, 4, 1},
					   {5, 0, 2, 3},
					   {0, 6, 1, 2}};

	DP_goldMine(4, 4, mine2);
	printf("\n"); // EOL

	printf("Example-3: \n");
	int mine3[4][4] = {{10, 33, 13, 15},
            		   {22, 21, 04, 1},
					   { 5,  0,  2, 3},
                       { 0,  6, 14, 2}};

	DP_goldMine(4, 4, mine3);
	printf("\n"); // EOL
}


static void DP_goldMine(int m, int n, int mine[m][n])
{
	int dp[m][n];
	memset(dp, 0, sizeof(int)*m*n);
	for (int k=0; k <m; k++)
		for (int l=0; l<n; l++)
			dp[k][l] = -1;

	int max = INT_MIN;

	for (int i=0; i<m; i++)
	{
		max = MAX(max, solve_mine(m, n, mine, i, 0, dp));
	}

	printf("gold mined = %d\n", max);
}


static int solve_mine(int m, int n, int mine[m][n], int i, int j, int dp[m][n])
{
	// base
	if ((i == m) || (j == n) || (i <0))
	{
		return 0;
	}

	// valid calculated value
	if (dp[i][j] != -1)
	{
		return dp[i][j];
	}

	int right_up_diag = solve_mine(m, n, mine, i-1, j+1, dp);
	int right = solve_mine(m, n, mine, i, j+1, dp);
	int right_down_diag = solve_mine(m, n, mine, i+1, j+1, dp);

	dp[i][j] = mine[i][j] + MAX(MAX(right_up_diag, right), right_down_diag);

	return dp[i][j];
}


// ----------------------------------------------------
// DP : equal partition subset (wihtout-DP)
// ----------------------------------------------------
static void DP_EqualPartitionSubset(int n, int a[n]);
static bool_t solve_part(int n, int a[n], int i, int part, int sum[2]);

void DP_EqualPartitionSubset_Program(void)
{
	printf("Example-1: \n");
	int a[4] =  {1, 5, 11, 5};
	DP_EqualPartitionSubset(4, a);
	printf("\n"); // EOL

	printf("Example-2: \n");
	int a2[3] = {1, 3, 5};
	DP_EqualPartitionSubset(3, a2);
	printf("\n"); // EOL
}


static void DP_EqualPartitionSubset(int n, int a[n])
{
	int sum[2];
	sum[0] = 0;
	sum[1] = 0;

	if (solve_part(n, a, 0, 0, sum))
	{
		printf("Yes, partition possible\n");
	}
	else
		printf("No, partition not possible\n");
}


static bool_t solve_part(int n, int a[n], int i, int part, int sum[2])
{
	// base condition
	if (i == n)
	{
		if (sum[0] == sum[1])
			return true;
		else
			return false;
	}

	// part-1
	sum[0] += a[i];
	bool_t result1 = solve_part(n, a, i+1, 0, sum);
	sum[0] -= a[i];

	bool_t result2 = false;

	// part-2
	if (i != 0)
	{
		sum[1] += a[i];
		result2 = solve_part(n, a, i+1, 1, sum);
		sum[1] -= a[i];
	}

	return (result1||result2);
}


// ----------------------------------------------------
// DP : edit distance (wihtout-DP)
// ----------------------------------------------------
static int solve_editDist(int i, int j, char *s, char *t, int count);

void DP_EditDistanace_Program(void)
{
	printf("Example-1: \n");
	char *s = "geek";
	char *t = "gesek";
	printf("num of edits= %d\n", solve_editDist(0, 0, s, t, 0));
	printf("\n"); // EOL

	printf("Example-2: \n");
	char *s2 = "gfg";
	char *t2 = "gfg";
	printf("num of edits= %d\n", solve_editDist(0, 0, s2, t2, 0));
	printf("\n"); // EOL

	printf("Example-3: \n");
	char *s3 = "geekcabc";
	char *t3 = "geek";
	printf("num of edits= %d\n", solve_editDist(0, 0, s3, t3, 0));
	printf("\n"); // EOL

	printf("Example-4: \n");
	char *s4 = "ge";
	char *t4 = "geek";
	printf("num of edits= %d\n", solve_editDist(0, 0, s4, t4, 0));
	printf("\n"); // EOL

	printf("Example-5: \n");
	char *s5 = "adceg";
	char *t5 = "abcfg";
	printf("num of edits= %d\n", solve_editDist(0, 0, s5, t5, 0));
	printf("\n"); // EOL
}


static int solve_editDist(int i, int j, char *s, char *t, int count)
{
	// base case
	if (s[i] == '\0' && t[j] == '\0')
	{
		return count;
	}
	else if (t[j] == '\0') // target is ended but src remaining
	{
		// just removal can make the sentence.
		return count + (strlen(s) -i); // remaining in s
	}
	else if (s[i] == '\0') // src s ended but target is remaining
	{
		// just insertion will make a sentence.
		return count + (strlen(t) -j);
	}

	int min = INT_MAX;

	if (s[i] == t[j])
	{
		min = solve_editDist(i+1, j+1, s, t, count); // nothing is done, as char matches.
	}
	else
	{
		// insertion
		int count_i = solve_editDist(i, j+1, s, t, count+1);

		// replace
		int count_rep = solve_editDist(i+1, j+1, s, t, count+1);

		// removal
		int count_rem = solve_editDist(i+1, j, s, t, count+1);

		min = MIN(MIN(count_i, count_rep), count_rem);
	}

	return min;
}


// ----------------------------------------------------
// DP : equal partition subset (DP)
// ----------------------------------------------------
static int solve_editDist(int i, int j, char *s, char *t, int count);
static int DP_solve_editDist(int i, int j, int m, int n, char *s, char *t, int count, int dp[m][n]);

void DP_EditDistanace_Program_dp(void)
{
	// ---------------------
	printf("Example-1: \n");
	char *s = "geek";
	char *t = "gesek";
	int m = strlen(s)+1;
	int n = strlen(t)+1;
	int dp[m][n];
	for (int k=0; k<m; k++)
		for (int l=0; l<n; l++)
			dp[k][l] = -1;

	printf("num of edits= %d\n", DP_solve_editDist(0, 0, m, n, s, t, 0, dp));
	printf("\n"); // EOL

	// ---------------------
	printf("Example-2: \n");
	char *s2 = "gfg";
	char *t2 = "gfg";
	int m2 = strlen(s2)+1;
	int n2 = strlen(t2)+1;
	int dp2[m2][n2];
	for (int k=0; k<m2; k++)
		for (int l=0; l<n2; l++)
			dp2[k][l] = -1;

	printf("num of edits= %d\n", DP_solve_editDist(0, 0, m2, n2, s2, t2, 0, dp2));
	printf("\n"); // EOL

	// ---------------------
	printf("Example-3: \n");
	char *s3 = "geekcabc";
	char *t3 = "geek";
	int m3 = strlen(s3)+1;
	int n3 = strlen(t3)+1;
	int dp3[m3][n3];
	for (int k=0; k<m3; k++)
		for (int l=0; l<n3; l++)
			dp3[k][l] = -1;

	printf("num of edits= %d\n", DP_solve_editDist(0, 0, m3, n3, s3, t3, 0, dp3));
	printf("\n"); // EOL

	// ---------------------
	printf("Example-4: \n");
	char *s4 = "ge";
	char *t4 = "geek";
	int m4 = strlen(s4)+1;
	int n4 = strlen(t4)+1;
	int dp4[m4][n4];
	for (int k=0; k<m4; k++)
		for (int l=0; l<n4; l++)
			dp4[k][l] = -1;

	printf("num of edits= %d\n", DP_solve_editDist(0, 0, m4, n4, s4, t4, 0, dp4));
	printf("\n"); // EOL

	// ---------------------
	printf("Example-5: \n");
	char *s5 = "adceg";
	char *t5 = "abcfg";
	int m5 = strlen(s5)+1;
	int n5 = strlen(t5)+1;
	int dp5[m5][n5];
	for (int k=0; k<m5; k++)
		for (int l=0; l<n5; l++)
			dp5[k][l] = -1;

	printf("num of edits= %d\n", DP_solve_editDist(0, 0, m5, n5, s5, t5, 0, dp5));
	printf("\n"); // EOL
}


static int DP_solve_editDist(int i, int j, int m, int n, char *s, char *t, int count, int dp[m][n])
{
	// if already calculated
	if (dp[i][j] != -1 )
	{
		return dp[i][j];
	}

	// base case
	if (s[i] == '\0' && t[j] == '\0')
	{
		return dp[i][j] = count;
	}
	else if (t[j] == '\0') // target is ended but src remaining
	{
		// just removal can make the sentence.
		return dp[i][j] = count + (strlen(s) -i); // remaining in s
	}
	else if (s[i] == '\0') // src s ended but target is remaining
	{
		// just insertion will make a sentence.
		return dp[i][j] = count + (strlen(t) -j);
	}

	int min = INT_MAX;

	if (s[i] == t[j])
	{
		min = solve_editDist(i+1, j+1, s, t, count); // nothing is done, as char matches.
	}
	else
	{
		// insertion
		int count_i = solve_editDist(i, j+1, s, t, count+1);

		// replace
		int count_rep = solve_editDist(i+1, j+1, s, t, count+1);

		// removal
		int count_rem = solve_editDist(i+1, j, s, t, count+1);

		min = MIN(MIN(count_i, count_rep), count_rem);
	}

	return dp[i][j] = min;
}


// ----------------------------------------------------
// DP : edit distance (Tabulation DP problem)
// ----------------------------------------------------
static void DP_Tabulation_solve_editDist(char *s, char *t);

void DP_EditDistanace_Tabulation_Program(void)
{
	printf("Example-1: \n");
	char *s = "geek";
	char *t = "gesek";
	DP_Tabulation_solve_editDist(s, t);
	printf("\n"); // EOL

	printf("Example-2: \n");
	char *s2 = "gfg";
	char *t2 = "gfg";
	DP_Tabulation_solve_editDist(s2, t2);
	printf("\n"); // EOL

	printf("Example-3: \n");
	char *s3 = "geekcabc";
	char *t3 = "geek";
	DP_Tabulation_solve_editDist(s3, t3);
	printf("\n"); // EOL

	printf("Example-4: \n");
	char *s4 = "ge";
	char *t4 = "geek";
	DP_Tabulation_solve_editDist(s4, t4);
	printf("\n"); // EOL

	printf("Example-5: \n");
	char *s5 = "adceg";
	char *t5 = "abcfg";
	DP_Tabulation_solve_editDist(s5, t5);
	printf("\n"); // EOL
}


static void DP_Tabulation_solve_editDist(char *s, char *t)
{
	// create dp
	int m = strlen(s)+1;
	int n = strlen(t)+1;
	int dp[m][n];
	for (int k=0; k<m; k++)
		for (int l=0; l<n; l++)
			dp[k][l] = -1;


	for (int i=0; i<m; i++)
	{
		for (int j=0; j<n; j++)
		{
			if (i==0 && j==0) // "NULL" to "NULL"
			{
				dp[i][j] = 0;
			}
			else if (i==0)	//0th row
			{
				dp[i][j] = dp[i][j-1] + 1; //insertion
			}
			else if (j==0)	// 0th col
			{
				dp[i][j] = dp[i-1][j] + 1; // removal
			}
			else	// everything else
			{
				if (s[i-1] == t[j-1])
				{
					dp[i][j] = dp[i-1][j-1]; // no-change (diagonal)
				}
				else
				{
					// min of insertion or removal or replacement
					dp[i][j] = MIN(MIN(dp[i-1][j], dp[i-1][j-1]), dp[i][j-1]) + 1;
				}
			}
		}
	}

	printf("num of edits= %d\n", dp[m-1][n-1]);
}

