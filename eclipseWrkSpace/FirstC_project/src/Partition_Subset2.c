/*
 * Partition_Subset2.c
 *
 *  Created on: 24-Jul-2022
 *      Author: sumit
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Partition_Subset2.h"
#include "Util.h"


void PartionSubset2_EqualSumPartitionProgram(void);
void PartionSubset2_DP_Partition_subsetProgram(void);


// ----------------------------------------------------
// Main-driver function
// ----------------------------------------------------
void PartionsSubset2_Program(void)
{
	// equal-sum-partion subset
	PartionSubset2_EqualSumPartitionProgram();

	// DP : partition subsets (divide people in k-different teams)
	PartionSubset2_DP_Partition_subsetProgram();
}


// ----------------------------------------------------
// BT: Equal-sum-partition
// ----------------------------------------------------
typedef struct element
{
	int val;	// can be a char or a string
	struct element *next;
}element_d;

typedef struct Partition
{
	element_d *head;
	element_d *tail;
	int size;
	int sum;
}Partition_d;

typedef struct Set
{
	Partition_d *parts;
	int size;
	struct Set *next;
}Set_d;

typedef struct List
{
	Set_d *head;
	Set_d *tail;
	int size;
}List_d;


static void PartionSubset2_EqualSumPartition_BT(int n, int *arr, int k);
static bool_t solve(int i, int n, int *arr, int k, Set_d *asf, List_d *list);
static bool_t isValid(Set_d *asf);
static Set_d *create_set(int k);
static void addElement(Partition_d *part, int val);
static int remElement(Partition_d *part, int val);
static Set_d *copySet(Set_d *old);
static void addToList(Set_d *asf, List_d *list);
static void printList(List_d *list);
static void printSet(Set_d *set);

void PartionSubset2_EqualSumPartitionProgram(void)
{
	printf("Example-1: \n");
	printf("K-partition subsets: {1, 2, 3, 4} : n=4, k=2\n");
	int array1[] = {1, 2, 3, 4};
	PartionSubset2_EqualSumPartition_BT(sizeof(array1)/ sizeof(array1[0]), array1, 2);
	printf("\n"); // EOL

	printf("Example-2: \n");
	printf("K-partition subsets: {1, 5, 11, 5} : n=4, k=2\n");
	int array2[] = {1, 5, 11, 5};
	PartionSubset2_EqualSumPartition_BT(sizeof(array2)/ sizeof(array2[0]), array2, 2);
	printf("\n"); // EOL

	printf("Example-3: \n");
	printf("K-partition subsets: {2, 1, 4, 5, 6} : n=5, k=3\n");
	int array3[] = {2, 1, 4, 5, 6};
	PartionSubset2_EqualSumPartition_BT(sizeof(array3)/ sizeof(array3[0]), array3, 3);
	printf("\n"); // EOL

	printf("Example-4: \n");
	printf("K-partition subsets: {2, 1, 4, 5, 6} : n=5, k=4\n");
	PartionSubset2_EqualSumPartition_BT(sizeof(array3)/ sizeof(array3[0]), array3, 4);
	printf("\n"); // EOL

	printf("Example-5: \n");
	printf("K-partition subsets: {2} : n=1, k=1\n");
	int array4[] = {2};
	PartionSubset2_EqualSumPartition_BT(sizeof(array4)/ sizeof(array4[0]), array4, 1);
	printf("\n"); // EOL

	printf("Example-6: \n");
	printf("K-partition subsets: {2, 1, 4, 5, 6} : n=5, k=0\n");
	PartionSubset2_EqualSumPartition_BT(sizeof(array3)/ sizeof(array3[0]), array3, 0);
	printf("\n"); // EOL

	printf("Example-7: \n");
	printf("K-partition subsets: {2, 1, 4, 5, 6} : n=5, k=7\n");
	PartionSubset2_EqualSumPartition_BT(sizeof(array3)/ sizeof(array3[0]), array3, 7);
	printf("\n"); // EOL
}


static void PartionSubset2_EqualSumPartition_BT(int n, int *arr, int k)
{
	Set_d *asf = create_set(k);
	List_d *list = memset(malloc(sizeof(List_d)), 0, sizeof(List_d));

	if (!solve(0, n, arr, k, asf, list))
		printf("No, not possible to partition\n");
	else
	{
		// print or return the list
		printList(list);
	}
}

static bool_t solve(int i, int n, int *arr, int k, Set_d *asf, List_d *list)
{
	bool_t result = false;

	// base condition
	if ((n<=1) || (k<=1) || k>n)
		return false;

	// exit condition
	if (i == n)
	{
		if (isValid(asf))
		{
			printSet(asf);

			// if reaches here means partions matched.
			// add set to the list
			addToList(asf, list);
			return true;
		}

		return false;
	}

	// trying options: all partitions one-by-one
	for (int p=0; p<k; p++)
	{
		if (!(&asf->parts[p])->size) // if empty-set: part-0 is empty
		{
			addElement(&asf->parts[p], arr[i]);
			result |= solve(i+1, n, arr, k, asf, list);
			remElement(&asf->parts[p], arr[i]);
			break;
		}
		else
		{
			addElement(&asf->parts[p], arr[i]);
			result |= solve(i+1, n, arr, k, asf, list);
			remElement(&asf->parts[p], arr[i]);
		}
	}

	return result;
}


static bool_t isValid(Set_d *asf)
{
	// check for equal sums in the partition
	for (int p=1; p<(asf->size); p++)
	{
		if ((&asf->parts[p-1])->sum != (&asf->parts[p])->sum)
		{
			return false;
		}
	}

	return true;
}

static Set_d *create_set(int k)
{
	Set_d *set = memset(malloc(sizeof(Set_d)), 0, sizeof(Set_d));
	set->parts = memset(malloc(sizeof(Partition_d)*k), 0, sizeof(Partition_d)*k);
	set->size = k;
	return set;
}


static void addElement(Partition_d *part, int val)
{
	element_d *new = memset(malloc(sizeof(element_d)), 0, sizeof(element_d));
	new->val = val;

	if (part->head == NULL) // first element
	{
		part->head = part->tail = new;
	}
	else // add to tail
	{
		part->tail->next = new;
		part->tail = part->tail->next;
	}

	part->size++;
	part->sum += val;
}


static int remElement(Partition_d *part, int val)
{
	element_d *cur = part->head;
	element_d *prev = NULL;

	while(cur)
	{
		if (cur->val == val)
			break;

		prev = cur;
		cur = cur->next;
	}

	if (cur == NULL)
	{
		return -1; // empty set
	}
	else if ((part->head == cur) && (part->tail == cur)) // rem the only element
	{
		free(part->head);
		part->head = part->tail = NULL;
	}
	else if (part->head == cur)
	{
		part->head = cur->next;
		free(cur);
	}
	else if (part->tail == cur)
	{
		prev->next = cur->next;
		part->tail = prev;
		free(cur);
	}
	else
	{
		prev->next = cur->next;
		free(cur);
	}

	part->size--;
	part->sum -= val;

	return 0;
}


static void addToList(Set_d *asf, List_d *list)
{
	Set_d *new = copySet(asf);

	if (list->head == NULL)
	{
		list->head = list->tail = new;
	}
	else
	{
		list->tail->next = new;
		list->tail = list->tail->next;
	}

	list->size++;
}


static Set_d *copySet(Set_d *old)
{
	Set_d *new = create_set(old->size);

	for(int p=0; p<old->size; p++)
	{
		element_d *cur = (&old->parts[p])->head;
		while (cur)
		{
			addElement(&new->parts[p], cur->val);
			cur = cur->next;
		}
	}

	return new;
}


static void printList(List_d *list)
{
	Set_d *cur = list->head;
	printf("print-list: ");
	while (cur)
	{
		// print set
		printf("{");
		for (int p=0; p<cur->size; p++)
		{
			printf("{");
			element_d *e = (&cur->parts[p])->head;
			while(e)
			{
				printf("%d", e->val);
				if(e->next != NULL)
					printf(",");

				e = e->next;
			}
			printf("}");

			if(p != cur->size-1)
				printf(",");
		}
		printf("}\n");
		if(cur->next != NULL)
			printf(",");

		cur = cur->next;
	}
}

static void printSet(Set_d *set)
{
	Set_d *cur = set;

	// print set
	printf("{");
	for (int p=0; p<cur->size; p++)
	{
		printf("{");
		element_d *e = (&cur->parts[p])->head;
		while(e)
		{
			printf("%d", e->val);
			if(e->next != NULL)
				printf(",");

			e = e->next;
		}
		printf("}");

		if(p != cur->size-1)
			printf(",");
	}
	printf("}\n");
}

// ----------------------------------------------------
// DP: partition subsets (divide people in k-different teams)
// ----------------------------------------------------
static void DP_subset_partition(int n, int k);

void PartionSubset2_DP_Partition_subsetProgram(void)
{
	printf("partition subsets (divide people in k-different teams)\n");
	printf("Example-1: \n");
	DP_subset_partition(5, 4);
	printf("\n"); // EOL

	printf("Example-2: \n");
	DP_subset_partition(2, 3);
	printf("\n"); // EOL

	printf("Example-3: \n");
	DP_subset_partition(3, 2);
	printf("\n"); // EOL

	printf("Example-4: \n");
	DP_subset_partition(4, 3);
	printf("\n"); // EOL

	printf("Example-5: \n");
	DP_subset_partition(4, 1);
	printf("\n"); // EOL
}

static void DP_subset_partition(int n, int k)
{
	//int dp[][] = memset(malloc(sizeof(int)*n), 0, sizeof(int)*n);
	int dp[k+1][n+1];
	memset(dp, 0, sizeof(int)*(n+1)*(k+1));

	// since n=0 || k=0 means 0-ways
	for (int i=1; i<k+1; i++)
	{
		for (int j=1; j<n+1; j++)
		{
			if ((i==1) || (i==j)) // k=1 or k==n then only 1-way.
			{
				dp[i][j] = 1;
			}
			else if (i>j)
			{
				dp[i][j] = 0;
			}
			else
			{
				dp[i][j] = (dp[i][j-1]*i) + dp[i-1][j-1];	// ((n-1)[k] * k) + (n-1)[k-1]
			}
		}
	}

	printf("num-ways: %d\n", dp[k][n]);
}


