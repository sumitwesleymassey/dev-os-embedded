#ifndef _HEAP_H__
#define _HEAP_H__

// to find size
//#define SIZEOFARRAY(var, type) (sizeof(var)/sizeof(type))

// if start index is '0'
#define CHILD_1(i)      ((i*2)+1)
#define CHILD_2(i)      ((i*2)+2)
#define PARENT(i)       ((i-1)/2)

void Heap_BinaryTree_Heap(void);

#endif  /* _HEAP_H__ */
