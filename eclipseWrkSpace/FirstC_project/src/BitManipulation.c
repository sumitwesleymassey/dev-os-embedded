/*
 * BitManipulation.c
 *
 *  Created on: 30-Jul-2023
 *      Author: sumit
 */
#include <stdio.h>
#include <stdlib.h>
#include "Util.h"
#include <string.h>
#include "BitManipulation.h"

void BitManipulation_PowerSet_Program(void);
void BitManipulation_NONRepeatingNumbers_Program(void);

// ----------------------------------------------------
// Main-driver function
// ----------------------------------------------------
void BitManipulation_DriverProgram(void)
{
    // Bit Manipulation : PowerSet
    printf("Bit Manipulatiiopn Power-Set program: \n");
    BitManipulation_PowerSet_Program();
    printf("\n"); // EOL

    // Bit Manipulation : PowerSet
    printf("Bit Manipulatiiopn Non Repeating Numbers: \n");
    BitManipulation_NONRepeatingNumbers_Program();
    printf("\n"); // EOL
}

// ----------------------------------------------------
// Bit Manipulation : PowerSet
// ----------------------------------------------------
static void BitManipulation_PowerSet(char *str, int size);
static void BitManipulation_PowerSet_Recursion(char *str, int size, int count, int i, char *ans);
static int two_pow_n(int n);

void BitManipulation_PowerSet_Program(void)
{
    printf("Example-1: \n");
    char *str1 = "abc";
    BitManipulation_PowerSet(str1, strlen(str1));
    printf("\n"); // EOL

    printf("Example-2: \n");
    char *str2 = "aa";
    BitManipulation_PowerSet(str2, strlen(str2));
    printf("\n"); // EOL

    printf("Example-3: \n");
    char *str3 = "abcde";
    BitManipulation_PowerSet(str3, strlen(str3));
    printf("\n"); // EOL
}

static void BitManipulation_PowerSet(char *str, int size)
{
    unsigned int numOfSubSequences = two_pow_n(size);
    char *ans = memset(malloc(sizeof(char)*size + 1), '\0', sizeof(char)*size + 1);
    for (int  i=1; i<numOfSubSequences; i++)
    {
        int count = 0;
        //int position = i;
//        while (count < size)
//        {
//            if (position & 1)
//            {
//                printf("%c",str[count]);
//            }
//            count ++;
//            position >>= 1;
//        }
        BitManipulation_PowerSet_Recursion(str, size, count, i, ans);
        printf("%s", ans);
        memset(ans, '\0', sizeof(char)*size + 1);
        printf(", "); // EOL
    }
}

static void BitManipulation_PowerSet_Recursion(char *str, int size, int count, int i, char *ans)
{
    if (count == size) //keep aof in a list
        return;

    if (i & 1)
    {
        //printf("%c", str[count]);
        memcpy(ans, &str[count], 1);
        ans++;
    }

    BitManipulation_PowerSet_Recursion(str, size, count+1, i >> 1, ans);
}

static int two_pow_n(int n)
{
    int two_pow_n = 1;
    while (n--)
    {
        two_pow_n *=2;
    }

    return two_pow_n;
}

// ----------------------------------------------------
// Bit Manipulation : Non Repeating Numbers
// ----------------------------------------------------
static void BitManipulation_NONRepeatingNumbers(int *arr, int size);

void BitManipulation_NONRepeatingNumbers_Program(void)
{
    printf("Example-1: \n");
    int arr1[] = {1, 2, 3, 2, 1, 4};
    BitManipulation_NONRepeatingNumbers(arr1, sizeof(arr1)/(sizeof(int)));
    printf("\n"); // EOL

    printf("Example-2: \n");
    int arr2[] = {2, 1, 3, 2};
    BitManipulation_NONRepeatingNumbers(arr2, sizeof(arr2)/(sizeof(int)));
    printf("\n"); // EOL

    printf("Example-3: \n");
    int arr3[] = {36, 50, 24, 56, 36, 24,  42, 50};
    BitManipulation_NONRepeatingNumbers(arr3, sizeof(arr3)/(sizeof(int)));
    printf("\n"); // EOL
}

static void BitManipulation_NONRepeatingNumbers(int *arr, int size)
{
    int xor_all = arr[0];
    for (int i=1; i<size; i++)
    {
        xor_all ^=arr[i];
    }

    int rmbsm = xor_all & (~(xor_all-1)); // xor_all & (-xor_all)
    int valueA = 0;
    int valueB = 0;
    for (int i=0; i<size; i++)
    {
        if (arr[i] & rmbsm)
        {
            valueA ^= arr[i];
        }
        else
        {
            valueB ^= arr[i];
        }
    }

    printf("valueA = %d and valueB = %d", valueA, valueB);
}




