/*
 * Graph.h
 *
 *  Created on: 24-Apr-2022
 *      Author: sumit
 */

#ifndef INCLUDE_GRAPH_H_
#define INCLUDE_GRAPH_H_

void Graph_Program(void);


// ----------------------------------------------------
// Adjacency-Matrix
// ----------------------------------------------------
void createGraph(int n, int graph[n][n]);
void displayGraph(int n, int graph[n][n]);
void addEdge(int n, int graph[n][n], int u, int v);


// ----------------------------------------------------
// Adjacency-List
// ----------------------------------------------------
typedef struct AdjacencyNode
{
	int v;
	int w;
	struct AdjacencyNode *next_node;

}AdjacencyNode_d;


typedef struct Vertex
{
	int v;
	AdjacencyNode_d *head_node;
	AdjacencyNode_d *tail_node;
	struct Vertex *next_vertex;
	int num_edges;
}Vertex_d;

typedef struct AdjList
{
	Vertex_d *head_vertex;
	Vertex_d *tail_vertex;
	int size;
}AdjList_d;


void addVertexToList(AdjList_d *list, int u);
Vertex_d *getVertex(AdjList_d *list, int uv);
void addAdjEdge(AdjList_d *list, int u, int v, int w);
void displayAjdList(AdjList_d *list);
void destroyAjdList(AdjList_d *list);


#endif /* INCLUDE_GRAPH_H_ */
