/*
 * String_PatternMatch.c
 *
 *  Created on: 12-Mar-2022
 *      Author: sumit
 */
#include <stdio.h>
#include <stdlib.h>
#include "String_PatternMatch.h"


static void String_PatternMatchingViaKMP(char *string, int sizeofString, char *pattern, int sizeofPatttern, int *lps);

static void String_FindLPS(char *pattern, int size, int *lps);
static void String_FindLPS_failed(char *pattern, int size, int lps[]);
void printArrayAndList(char pattern[], int lps[], int size);



void String_PatternMatchProgram(void)
{
	//
	//  index matched @       10  14          26  30          42   46
	//                         |   |           |   |           |   |
	char string[] = "ababcabcabababd ababcabcabababd ababcabcabababd";
	char pattern1[] = "ababd";
	//	         	   00120


	//
	//  index matched @   4      12        22      30 33       41
	//                    |       |         |       |  |       |
	char string2[] = "ababaaaabaacdbd ababcaaaaabaacd aaaaabaacdababd";
	char pattern2[] = "aaaabaacd";
	//				   012301200

	int sizeofString = sizeof(string) -1;
	//int sizeofPatttern = sizeof(pattern2) -1;
	int sizeofPatttern = sizeof(pattern1) -1;;

	int *lps = (int *)malloc(sizeofPatttern);

	String_FindLPS(pattern1, sizeofPatttern, lps);

	printArrayAndList(pattern1, lps, sizeofPatttern);

	String_PatternMatchingViaKMP(string, sizeofString, pattern1, sizeofPatttern, lps);

	free(lps);
}

/**
 * Naive Matching
 *
 * then do the KMP algo matching
 */

static void String_PatternMatchingViaKMP(char *string, int sizeofString, char *pattern, int sizeofPatttern, int *lps)
{
	int i=0;
	int j=0;
	int count = 0;

	// iterate over string and patterns
	while (j<sizeofString && i<sizeofPatttern)
	{
		// doesnt match
		if (string[j] != pattern[i])
		{
			// if i=0 already means need to goto next of j
			if (i!=0)
				i = lps[i-1];
			else
				j++;
		}
		else
		{
			i++;
			j++;
		}

		// to return count of "pattern matched" from waht index to what..
		if (i == sizeofPatttern)
		{
			printf("Pattern matched: index=%d - index=%d \n", j-sizeofPatttern, j-1);
			i=0; //reset pattern counter
			count++;
		}
	}

	// check if pattern matched or not.
	if (0 == count)
	{
		printf("Pattern not matched\n");
	}
	else
	{
		printf("count: %d\n", count);
	}
}



static void String_FindLPS(char *pattern, int size, int lps[])
{
	int i=0;
	int j=1;

	// first value will be 0
	lps[0] = 0;

	// non-optimised way
	/*for(;j<size;j++)
	{
		if (pattern[i] == pattern[j])
		{
			lps[j] = i+1;
			i++;
		}
		else
		{
			i=0;
			if (pattern[i] == pattern[j])
			{
				lps[j] = i+1;
				i++;
			}
			else
				lps[j] = i;
		}
	}*/

	// little optimised
	while (j<size)
	{
		if (pattern[i] == pattern[j])
		{
			lps[j] = ++i;
			j++;
		}
		else
		{
			if (i!=0) {
				i=0;
			} else
			{
				lps[j] = i;
				j++;
			}
		}
	}
}



static void String_FindLPS_failed(char *pattern, int size, int lps[])
{
	int i=0;
	int j=0;
	int k=1;

	lps[0] = 0;

	for(;k<size; k++)
	{
		j = k;
		i = 0;
		while ((pattern[j] == pattern[i]) && i<k)
		{
			i++; j--;
		}

		lps[k] = i;
	}
}


void printArrayAndList(char pattern[], int lps[], int size)
{
	if(pattern!=NULL)
	{
		printf("Pat[]= ");
		for (int i=0; i<size; i++)
		{
			printf("%c ", pattern[i]);
		}
		printf("\n");
	}

	// print LPS
	printf("LPS[]= ");

	// print array
	for (int i=0; i<size; i++)
	{
		printf("%d ", lps[i]);
	}
	printf("\n");
}
