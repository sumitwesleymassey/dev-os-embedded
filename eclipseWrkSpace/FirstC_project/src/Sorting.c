#include <stdio.h>
#include <stdlib.h>
#include "Sorting.h"

static void Sorting_MergeSort(int arr[], int sizeOfArr);
static void Sorting_Merge(int left[], int sizeofLeft, int right[], int sizeofRight, int arr[]);

static int Sorting_PerformSort(int start, int end, int arr[]);
static void Sorting_QuickSort(int start, int end, int arr[]);

// ----------------------------------------------------
// Merge Sorting
// ----------------------------------------------------
void Sorting_MergeSortProgram(void)
{
	int i=0;
	int arr[] = {23,  4, 56, 13, 21, 37, 1};

	int MAX = (sizeof(arr)/sizeof(int));
	Sorting_MergeSort(arr, MAX);

	// Print
	for ( ; i<MAX; i++)
	{
		printf("%d ", arr[i]);
	}
}


static void Sorting_MergeSort(int arr[], int sizeOfArr)
{
	if(sizeOfArr == 1)	// base return value
		return;

	int middle = sizeOfArr/2;
	int *pLeft = (int *)malloc(middle);
	int *pRight = (int *)malloc(sizeOfArr - middle);

	int i=0,j=0;

	// copy to divided arrays
	for (;i<sizeOfArr;i++)
	{
		if (i<middle)
		{
			pLeft[i] = arr[i];
		}
		else
		{
			pRight[j] = arr[i];
			j++;
		}

	}

	Sorting_MergeSort(pLeft, middle);	// Left
	Sorting_MergeSort(pRight, (sizeOfArr - middle));	// Right

	Sorting_Merge(pLeft, middle, pRight, (sizeOfArr - middle), arr);	// Merge "Left+Right" --> "Array"

	free(pLeft);
	free(pRight);

	// function exit.
	return;
}

static void Sorting_Merge(int left[], int sizeofLeft, int right[], int sizeofRight, int arr[])
{
	// counters
	int i=0,l=0,r=0;

	// compare till either left is complete or right is complete
	// and position the values.
	while(l<sizeofLeft && r<sizeofRight)
	{
		if (left[l]<right[r])
		{
			arr[i] = left[l];
			i++;
			l++;
		}
		else
		{
			arr[i] = right[r];
			i++;
			r++;
		}
	}

	// remainings of Left or Right
	while (l<sizeofLeft)
	{
		arr[i] = left[l];
		i++;
		l++;
	}

	while (r<sizeofRight)
	{
		arr[i] = right[r];
		i++;
		r++;
	}
}


// ----------------------------------------------------
// Quick Sorting  |Time: O(nlog(n)) | Space: O(log(n))
// ----------------------------------------------------
void Sorting_QuickSortProgram(void)
{
	int i=0;
	int arr[] = {23,  4, 56, 13, 21, 37, 1};
	int arrb[] = {45, 67,  0,  5, 27, 91, 3};
	// {1,  4, 56, 13, 21, 37, 23};
	// {1,  4, 13, 56, 21, 37, 23};
	// {1,  4, 13, 21, 56, 37, 23};
	// {1,  4, 13, 21, 23, 37, 56};


	int MAX = (sizeof(arr)/sizeof(int));
	Sorting_QuickSort(0, MAX-1, arrb);

	// Print
	for ( ; i<MAX; i++)
	{
		printf("%d ", arrb[i]);
	}
}


static void Sorting_QuickSort(int start, int end, int arr[])
{
	//base case
	if(start > end)
		return;

	int pivot = Sorting_PerformSort(start, end, arr);

	// divide the code
	Sorting_QuickSort(start, pivot-1, arr); // left
	Sorting_QuickSort(pivot+1, end, arr); 	// right

	return;
}

static int Sorting_PerformSort(int start, int end, int arr[])
{
	int j=start;
	int i = j-1;
	int temp;

	for( ;j<end; j++)
	{
		if(arr[j]<arr[end])
		{
			i++;
			//swap
			temp = arr[j];
			arr[j] = arr[i];
			arr[i] = temp;
		}
	}

	//swap pivot
	temp = arr[j]; // j == end
	i++;
	arr[j] = arr[i];
	arr[i] = temp;

	return i; // return pivot;
}


