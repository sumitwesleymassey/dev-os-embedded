/*
 * BinaryTrees_session_2.c
 *
 *  Created on: 10-Apr-2022
 *      Author: sumit
 */


#include <stdio.h>
#include <stdlib.h>
#include "BinaryTrees_session_2.h"
#include "Util.h"


#define DIAGONAL_SUM

void *insertNodetoleft(TreeNode_d *root, char value, bool_t toleft);
int BinaryTrees_treeHeight(TreeNode_d *root);
static void BT_preorder_traversal(TreeNode_d *root, DynamicMemory_d *HT, int hd);
static void BT_min_max_Hdistance(TreeNode_d *root, int hd, int *min, int *max);
void BT_preorder_diagonaldistance(TreeNode_d *root, DynamicMemory_d *HT, int Dd);

static char *solve(TreeNode_d *root, DynamicMemory_Map_d *map, DynamicMemory_List_d *list);

void BinaryTrees_ProgramSession2(void)
{
	// spiral traversal
	TreeNode_d *root_spiral = constructCustomBinaryTree();
	printf("spiral traversal: ");
	BinrayTrees_Spiral(root_spiral, 12);
	printf("\n"); // EOL

	printf("level-order traversal: ");
	printf("\n"); // EOL
	BinaryTrees_LevelOrder_Traversal(root_spiral, 12);

	printf("left view traversal: ");
	BinaryTrees_Sideview_Traversal(root_spiral, 12);
	printf("\n"); // EOL

	printf("right view traversal: ");
	BinaryTrees_rightview(root_spiral, 12);
	printf("\n"); // EOL

	printf("vertical order traversal: ");
	BinaryTrees_VerticalOrder_Traversal(root_spiral, 12);
	printf("\n"); // EOL

	printf("BotView traversal: ");
	BinaryTrees_BotView(root_spiral, 12);
	printf("\n"); // EOL

	printf("TopView traversal: ");
	BinaryTrees_TopView(root_spiral, 12);
	printf("\n"); // EOL

	printf("diagonal traversal: ");
	printf("\n"); // EOL
	BinaryTrees_TraverseDiagonals(root_spiral, 12);

	printf("diagonal traversal without Q: ");
	printf("\n"); // EOL
	BinaryTree_DiagonalTraversal(root_spiral);

	printf("node having K leaves: \n");
	printf ("total leaves = %d \n", BinaryTrees_Nodes_with_K_Leaves(root_spiral, 3));

	printf("LCA for i and j: %c\n", BinaryTrees_LowestCommonAncestor(root_spiral, 'i', 'j')->value);
	printf("LCA for i and e: %c\n", BinaryTrees_LowestCommonAncestor(root_spiral, 'i', 'e')->value);
	printf("LCA for k and l: %c\n", BinaryTrees_LowestCommonAncestor(root_spiral, 'k', 'l')->value);
	printf("LCA for c and g: %c\n", BinaryTrees_LowestCommonAncestor(root_spiral, 'c', 'g')->value);


	printf("diameter of a tree: %d\n", BinaryTrees_findDiameter(root_spiral));
	printf("diameter of a tree: %d\n", BinaryTrees_findDiameter(root_spiral));

	printf("isIdentical: %d\n", BinaryTrees_isIdentical(root_spiral, root_spiral));
	printf("isIdentical: %d\n", BinaryTrees_isIdentical(root_spiral, constructCustomBinaryTree2()));

	printf("isSubtree: %d\n", BinaryTrees_isSubTree(root_spiral, constructCustomSubTree()));
	printf("isDupacateSubtree present: \n");
	BinaryTrees_isDupalicateSubTreePresent(constructCustomTreeWithDuplicates());
	printf("\n"); // EOL

	printf("print all ancestors: \n");
	BinaryTrees_PrintAllAncestors(root_spiral, 'i');
	printf("\n"); // EOL

	printf("find distance between 2 nodes: %d\n", BinaryTrees_distance_between(root_spiral, 'h', 'e'));
}


// ----------------------------------------------------
// distance between 2 nodes
// ----------------------------------------------------
static TreeNode_d *find_LCA(TreeNode_d *root, int A, int B);
static int distanceFromAncestor(TreeNode_d *root, int A);

int BinaryTrees_distance_between(TreeNode_d *root, int A, int B)
{
	if (root == NULL)
		return 0;

	// find LCA
	TreeNode_d *lca = find_LCA(root, A, B);

	int distA = distanceFromAncestor(lca, A) -1; // count of ancestors to be reduced
	int distB =	distanceFromAncestor(lca, B) -1; // count of ancestors to be reduced

	// left dist + right distance // dont include ancestor here.
	return distA + distB;
}

static int distanceFromAncestor(TreeNode_d *root, int A)
{
	if (root == NULL)
		return 0;

	if (root->value == A)
		return 1;

	int distL = distanceFromAncestor(root->left, A);
	int distR = distanceFromAncestor(root->right, A);

	if (!distL && !distR)
		return 0;

	return (distL ? distL+1 : distR+1);
}


static TreeNode_d *find_LCA(TreeNode_d *root, int A, int B)
{
	if (root == NULL)
		return NULL;

	if ((root->value == A) || (root->value == B))
		return root;

	TreeNode_d *lNode = find_LCA(root->left, A, B);
	TreeNode_d *rNode = find_LCA(root->right, A, B);

	if (lNode && rNode)
		return root;

	return lNode ? lNode : rNode;
}




// ----------------------------------------------------
// print all ancestors of a node
// ----------------------------------------------------
bool_t BinaryTrees_PrintAllAncestors(TreeNode_d *root, int A)
{
	bool_t result = false;

	if (root == NULL)
		return false;

	if (root->value == A)
		return true;	// return true if found


	// if either a node is found in either left or right
	// that means cur-node is an ancestor.
	if (BinaryTrees_PrintAllAncestors(root->left, A) || BinaryTrees_PrintAllAncestors(root->right, A))
	{
		printf("%c ", root->value);
		result = true;
	}

	return result;
}


// ----------------------------------------------------
// if one tree is a sub tree of another
// ----------------------------------------------------
void BinaryTrees_FindAllDupacateSubTreePresent(TreeNode_d *root)
{

}



// ----------------------------------------------------
// if one tree is a sub tree of another
// ----------------------------------------------------

bool_t BinaryTrees_isDupalicateSubTreePresent(TreeNode_d *root)
{
	bool_t result = false;

	if (!root)
		return false;

	DynamicMemory_Map_d *map = Util_createHashMap(0, 0, 0);
	DynamicMemory_List_d *list = Util_createList();
	solve(root, map, list);

	// traverse list
	LL_TreeNode_d *cur = list->head;
	while (cur)
	{
		print_preorder(cur->node);
		printf("\n"); // EOL

		result = true;
		cur = cur->next;
	}

	return result;
}


static char *solve(TreeNode_d *root, DynamicMemory_Map_d *map, DynamicMemory_List_d *list)
{
	char *str;
	if (root != NULL)
	{
		char *left = solve(root->left, map, list);
		char *right = solve(root->right, map, list);

		int size = strlen(left) + strlen(right) + 2;
		str = (char *)malloc(size);
		str[0] = '\0';

		// concatenate
		strcat(str, left);
		strcat(str, (char *)&root->value);
		strcat(str, right);

		Util_HashMap_put(map, str, strlen(str), Util_HashMap_getValue(map, str, strlen(str)) + 1);

		if (Util_HashMap_getValue(map, str,strlen(str)) == 2)
		{
			Util_List_add(list, root); // add to list
		}

		// delete previous allocation
		free(left);
		free(right);
	}
	else
	{
		str = malloc(2);
		str[0] = '$';
		str[1] = '\0';
	}

	return str;
}

void print_preorder(TreeNode_d *root)
{
	if (root == NULL)
		return;

	printf("%d ", root->value);
	print_preorder(root->left);
	print_preorder(root->right);

	return;
}


// ----------------------------------------------------
// if one tree isomorphic of the other
// ----------------------------------------------------
bool_t BinaryTrees_isIsoMorphic(TreeNode_d *root)
{
	return 0;
}


// ----------------------------------------------------
// if one tree isomorphic of the other
// ----------------------------------------------------
bool_t BinaryTrees_isGraphATree(TreeNode_d *root)
{
	return 0;
}



// ----------------------------------------------------
// if one tree is a sub tree of another
// ----------------------------------------------------
bool_t BinaryTrees_isSubTree(TreeNode_d *root1, TreeNode_d *root2)
{
	// if both null then 2 is a sutree of 1
	if (!root1 && !root2)	// same
		return true;

	// if anyof the one is null then other is not null
	// means not identical so can be subtree
	//	null  || root2
	//  root1 || null
	if (!root1 || !root2)
		return false;

	if(BinaryTrees_isIdentical(root1, root2))
		return true;

	return (BinaryTrees_isSubTree(root1->left, root2) || BinaryTrees_isSubTree(root1->right, root2));
}


// ----------------------------------------------------
// If 2 trees are idetntical
// ----------------------------------------------------
bool_t BinaryTrees_isIdentical(TreeNode_d *root1, TreeNode_d *root2)
{
	bool_t result = false;

	// both are null, then they are same.
	if (!root1 && !root2)
		return true;

	// if anyof the one is null then other is not null
	// means not identical
	//	null  || root2
	//  root1 || null
	if (!root1 || !root2)
		return false;

	// if cur-node is sam and left sutree and right tree matches
	if ((root1->value == root2->value) && BinaryTrees_isIdentical(root1->left, root2->left) && 	BinaryTrees_isIdentical(root1->right, root2->right))
		result = true;

	return result;
}


// ----------------------------------------------------
// 1.0) Traverse and print all the diagonals in the tree (with Q)
// ----------------------------------------------------
void BinaryTrees_TraverseDiagonals(TreeNode_d * root, int size)
{
	if (root == NULL)
		return;

	// create Q
	int height = BinaryTrees_treeHeight(root); // cause Delimetters will be added as well
	Queue_d *Q = Util_createQ(size + height + 1);

	// root and NULL delimeter
	Util_enqueue(Q, root);
	Util_enqueue(Q, NULL);

#ifdef DIAGONAL_SUM
	int diagonal_sum=0;
#endif

	// traverse till q is empty
	while (!Util_isQEmpty(Q))
	{
		TreeNode_d *p = Util_dequeue(Q);
		if(p == NULL)
		{
			Util_enqueue(Q, NULL);	// add new delimeter for the next diagonal level
#ifdef DIAGONAL_SUM
			printf(" : sum=%d", diagonal_sum);
			diagonal_sum = 0;			//reset the diagonal sum
#endif
			printf("\n"); // EOL

			// dequeue again
			p = Util_dequeue(Q);
			if (p == NULL)
				break; // done
		}

		// traverse all the way to right
		// 		  		root (Pdd=0)
		//		  		/  \
		// (dd=Pdd+1) left   right (Pdd=0)
		//
		//
		//
		while(p!=NULL)
		{
			// if has left child enq it.
			if (p->left)
				Util_enqueue(Q, p->left);

#ifdef DIAGONAL_SUM
			// add sum
			diagonal_sum += p->value;
#endif

			// print the node (of same diagonal dist.)
			printf("%c ", p->value);
			p = p->right;
		}
	}

	printf("\n"); // EOL
}

// ----------------------------------------------------
// 1.1) Traverse and print all the diagonals in the tree (without Q)
// ----------------------------------------------------
void BinaryTree_DiagonalTraversal(TreeNode_d *root)
{
	if (root == NULL)
		return;

#ifdef DIAGONAL_SUM
	int diagonal_sum = 0;
#endif

	//create HT
	int height = BinaryTrees_treeHeight(root);
	DynamicMemory_d *HT = Util_createHashTable(0, 0, height-1);

	// find the Dd for each node using pre order
	BT_preorder_diagonaldistance(root, HT, 0);

	// print the diagonals
	for (int i=0; i<height; i++)
	{
		LL_d *cur = HT[i].head;
		while(cur)
		{
#ifdef DIAGONAL_SUM
			diagonal_sum += cur->value;
#endif
			printf("%c ", cur->value);
			cur = cur->next;
		}

#ifdef DIAGONAL_SUM
		printf(": sum=%d", diagonal_sum);
		diagonal_sum = 0; 		// reset the sum
#endif

		printf("\n"); // EOL
	}

	printf("\n"); // EOL
}

void BT_preorder_diagonaldistance(TreeNode_d *root, DynamicMemory_d *HT, int Dd)
{
	if (root == NULL)
		return;

	// add the element to HT
	Util_HashTable_add(HT, Dd, root->value);

	BT_preorder_diagonaldistance(root->left, HT, Dd+1);
	BT_preorder_diagonaldistance(root->right, HT, Dd);
	return;
}

// ----------------------------------------------------
// 2) Lowest common ancestor between 2 nodes A and B
// ----------------------------------------------------
TreeNode_d *BinaryTrees_LowestCommonAncestor(TreeNode_d *root, int A, int B)
//TreeNode_d *BinaryTrees_LowestCommonAncestor(TreeNode_d *root, TreeNode_d *A, TreeNode_d *B)
{
	if (root == NULL)
		return NULL;

	// if found
	if (root->value == A || root->value == B)
	//if (root == A || root == B)
		return root;

	//
	TreeNode_d *left = BinaryTrees_LowestCommonAncestor(root->left, A, B);
	TreeNode_d *right = BinaryTrees_LowestCommonAncestor(root->right, A, B);

	if (left && right)	// both found
		return root;	// return cur node
	else if (left)
		return left;	// return non-null
	else
		return right;	// return non-null
}


// ----------------------------------------------------
// 3) Print a node having 'K' leaves (using post order)
// ----------------------------------------------------
int BinaryTrees_Nodes_with_K_Leaves(TreeNode_d *root, int K)
{
	if (root == NULL)
		return 0;

	// leaf node condition
	if (root->left == NULL && root->right == NULL)
	{
		printf("Leaf: %c\n", root->value);
		return 1;
	}
	// total leaves in the tree = in l_tree+ in r_tree
	int total_count = BinaryTrees_Nodes_with_K_Leaves(root->left, K) + BinaryTrees_Nodes_with_K_Leaves(root->right, K);

	if (total_count == K)
		printf("K leaves node = %c\n", root->value);

	return total_count;
}


// ----------------------------------------------------
// 4) find diameter of a tree
// ----------------------------------------------------
int BinaryTrees_findDiameter(TreeNode_d *root)
{
	if (root == NULL)
		return 0;

	int left_dia = BinaryTrees_findDiameter(root->left);
	int right_dia = BinaryTrees_findDiameter(root->right);

	// find current_dia = find left and right height
	int cur_dia = BinaryTrees_treeHeight(root->left) +  BinaryTrees_treeHeight(root->right) + 1;

	// return greator of them three
	return MAX(MAX(left_dia, right_dia), cur_dia);
}



// ----------------------------------------------------
// BotView and TopView traversal
// ----------------------------------------------------
void BinaryTrees_BotView(TreeNode_d *root, int size)
{
	if (root == NULL)
		return;

	//find min and max
	int min = 0;
	int max = 0;
	BT_min_max_Hdistance(root, 0, &min, &max);

	// creat HT
	DynamicMemory_d *HT = Util_createHashTable(size, min, max);

	// fill the HT
	BT_preorder_traversal(root, HT, 0);

	// print HT first element of each item
	for (int i=min; i< max+1 ;i++)
	{
		printf("%c ", HT[i].tail->value);
	}
}


void BinaryTrees_TopView(TreeNode_d *root, int size)
{
	if (root == NULL)
		return;

	//find min and max
	int min = 0;
	int max = 0;
	BT_min_max_Hdistance(root, 0, &min, &max);

	// creat HT
	DynamicMemory_d *HT = Util_createHashTable(size, min, max);

	// fill the HT
	BT_preorder_traversal(root, HT, 0);

	// print HT first element of each item
	for (int i=min; i< max+1 ;i++)
	{
		printf("%c ", HT[i].head->value);
	}
}


// ----------------------------------------------------
// Vertical order traversal
// ----------------------------------------------------
/*void BinaryTrees_VerticalOrder_Traversal(TreeNode_d *root, int size)
{
	if (root == NULL)
		return;

	// create Q
	//int height = BinaryTrees_treeHeight(root);
	//Queue_d *Q = Util_createQ(size + height + 1);
	//int hd = 0;	// horizontal distance

	//find min and max
	int min = 0;
	int max = 0;
	BT_min_max_Hdistance(root, 0, &min, &max);

	// creat HT
	DynamicMemory_d *HT = Util_createHashTable(size, min, max);

	// root and NULL
	Util_enqueue(Q, root);

	TreeNode_d *p = NULL;
	while(!Util_isQEmpty(Q))
	{
		p = Util_dequeue(Q);

		if (p->left)
		{
			Util_enqueue(Q, p->left);
			// add entry to HT for (p->left, hd-1)
		}
		if (p->right)
		{
			Util_enqueue(Q, p->right);
			// add entry to HT for (p->right, hd+1)
		}
	}

	BT_preorder_traversal(root, HT, 0);

	// print according to the HT order
}*/

void BinaryTrees_VerticalOrder_Traversal(TreeNode_d *root, int size)
{
	if (root == NULL)
		return;

	//find min and max
	int min = 0;
	int max = 0;
	BT_min_max_Hdistance(root, 0, &min, &max);

	// creat HT
	DynamicMemory_d *HT = Util_createHashTable(size, min, max);

	// fill the HT
	BT_preorder_traversal(root, HT, 0);

	// print according to the HT order
	for (int i=min; i< max+1 ;i++)
	{
		LL_d *cur = HT[i].head;
		while (cur)
		{
			printf("%c ", cur->value);
			cur = cur->next;
		}
	}

	// destroy HT
}

void BT_preorder_traversal(TreeNode_d *root, DynamicMemory_d *HT, int hd)
{
	if (root == NULL)
		return;

	// add entry
	Util_HashTable_add(HT, hd, root->value);

	BT_preorder_traversal(root->left, HT, hd-1);
	BT_preorder_traversal(root->right, HT, hd+1);
}


static void BT_min_max_Hdistance(TreeNode_d *root, int hd, int *min, int *max)
{
	if(root == NULL)
		return;

	if (hd < *min)
		*min = hd;
	else if (hd > *max)
		*max = hd;

	BT_min_max_Hdistance(root->left, hd-1, min, max);
	BT_min_max_Hdistance(root->right, hd+1, min, max);
}


// ----------------------------------------------------
// size view of the tree
// ----------------------------------------------------
void BinaryTrees_Sideview_Traversal(TreeNode_d *root, int size)
{
	if (root == NULL)
		return;

	int height = BinaryTrees_treeHeight(root);
	Queue_d *Q = Util_createQ(size + height + 1);
	DynamicMemory_d *out = Util_createDynamicMem();

	// root and NULL
	Util_enqueue(Q, root);
	Util_enqueue(Q, NULL);

	//printf("%c ", root->value);
	Util_DynamicMemory_add(out, root->value);

	while (!Util_isQEmpty(Q))
	{
		TreeNode_d *p = Util_dequeue(Q);
		if(p == NULL)
		{
			Util_enqueue(Q, NULL);
			p = Util_dequeue(Q);
			if (p == NULL)
				break;

			//printf("%c ", p->value);
			Util_DynamicMemory_add(out, p->value);
		}

		if (p->left != NULL)
			Util_enqueue(Q, p->left);
		if (p->right != NULL)
			Util_enqueue(Q, p->right);
	}

	// print the nodes
	LL_d *cur = out->head;
	while (cur)
	{
		printf("%c ", (char)cur->value);
		cur = cur->next;
	}

	Util_destroyDynamicMem(out);
}


void BinaryTrees_rightview(TreeNode_d *root, int size)
{
	if (root == NULL)
		return;

	// create queue and dynamic mem forstoring output
	Queue_d *Q = Util_createQ(BinaryTrees_treeHeight(root) + size + 1);
	DynamicMemory_d *out = Util_createDynamicMem();

	// prev deq'ed item
	TreeNode_d *prev = NULL;
	TreeNode_d *p = NULL;

	//	enq(root)
	//	enq(NULL)
	//	loop till q is empty
	//		p = deq
	//		p is NULL
	//			enq(NULL)
	//			print previous
	//			p = deq
	//			p is still NULL -> break, done
	//
	//		save prev
	//		enq l and r
	Util_enqueue(Q, root);
	Util_enqueue(Q, NULL);

	while (!Util_isQEmpty(Q))
	{
		p = Util_dequeue(Q);
		if (p == NULL)
		{
			Util_enqueue(Q, NULL);

			if (prev != NULL)
			{
				//printf("%c ", prev->value);
				//			or
				Util_DynamicMemory_add(out, prev->value);
			}
			p = Util_dequeue(Q);
			if (p == NULL)
				break;	// done
		}

		prev = p; // save p as previous

		// enq childeren
		if (p->left)
			Util_enqueue(Q, p->left);

		if (p->right)
			Util_enqueue(Q, p->right);
	}

	// print the output
	LL_d *cur = out->head;
	while(cur)
	{
		printf("%c ", cur->value);
		cur = cur->next;
	}
}


// ----------------------------------------------------
// Level order traversal
// ----------------------------------------------------
void BinaryTrees_LevelOrder_Traversal(TreeNode_d *root, int size)
{
	if (root == NULL)
		return;

	int height = BinaryTrees_treeHeight(root); // cause Delimetters will be added as well
	Queue_d *Q = Util_createQ(size + height + 1);

	// enq root and then NULL delimeter
	Util_enqueue(Q, root);
	Util_enqueue(Q, NULL);

	//while qis not empty
	while(!Util_isQEmpty(Q))
	{
		TreeNode_d *p = Util_dequeue(Q);
		if (p == NULL) // delimeter found
		{
			printf("\n");	// EOL
			Util_enqueue(Q, NULL);
			p = Util_dequeue(Q);
			if (p == NULL)
			{
				// done
				break;
			}
		}
		printf("%c ", p->value);

		if (p->left != NULL)
			Util_enqueue(Q, p->left);
 		if (p->right != NULL)
			Util_enqueue(Q, p->right);
	}

	Util_destroyQ(Q);
}


// ----------------------------------------------------
// Spiral/ Zig-Zag order traversal
// ----------------------------------------------------
void BinrayTrees_Spiral(TreeNode_d *root, int size)
{
	Stack_d *stack1 = Util_createStack(size);
	Stack_d *stack2 = Util_createStack(size);

	// push root
	(stack1->push)(stack1, root);

	// till stack1 is not empty
	while ((stack1->sp > -1) || (stack2->sp > -1))
	{
		// loop for s1
		while (stack1->sp >-1)
		{
			TreeNode_d *node = (stack1->pop)(stack1);
			printf("%c ", node->value);

			// left then right
			// so that when you pop you pop right first then left
			(stack2->push)(stack2, node->left);
			(stack2->push)(stack2, node->right);
		}


		// loop for s2
		while (stack2->sp >-1)
		{
			TreeNode_d *node = (stack2->pop)(stack2);
			printf("%c ", node->value);

			// right then left
			// so that when you pop you pop left first then right
			(stack1->push)(stack1, node->right);
			(stack1->push)(stack1, node->left);
		}
	}

	Util_destroyStack(stack1);
	Util_destroyStack(stack2);
}



// ----------------------------------------------------
// construct binary tree
// ----------------------------------------------------
/**
 *
 *				a
 *			  /	  \
 *          b       c
 *        /  \	   /  \
 *		 d    e   f    g
 *	    /  \     / \    \
 *	   h 	i	j   k    l
 *
 */

TreeNode_d *constructCustomBinaryTree(void)
{
	TreeNode_d *root = malloc(sizeof(TreeNode_d));
	root->value = 'a';
	root->left = NULL;
	root->right = NULL;

	insertNodetoleft(root, 'b', true);
	insertNodetoleft(root, 'c', false);
	insertNodetoleft(root->left, 'd', true);
	insertNodetoleft(root->left, 'e', false);
	insertNodetoleft(root->right, 'f', true);
	insertNodetoleft(root->right, 'g', false);

	insertNodetoleft(root->left->left, 'h', true);
	insertNodetoleft(root->left->left, 'i', false);

	insertNodetoleft(root->right->left, 'j', true);
	insertNodetoleft(root->right->left, 'k', false);
	insertNodetoleft(root->right->right, 'l', false);
	return root;
}


TreeNode_d *constructCustomBinaryTree2(void)
{
	TreeNode_d *root = malloc(sizeof(TreeNode_d));
	root->value = 'a';
	root->left = NULL;
	root->right = NULL;

	insertNodetoleft(root, 'b', true);
	insertNodetoleft(root, 'c', false);
	insertNodetoleft(root->left, 'd', true);
	insertNodetoleft(root->left, 'e', false);
	insertNodetoleft(root->right, 'f', true);
	insertNodetoleft(root->right, 'g', false);

	insertNodetoleft(root->left->left, 'h', true);
	insertNodetoleft(root->left->left, 'i', false);

	insertNodetoleft(root->right->left, 'j', true);
	insertNodetoleft(root->right->left, 'k', false);
	return root;
}


TreeNode_d *constructCustomSubTree(void)
{
	TreeNode_d *root = malloc(sizeof(TreeNode_d));
	root->value = 'f';
	root->left = NULL;
	root->right = NULL;

	insertNodetoleft(root, 'j', true);
	insertNodetoleft(root, 'k', false);
	return root;
}



TreeNode_d *constructCustomTreeWithDuplicates(void)
{
	TreeNode_d *root = malloc(sizeof(TreeNode_d));
	root->value = '1';
	root->left = NULL;
	root->right = NULL;

	insertNodetoleft(root, 2, true);
	insertNodetoleft(root, 3, false);

	insertNodetoleft(root->left, 4, true);

	insertNodetoleft(root->right, 2, true);
	insertNodetoleft(root->right, 4, false);

	insertNodetoleft(root->right->left, 4, true);
	return root;
}


void *insertNodetoleft(TreeNode_d *root, char value, bool_t toleft)
{
	TreeNode_d *node = malloc(sizeof(TreeNode_d));
	node->value = value;
	node->left = NULL;
	node->right = NULL;
	if (root->left == NULL && toleft)
		root->left = node;
	else
		root->right = node;

	return node;
}


int BinaryTrees_treeHeight(TreeNode_d *root)
{
	if (root == NULL)
		return 0;

	int leftdepth = BinaryTrees_treeHeight(root->left);
	int rightdepth = BinaryTrees_treeHeight(root->right);
	if (leftdepth > rightdepth)
		return 1 + leftdepth;
	else
		return 1 + rightdepth;
}

