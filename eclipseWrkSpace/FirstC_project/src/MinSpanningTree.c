/*
 * MinSpanningTree.c
 *
 *  Created on: 22-May-2022
 *      Author: sumit
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void MST_PrimsAlgo(void);
void MST_KruskalsAlgo(void);


// ----------------------------------------------------
// Main-driver function
// ----------------------------------------------------

void MST_Program(void)
{
	// Matrix
	printf("Prim's algo: \n");
	MST_PrimsAlgo();

	// List
	printf("\nKruskal's algo: \n");
	MST_KruskalsAlgo();
}

// ----------------------------------------------------
// Prims-algo for MST
// ----------------------------------------------------
#define V 5

typedef struct Edge
{
	int wt;
	int src;
	int dst;
	struct Edge *next;
}Edge_d;


typedef struct ListMST
{
	Edge_d *head;
	Edge_d *tail;
	int size;
}ListMST_d;


static ListMST_d *primMST(int n, int graph[n][n], int *visited);
void addEdgeToList(ListMST_d *list, Edge_d *e);
Edge_d *remEdgeFromList(ListMST_d *list);
Edge_d *remHeadFromList(ListMST_d *list);
Edge_d *createEdge(int src, int neighbor, int n, int graph[n][n]);
void printMST(ListMST_d *MST_edges);

static ListMST_d *kruskalsMST(int n, int graph[n][n], int *visited);
Edge_d *MergeSort(Edge_d *head, Edge_d *tail);

void MST_PrimsAlgo(void)
{
	/* Let us create the following graph
	        2   3
	    (0)--(1)--(2)
	     |   / \   |
	    6| 8/   \5 |7
	     | /     \ |
	    (3)-------(4)
	            9
	 */

	int graph[V][V] = { { 0, 2, 0, 6, 0 },
						{ 2, 0, 3, 8, 5 },
						{ 0, 3, 0, 0, 7 },
						{ 6, 8, 0, 0, 9 },
						{ 0, 5, 7, 9, 0 } };

	int *visited = memset(malloc(sizeof(int)*V), 0, sizeof(int)*V);
	printMST(primMST(V, graph, visited));


	printf("Graph2:\n");
	int graph2[9][9] = { { 0,  4,  0,  0,  0,  0,  0,  8,  0},
						 { 4,  0,  8,  0,  0,  0,  0, 11,  0},
						 { 0,  8,  0,  7,  0,  4,  0,  0,  2},
						 { 0,  0,  7,  0,  9, 14,  0,  0,  0},
						 { 0,  0,  0,  9,  0, 10,  0,  0,  0},
						 { 0,  0,  4, 14, 10,  0,  2,  0,  0},
						 { 0,  0,  0,  0,  0,  2,  0,  1,  6},
						 { 8, 11,  0,  0,  0,  0,  1,  0,  7} };

	int *visited2 = memset(malloc(sizeof(int)*9), 0, sizeof(int)*9);
	printMST(primMST(9, graph2, visited2));
}

static ListMST_d *primMST(int n, int graph[n][n], int *visited)
{
	int count = n-1;

	// create list
	ListMST_d *list = memset(malloc(sizeof(ListMST_d)), 0, sizeof(ListMST_d));
	ListMST_d *MST_edges = memset(malloc(sizeof(ListMST_d)), 0, sizeof(ListMST_d));

	// create frst edge
	Edge_d *edge = memset(malloc(sizeof(Edge_d)), 0, sizeof(Edge_d));
	edge->dst = 0;
	edge->src = -1;
	edge->wt = 0;

	addEdgeToList(list, edge);

	while (list->size && count)
	{
		edge = remEdgeFromList(list);	//r
		if (visited[edge->dst] == 1)	//c: cyclic will be taken care by this check only
			continue;

		visited[edge->dst] = 1; //m

		//work: add to MST_edges
		if(edge->src != -1) // skip first imaginary entry.
		{
			addEdgeToList(MST_edges, edge);
			count--;
		}

		for (int i=0; i<n; i++)
		{
			if (graph[edge->dst][i] !=0)
			{
				addEdgeToList(list, createEdge(edge->dst, i, n, graph));
			}
		}
	}

	// free the list & visited array
	free(list);
	free(visited);

	return MST_edges;
}

// ----------------------------------------------------
// Kruskals-algo for MST
// ----------------------------------------------------
void MST_KruskalsAlgo(void)
{
	/* Let us create the following graph
	        2   3
	    (0)--(1)--(2)
	     |   / \   |
	    6| 8/   \5 |7
	     | /     \ |
	    (3)-------(4)
	            9
	 */

	int graph[V][V] = { { 0, 2, 0, 6, 0 },
						{ 2, 0, 3, 8, 5 },
						{ 0, 3, 0, 0, 7 },
						{ 6, 8, 0, 0, 9 },
						{ 0, 5, 7, 9, 0 } };

	int *visited = memset(malloc(sizeof(int)*V), 0, sizeof(int)*V);
	printMST(kruskalsMST(V, graph, visited));

	printf("Graph2:\n");
	int graph2[9][9] = { { 0,  4,  0,  0,  0,  0,  0,  8,  0},
						 { 4,  0,  8,  0,  0,  0,  0, 11,  0},
						 { 0,  8,  0,  7,  0,  4,  0,  0,  2},
						 { 0,  0,  7,  0,  9, 14,  0,  0,  0},
						 { 0,  0,  0,  9,  0, 10,  0,  0,  0},
						 { 0,  0,  4, 14, 10,  0,  2,  0,  0},
						 { 0,  0,  0,  0,  0,  2,  0,  1,  6},
						 { 8, 11,  0,  0,  0,  0,  1,  0,  7} };

	int *visited2 = memset(malloc(sizeof(int)*9), 0, sizeof(int)*9);
	printMST(kruskalsMST(9, graph2, visited2));

}


static ListMST_d *kruskalsMST(int n, int graph[n][n], int *visited)
{
	int count = n-1; // number of edges in the MST
	ListMST_d *list = memset(malloc(sizeof(ListMST_d)), 0, sizeof(ListMST_d));
	ListMST_d *MST_edges = memset(malloc(sizeof(ListMST_d)), 0, sizeof(ListMST_d));

	// create all the edges and add it to the list
	for (int u=0; u<n; u++)
	{
		for (int v=0; v<n; v++)
		{
			if (graph[u][v] != 0 && visited[v] == 0) // check visited to avoid adding bidrectional edge again.
			{
				addEdgeToList(list, createEdge(u, v, n, graph));
			}
		}
		visited[u] = 1;
	}

	// sort the list
	int size = list->size;
	list->head = MergeSort(list->head, list->tail);
	list->size = size;

	// reset visited flag
	memset(visited, 0, sizeof(int)*n);

	// pick the first non-cyclic edges in the list
	while (list->size && count)
	{
		Edge_d *e = remHeadFromList(list);	// rem
		if(visited[e->src] == 1 && visited[e->dst] == 1)	// check
			continue;

		//IMP: without find-union we cant find cyclic or not
		visited[e->src] = 1;	// mark both vertices
		visited[e->dst] = 1;

		// add to MST
		addEdgeToList(MST_edges, e);
		count--;
	}

	// free the list & visited array
	//free(list);
	free(visited);

	return MST_edges;
}


// ----------------------------------------------------
// Utility functions for MST
// ----------------------------------------------------
Edge_d *createEdge(int src, int neighbor, int n, int graph[n][n])
{
	Edge_d *new = memset(malloc(sizeof(Edge_d)), 0, sizeof(Edge_d));
	new->src = src;
	new->dst = neighbor;
	new->wt = graph[src][neighbor];
	// only difference with djikstra-algo is it doesnt add up the consecutive weights like:
	//		pair->weight = vertex->weight + graph[vertex->val][neighbor];

	return new;
}


void addEdgeToList(ListMST_d *list, Edge_d *e)
{
	if (list->head == NULL)
	{
		list->head = list->tail = e;
	}
	else
	{
		list->tail->next = e;
		list->tail = e;
	}

	list->size++;
}

Edge_d *remEdgeFromList(ListMST_d *list)
{
	Edge_d *result = NULL;
	unsigned int min = INT_MAX;

	Edge_d *cur = list->head;
	Edge_d *prev = NULL;
	Edge_d *last_prev = NULL;
	while(cur)
	{
		if (cur->wt < min) // check total wieght
		{
			min = cur->wt;
			result = cur;
			last_prev = prev;
		}
		prev = cur;
		cur = cur->next;
	}

	// remove the prioity-node and return
	if (result == list->head)
	{
		list->head = result->next;
		if(list->tail == result)
			list->tail = NULL;
	}
	else if (result == list->tail)
	{
		list->tail = last_prev;
		list->tail->next = NULL;
	}
	else
	{
		last_prev->next = result->next;
	}

	list->size--;
	result->next = NULL;	// when removing the next shall point to NULL
	return result;
}


Edge_d *remHeadFromList(ListMST_d *list)
{
	if (list->size == 0)	// list is empty
		return NULL;

	Edge_d *cur = list->head;

	if (list->size == 1)
	{
		list->tail = NULL;
	}

	// remove head
	list->head = cur->next;
	cur->next = NULL;	// when removing the next shall point to NULL
	list->size--;

	return cur;
}

void printMST(ListMST_d *MST_edges)
{
	// print MST
	Edge_d *cur = MST_edges->head;
	Edge_d *next = NULL;
	while (cur)
	{
		next = cur->next;
		printf("%d --> %d : wt=%d\n", cur->src, cur->dst, cur->wt);
		free(cur); // free each edge.
		cur = next;
	}

	// free MST list
	free(MST_edges);
}

// ----------------------------------------------------
// MergeSort
// ----------------------------------------------------
static Edge_d *findmid(Edge_d *head, Edge_d *tail);
static Edge_d *Merge(Edge_d *list1, Edge_d *list2);

Edge_d *MergeSort(Edge_d *head, Edge_d *tail)
{
	if (head == tail)
	{
		Edge_d *newnode = malloc(sizeof(Edge_d));
		memcpy(newnode, head, sizeof(Edge_d));
		newnode->next = NULL;
		return newnode;
	}

	// find mid
	Edge_d *mid = findmid(head, tail);

	Edge_d *left = MergeSort(head, mid);
	Edge_d *right = MergeSort(mid->next, tail);

	return Merge(left, right);
}

static Edge_d *findmid(Edge_d *head, Edge_d *tail)
{
	Edge_d *fast = head;
	Edge_d *slow = head;

	while (fast != tail && fast->next != tail)
	{
		fast = fast->next->next;
		slow = slow->next;
	}

	return slow;
}

static Edge_d *Merge(Edge_d *list1, Edge_d *list2)
{
	Edge_d *dummy = memset(malloc(sizeof(Edge_d)), 0, sizeof(Edge_d));
	Edge_d *prev = dummy;
	Edge_d *cur1 = list1;
	Edge_d *cur2 = list2;

	while (cur1 != NULL && cur2 != NULL)
	{
		if (cur1->wt <= cur2->wt)
		{
			prev->next = cur1;
			cur1 = cur1->next;
		}
		else
		{
			prev->next = cur2;
			cur2 = cur2->next;
		}
		prev = prev->next;
	}

	prev->next = (cur1 != NULL) ? cur1: cur2;

	return dummy->next;
}


