/*
 ============================================================================
 Name        : FirstC_project.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "Sorting_revision.h"
#include "Sorting.h"
#include "Heap.h"
#include "KnapSack.h"
#include "String_PatternMatch.h"
#include "BinaryTrees.h"
#include "BinaryTrees_session_2.h"
#include "DSA_Strings.h"
#include "Partition_Subset.h"
#include "Graph.h"
#include "DSA_Graphs.h"
#include "Graphs_ShortestPath.h"
#include "Grid.h"
#include "DSA_Graphs2.h"
#include "MinSpanningTree.h"
#include "Subsequence.h"
#include "StackAndQueue.h"
#include "DynamicProgramming.h"
#include "SubString.h"
#include "DynamicProgramming_2.h"
#include "Partition_Subset2.h"
#include "ArmTest.h"
#include "BitManipulation.h"
#include "Miscellaneous.h"

int main(void) {
	puts("!!!Hello World!!!"); /* prints !!!Hello World!!! */

	/// lets start some code and let seee our first secure coding standards.
	// I will define all of it and then we can merge it in the branch.
	// to protect all of it.
	// gonna write the java api implementation or the OS to compile and create the HEX.

	// 1)
	// print_Stars();


	// 2)
	// printf("%d", power(5,4));


	// 3) Mergesort example
	//Sorting_MergeSortProgram();

	// 4) Heap example
	//Heap_BinaryTree_Heap();

	// 5)
	//LinkedListProgram();

	// 6)
	//Sorting_Rev_QuickSortProgram();
	//Sorting_Rev_MergeSortProgram();

	// 7)
	//KnapSackProgram();

	// 8)
	//String_PatternMatchProgram();

	// 9)
	//BinaryTrees_Program();
	//BinaryTrees_ProgramSession2();

	// 10) DSA_Strings
	//DSA_Strings_Program();

	// 11)
	//BackTracking_Program();

	// 12)
	//Partions_Subset_Program();

	// 13) graphs basics
	//Graph_Program();

	// 14) DSA graphs
	//DSA_Graphs_DriverProgram();

	// 15) Graphs Shortest Path
	//Graphs_ShortestPath_Program();

	// 16) grids
	//Grid_Program();

	// 17) DSA graphs2
	//DSA_Graphs2_DriverProgram();

	// 18) MST
	//MST_Program();

	// 19) Subsequence
	//Subsequence_Program();

	// 20) Q using 2-stacks
	//StackAndStack_Program();

	// 21) Dynamic-Programming
	//DynamicProgramming_Program();

	// 22) Dynamic-Programming
	//SubString_Program();

	// 23) Dynamic-Programming-2
	//DynamicProgramming2_DriverProgram();

	// 24) partion-subsets
	//PartionsSubset2_Program();

	// Miscellaneous
	//Miscellaneous();
	//Miscellaneous_2();

	// 25) arm-test
	ArmTest_Program();

	// 26) Bit Manipulation:
	//BitManipulation_DriverProgram();

	//27) Miscellaneous
	//Miscellaneous_DriverProgram();

	return EXIT_SUCCESS;
}

