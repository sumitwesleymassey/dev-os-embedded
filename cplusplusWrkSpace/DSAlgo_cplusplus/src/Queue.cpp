

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "Queue.h"

using namespace std;

int front;
int end;

void *creatQ(int size)
{
	front = -1;
	end = -1;
	return malloc(size);
}


void deleteQ(void *Q)
{
	free(Q);
}


void enqueue(void *Q, void *element, int qSize)
{
	if (end == -1 && front == -1) // first item
	{
		front++;
		end++;

		void **ptr = ((void **)Q + end);
		*ptr= element;
	}
	else if ((end+1)%qSize == front)
	{
		cout << "Queue is full\n";
	}
	else
	{
		end = (end+1)%qSize; ///end++
		void **ptr = ((void **)Q + end);
		*ptr= element;
	}

}


void *dequeue(void *Q, int qSize)
{
	void *frontElement = NULL;

	if (front ==-1 && end ==-1)
	{
		cout << "Queue is empty\n";
		frontElement = (void *)0x5a5a5a5a;
	}
	else if (end == front) // last element
	{
		//frontElement = (void *)(*((int *)Q + front));
		frontElement = *((void **)Q + front);

		// reset q
		end = -1;
		front = -1;
	}
	else
	{
		//frontElement = (void *)(*((int *)Q + front));
		frontElement = *((void **)Q + front);
		front = (front+1)%qSize; // front++
	}

	return frontElement;
}


bool isQueueEmpty(void *Q)
{
	if (front ==-1 && end ==-1)
		return true;
	else
		return false;
}
