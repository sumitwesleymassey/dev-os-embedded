/*
 * Arrays.c
 *
 *  Created on: 04-May-2022
 *      Author: sumit
 */
#include <stdio.h>
#include <stdlib.h>

// ----------------------------------------------------
// dynamic array with malloc and int *ptr to the array
// ----------------------------------------------------
void dynamic_array_single_ptr(void)
{
	// Dimensions of the 2D array
	int m = 3, n = 4, c = 0;

	// Declare a memory block of
	// size m*n
	int *arr = malloc(sizeof(int) *m*n);

	// Traverse the 2D array
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {

			// Assign values to
			// the memory block
			*(arr + i * n + j) = ++c;
		}
	}

	// Traverse the 2D array
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {

			// Print values of the
			// memory block
			printf("%2d ",  *(arr + i * n + j));
		}
		printf("\n");
	}

	//Delete the array created
	free(arr);

	return;
}


// ----------------------------------------------------
// dynamic array with malloc and int **ptr
// ----------------------------------------------------
void dynamic_array_double_ptr(void)
{
	// Dimensions of the array
	int m = 3, n = 4, c = 0;

	// Declare memory block of size M
	int** a = malloc(sizeof(int*) *m);

	for (int i = 0; i < m; i++) {

		// Declare a memory block
		// of size n
		a[i] = malloc(sizeof(int) *n);
	}

	// Traverse the 2D array
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {

			// Assign values to the
			// memory blocks created
			a[i][j] = ++c;
		}
	}

	// Traverse the 2D array
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {

			// Print the values of
			// memory blocks created
			printf("%2d ",  a[i][j]);
		}
		printf("\n");
	}

	//Delete the array created
	for(int i=0;i<m;i++)    //To delete the inner arrays
		free(a[i]);

	free(a);              //To delete the outer array
	//which contained the pointers
	//of all the inner arrays

	return;
}
