/*
 * Module for digital outputs of the system source.
 */

#include <stdlib.h>
#include "mdl_do.h"


/*-----------------------------------------------------------
* PUBLIC LIST API documented in mdl_do.h
*----------------------------------------------------------*/
void DO_setOutputState(DO_index_E index, DO_state_E state)
{
    // does nothing
}
